Welcome to ZeldaDev's documentation!
====================================

Here you can find an intro to our ways of working, our coding practices and repo documentation.

For documentation specific to:

    `Student Web App Docs <https://zeldav1.readthedocs.io/projects/Web-App/en/latest/>`_.
        These are the docs specific to the internals of the Angular Student Web App.

    `Client Side Docs <https://zeldav1.readthedocs.io/projects/Client-Webportal/en/latest/>`_.
        These are the docs specific to the Angular Client Side Docs.

    Machine Learning Docs
        These are the docs specific to the ML Component of the Zelda infrastructure.

    `Field Vision Docs <https://fieldvision.readthedocs.io/en/latest/>_`.
        These are the docs specific to the MLKit implementation.

    `Internal Upload Content Page Docs <https://zeldav1.readthedocs.io/projects/UploadContent/en/latest/>`_.



Contents
========

.. toctree::
   :maxdepth: 1
   :titlesonly:

   Introduction <introduction>
   Getting Started! <gettingstarted>
   Ramping up <rampingup>
   Our Dev Process <devprocess>
   Setting up Documentation <documentation>
   Setting up Pipelines <pipelines>
   Deploying an app <hosting>
   Android Docs <android>
   Student Web App Docs <https://zeldav1.readthedocs.io/projects/Web-App/en/latest/>
   Client Side Docs <https://zeldav1.readthedocs.io/projects/Client-Webportal/en/latest/>
   Machine Learning Docs <machinelearning>
   Field Vision MLKit <https://fieldvision.readthedocs.io/en/latest/>
   Internal Upload Content Page Docs <https://zeldav1.readthedocs.io/projects/UploadContent/en/latest/>

