###############
Our Dev Process
###############

*******
Purpose
*******

The purpose of our Dev Process is to ensure that we follow these essential team dynamics:

Communication
-------------

As the team grows and our components become more integrated, it is essential that we follow a structured process that
allows us to keep up to date with team progress. Communication should be seamless and informed, with the goal of easing
decision making that applies to multiple parties and facilitating easy collaboration where necessary.

Accountability
--------------

As a lot of the team works from home, it is essential to ensure that the rate of development is consistent.
There should be adequate and reasonable expectation around work outputs on a weekly basis.
This will allow us to build realistic timelines for projects.

Time Management
---------------

It is easy to get distracted or to lose momentum, especially if you are in charge of your own work output.
For this reason it is essential that we establish and maintain a metric for work output expectations based on
realistic use of time.

Code Quality and Best Practices
-------------------------------

It is essential that we have a set of guidelines that we have agreed upon to ensure that code quality is maintained.
This includes readable code, following language conventions and descriptive commit messages.
Best practises is the manner in which we conduct the process of issue tracking, repository usage, Kanban usage and
also includes testing and documentation.

****************
The Code Process
****************

We want to maintain consistency and naming conventions across branches, commit messages and pull requests.
This allows us to easily find and understand where changes were made.

The steps are as follows:
Create an issue on `KanbanFlow <https://kanbanflow.com/board/FSG2ep>`_ outlining the changes you want to make in a branch.
The issue title should follow this convention:

    ``<ISSUE NUMBER>-<BRIEF DESCRIPTION, TEXT IN CAMEL CASE>``

Then checkout a new branch from the correct parent feature branch with the same title as the issue on KanbanFlow
      ``$ git checkout -b <SAME ISSUE NUMBER>-<SAME BRIEF DESCRIPTION>``

Make one component/feature change, make a commit that adequately describes this change -
please please commit between changes - even if they’re small!

    ``$ git add .``

    ``$ git commit -a -m "Something something something"``

Push it back up to Bitbucket as often as you can.
This will make sure the code is safe and also run the CI Pipeline tests to make sure it passes.

    ``$ git push``

If this is the first time you’re pushing you may need:

    ``git push --set-upstream origin <BRANCH-NAME>``

Open a pull request (PR) through the `Bitbucket UI <https://bitbucket.org/dashboard/overview>`_.
Make sure you specify and explain all the changes you have made,
if you’ve made descriptive commits you won’t have to add anything more as the commit messages are added by default.

Check the code content before you publish to make sure all the right code is included.
Add a reviewer/s you would like to look at your code.

Once the request is accepted you can go ahead and merge, you can click this on the UI or through slack.

To make sure you’re up to date locally, go back to your command line and pull from the feature branch you just merged to.
Feature branches are described in more detail in the "Organising branches" section.

    ``$ git checkout <FEATURE-BRANCH>``
    ``$ git pull``

Also make sure you keep master up to date if applicable

    ``$ git checkout master``
    ``$ git pull``

If you have other unfinished branches on the same feature branch, make sure you catch them up with the current changes.

    ``$ git checkout master git pull git checkout <UNFINISHED-BRANCH-NAME>``
    ``git merge <FEATURE-BRANCH>``

And resolve any conflicts.

*******************
Organising Branches
*******************

Maintaining branch processes and order can be difficult, especially if you’re working on your own.
However, having feature branches is really important, especially if you want to keep different features that are in
development away from each other and off master - which should just be for stuff that’s being added to the current
version in deployment.

Here’s a diagram illustrating how they should work:

.. image:: branches.png

***************************
Assigning Issues As Tickets
***************************

1. Add Task Tickets to To-do column
-----------------------------------

This column is the backlog, these tickets are necessary but not necessarily immediate. Once they are allocated for work
they are moved into the "This Week" column.

.. image:: ticket.png

The following key information that must be added to the ticket (as illustrated above) include:

*   **Title**: Must correspond with the branch name and number (if applicable)
*   **Assign component**: ie Android
*   **Assign member**: this is the person/s responsible for completing the task
*   **Assign label**: this corresponds to the parent branch (ie master, feature-GRRL)
*   **Add description**: the core changes or details of the issue to be resolved
*   **Add points estimate**: here I have given it 2 points which corresponds to 4 hours

2. Add to Review column once the task is completed. Add the PR url in the description
-------------------------------------------------------------------------------------

The Pull Request should have a reviewer, a description of what you changed and a title that matches the branch name and ticket name

Don’t forget to check the contents of the pull request to make sure it contains ONLY the changes relevant to this
pull request and that you haven’t made any mistakes.

.. image:: pr.png

Move to the Done column once the PR is accepted and merged.\

*************************************
Issue Tracking and Repository Process
*************************************

Here’s a diagram illustrating how they should work:

.. image:: process1.png

.. image:: process2.png

