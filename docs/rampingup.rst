##########
Ramping Up
##########

    Here are some tools, tips, links and courses to get you going as quickly as possible.

New to Unix?
============

    https://www.csoft.net/docs/course.html

New to Git?
===========

    https://www.udacity.com/course/version-control-with-git--ud123

Angular2
========

    How to learn Angular 2: http://www.angular2.com/
    Design doc: https://docs.google.com/document/d/1JIlz8LTUBqNm_GCTihHLsmBmnC2ZBZCLFHhiJ2ZwhzU/edit
    Firebase JS SDK: https://github.com/firebase/firebase-js-sdk
    Firebase Webapp Init: https://firebase.google.com/docs/web/setup
    Spring MVC and Angular stack: https://dzone.com/articles/web-app-architecture-spring-0
    Help: https://blog.angular-university.io/top-10-angular-2-tutorials-blogs-and-podcasts/

Android
=======

    Best Principles: https://docs.google.com/document/d/1sH9hy8qQOHgKDh-mno_WWZBwYz0wDFe4ss6gXU149i0/edit?usp=sharing
    Naming conventions: https://medium.com/@mikelimantara/overview-of-android-project-structure-and-naming-conventions-b08f6d0b7291

Machine Learning
================

    TensorFlow: https://www.tensorflow.org/
    Keras: https://keras.io/
    Using Keras: https://elitedatascience.com/keras-tutorial-deep-learning-in-python
    Using Keras: https://machinelearningmastery.com/introduction-python-deep-learning-library-keras/
    Firebase Python SDK: https://firebase.google.com/docs/reference/admin/python/
    Firebase Python pip: https://pypi.python.org/pypi/python-firebase/1.2
    Firebase Python API library: https://developers.google.com/api-client-library/python/apis/firebaserules/v1
    Characteristics map: https://drive.google.com/file/d/1j6PsCi4gCRq-pFd6hm_n5hpzBJADle8r/view?usp=sharing
    Intro to Machine Learning Stanford course: https://www.coursera.org/learn/machine-learning?authMode=login
    Practical Machine Learning Coursera: https://www.coursera.org/learn/practical-machine-learning


