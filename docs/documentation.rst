########################
Setting up Documentation
########################

========================

==================
Linking your Repo:
==================

Cd into project directory, then:

    ``mkdir docs``
    ``sudo pip install sphinx``

*(call project name ZeldaDev)*
*Add your name as author and just press enter for the other commands.*

To create docs:

    ``sphinx-quickstart``

To render docs:

    ``make html``

To add ReadTheDocs theme:

    ``pip install sphinx_rtd_theme``

    Edit your conf.py to add the sphinx_rtd_theme:

    ``html_theme = "sphinx_rtd_theme"``


Find your docs in the /docs folder, in the .rst files.
Preview the docs in _build/html/index.html

Cheatsheet of documentation directives:
http://dont-be-afraid-to-commit.readthedocs.io/en/latest/cheatsheet.html
