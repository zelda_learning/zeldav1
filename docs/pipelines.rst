####################
Setting up Pipelines
####################

Bitbucket Pipelines is an integrated CI/CD service, built into Bitbucket. It allows you to automatically build, test and even deploy your code, based on a configuration file in your repository.

To set up Pipelines you need to create and configure the `bitbucket-pipelines.yml` file in the root directory of your repository. This file is your build configuration, and using configuration-as-code means it is versioned and always in sync with the rest of your code.

Writing your YAML:
==================

	1. By selecting Pipelines in your repo toolbar and configure for your repo language, BitBucket will autogenerate and push a `bitbucket-pipelines.yml`

	2. The bitbucket-pipelines.yml file holds all the build configurations for your repository. YAML is a file format that is easy to read, but writing it requires care. Indenting must use spaces, as tab characters are not allowed.

	3. There is a lot you can configure in the bitbucket-pipelines.yml file, but at its most basic the required keywords are:

	**pipelines**: contains all your pipeline definitions.

	**default**: contains the steps that run on every push.

	**step**: each step starts a new Docker container with a clone of your repository, then runs the contents of your script section.

	**script**: a list of commands that are executed in sequence.
