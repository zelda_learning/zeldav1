#################
Welcome to Zelda!
#################

We have four main areas of development:
    1. The Android app
    2. The student facing webapp
    3. The client facing webportal
    4. The machine learning backend

All the links you’ll ever need:
===============================

Check out the following and make sure you have the permissions for the project components you work on.

Admin
-----
    `Zelda Internal Home Page <https://sites.google.com/zelda.org.za/zeldainternalhomepage/getting-started>`_

    `App Dev Docs Drive <https://drive.google.com/drive/u/0/folders/0BxecdN8bkRHAU0czQ0YxQjlaWWs>`_

    `Slack <https://zeldahq.slack.com/>`_

Repositories
------------

    `Android app repository <https://bitbucket.org/zelda_learning/zeldav1>`_

    `Student Webapp repository <https://bitbucket.org/CarlaLlama/web_platform>`_

    `Client Webportal repository <https://bitbucket.org/zelda_learning/web_portal>`_

Consoles
--------

    `Firebase Dev console <https://console.firebase.google.com/u/0/project/zeldav1-82682/database/zeldav1-82682/data>`_

    `Firebase Prod console <https://console.firebase.google.com/u/0/project/zeldav1-82682/database/zeldav1-82682/data>`_
