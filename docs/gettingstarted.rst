###############
Getting Started
###############

If you want to get started as quickly as possible, here is a rundown of how to set up our dependencies and get going.

====================
Cloning a repository
====================

1. Setup git

    ``sudo apt-get install git``

2. Get added to the project as an Admin/Developer and find the repo you want to clone in the Zelda Project directory here: https://bitbucket.org/account/user/zelda_learning/projects/ZEL

3. Example cloning zeldav1 repo

    ``git clone https://USERNAME@bitbucket.org/zelda_learning/zeldav1.git``
    ``cd <REPO>``
    ``git checkout master``

4. A problem you may encounter is when pulling a new repository, you only get and can update the current branch. In order to get all branches use the following::

    ``git branch -r | grep -v '\->' | while read remote; do git branch --track "${remote#origin/}" "$remote"; done``
    ``git fetch --all``
    ``git pull --all``

=====================
Adding code to a repo
=====================

Now that the repository is ready, you can now start adding code to it.

The steps are as follows:

1. Create an issue on BitBucket outlining the changes you want to make in a branch.

    ``git flow feature start <ISSUE NUMBER>-<BRIEF DESCRIPTION, SEPARATED BY DASH "-" >``
    ``git branch``
    ``git flow feature publish <ISSUE NUMBER>-<BRIEF DESCRIPTION, SEPARATED BY DASH "-" >``

If you have problems with gitflow you can create an issue through the BitBucket UI.

2. Start a new branch with a name *<ISSUE NUMBER>-<SAME BRIEF DESCRIPTION AS ABOVE >*

   ``git checkout -b <ISSUE NUMBER>-<SAME BRIEF DESCRIPTION AS ABOVE >``

3. Write code MAKE SURE YOU'RE DOING THIS ON THE RIGHT BRANCH!

This is where the actual magic happens.

4. Commit it

    ``git add .``
    ``git commit -a -m "hey look, real work!"``

*Please commit between changes, even if small!*

5. Push it back up to bitbucket

    ``git push``

If this is the first time you're pushing you may need::

    ``git push --set-upstream origin <BRANCH-NAME>``


6. Open a pull request (PR)

    You can do this using the bitbucket UI.
    Make sure you specify and explain all the changes you have made. Add a reviewer you would like to look at your code.
    Doing this will automatically close the issue when the PR is merged.

7. Get it tested (we're still setting this up!), reviewed and +1'ed

8. Merge!

    Once you've got a +1 you can click Merge on the UI. Back in your command line:

    ``git checkout master``
    ``git pull``

If you have other unfinished branches make sure you catch them up with the current changes.

    ``git checkout master``
    ``git pull``
    ``git checkout <UNFINISHED-BRANCH-NAME>``
    ``git merge master``

And resolve any conflicts.

