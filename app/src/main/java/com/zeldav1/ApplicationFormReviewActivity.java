package com.zeldav1;

import android.content.Context;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.utils.CustomUtils;
import com.utils.adapters.ApplicationFormReviewAdapter;
import com.utils.services.FirebaseStorageService;
import com.utils.services.FirebaseFirestoreService;
import com.zelda.object.ApplicationForm;
import com.zelda.object.ApplicationFormSectionA;
import com.zelda.object.Bursary;
import com.zelda.object.UserApplicationFormA;
import com.zelda.object.UserState;

import java.util.Objects;

import static com.utils.Constants.APPLICATION_FORM;
import static com.utils.Constants.APPLICATION_FORM_SECTION_A_TAG;
import static com.utils.Constants.APPLICATION_FORM_SECTION_A;
import static com.utils.Constants.APPLICATION_FORM_SUBMITTED_TAG;
import static com.utils.Constants.APPLICATION_STATUS_IN_REVIEW;
import static com.utils.Constants.APPLICATION_STATUS_PENDING;
import static com.utils.Constants.APPLICATION_STATUS_RETURNED;
import static com.utils.Constants.APPLICATION_STATUS_SUBMITTED;
import static com.utils.Constants.BURSARY_TAG;
import static com.utils.Constants.UNREAD_MESSAGE_TAG;
import static com.utils.Constants.USER_APPLICATION_FORM_A;
import static com.utils.Constants.USER_APPLICATION_FORM_A_TAG;
import static com.utils.Constants.USER_STATE_TAG;

public class ApplicationFormReviewActivity extends BaseActivity {
    private static final String TAG = ApplicationFormReviewActivity.class.getSimpleName();
    private final Context mContext = this;

    private UserState mUserState;
    private Boolean mUnreadBoolean;
    private int mApplicationFormSections;
    private FirebaseFirestoreService mFirebaseUtil;

    private ApplicationForm mApplicationForm;
    private ApplicationFormSectionA mApplicationFormSectionA;
    private Bursary mBursary;

    private UserApplicationFormA mUserApplicationFormA;
    private ConstraintLayout mReviewContainer, mLoadingContainer;

    private String applicationId = null;
    private String applicationFormId = null;
    private TextView mSubmitInfoHeading;
    private ProgressBar mLoadingProgressBar;

    private RecyclerView mApplicationReviewRecyclerView;
    private ApplicationFormReviewAdapter mApplicationReviewViewAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_application_form_review);
        hidePDialog();

        mFirebaseUtil = new FirebaseFirestoreService();
        setUpLinks();

        mUserState = (UserState) getIntent().getSerializableExtra(USER_STATE_TAG);
        mUnreadBoolean = getIntent().getBooleanExtra(UNREAD_MESSAGE_TAG, false);
        mBursary = (Bursary)getIntent().getSerializableExtra(BURSARY_TAG);

        if (null != mBursary){
            applicationId = mBursary.getId();
            applicationFormId = mUserState.getCompletedApplicationIds().get(applicationId);
            updateBursaryInfo();
        }

        mApplicationForm = (ApplicationForm) getIntent().getSerializableExtra(APPLICATION_FORM_SUBMITTED_TAG);
        if (mApplicationForm == null) {
            findApplicationForm();
        }
        else
            updateApplicationFormInfo();

        mApplicationFormSectionA = (ApplicationFormSectionA) getIntent().getSerializableExtra(APPLICATION_FORM_SECTION_A_TAG);
        if (mApplicationFormSectionA == null) {
            findApplicationFormSectionA();
        }
        else {
            Log.d(TAG, "UAPP: " + mApplicationFormSectionA.toString());
            updateApplicationAdapter();
            updateLoadingScreen();
        }

        mUserApplicationFormA = (UserApplicationFormA) getIntent().getSerializableExtra(USER_APPLICATION_FORM_A_TAG);
        if (mUserApplicationFormA == null) {
            findUserApplicationFormA();
        }
        else {
            Log.d(TAG, "UAPP: " + mUserApplicationFormA.toString());
            updateApplicationAdapter();
            updateLoadingScreen();
        }

        Log.d(TAG, "UserState: " + mUserState.getId());
        Log.d(TAG, "mUnreadBoolean: " + mUnreadBoolean);

    }

    private void setUpLinks() {
        Toolbar mToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mSubmitInfoHeading = findViewById(R.id.application_form_review_information);
        mLoadingProgressBar = findViewById(R.id.application_form_review_loading_progress_bar);

        mReviewContainer = findViewById(R.id.application_form_review_content_container);
        mLoadingContainer = findViewById(R.id.application_form_review_loading_container);

        mApplicationReviewRecyclerView = findViewById(R.id.application_form_review_recycler_view);

        if (null != mApplicationFormSectionA){
            mApplicationReviewViewAdapter = new ApplicationFormReviewAdapter(mApplicationFormSectionA, mUserApplicationFormA, true);
        }
        else {
            mApplicationReviewViewAdapter = new ApplicationFormReviewAdapter(true);
        }

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        mApplicationReviewRecyclerView.setLayoutManager(mLayoutManager);

        mApplicationReviewRecyclerView.setAdapter(mApplicationReviewViewAdapter);
        mApplicationReviewRecyclerView.setHasFixedSize(true);
        mApplicationReviewViewAdapter.notifyDataSetChanged();
    }

    private void findApplicationForm() {
        if (mBursary != null) {
            applicationId = mBursary.getId();
            applicationFormId = mUserState.getCompletedApplicationIds().get(applicationId);
            Log.d(TAG, "find application form for applicationID: " + applicationId +
                    "\nApplicationFormId:" + mUserState.getCompletedApplicationIds().get(applicationFormId));

            mFirebaseUtil.fetchDocument(value -> {
                if (null != value) {
                    mApplicationForm = value.toObject(ApplicationForm.class);
                    assert mApplicationForm != null;
                    mApplicationForm.setApplicationFormId(applicationFormId);
                    updateApplicationFormInfo();
                } else {
                    Log.d("findApplicationForm", "No Application Form found. Create new object: ");
                }
            }, APPLICATION_FORM, applicationFormId);
        }
    }

    private void findApplicationFormSectionA() {
        if (applicationId != null) {
            mFirebaseUtil.fetchDocument(value -> {
                if (null != value) {
                    mApplicationFormSectionA = value.toObject(ApplicationFormSectionA.class);
                    updateApplicationAdapter();
                    Log.d(TAG, "AppFormA found" + mApplicationFormSectionA.toString());
                } else {
                    Log.d(TAG, "No Application Form A found. Issue");
                }
            }, APPLICATION_FORM_SECTION_A, applicationId);
        }
    }

    private void findUserApplicationFormA() {
        if (applicationFormId != null) {
            mFirebaseUtil.fetchDocument(value -> {
                if (null != value) {
                    mUserApplicationFormA = value.toObject(UserApplicationFormA.class);
                    Log.d(TAG, "UserFormA found"+ mUserApplicationFormA.toString());
                    updateApplicationAdapter();
                    updateLoadingScreen();
                } else {
                    Log.d(TAG, "No UserApplicationFormA found. Issue");
                }
            }, USER_APPLICATION_FORM_A, applicationFormId);
        }
    }

    private void updateBursaryInfo(){
        if (null != mBursary){

            ((TextView)findViewById(R.id.application_form_review_bursary_title)).setText(mBursary.getTitle());
            FirebaseStorageService.setGlideImage(findViewById(R.id.application_form_review_image), CustomUtils.setImageNameOnType(Bursary.class, mBursary.getId()));

        }
    }

    private void updateApplicationFormInfo(){
        if (null != mApplicationForm){
            ((TextView)findViewById(R.id.application_form_review_application_status_text)).setText(mApplicationForm.getStatus());
            SpannableString text = new SpannableString("");

            switch (mApplicationForm.getStatus()){
                case APPLICATION_STATUS_SUBMITTED:
                    findViewById(R.id.application_form_review_application_progress_bar_submitted_progress).setVisibility(View.VISIBLE);
                    findViewById(R.id.application_form_review_application_progress_bar_in_review_progress).setVisibility(View.GONE);
                    findViewById(R.id.application_form_review_application_progress_bar_feedback_progress).setVisibility(View.GONE);
                    ((TextView)findViewById(R.id.application_form_review_information_text)).setText(getResources().getString(R.string.application_form_review_text_submitted));
                    break;
                case APPLICATION_STATUS_PENDING:
                    findViewById(R.id.application_form_review_application_progress_bar_submitted_progress).setVisibility(View.GONE);
                    findViewById(R.id.application_form_review_application_progress_bar_in_review_progress).setVisibility(View.VISIBLE);
                    findViewById(R.id.application_form_review_application_progress_bar_feedback_progress).setVisibility(View.GONE);
                    ((TextView)findViewById(R.id.application_form_review_information_text)).setText(getResources().getString(R.string.application_form_review_text_in_review));
                    break;
                case APPLICATION_STATUS_IN_REVIEW:
                    findViewById(R.id.application_form_review_application_progress_bar_submitted_progress).setVisibility(View.GONE);
                    findViewById(R.id.application_form_review_application_progress_bar_in_review_progress).setVisibility(View.VISIBLE);
                    findViewById(R.id.application_form_review_application_progress_bar_feedback_progress).setVisibility(View.GONE);
                    ((TextView)findViewById(R.id.application_form_review_information_text)).setText(getResources().getString(R.string.application_form_review_text_in_review));
                    break;
                case APPLICATION_STATUS_RETURNED:
                    findViewById(R.id.application_form_review_application_progress_bar_submitted_progress).setVisibility(View.GONE);
                    findViewById(R.id.application_form_review_application_progress_bar_in_review_progress).setVisibility(View.GONE);
                    findViewById(R.id.application_form_review_application_progress_bar_feedback_progress).setVisibility(View.VISIBLE);
                    ((TextView)findViewById(R.id.application_form_review_information_text)).setText(getResources().getString(R.string.application_form_review_text_returned_accepted));
                    break;
            }

            text = new SpannableString(getResources().getString(R.string.application_form_review_heading));
            text.setSpan(new UnderlineSpan(), 0, text.length(), 0);
            ((TextView)findViewById(R.id.application_form_review_information)).setText(text);
        }
    }

    private void updateApplicationAdapter(){

        if (null != mApplicationFormSectionA)
            mApplicationReviewViewAdapter.updateApplicationForm(mApplicationFormSectionA);
        if (null != mUserApplicationFormA)
            mApplicationReviewViewAdapter.updateUserApplicationFormA(mUserApplicationFormA);

        mApplicationReviewRecyclerView.setAdapter(mApplicationReviewViewAdapter);
        mApplicationReviewRecyclerView.setHasFixedSize(true);
        mApplicationReviewViewAdapter.notifyDataSetChanged();
    }

    private void updateLoadingScreen(){
        mLoadingContainer.setVisibility(View.GONE);
        mReviewContainer.setVisibility(View.VISIBLE);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // navigate up on back button click
        if (!CustomUtils.menuOnClick(item, mUnreadBoolean, mUserState, this))
            onBackPressed();
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_top_toolbar, menu);
        MenuItem messageIcon = menu.findItem(R.id.action_message);
        messageIcon.setIcon(mUnreadBoolean?R.drawable.chat_notification:R.drawable.chat_white);
        return true;
    }

}
