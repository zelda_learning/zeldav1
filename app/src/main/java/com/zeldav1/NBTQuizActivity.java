package com.zeldav1;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;

import com.utils.CustomUtils;
import com.utils.fragments.NBTQuizQuestionPageFragment;
import com.utils.fragments.NBTQuizStartFragment;
import com.zelda.object.NBTQuiz;
import com.zelda.object.UserState;

import java.util.Objects;

import static com.utils.Constants.NBT_QUIZ_RETRY_TAG;
import static com.utils.Constants.QUESTION_NUMBER_TAG;
import static com.utils.Constants.QUIZ_ID_TAG;
import static com.utils.Constants.UNREAD_MESSAGE_TAG;
import static com.utils.Constants.USER_STATE_TAG;

public class NBTQuizActivity extends AppCompatActivity {

    private static final String TAG = NBTQuizActivity.class.getSimpleName();
    private final Context mContext = this;


    private NBTQuiz mNBTQuiz;
    private UserState mUserState;
    private Boolean mUnreadBoolean;

    FrameLayout container;
    FragmentManager myFragmentManager;
    NBTQuizStartFragment myFragment1;
    NBTQuizQuestionPageFragment myFragment2;

    final static String TAG_1 = "FRAGMENT_1";
    final static String TAG_2 = "FRAGMENT_2";
    final static String TAG_3 = "FRAGMENT_3";
    final static String KEY_MSG_1 = "FRAGMENT1_MSG";
    final static String KEY_MSG_2 = "FRAGMENT2_MSG";
    final static String KEY_MSG_3 = "FRAGMENT3_MSG";

    private Button mNextQuestionButton;
    private static Bundle mBundle = new Bundle();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nbt_quiz);
        container = findViewById(R.id.nbt_quiz_frame_layout_container);


        mUserState = (UserState)getIntent().getSerializableExtra(USER_STATE_TAG);
        mNBTQuiz = (NBTQuiz)getIntent().getSerializableExtra(QUIZ_ID_TAG);
        mUnreadBoolean = getIntent().getBooleanExtra(UNREAD_MESSAGE_TAG, false);

        Log.d(TAG, mNBTQuiz.toString());

        setUpLinks();

        mNextQuestionButton = findViewById(R.id.quiz_next_button);
        mNextQuestionButton.setOnClickListener(arg0 -> {
            if (mNextQuestionButton.getVisibility() == View.GONE){

                Log.d(TAG, "load question");
                mNextQuestionButton.setVisibility(View.GONE);
                mNextQuestionButton.setText(getString(R.string.quiz_start));
                NBTQuizStartFragment fragment = (NBTQuizStartFragment)myFragmentManager.findFragmentByTag(TAG_1);

                if (fragment == null) {

                    mBundle.putString(KEY_MSG_1, "Replace MyFragment1");
                    myFragment1.setArguments(mBundle);

                    FragmentTransaction fragmentTransaction = myFragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.nbt_quiz_frame_layout_container, myFragment1, TAG_1);
                    fragmentTransaction.commit();

                }else{

                    fragment.setMsg("MyFragment1 already loaded");
                }
            }
            else if (mNextQuestionButton.getVisibility() == View.VISIBLE){

                Log.d(TAG, "load question");
                mNextQuestionButton.setVisibility(View.GONE);
                NBTQuizQuestionPageFragment fragment = (NBTQuizQuestionPageFragment)myFragmentManager.findFragmentByTag(TAG_2);

                if (fragment == null) {

                    mBundle.putString(KEY_MSG_2, "Replace MyFragment2");
                    mBundle.putInt(QUESTION_NUMBER_TAG, 1);
                    mBundle.putBoolean(NBT_QUIZ_RETRY_TAG, false);

                    myFragment2.setArguments(mBundle);

                    FragmentTransaction fragmentTransaction = myFragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.nbt_quiz_frame_layout_container, myFragment2, TAG_2);
                    fragmentTransaction.commit();

                }else{
                    fragment.setMsg("MyFragment2 already loaded");
                }
            }


        });

        myFragmentManager = getSupportFragmentManager();
        myFragment1 = new NBTQuizStartFragment();
        myFragment1.setArguments(mBundle);
        myFragment2 = new NBTQuizQuestionPageFragment();
        myFragment2.setArguments(mBundle);

        if(savedInstanceState == null){
            //if's the first time created

            FragmentTransaction fragmentTransaction = myFragmentManager.beginTransaction();
            fragmentTransaction.add(R.id.nbt_quiz_frame_layout_container, myFragment1, TAG_1);
            fragmentTransaction.commit();
        }
    }

    private void setUpLinks() {
        Log.d(TAG, "setUpLinks");
        mBundle.putSerializable(QUIZ_ID_TAG, mNBTQuiz);
        mBundle.putSerializable(USER_STATE_TAG, mUserState);
        mBundle.putSerializable(UNREAD_MESSAGE_TAG, mUnreadBoolean);

        Toolbar mToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_top_toolbar, menu);
        MenuItem messageIcon = menu.findItem(R.id.action_message);
        messageIcon.setIcon(mUnreadBoolean?R.drawable.chat_notification:R.drawable.chat_white);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // navigate up on back button click
        if (!CustomUtils.menuOnClick(item, mUnreadBoolean, mUserState, this)){
            onBackPressed();
        }

        return true;
    }

    private void setUpQuestionPageLinks() {
        Log.d(TAG, "setUpQuestionPageLinks");

        Toolbar mToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mNextQuestionButton = findViewById(R.id.quiz_next_button);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public static Bundle getBundle() {
        return mBundle;
    }
}
