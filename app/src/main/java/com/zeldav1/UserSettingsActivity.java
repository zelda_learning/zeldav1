package com.zeldav1;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.utils.CustomUtils;
import com.utils.dialogFragments.BackSaveDialogFragment;
import com.utils.services.FirebaseFirestoreService;
import com.utils.viewholders.InputEmailViewHolder;
import com.zelda.object.UserState;

import static com.utils.Constants.UNREAD_MESSAGE_TAG;
import static com.utils.Constants.USER_STATE_FIRESTORE;
import static com.utils.Constants.USER_STATE_TAG;

public class UserSettingsActivity extends BaseActivity implements
        BackSaveDialogFragment.ApplicationBackSaveDialogFragmentListener {

    private static final String TAG = UserSettingsActivity.class.getSimpleName();
    private final Context mContext = this;

    private FirebaseAuth mAuth;
    private FirebaseUser mCurrentUser;
    private String newDisplayName;
    private UserState mUserState;
    private Boolean mUnreadBoolean;
    private FirebaseFirestoreService mFirebaseUtil;

    private boolean hidePasswordText = false;

    private Button mResetPasswordButton, mUpdatePasswordButton, mCancelButton, mDeleteAccountButton;
    private View mEmailView, mPhoneView, mOldPasswordView, mNewPasswordView;
    private TextView mDisplayNameTextView, mEmailTextView, mPhoneTextView, mOldPasswordTextView, mNewPasswordTextView;
    private EditText mDisplayNameEditText, mEmailEditText, mPhoneEditText, mOldPasswordEditText, mNewPasswordEditText;
    private InputEmailViewHolder emailViewHolder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_settings);

        if (getIntent().hasExtra(USER_STATE_TAG)) {
            mUserState = (UserState) getIntent().getSerializableExtra(USER_STATE_TAG);
            mUnreadBoolean = getIntent().getBooleanExtra(UNREAD_MESSAGE_TAG, false);
            Log.d(TAG, "UserSettings with User: " + mUserState);
        }

        mAuth = FirebaseAuth.getInstance();
        mCurrentUser = mAuth.getCurrentUser();

        mFirebaseUtil = new FirebaseFirestoreService();
        setUpLinks();
        updateScreen();

    }

    private void updateScreen() {
        Log.d(TAG, "updateScreen");
        if (null != mCurrentUser){
            mDisplayNameTextView.setText(mContext.getResources().getString(R.string.user_settings_display_name));
            mDisplayNameEditText.setText(mCurrentUser.getDisplayName());
            mDisplayNameEditText.setSelection(mDisplayNameEditText.getText().length());
            mDisplayNameEditText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    newDisplayName = s.toString();
                }
            });

            if (null != mCurrentUser.getPhoneNumber() && mCurrentUser.getPhoneNumber().length() > 0){
                Log.d(TAG, "phoneNumber not null: " + mCurrentUser.getPhoneNumber());
                Log.d(TAG, "phoneNumber length: " + mCurrentUser.getPhoneNumber().length());
                mPhoneView.setVisibility(View.VISIBLE);
                mPhoneTextView.setText(mContext.getResources().getString(R.string.user_settings_phone_number));
                mPhoneEditText.setText(mCurrentUser.getPhoneNumber());

                mUpdatePasswordButton.setVisibility(View.GONE);
                mCancelButton.setVisibility(View.GONE);
                mResetPasswordButton.setVisibility(View.GONE);
                mDeleteAccountButton.setVisibility(View.VISIBLE);
            }

            else {
                Log.d(TAG, "phoneNumber null");
                mPhoneView.setVisibility(View.GONE);
            }

            if (null != mCurrentUser.getEmail()){
                Log.d(TAG, "getEmail not null: " + mCurrentUser.getEmail());
                mEmailView.setVisibility(View.VISIBLE);
                mEmailTextView.setText(mContext.getResources().getString(R.string.user_settings_email));
                mEmailEditText.setText(mCurrentUser.getEmail());

                if (mCurrentUser.isEmailVerified()){
                    mEmailView.findViewById(R.id.item_input_verified_textview).setVisibility(View.VISIBLE);
                }
                else {

                }

                mUpdatePasswordButton.setVisibility(View.VISIBLE);
                mCancelButton.setVisibility(View.GONE);
                mResetPasswordButton.setVisibility(View.VISIBLE);
                mDeleteAccountButton.setVisibility(View.VISIBLE);
            }
            else {
                Log.d(TAG, "email null");
                mEmailView.setVisibility(View.GONE);
            }

            mOldPasswordView.setVisibility(View.GONE);
            mNewPasswordView.setVisibility(View.GONE);
            mUpdatePasswordButton.setOnClickListener(v -> {
                Log.d(TAG, "Update password");
                updatePasswordScreen();
            });

            mCancelButton.setOnClickListener(v -> {
                Log.d(TAG, "Cancel Update password");
                mOldPasswordEditText.setText("");
                mNewPasswordEditText.setText("");
                updateScreen();
            });


        }

    }

    private void updatePasswordScreen(){

        mNewPasswordView.setVisibility(View.VISIBLE);
        mNewPasswordTextView.setText(mContext.getResources().getString(R.string.user_settings_new_password));
        mNewPasswordEditText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);

        mOldPasswordTextView.setText(mContext.getResources().getString(R.string.user_settings_old_password));
        mOldPasswordView.setVisibility(View.VISIBLE);
        mOldPasswordEditText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);

        mUpdatePasswordButton.setVisibility(View.VISIBLE);
        mCancelButton.setVisibility(View.VISIBLE);
        mResetPasswordButton.setVisibility(View.GONE);
        mDeleteAccountButton.setVisibility(View.GONE);

        mUpdatePasswordButton.setOnClickListener(v -> {
            Log.d(TAG, "Update password firestore");
            updateScreen();
        });
    }

    private void setUpLinks() {
        Log.d(TAG, "Set up links");

        Toolbar mToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        View displayCardView = findViewById(R.id.item_display_name_card);
        mDisplayNameTextView = displayCardView.findViewById(R.id.item_input_edittext_field_heading);
        mDisplayNameEditText = displayCardView.findViewById(R.id.item_input_edittext);

        mEmailView = findViewById(R.id.item_user_email_view);
        emailViewHolder = new InputEmailViewHolder(mEmailView);
        mEmailTextView = mEmailView.findViewById(R.id.item_input_email_textview);
        mEmailEditText = mEmailView.findViewById(R.id.item_input_email_edittext);

        mPhoneView = findViewById(R.id.item_user_phone_view);
        mPhoneTextView = mPhoneView.findViewById(R.id.item_input_edittext_field_heading);
        mPhoneEditText = mPhoneView.findViewById(R.id.item_input_edittext);

        mOldPasswordView = findViewById(R.id.item_old_password_view);
        mOldPasswordTextView = mOldPasswordView.findViewById(R.id.item_input_password_field_heading);
        mOldPasswordEditText = mOldPasswordView.findViewById(R.id.item_input_password_edittext);

        mNewPasswordView = findViewById(R.id.item_new_password_view);
        mNewPasswordTextView = mNewPasswordView.findViewById(R.id.item_input_password_field_heading);
        mNewPasswordEditText = mNewPasswordView.findViewById(R.id.item_input_password_edittext);

        mUpdatePasswordButton = findViewById(R.id.user_settings_update_password);
        mCancelButton = findViewById(R.id.user_settings_cancel_update_password);
        mResetPasswordButton = findViewById(R.id.user_settings_reset_password);
        mDeleteAccountButton = findViewById(R.id.user_settings_delete);

        emailViewHolder.bindData(mCurrentUser);
    }

    public void onClickTogglePasswordText(View view) {
        hidePasswordText = !hidePasswordText;
        if (hidePasswordText){
            Log.d(TAG, "hidePasswordText ");
            mNewPasswordEditText.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            mOldPasswordEditText.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            mOldPasswordEditText.setSelection(mOldPasswordEditText.getText().length());

        }
        else {
            Log.d(TAG, "showPasswordText ");
            mNewPasswordEditText.setTransformationMethod(PasswordTransformationMethod.getInstance());
            mOldPasswordEditText.setTransformationMethod(PasswordTransformationMethod.getInstance());
        }
    }

    public void onClickResetPassword(View view){
        Log.d(TAG, "Reset password");

        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle(getString(R.string.item_reset_password_title));
        alertDialog.setMessage(getString(R.string.item_reset_password_text));

        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE,"Confirm", (dialog, which) -> {
            sendResetPasswordEmail();
        });

        alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Deny", (dialog, which) -> {
            alertDialog.hide();
            updateScreen();
        });

        alertDialog.show();
    }

    public void onClickDeleteAccount(View view){

        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle(getString(R.string.item_delete_account_title));
        alertDialog.setMessage(getString(R.string.item_delete_account_text));

        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE,"Confirm", (dialog, which) -> {
            deleteAccount();
        });

        alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Deny", (dialog, which) -> {
            alertDialog.hide();
            updateScreen();
        });

        alertDialog.show();
    }

    private void sendResetPasswordEmail(){
        Log.d(TAG, "sendResetPasswordEmail");

    }

    private void deleteAccount(){
        Log.d(TAG, "deleteAccount");

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.d(TAG, "create options");
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_top_toolbar, menu);
        MenuItem messageIcon = menu.findItem(R.id.action_message);
        messageIcon.setIcon(mUnreadBoolean?R.drawable.chat_notification:R.drawable.chat_white);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (!CustomUtils.menuOnClick(item, mUnreadBoolean, mUserState, this)){
            onBackPressed();
        }

        return true;
    }

    @Override
    public void onBackPressed() {
        if (null != newDisplayName && !newDisplayName.equals(mCurrentUser.getDisplayName())){
            Log.d(TAG, "DP changed to: " + newDisplayName);
            DialogFragment dialog = new BackSaveDialogFragment();
            dialog.show(getSupportFragmentManager(), "BackSaveDialogFragment");
        }
        else {
            Log.d(TAG, "No changes:");
            super.onBackPressed();
            finish();
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }


    @Override
    public void onBackDialogPositiveClick(DialogFragment dialog) {
        Log.d(TAG, "update user name: " + newDisplayName);
        if (null != mUserState){
            mUserState.setUsername(newDisplayName);
            mFirebaseUtil.setFirestoreDocumentMerge(USER_STATE_FIRESTORE, mCurrentUser.getUid(), mUserState);
        }

        UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                .setDisplayName(newDisplayName)
                .build();

        mCurrentUser.updateProfile(profileUpdates)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "User profile updated.");
                        }
                    }
                });

        finish();
    }

    @Override
    public void onBackDialogNegativeClick(DialogFragment dialog) {
        finish();
    }
}