package com.zeldav1;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LevelListDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.utils.CustomUtils;
import com.utils.services.FirebaseStorageService;
import com.zelda.object.Tip;
import com.zelda.object.UserState;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

import static com.utils.Constants.TIP_ID_TAG;
import static com.utils.Constants.UNREAD_MESSAGE_TAG;
import static com.utils.Constants.USER_STATE_TAG;

public class TipViewActivity extends BaseActivity implements Html.ImageGetter {
    private static final String TAG = TipViewActivity.class.getSimpleName();
    private final Context mContext = this;

    private UserState mUserState;
    private Tip mTip;
    private Boolean mUnreadBoolean;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mUserState = (UserState)getIntent().getSerializableExtra(USER_STATE_TAG);
        mTip = (Tip)getIntent().getSerializableExtra(TIP_ID_TAG);
        mUnreadBoolean = getIntent().getBooleanExtra(UNREAD_MESSAGE_TAG, false);

        Log.d(TAG, "TipView with User: " + mUserState);

        setContentView(R.layout.activity_tip_view);
        startPDialog(mContext);
        setUpLinks();

        makeResourceView();
        hidePDialog();
    }

    private void setUpLinks() {
        Log.d(TAG, "Set up links");

        Toolbar mToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void makeResourceView() {
        Log.d(TAG, "Setting up TipView:" + mTip);

        try{
            ((TextView)findViewById(R.id.tip_view_title)).setText(mTip.getTitle());
            ((TextView)findViewById(R.id.tip_view_subtitle)).setText(mTip.getSubtitle());
            ((TextView)findViewById(R.id.tip_view_text)).setText(mTip.getText());

            Spanned spanned = Html.fromHtml(mTip.getStringText(), this, null);
            ((TextView)findViewById(R.id.tip_view_text)).setText(spanned);

            FirebaseStorageService.setGlideImage(findViewById(R.id.tip_view_image), CustomUtils.setImageNameOnType(Tip.class, mTip.getId()));
        }
        catch (NullPointerException e){
            Toast.makeText(mContext, "Whoops, couldn't load this tip! Please let us know in the messages tab if you'd like this fixed ASAP.", Toast.LENGTH_LONG).show();
            Log.e(TAG, "TipView binding error: " + e.getMessage());

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.d(TAG, "Create toolbar options");
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_top_toolbar, menu);
        MenuItem messageIcon = menu.findItem(R.id.action_message);
        messageIcon.setIcon(mUnreadBoolean?R.drawable.chat_notification:R.drawable.chat_white);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // navigate up on back button click
        if (!CustomUtils.menuOnClick(item, mUnreadBoolean, mUserState, this)){
            onBackPressed();
        }

        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
    }

    @Override
    public Drawable getDrawable(String source) {
        LevelListDrawable drawableList = new LevelListDrawable();
        drawableList.addLevel(0, 0, null);
        drawableList.setBounds(0, 0, 0, 0);

        new LoadImage(TipViewActivity.this, findViewById(R.id.tip_root_view)).execute(source, drawableList);

        return drawableList;
    }

    static class LoadImage extends AsyncTask<Object, Void, Bitmap> {

        private LevelListDrawable mDrawable;
        @SuppressLint("StaticFieldLeak")
        private View rootView;

        private LoadImage(Context context, View rootView){
            this.rootView = rootView;
        }

        @Override
        protected Bitmap doInBackground(Object... params) {
            String source = (String) params[0];
            mDrawable = (LevelListDrawable) params[1];

            try {
                InputStream is = new URL(source).openStream();
                return BitmapFactory.decodeStream(is);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            Log.d(TAG, "onPostExecute drawable " + mDrawable);
            Log.d(TAG, "onPostExecute bitmap " + bitmap);
            if (bitmap != null) {
                Log.d(TAG, "onPostExecute nonul " + bitmap);
                BitmapDrawable d = new BitmapDrawable(bitmap);
                mDrawable.addLevel(1, 1, d);
                mDrawable.setBounds(0, 0, bitmap.getWidth(), bitmap.getHeight());
                mDrawable.setLevel(1);

                CharSequence t = ((TextView)rootView.findViewById(R.id.tip_view_text)).getText();
                ((TextView)rootView.findViewById(R.id.tip_view_text)).setText(t);

                Log.d(TAG, "hidden");
            }
        }

    }

}