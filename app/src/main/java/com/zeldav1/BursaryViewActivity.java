package com.zeldav1;

import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.Toolbar;
import android.text.util.Linkify;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.SetOptions;
import com.utils.CustomUtils;
import com.utils.FirebaseAnalyticsHelper;
import com.utils.services.FirebaseFirestoreService;
import com.utils.services.FirebaseStorageService;
import com.zelda.object.ApplicationForm;
import com.zelda.object.Bursary;
import com.zelda.object.UserState;

import java.util.Objects;
import java.util.UUID;
import java.util.regex.Pattern;

import static com.utils.Constants.APPLICATION_FORM;
import static com.utils.Constants.APPLICATION_FORM_CONTINUE_TAG;
import static com.utils.Constants.APPLICATION_FORM_SECTIONS_TAG;
import static com.utils.Constants.APPLY_ON_CLICK_METRIC;
import static com.utils.Constants.BURSARIES;
import static com.utils.Constants.BURSARY_ID_TAG;
import static com.utils.Constants.BURSARY_TAG;
import static com.utils.Constants.CBA_ID_APPLICATION_FORM;
import static com.utils.Constants.FROM_BURSARY_TAG;
import static com.utils.Constants.FROM_CBA_TAG;
import static com.utils.Constants.FROM_HOME_TAG;
import static com.utils.Constants.FROM_NEW_USER_TAG;
import static com.utils.Constants.INITIAL_PROGRESS;
import static com.utils.Constants.TERMS_CONDITIONS_TAG;
import static com.utils.Constants.UNREAD_MESSAGE_TAG;
import static com.utils.Constants.USER_STATE_FIRESTORE;
import static com.utils.Constants.USER_STATE_TAG;

public class BursaryViewActivity extends BaseActivity {
    private static final String TAG = BursaryViewActivity.class.getSimpleName();
    private final Context mContext = this;

    private Boolean mUnreadBoolean, fromCBA, fromNewUser, fromHome;
    boolean continueApplicationForm = false, reviewApplicationForm = false;
    private FirebaseAnalyticsHelper mFirebaseAnalyticsHelper;

    private FirebaseAuth mAuth;
    private FirebaseUser mCurrentUser;

    private Button mBursaryApplyButton;

    private FirebaseFirestoreService mFirebaseUtil;
    private ConstraintLayout mDisplayContainer, mLoadingContainer;
    private Bursary mBursary;
    private UserState mUserState;
    private TextView mPinnedText, mDateClosedTextView, mTitleTextView, mDescriptionTextView, mCompanyName, mApplicableFieldsTextView,
            mSupportProvidedTextView, mRequirementsTextView, mApplicationProcessTextView;
    private ImageView mPinnedIcon;
    private boolean pinned = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mUserState = (UserState)getIntent().getSerializableExtra(USER_STATE_TAG);
        mBursary = (Bursary)getIntent().getSerializableExtra(BURSARY_TAG);
        mUnreadBoolean = getIntent().getBooleanExtra(UNREAD_MESSAGE_TAG, false);
        fromCBA = getIntent().getBooleanExtra(FROM_CBA_TAG, false);
        fromNewUser = getIntent().getBooleanExtra(FROM_NEW_USER_TAG, false);
        fromHome = getIntent().getBooleanExtra(FROM_HOME_TAG, false);
        mFirebaseAnalyticsHelper = new FirebaseAnalyticsHelper(this);

        setContentView(R.layout.activity_bursary_view);
        setUpLinks();
        startPDialog(mContext);

        mAuth = FirebaseAuth.getInstance();
        mCurrentUser = mAuth.getCurrentUser();

        mFirebaseUtil = new FirebaseFirestoreService();

        String bursaryId = getIntent().getStringExtra(BURSARY_ID_TAG);
        if (null != bursaryId){
            Log.d(TAG, "Find bursary: " + bursaryId);
            findBursary(bursaryId);
        }

        findUser();

        makeBursaryView();
        hidePDialog();

    }

    private void setUpLinks() {
        Log.d(TAG, "Set up links");

        Toolbar mToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mLoadingContainer = findViewById(R.id.item_bursary_loading_container);
        mDisplayContainer = findViewById(R.id.bursary_scroll_container);
        mLoadingContainer.setVisibility(View.VISIBLE);

        mTitleTextView = findViewById(R.id.bursary_view_title);
        mDescriptionTextView = findViewById(R.id.bursary_view_description_text);
        mDateClosedTextView = findViewById(R.id.bursary_view_date_closed);
        mCompanyName = findViewById(R.id.bursary_view_company_name);
        mApplicableFieldsTextView = findViewById(R.id.bursary_view_applicable_fields);
        mSupportProvidedTextView = findViewById(R.id.bursary_view_support_provided);
        mRequirementsTextView = findViewById(R.id.bursary_view_requirements);
        mApplicationProcessTextView = findViewById(R.id.bursary_view_application_process);

        mBursaryApplyButton = findViewById(R.id.bursary_view_apply_button);
        mPinnedIcon = findViewById(R.id.bursary_view_pinned);

        mPinnedText = findViewById(R.id.bursary_view_pinned_text);
        mPinnedText.setVisibility(View.VISIBLE);

        if (fromNewUser)
            findViewById(R.id.bursary_view_skip_button).setVisibility(View.VISIBLE);
        else
            findViewById(R.id.bursary_view_skip_button).setVisibility(View.GONE);
    }

    private void findUser(){
        if (null != mCurrentUser) {
            mFirebaseUtil.assignUserListener(value -> {
                if (null != value) {
                    mUserState = value.toObject(UserState.class);
                    mUserState.setId(value.getId());
                    Log.d(TAG, "UserState Pinned: " + mUserState.getPinnedBursaries());

                    if (null != mBursary) {
                        if (null != mUserState.getStartedApplicationIds())
                            continueApplicationForm = mUserState.getStartedApplicationIds().keySet().contains(mBursary.getId());

                        if (null != mUserState.getCompletedApplicationIds())
                            reviewApplicationForm = mUserState.getCompletedApplicationIds().keySet().contains(mBursary.getId());
                    }

                    updatePinIcon();

                } else {
                    Log.d(TAG, "No User State found.");
                }
            }, mCurrentUser.getUid());
        }
    }

    private void findBursary(String bursaryId){
        mFirebaseUtil.fetchDocument(value -> {
            if (null != value){
                mBursary = value.toObject(Bursary.class);
                Log.d(TAG, "mBursary:" + mBursary.toString());
            }
            else{
                Log.d(TAG, "No mBursary found.");
                mBursary = null;
            }
            makeBursaryView();
        }, BURSARIES, bursaryId);
    }

    private void makeBursaryView() {
        if (null != mBursary && null != mUserState) {
            if (null != mUserState.getStartedApplicationIds())
                continueApplicationForm = mUserState.getStartedApplicationIds().keySet().contains(mBursary.getId());
            if (null != mUserState.getCompletedApplicationIds())
                reviewApplicationForm = mUserState.getCompletedApplicationIds().keySet().contains(mBursary.getId());

            if (continueApplicationForm) {
                mBursaryApplyButton.setText(getResources().getString(R.string.bursary_view_continue_application));
            }

            if (reviewApplicationForm) {
                mBursaryApplyButton.setText(getResources().getString(R.string.bursary_view_review_application));
            }

            TextView tcLink = findViewById(R.id.bursary_view_terms_conditions_link);
            tcLink.setVisibility(View.GONE);

             if (mBursary.getId().equals(CBA_ID_APPLICATION_FORM)){
                Log.d(TAG, "CBA application!");
                 mPinnedIcon.setVisibility(View.GONE);
                 mPinnedText.setVisibility(View.GONE);
                tcLink.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
                tcLink.setVisibility(View.VISIBLE);

                tcLink.setOnClickListener(v -> {
                    Intent intent = new Intent(mContext, AboutActivity.class);
                    intent.putExtra(USER_STATE_TAG, mUserState);
                    intent.putExtra(UNREAD_MESSAGE_TAG, mUnreadBoolean);
                    intent.putExtra(TERMS_CONDITIONS_TAG, true);
                    startActivity(intent);
                });

                if (!mUserState.isOptedIn()){
                    Log.d(TAG, "Initial progress, not submitted");
                    mBursaryApplyButton.setText(mContext.getResources().getString(R.string.item_uv_opt_in_yes));
                    mBursaryApplyButton.setOnClickListener(this::createOptInAlertDialog);
                }
                else {
                    Log.d(TAG, "Started");
                    mBursaryApplyButton.setOnClickListener(v -> {
                        launchCBAActivity();
                    });

                    if (!mUserState.getSubmitted() && mUserState.getProgress() != INITIAL_PROGRESS || fromCBA) {
                        Log.d(TAG, "Not submitted");
                        mBursaryApplyButton.setText(getResources().getString(R.string.bursary_view_continue_application));
                    }
                    else if (!mUserState.getSubmitted() && mUserState.getProgress() == INITIAL_PROGRESS){
                        Log.d(TAG, "Not started");
                        mBursaryApplyButton.setText(getResources().getString(R.string.cba_start));
                    }

                    else if (mUserState.getSubmitted() && mUserState.getProgress() == 100) {
                        Log.d(TAG, "Submitted");
                        mBursaryApplyButton.setText(getResources().getString(R.string.cba_submission_edit));
                    }
                }

            }
            else {

                 mPinnedIcon.setOnClickListener(v -> {
                     if (pinned) {
                         Log.d(TAG, "Bursary pinned already. Remove");
                         mUserState.removeBursaryFromPinnedList(mBursary.getId());
                     } else {
                         Log.d(TAG, "Bursary added to pin");
                         mUserState.addBursaryToPinned(mBursary.getId());
                     }

                     Log.d(TAG, "mUserState Pinned:" + mUserState.getPinnedBursaries());

                     startPDialog(mContext, getString(R.string.loading_updating));
                     mFirebaseUtil.getFirestore().collection(USER_STATE_FIRESTORE).document(mUserState.getId())
                             .set(mUserState, SetOptions.merge())
                             .addOnSuccessListener(aVoid -> {
                                 Log.d(TAG, "DocumentSnapshot successfully written!");
                                 hidePDialog();
                             })
                             .addOnFailureListener(e -> Log.w(TAG, "Error writing document", e));

                     pinned = !pinned;

                     updatePinIcon();
                 });

                 mPinnedText.postDelayed(() -> mPinnedText.animate()
                         .translationY(mPinnedText.getHeight())
                         .alpha(0.0f)
                         .setDuration(500), 3500);

                 Pattern pattern = Pattern.compile("developer.android.com");
                 Linkify.addLinks(mBursaryApplyButton, pattern, mBursary.getBursaryUrl());

                 updatePinIcon();
                 mBursaryApplyButton.setOnClickListener(view -> {
                     if (reviewApplicationForm) {
                         launchReviewActivity();
                     } else {
                         if (!mBursary.getInternalApplicationForm() && null != mBursary.getBursaryUrl()) {
                             mFirebaseAnalyticsHelper.addFirebaseEventMetric(mBursary.getId(), mBursary.getTitle(), APPLY_ON_CLICK_METRIC);

                             Uri webpage = Uri.parse(mBursary.getBursaryUrl());
                             if (!mBursary.getBursaryUrl().startsWith("http://") && !mBursary.getBursaryUrl().startsWith("https://")) {
                                 webpage = Uri.parse("http://" + mBursary.getBursaryUrl());
                             }

                             Intent browserIntent = new Intent(Intent.ACTION_VIEW, webpage);
                             try {
                                 startActivity(browserIntent);
                             } catch (ActivityNotFoundException e) {
                                 Log.e(TAG, "URL not found exception: " + e);
                                 Toast.makeText(getApplicationContext(), R.string.bursary_view_url_exception, Toast.LENGTH_SHORT).show();
                             } catch (Exception e) {
                                 Log.e(TAG, "URL exception: " + e);
                                 Toast.makeText(getApplicationContext(), R.string.bursary_view_url_exception, Toast.LENGTH_SHORT).show();
                             }
                         }

                         else {
                             Intent intent = new Intent(BursaryViewActivity.this, ApplicationFormActivity.class);
                             intent.putExtra(USER_STATE_TAG, mUserState);
                             intent.putExtra(UNREAD_MESSAGE_TAG, mUnreadBoolean);
                             intent.putExtra(BURSARY_TAG, mBursary);
                             intent.putExtra(APPLICATION_FORM_SECTIONS_TAG, mBursary.getSections());
                             intent.putExtra(APPLICATION_FORM_CONTINUE_TAG, continueApplicationForm);
                             startActivity(intent);
                         }
                     }
                 });
             }

            mTitleTextView.setText(mBursary.getTitle());
            mDateClosedTextView.setText(getString(R.string.application_view_date_closed, mBursary.getClosingDate()));
            mDescriptionTextView.setText(mBursary.getSubtitle());
            mApplicableFieldsTextView.setText(mBursary.getApplicableFields());

            mSupportProvidedTextView.setText(mBursary.getSupportProvided());
            mRequirementsTextView.setText(mBursary.getRequirements());
            mApplicationProcessTextView.setText(mBursary.getApplicationProcess());
            FirebaseStorageService.setGlideImage(findViewById(R.id.bursary_view_image), CustomUtils.setImageNameOnType(Bursary.class, mBursary.getId()));

            if (null != mBursary.getCompanyName()) {
                mCompanyName.setVisibility(View.VISIBLE);
                mCompanyName.setText(mBursary.getCompanyName());
            }
            mLoadingContainer.setVisibility(View.GONE);
            mDisplayContainer.setVisibility(View.VISIBLE);
        }
    }

    private void updatePinIcon(){
        if (mUserState.getPinnedBursariesSize() > 0 && null != mBursary) {
            for (String bursaryId : mUserState.getPinnedBursaries()) {
                Log.d(TAG, "UserPinned: " + bursaryId + " This: " + mBursary.getId());
                if (bursaryId.equals(mBursary.getId())) {
                    pinned = true;
                    mPinnedIcon.setImageResource(R.drawable.star_filled);
                    mPinnedText.setText(getString(R.string.bursary_view_pinned_text));
                    break;
                }
                else {
                    mPinnedIcon.setImageResource(R.drawable.star_unfilled);
                    mPinnedText.setText(getString(R.string.bursary_view_unpinned_text));
                }
            }
            Log.d(TAG, "Pinned : " + pinned);
        }
        else {
            mPinnedIcon.setImageResource(R.drawable.star_unfilled);
            mPinnedText.setText(getString(R.string.bursary_view_unpinned_text));
        }

    }

    public void createOptInAlertDialog(View view){
        AlertDialog alertDialog = new AlertDialog.Builder(this, R.style.AlertDialogTheme).create();
        alertDialog.setTitle(getString(R.string.prompt_cba_opt_in_title));
        alertDialog.setMessage(getString(R.string.prompt_cba_confirm_tc));

        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, mContext.getResources().getString(R.string.prompt_cba_tc_yes_start), (dialog, which) -> {

            if (fromNewUser){
                Log.d(TAG, "fromNewUser");
                Log.d(TAG, "yes, opt in & start CBA");
                Log.d(TAG, "launchCBAActivity");
                mFirebaseAnalyticsHelper.logOptInRegistrationMetric();
                mFirebaseAnalyticsHelper.logOpenCBARegMetric();
            }
            else {
                Log.d(TAG, "Not fromNewUser");
                Log.d(TAG, "createApplicationForm");
                Log.d(TAG, "launchCBAActivity");
                mFirebaseAnalyticsHelper.logOptInMetric();
                mFirebaseAnalyticsHelper.logOpenCBAMetric();
            }
            createApplicationForm();
            launchCBAActivity();

        });

        alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, mContext.getResources().getString(R.string.prompt_cba_tc_yes_later), (dialog, which) -> {
            if (fromNewUser){
                Log.d(TAG, "fromNewUser");
                Log.d(TAG, "yes, opt in & skip CBA");
                Log.d(TAG, "launchHomeActivity");
                mFirebaseAnalyticsHelper.logOptInRegistrationMetric();
            }
            else {
                Log.d(TAG, "Not fromNewUser");
                Log.d(TAG, "createApplicationForm");
                Log.d(TAG, "launchHomeActivity");
                mFirebaseAnalyticsHelper.logOptInMetric();
            }
            mFirebaseAnalyticsHelper.logSkipCBAMetric();
            createApplicationForm();
            launchHomeActivity();
        });

        alertDialog.setButton(DialogInterface.BUTTON_NEUTRAL, mContext.getResources().getString(R.string.prompt_cba_tc_no), (dialog, which) -> {
            alertDialog.hide();
        });

        alertDialog.show();
    }

    protected void createApplicationForm(){
        String applicationFormId = UUID.randomUUID().toString();
        ApplicationForm applicationForm = new ApplicationForm(applicationFormId, CBA_ID_APPLICATION_FORM, false, mCurrentUser.getUid());

        mFirebaseUtil.setFirestoreDocument(APPLICATION_FORM, applicationFormId, applicationForm);
        mUserState.setOptIn(applicationFormId);
        mFirebaseUtil.setFirestoreDocument(USER_STATE_FIRESTORE, mCurrentUser.getUid(), mUserState);

    }

    private void launchReviewActivity() {
        Intent intent = new Intent(BursaryViewActivity.this, ApplicationFormReviewActivity.class);
        intent.putExtra(USER_STATE_TAG, mUserState);
        intent.putExtra(UNREAD_MESSAGE_TAG, mUnreadBoolean);
        intent.putExtra(BURSARY_TAG, mBursary);
        startActivity(intent);
    }

    private void launchCBAActivity(){
        if (fromCBA)
            finish();

        else {
            Intent intent = new Intent(BursaryViewActivity.this, UserViewActivity.class);

            intent.putExtra(USER_STATE_TAG, mUserState);
            intent.putExtra(UNREAD_MESSAGE_TAG, mUnreadBoolean);
            intent.putExtra(FROM_BURSARY_TAG, true);
            finish();
            overridePendingTransition( 0, 0);
            startActivity(intent);
            overridePendingTransition( 0, 0);
        }
    }

    private void launchHomeActivity(){
        if (fromCBA)
            finish();
        else {
            Intent intent = new Intent(BursaryViewActivity.this, HomeActivity.class);

            intent.putExtra(USER_STATE_TAG, mUserState);
            intent.putExtra(UNREAD_MESSAGE_TAG, mUnreadBoolean);
            finish();
            overridePendingTransition( 0, 0);
            startActivity(intent);
            overridePendingTransition( 0, 0);
        }
    }

    public void skipCBAAbout(View view) {
        if (fromNewUser) {
            Log.d(TAG, "Skip from new user");
            mFirebaseAnalyticsHelper.logNotOptInMetric();
            mFirebaseAnalyticsHelper.logSkipCBAMetric();
            Log.d(TAG, "launchHomeActivity");
            launchHomeActivity();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_top_toolbar, menu);
        MenuItem messageIcon = menu.findItem(R.id.action_message);
        messageIcon.setIcon(mUnreadBoolean?R.drawable.chat_notification:R.drawable.chat_white);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // navigate up on back button click
        if (!CustomUtils.menuOnClick(item, mUnreadBoolean, mUserState, this)){
            onBackPressed();
        }

        return true;
    }

    @Override
    public void onBackPressed() {
        if (fromNewUser) {
            Log.d(TAG, "Back, from NewUser");
            mFirebaseAnalyticsHelper.logNotOptInMetric();
            mFirebaseAnalyticsHelper.logSkipCBAMetric();
            launchHomeActivity();
        }
        else if (fromHome){
            Log.d(TAG, "Back,  fromHome");
            launchHomeActivity();
        }
        else{
            super.onBackPressed();
            if (fromCBA){
                Log.d(TAG, "fromCBA");
            }
            else
                Log.d(TAG, "not fromCBA");

            finish();
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

}