package com.zeldav1;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.utils.FirebaseAnalyticsHelper;
import com.utils.services.FirebaseFirestoreService;
import com.zelda.object.UserState;

import java.util.HashMap;

import static com.utils.Constants.BURSARY_FIELD_ALL;
import static com.utils.Constants.BURSARY_ID_TAG;
import static com.utils.Constants.CBA_ID_APPLICATION_FORM;
import static com.utils.Constants.FALSE;
import static com.utils.Constants.FROM_NEW_USER_TAG;
import static com.utils.Constants.INITIAL_PROGRESS;
import static com.utils.Constants.USER_FIELD_PREFERENCE_FIRESTORE;
import static com.utils.Constants.USER_OPT_IN;
import static com.utils.Constants.USER_PROGRESS;
import static com.utils.Constants.USER_STATE_FIRESTORE;
import static com.utils.Constants.USER_STATE_TAG;
import static com.utils.Constants.USER_USERNAME;

public class NewUserActivity extends BaseActivity {
    private static final String TAG = NewUserActivity.class.getSimpleName();

    private Context mContext = this;
    private FirebaseAnalyticsHelper mFirebaseAnalyticsHelper;

    private TextView mUserStateText, mSkipText, mUserStateAdditionalText, mUserStateSubtitleText;
    private EditText mUsernameEditText;

    private int progress;

    private boolean isReached = false;

    private Button mButtonA, mButtonB;
    private FirebaseUser mCurrentUser;
    private UserState userState;
    private FirebaseFirestoreService mFirebaseUtil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_user_activity);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        mCurrentUser = FirebaseAuth.getInstance().getCurrentUser();

        if (null != mCurrentUser) {
            mFirebaseUtil = new FirebaseFirestoreService();
            mFirebaseAnalyticsHelper = new FirebaseAnalyticsHelper(mContext);
            setUpLinks();
            setCurrentView();
        }
        else {
            Intent intent = new Intent(NewUserActivity.this, LoginActivity.class);
            startActivity(intent);
            finish();
        }
    }

    private void setCurrentView(){
        switch(progress){
            case 0:
                Log.d(TAG, "Progress 0: "+ progress);
                mUserStateText.setText(R.string.new_user_welcome_state_a);
                mUserStateSubtitleText.setText(R.string.new_user_zomila_summary);
                mUserStateSubtitleText.setVisibility(View.VISIBLE);
                mUserStateAdditionalText.setText(R.string.new_user_welcome_name_state_a);
                mUsernameEditText.setVisibility(View.VISIBLE);

                if (null != mCurrentUser && null != mCurrentUser.getDisplayName())
                    mUsernameEditText.setText(mCurrentUser.getDisplayName());

                mUsernameEditText.addTextChangedListener(new TextWatcher(){
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        int length  = s.length();
                        char lastChar = ' ';

                        if(length > 0) {
                            lastChar = mUsernameEditText.getText().charAt(length-1);
                            Log.d(TAG, "length: " + length + " last: " + s.charAt(length - 1) + " reached " + isReached);
                        }

                        if (s.length() > 10 && s.length() < 15 && lastChar != '\n' && !isReached){
                            Log.d(TAG, "between 10-15: " + "not reached ");
                            if (lastChar == ' ') {
                                Log.d(TAG, "space detected: ");
                                mUsernameEditText.append("\n");
                                isReached = true;
                            }
                            else{
                                Log.d(TAG, "other");
                            }
                        }

                        else if(s.length() >= 15 && !isReached) {
                            Log.d(TAG, "15 or more:  not reached");
                            if (lastChar != '\n') {
                                mUsernameEditText.append("\n");
                                isReached = true;
                            }
                            else{
                                Log.d(TAG, "other 15");
                            }
                        }

                        else if (s.length() < 10 && isReached){
                            Log.d(TAG, "less than 10 reached ");
                            isReached = false;
                        }

                        else {
                            Log.d(TAG, "do nothing.");
                        }
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                    }
                });

                mButtonA.setVisibility(View.VISIBLE);
                mButtonA.setText(R.string.next);
                mButtonB.setVisibility(View.VISIBLE);
                mButtonB.setText(R.string.new_user_about_us);

                mButtonA.setOnClickListener(v -> {
                    Log.d(TAG, "next clicked: "+ progress);
                    progress++;
                    setCurrentView();
                });

                mButtonB.setOnClickListener(v -> {
                    Log.d(TAG, "About zomila clicked: "+ progress);
                    mFirebaseAnalyticsHelper.logAboutZomilaMetric();
                    launchAboutZomila();
                });
                break;

            case 1:
                mUserStateText.setText(R.string.new_user_find_out);
                mUserStateAdditionalText.setText(R.string.new_user_zomila_summary);
                Log.d(TAG, "Progress 1: "+ progress);

                mUsernameEditText.setVisibility(View.GONE);
                mUserStateSubtitleText.setVisibility(View.GONE);

                mButtonA.setVisibility(View.VISIBLE);
                mButtonA.setText(getResources().getString(R.string.new_user_find_out_now));
                mButtonB.setVisibility(View.VISIBLE);
                mButtonB.setText(getResources().getString(R.string.new_user_no_thanks));

                mButtonA.setOnClickListener(v -> {
                    Log.d(TAG, "About CBA");
                    addNewUser();
                    mFirebaseAnalyticsHelper.logAboutCBAMetric();
                    launchBursaryView();
                });

                mButtonB.setOnClickListener(v -> {
                    Log.d(TAG, "No thanks clicked.");
                    addNewUser();
                    mFirebaseAnalyticsHelper.logNotOptInMetric();
                    mFirebaseAnalyticsHelper.logSkipCBAMetric();
                    launchHome();
                });

                break;
        }
        hidePDialog();
    }

    public void setUpLinks(){
        progress = 0;
        mUserStateText = findViewById(R.id.new_user_state_text);
        mUserStateAdditionalText = findViewById(R.id.new_user_state_additional_text);
        mUserStateSubtitleText = findViewById(R.id.new_user_state_subtitle_text);
        mSkipText = findViewById(R.id.new_user_skip_text);
        mButtonA = findViewById(R.id.button_A);
        mButtonB = findViewById(R.id.button_B);
        mUsernameEditText = findViewById(R.id.new_user_username);
        mContext = NewUserActivity.this;

        mSkipText.setOnClickListener(v -> {
            addNewUser();
            mFirebaseAnalyticsHelper.logNotOptInMetric();
            mFirebaseAnalyticsHelper.logSkipCBAMetric();
            launchHome();
        });
    }

    @Override
    public void onBackPressed() {
        switch (progress){
            case 0:
                finish();

            case 2:
                progress-=2;
                setCurrentView();
                break;

            default:
                progress--;
                setCurrentView();
                break;
        }
    }

    private void addNewUser(){
        Log.d(TAG, "addNewUser");

        if (null != mCurrentUser) {
            startPDialog(mContext, getString(R.string.new_user_loading_promt));

            HashMap<String, Object> userToCreate = new HashMap<>();
            HashMap<String, Boolean> tempInterestFields = new HashMap<>();
            tempInterestFields.put(BURSARY_FIELD_ALL, true);

            userToCreate.put(USER_PROGRESS, INITIAL_PROGRESS);
            userToCreate.put(USER_FIELD_PREFERENCE_FIRESTORE, tempInterestFields);
            userToCreate.put(USER_USERNAME, mUsernameEditText.getText().toString());
            userToCreate.put(USER_OPT_IN, FALSE);
            String userId = mCurrentUser.getUid();


            mFirebaseUtil.setFirestoreDocument(mContext, USER_STATE_FIRESTORE, userId, userToCreate, getResources().getString(R.string.new_user_welcome));
            Log.d(TAG, "User created: " + userToCreate.toString());

            mFirebaseUtil.submitRegistrationToken(userId);
            sendVerification();
        }
    }

    private void sendVerification() {
        if (mCurrentUser != null && mCurrentUser.getProviders() != null) {
            Log.d(TAG, "Provider found");
            if (mCurrentUser.getEmail() != null && mCurrentUser.getProviders().get(0).contains("password") || mCurrentUser.getProviders().get(0).contains("google")) {
                Log.d(TAG, "Password user found.");
                mCurrentUser.sendEmailVerification().addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        Log.d(TAG, "Verification email sent");
                        Toast.makeText(mContext, getResources().getString(R.string.item_email_verification_sent_text, mCurrentUser.getEmail()), Toast.LENGTH_SHORT).show();
                    } else {
                        Log.e(TAG, "sendEmailVerification", task.getException());
                        Toast.makeText(mContext, getResources().getString(R.string.item_email_verification_failed_text), Toast.LENGTH_SHORT).show();
                    }
                });
            } else if (mCurrentUser.getProviders().get(0).contains("facebook")) {
                Log.d(TAG, "facebook user found");
            } else if (mCurrentUser.getProviders().get(0).contains("phone")) {
                Log.d(TAG, "phone user found");
            } else if (mCurrentUser.getProviders().get(0).contains("google")) {
                Log.d(TAG, "google user found");
            }
            if (mCurrentUser.getEmail() == null){
                Log.d(TAG, "User has no email");

            }
        }
    }

    private void launchAboutZomila(){
        Intent intent = new Intent(NewUserActivity.this, AboutActivity.class);
        intent.putExtra(USER_STATE_TAG, userState);
        startActivity(intent);
    }

    private void launchBursaryView(){
        Intent intent = new Intent(NewUserActivity.this, BursaryViewActivity.class);
        intent.putExtra(USER_STATE_TAG, userState);
        intent.putExtra(BURSARY_ID_TAG, CBA_ID_APPLICATION_FORM);
        intent.putExtra(FROM_NEW_USER_TAG, true);
        startActivity(intent);
        finish();
    }

    private void launchHome(){
        Intent intent = new Intent(NewUserActivity.this, HomeActivity.class);
        intent.putExtra(USER_STATE_TAG, userState);
        startActivity(intent);

        if (!this.isFinishing())
            finish();
    }

}
