package com.zeldav1;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.annotation.VisibleForTesting;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

public class BaseActivity extends AppCompatActivity{

    @VisibleForTesting
    private ProgressDialog pDialog;
    private static ProgressDialog staticPDialog;

    public void startPDialog(Context context) {
        if (pDialog == null) {
            pDialog = new ProgressDialog(this);
            pDialog.setMessage(getString(R.string.loading));
            pDialog.setIndeterminate(true);
        }

        pDialog.show();
    }
    public void startPDialog(Context context, String message) {
        if (pDialog == null) {
            pDialog = new ProgressDialog(context);
            pDialog.setMessage(message);
            pDialog.setIndeterminate(true);
        }

        pDialog.show();
    }

    public void hidePDialog() {
        if (pDialog != null) {
            pDialog.dismiss();
            pDialog = null;
        }
    }

    public static void startStaticPDialog(Context context, String message) {
        if (staticPDialog == null) {
            staticPDialog = new ProgressDialog(context);
            staticPDialog.setMessage(message);
            staticPDialog.setIndeterminate(true);
        }

        staticPDialog.show();
    }

    public static void hideStaticPDialog() {
        if (staticPDialog != null) {
            staticPDialog.dismiss();
            staticPDialog = null;
        }
    }

    public void hideKeyboard(View view) {
        final InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        hidePDialog();
    }
}
