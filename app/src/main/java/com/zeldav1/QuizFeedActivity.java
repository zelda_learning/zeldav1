package com.zeldav1;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.util.Linkify;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.firestore.Query;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;
import com.utils.FirebaseAnalyticsHelper;
import com.utils.CustomUtils;
import com.utils.adapters.ViewAdapter;
import com.utils.services.FirebaseFirestoreService;
import com.utils.viewholders.QuizViewHolder;
import com.zelda.object.NBTQuiz;
import com.zelda.object.UserState;

import java.util.Objects;
import java.util.regex.Pattern;

import static com.utils.Constants.NBT_QUIZZES;
import static com.utils.Constants.BURSARY_FIELD_PINNED;
import static com.utils.Constants.QUIZ_ID_TAG;
import static com.utils.Constants.SELECT_QUIZ_METRIC;
import static com.utils.Constants.UNREAD_MESSAGE_TAG;
import static com.utils.Constants.USER_STATE_TAG;

public class QuizFeedActivity extends BaseActivity {
    private static final String TAG = QuizFeedActivity.class.getSimpleName();
    private final Context mContext = this;
    private FirebaseAnalyticsHelper mFirebaseAnalyticsHelper;

    private BottomNavigationViewEx mBottomNavigationBar;

    private RecyclerView mRecyclerView;
    private FirebaseUser mCurrentUser;
    private UserState mUserState;
    private boolean fromHome = false;
    private ViewAdapter<NBTQuiz> mQuizFeedAdapter;
    private FirestoreRecyclerAdapter<NBTQuiz, QuizViewHolder> mQuizFeedFirestoreAdapter;

    private ValueEventListener mQuizVEL = null;
    private Boolean mUnreadBoolean;
    private FirebaseFirestoreService mFirebaseUtil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz_feed);

        mCurrentUser = FirebaseAuth.getInstance().getCurrentUser();
        mUserState = (UserState)getIntent().getSerializableExtra(USER_STATE_TAG);
        mUnreadBoolean = getIntent().getBooleanExtra(UNREAD_MESSAGE_TAG, false);
        if (null != getIntent().getSerializableExtra(BURSARY_FIELD_PINNED)){
            fromHome = (boolean)getIntent().getSerializableExtra(BURSARY_FIELD_PINNED);
        }
        mFirebaseAnalyticsHelper = new FirebaseAnalyticsHelper(this);

        Log.d(TAG, "QuizFeed with User: " + mUserState);

        mFirebaseUtil = new FirebaseFirestoreService();

        findUser();
        setUpLinks();

    }

    private void setUpLinks() {
        Log.d(TAG, "Setting up quizFeed links");

        Toolbar mToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(false);

        mBottomNavigationBar = findViewById(R.id.bottom_nav_bar);
        mBottomNavigationBar.setSelectedItemId(R.id.action_bursary);
        mBottomNavigationBar.enableAnimation(false);
        mBottomNavigationBar.enableItemShiftingMode(false);
        mBottomNavigationBar.enableShiftingMode(false);

        mBottomNavigationBar.setSelectedItemId(R.id.action_quiz);
        mBottomNavigationBar.setOnNavigationItemSelectedListener(item -> {
            Class startClass = null;
            switch (item.getItemId()) {
                case R.id.action_bursary:
                    startClass = BursaryFeedActivity.class;
                    break;

                case R.id.action_home:
                    startClass = HomeActivity.class;
                    break;

                case R.id.action_tips:
                    startClass = TipFeedActivity.class;
                    break;

                case R.id.action_university:
                    startClass = UniversityFeedActivity.class;
                    break;

                case R.id.action_quiz:
                    break;
            }
            if (null != startClass) {
                Intent intent = new Intent(QuizFeedActivity.this, startClass);
                intent.putExtra(UNREAD_MESSAGE_TAG, mUnreadBoolean);
                intent.putExtra(USER_STATE_TAG, mUserState);
                startActivity(intent);
                finish();
            }
            return false;
        });

        mRecyclerView = findViewById(R.id.quiz_recycler_view);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mContext);
        mRecyclerView.setLayoutManager(mLayoutManager);

        final GestureDetector mGestureDetector = new GestureDetector(mContext, new GestureDetector.SimpleOnGestureListener(){
            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                return true;
            }
        });

        mRecyclerView.addOnItemTouchListener(
                new RecyclerView.OnItemTouchListener() {

                    @Override
                    public void onTouchEvent(RecyclerView rv, MotionEvent e) {

                    }

                    @Override
                    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

                    }

                    @Override
                    public boolean onInterceptTouchEvent(RecyclerView recyclerView, MotionEvent motionEvent) {
                        View child = recyclerView.findChildViewUnder(motionEvent.getX(), motionEvent.getY());

                        if (child != null && mGestureDetector.onTouchEvent(motionEvent)) {
                            NBTQuiz nbtQuiz = mQuizFeedFirestoreAdapter.getItem(recyclerView.getChildLayoutPosition(child));
                            nbtQuiz.setImageName(CustomUtils.setImageNameOnType(NBTQuiz.class, nbtQuiz.getId()));
                            Log.d(TAG, "touch intercept q Feed:" + nbtQuiz);
                            mFirebaseAnalyticsHelper.addFirebaseEventMetric(nbtQuiz.getId(), nbtQuiz.getTitle(), SELECT_QUIZ_METRIC);

                            Intent intent = new Intent(QuizFeedActivity.this, NBTQuizActivity.class);
                            intent.putExtra(QUIZ_ID_TAG, nbtQuiz);
                            intent.putExtra(USER_STATE_TAG, mUserState);
                            intent.putExtra(UNREAD_MESSAGE_TAG, mUnreadBoolean);
                            startActivity(intent);
                        }
                        return false;

                    }
                });


        Pattern pattern = Pattern.compile("developer.android.com");
        Linkify.addLinks((Button)findViewById(R.id.quiz_feed_apply_button), pattern, getString(R.string.quiz_nbt_apply_link));

        Button nbtApplicationLink = findViewById(R.id.quiz_feed_apply_button);

        nbtApplicationLink.setOnClickListener(view -> {
            try {
                Log.d(TAG, "Try load university Link");

                Uri uri = Uri.parse(getString(R.string.quiz_nbt_apply_link));
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
            catch(NullPointerException e) {
                Log.d(TAG, "button has no link.");
            }
        });
        createFeedAdapter();
    }

    private void findUser(){
        if (null != mCurrentUser) {
            Log.d(TAG, "Current user: " + mCurrentUser.getUid());
            mFirebaseUtil.assignUserListener(value -> {
                if (null != value) {
                    mUserState = value.toObject(UserState.class);
                    mUserState.setId(mCurrentUser.getUid());
                    Log.d(TAG, "USER STATE UPDATED");
                    if (null != mQuizFeedFirestoreAdapter)
                        mQuizFeedFirestoreAdapter.notifyDataSetChanged();

                } else {
                    Log.d(TAG, "No such document, should check firebase");
                }
            }, mCurrentUser.getUid());
        }
    }

    private void createFeedAdapter(){
        Log.d(TAG, "Fetching NBTQuiz objects from Firestore");

        Query query = mFirebaseUtil.getFirestore()
                .collection(NBT_QUIZZES)
                .limit(50);

        FirestoreRecyclerOptions<NBTQuiz> options = new FirestoreRecyclerOptions.Builder<NBTQuiz>()
                .setQuery(query, NBTQuiz.class)
                .build();

        mQuizFeedFirestoreAdapter = new FirestoreRecyclerAdapter<NBTQuiz, QuizViewHolder>(options) {
            @Override
            protected void onBindViewHolder(@NonNull QuizViewHolder holder, int position, @NonNull NBTQuiz NBTQuiz) {
                if (null != mUserState && null != mUserState.getCompletedNBTQuizzes().get(NBTQuiz.getId())) {
                    holder.bindData(NBTQuiz, mUserState.getCompletedNBTQuizzes().get(NBTQuiz.getId()));
                } else
                    holder.bindData(NBTQuiz);
            }

            @NonNull
            @Override
            public QuizViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
                View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_feed_quiz, viewGroup, false);
                return new QuizViewHolder(itemView, mRecyclerView);
            }

            @NonNull
            @Override
            public NBTQuiz getItem(int position){
                return super.getItem(position);
            }
        };

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mQuizFeedFirestoreAdapter);
        mQuizFeedFirestoreAdapter.notifyDataSetChanged();
        mRecyclerView.setHasFixedSize(true);
        hidePDialog();
    }

    private void launchActivity(Intent intent){
        intent.putExtra(USER_STATE_TAG, mUserState);
        finish();
        overridePendingTransition( 0, 0);
        startActivity(intent);
        overridePendingTransition( 0, 0);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.d(TAG, "create options");
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_top_toolbar, menu);

        MenuItem messageIcon = menu.findItem(R.id.action_message);
        messageIcon.setIcon(mUnreadBoolean?R.drawable.chat_notification:R.drawable.chat_white);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // navigate up on back button click
        if (!CustomUtils.menuOnClick(item, mUnreadBoolean, mUserState, this)){
            onBackPressed();
        }

        return true;
    }

    @Override
    public void onResume(){
        super.onResume();
        if (null != mQuizFeedFirestoreAdapter)
            mQuizFeedFirestoreAdapter.startListening();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mFirebaseUtil.removeUserListener();
        mQuizFeedFirestoreAdapter.stopListening();
    }
}
