package com.zeldav1;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Button;

import com.utils.services.FirebaseStorageService;
import com.zelda.object.TouchImageView;

import static com.utils.Constants.FILE_TYPE_JPG;
import static com.utils.Constants.IMAGE_ID_TAG;

public class FullScreenImageActivity extends AppCompatActivity {
    private static final String TAG = FullScreenImageActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fullscreen_image);

        String imageId = getIntent().getStringExtra(IMAGE_ID_TAG);
        Log.d(TAG, "Fullscreen activity for: " + imageId);

        TouchImageView touchImageView = findViewById(R.id.fullScreenTouchImageView);
        Button closeButton = findViewById(R.id.closeFullScreenButton);

        if (imageId.contains(".")) {
            FirebaseStorageService.setGlideImage(touchImageView, imageId);
        }
        else
            FirebaseStorageService.setGlideImageJPG(touchImageView, imageId);

        closeButton.setOnClickListener(v -> this.finish());
    }
}
