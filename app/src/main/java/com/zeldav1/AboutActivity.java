package com.zeldav1;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.utils.CustomUtils;
import com.utils.services.FirebaseFirestoreService;
import com.zelda.object.About;
import com.zelda.object.UserState;

import static com.utils.Constants.DISPLAY_TEXT_ABOUT;
import static com.utils.Constants.DISPLAY_TEXT_COLLECTION;
import static com.utils.Constants.TERMS_CONDITIONS_TAG;
import static com.utils.Constants.UNREAD_MESSAGE_TAG;
import static com.utils.Constants.USER_STATE_TAG;

public class AboutActivity extends BaseActivity{
    private static final String TAG = AboutActivity.class.getSimpleName();
    private final Context mContext = this;
    private UserState mUserState;
    private FirebaseFirestoreService mFirebaseUtil;

    private About aboutObject;
    private Boolean mUnreadBoolean, mTC;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mUserState = (UserState)getIntent().getSerializableExtra(USER_STATE_TAG);
        mUnreadBoolean = getIntent().getBooleanExtra(UNREAD_MESSAGE_TAG, false);
        mTC = getIntent().getBooleanExtra(TERMS_CONDITIONS_TAG, false);
        Log.d(TAG, "AboutActivity with User: " + mUserState + "\nmTC: " + mTC);

        setContentView(R.layout.activity_about);
        setUpLinks();

        mFirebaseUtil = new FirebaseFirestoreService();

        findAboutObject();

    }

    private void findAboutObject() {
        startPDialog(mContext);
        mFirebaseUtil.fetchDocument(value -> {
            if (null != value){
                aboutObject = value.toObject(About.class);
            }
            else{
                Log.d(TAG, "ABOUT not found.");
            }
            displayAboutScreen();
        }, DISPLAY_TEXT_COLLECTION, DISPLAY_TEXT_ABOUT);
    }

    private void setUpLinks() {
        Toolbar mToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void displayAboutScreen(){
        ((TextView)findViewById(R.id.about_text_header)).setText(R.string.about_text_header);

        if (null != aboutObject && null != aboutObject.getAboutText()) {
            Log.d(TAG, "ABOUT found: " + aboutObject.getAboutText());
            ((TextView) findViewById(R.id.about_text)).setText(CustomUtils.fromHtml(aboutObject.getAboutText()));
        }
        else
            ((TextView)findViewById(R.id.about_text)).setText(R.string.about_text);

        if (mTC)
            displayTermsConditions();

        hidePDialog();
    }

    private void displayPrivacyPolicy() {
        Log.d(TAG, "Setting up PrivacyPolicyView.");
        findViewById(R.id.about_scrollview).scrollTo(0, 0);
        ((TextView)findViewById(R.id.about_text_header)).setText(R.string.privacy_policy_header);

        if (null != aboutObject && null != aboutObject.getPrivacyPolicyText()) {
            Log.d(TAG, "getPrivacyPolicyText found: " + aboutObject.getPrivacyPolicyText());
            ((TextView)findViewById(R.id.about_text)).setText(CustomUtils.fromHtml(aboutObject.getPrivacyPolicyText()));

        }else
            ((TextView)findViewById(R.id.about_text)).setText(R.string.privacy_policy_text);

    }

    private void displayTermsConditions() {
        Log.d(TAG, "Setting up Terms and Conditions View");
        findViewById(R.id.about_scrollview).scrollTo(0, 0);
        ((TextView)findViewById(R.id.about_text_header)).setText(R.string.terms_and_conditions_header);

        if (null != aboutObject && null != aboutObject.getTermsConditionsText()){
            Log.d(TAG, "getTermsConditionsText found: " + aboutObject.getTermsConditionsText());
            ((TextView)findViewById(R.id.about_text)).setText(CustomUtils.fromHtml(aboutObject.getTermsConditionsText()));
        }
        else
            ((TextView)findViewById(R.id.about_text)).setText(R.string.terms_and_conditions_text);
    }

    public void onPrivacyPolicyClick(View view) {
        displayPrivacyPolicy();
    }

    public void onTermsConditionsClick(View view) {
        displayTermsConditions();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.d(TAG, "Create options");
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_about_activity, menu);
        MenuItem messageIcon = menu.findItem(R.id.action_message);
        messageIcon.setIcon(mUnreadBoolean?R.drawable.chat_notification:R.drawable.chat_white);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // navigate up on back button click
        if (item.getItemId() == R.id.action_faq){
            finish();
        }

        if (!CustomUtils.menuOnClick(item, mUnreadBoolean, mUserState, this)){
            onBackPressed();
        }

        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
    }
}