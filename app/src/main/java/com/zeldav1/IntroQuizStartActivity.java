package com.zeldav1;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;


import static com.utils.Constants.UNREAD_MESSAGE_TAG;
import static com.utils.Constants.USER_FIELD_PREFERENCE_FIRESTORE;
import static com.utils.Constants.USER_STATE_TAG;

public class IntroQuizStartActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro_quiz_start);

        Button startButton = findViewById(R.id.intro_quiz_start_button);
        startButton.setOnClickListener(v -> {
            Intent intent = new Intent(IntroQuizStartActivity.this, IntroQuizActivity.class);
            intent.putExtras(getIntent());
            startActivity(intent);
            finish();
        });
    }
}
