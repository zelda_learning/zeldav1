package com.zeldav1;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Spinner;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.Query;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.utils.CustomUtils;
import com.utils.FirebaseAnalyticsHelper;
import com.utils.adapters.HintSpinnerAdapter;
import com.utils.services.FirebaseFirestoreService;
import com.utils.viewholders.UniversityViewHolder;
import com.zelda.object.University;
import com.zelda.object.UserState;

import java.util.ArrayList;
import java.util.Arrays;

import static com.utils.Constants.SELECT_UNIVERSITY_METRIC;
import static com.utils.Constants.UNIVERSITIES;
import static com.utils.Constants.CLOSING_DATE;
import static com.utils.Constants.UNIVERSITY_ID_TAG;
import static com.utils.Constants.UNIVERSITY_TYPE_ALL;
import static com.utils.Constants.UNIVERSITY_TYPE_PRIVATE;
import static com.utils.Constants.UNIVERSITY_TYPE_PROMPT;
import static com.utils.Constants.UNIVERSITY_TYPE_PUBLIC;
import static com.utils.Constants.UNIVERSITY_TYPE_T_VET;
import static com.utils.Constants.UNIVERSITY_TYPE_VOCATIONAL;
import static com.utils.Constants.UNREAD_MESSAGE_TAG;
import static com.utils.Constants.USER_STATE_TAG;

public class UniversityFeedActivity extends BaseActivity{
    private static final String TAG = UniversityFeedActivity.class.getSimpleName();
    private Context mContext = this;
    private FirebaseAnalyticsHelper mFirebaseAnalyticsHelper;

    private BottomNavigationViewEx mBottomNavigationBar;

    private RecyclerView mUniversitiesRecyclerView;
    private UserState mUserState;
    private Boolean mUnreadBoolean;

    private FirestoreRecyclerAdapter<University, RecyclerView.ViewHolder> mUniversityFeedAdapter;

    private FirebaseFirestoreService mFirebaseUtil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_universities_feed);

        mUserState = (UserState) getIntent().getSerializableExtra(USER_STATE_TAG);
        mUnreadBoolean = getIntent().getBooleanExtra(UNREAD_MESSAGE_TAG, false);
        mFirebaseAnalyticsHelper = new FirebaseAnalyticsHelper(this);
        mFirebaseUtil = new FirebaseFirestoreService();
        Log.d(TAG, "UniversityFeed with User: " + mUserState);

        setUpLinks();

    }

    private void setUpLinks() {
        Log.d(TAG, "Set up links");

        Toolbar mToolbar = findViewById(R.id.toolbar_with_spinner);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        mBottomNavigationBar = findViewById(R.id.bottom_nav_bar);
        mBottomNavigationBar.setSelectedItemId(R.id.action_university);
        mBottomNavigationBar.enableAnimation(false);
        mBottomNavigationBar.enableItemShiftingMode(false);
        mBottomNavigationBar.enableShiftingMode(false);

        mBottomNavigationBar.setSelectedItemId(R.id.action_university);
        mBottomNavigationBar.setOnNavigationItemSelectedListener(item -> {
            Class startClass = null;
            switch (item.getItemId()) {
                case R.id.action_bursary:
                    startClass = BursaryFeedActivity.class;
                    break;

                case R.id.action_home:
                    startClass = HomeActivity.class;
                    break;

                case R.id.action_tips:
                    startClass = TipFeedActivity.class;
                    break;

                case R.id.action_university:
                    break;

                case R.id.action_quiz:
                    startClass = QuizFeedActivity.class;
                    break;

            }
            if (null != startClass) {
                Intent intent = new Intent(UniversityFeedActivity.this, startClass);
                intent.putExtra(USER_STATE_TAG, mUserState);
                intent.putExtra(UNREAD_MESSAGE_TAG, mUnreadBoolean);
                startActivity(intent);
                finish();
            }
            return false;
        });

        mUniversitiesRecyclerView = findViewById(R.id.universities_recycler_view);

        final GestureDetector mGestureDetector = new GestureDetector(mContext, new GestureDetector.SimpleOnGestureListener(){
            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                return true;
            }
        });

        mUniversitiesRecyclerView.addOnItemTouchListener(
                new RecyclerView.OnItemTouchListener() {

                    @Override
                    public void onTouchEvent(RecyclerView rv, MotionEvent e) {

                    }

                    @Override
                    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

                    }

                    @Override
                    public boolean onInterceptTouchEvent(RecyclerView recyclerView, MotionEvent motionEvent) {
                        View child = recyclerView.findChildViewUnder(motionEvent.getX(), motionEvent.getY());

                        if (child != null && mGestureDetector.onTouchEvent(motionEvent)) {
                            University university = mUniversityFeedAdapter.getItem(recyclerView.getChildLayoutPosition(child));
                            Log.d(TAG, "touch intercept University Feed:" + university);
                            mFirebaseAnalyticsHelper.addFirebaseEventMetric(university.getId(), university.getTitle(), SELECT_UNIVERSITY_METRIC);

                            Intent intent = new Intent(UniversityFeedActivity.this, UniversityViewActivity.class);
                            intent.putExtra(UNIVERSITY_ID_TAG, university);
                            intent.putExtra(USER_STATE_TAG, mUserState);
                            intent.putExtra(UNREAD_MESSAGE_TAG, mUnreadBoolean);
                            startActivity(intent);
                        }
                        return false;
                    }
                });

    }

    private void filterUniversitiesOnType(String type) {
        Log.d(TAG, "filterUniversitiesOnType: " + type);
        Query query;

        if(type.equals(UNIVERSITY_TYPE_PROMPT) || type.equals(UNIVERSITY_TYPE_ALL)){
            query = mFirebaseUtil.getFirestore()
                    .collection(UNIVERSITIES)
                    .orderBy(CLOSING_DATE)
                    .limit(50);

        } else {
            query = mFirebaseUtil.getFirestore()
                    .collection(UNIVERSITIES)
                    .whereEqualTo("type", type)
                    .orderBy(CLOSING_DATE)
                    .limit(50);
        }
        updateFeedAdapter(query);
    }

    private void updateFeedAdapter(Query query){
        Log.d(TAG, "Fetching University objects from Firestore");

        FirestoreRecyclerOptions<University> options = new FirestoreRecyclerOptions.Builder<University>()
                .setQuery(query, University.class)
                .build();

        if(null != mUniversityFeedAdapter){
            mUniversityFeedAdapter.stopListening();
        }

        mUniversityFeedAdapter = new FirestoreRecyclerAdapter<University, RecyclerView.ViewHolder>(options) {
            @Override
            protected void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position, @NonNull University university) {
                ((UniversityViewHolder)holder).bindData(university);
            }

            @NonNull
            @Override
            public UniversityViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
                View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_feed_university, viewGroup, false);
                return new UniversityViewHolder(itemView);
            }

            @NonNull
            @Override
            public University getItem(int position){
                return super.getItem(position);
            }
        };

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        mUniversitiesRecyclerView.setLayoutManager(mLayoutManager);
        mUniversitiesRecyclerView.setAdapter(mUniversityFeedAdapter);
        mUniversityFeedAdapter.notifyDataSetChanged();
        mUniversitiesRecyclerView.setHasFixedSize(true);
        mUniversityFeedAdapter.startListening();
        hidePDialog();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.d(TAG, "create options");

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_top_toolbar, menu);

        MenuItem messageIcon = menu.findItem(R.id.action_message);
        messageIcon.setIcon(mUnreadBoolean?R.drawable.chat_notification:R.drawable.chat_white);

        ArrayList<String> items = new ArrayList<>();
        items.add(UNIVERSITY_TYPE_ALL);
        items.add(UNIVERSITY_TYPE_PUBLIC);
        items.add(UNIVERSITY_TYPE_PRIVATE);
        items.add(UNIVERSITY_TYPE_T_VET);
        items.add(UNIVERSITY_TYPE_VOCATIONAL);

        MaterialSpinner spinner = findViewById(R.id.toolbar_spinner);
        spinner.setItems(items);
        filterUniversitiesOnType(UNIVERSITY_TYPE_ALL);

        spinner.setOnItemSelectedListener((view, position, id, item) -> {
            String[] universityTypeSelectArray = getResources().getStringArray(R.array.university_type_select);
            Log.d(TAG, "field select pos: " + Arrays.toString(universityTypeSelectArray) + " pos" + position);
            filterUniversitiesOnType(universityTypeSelectArray[position]);
        });

        spinner.setOnNothingSelectedListener(spinnerL -> Log.d(TAG, "Nothing selected"));
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // navigate up on back button click
        if (!CustomUtils.menuOnClick(item, mUnreadBoolean, mUserState, this)){
            onBackPressed();
        }

        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void onResume(){
        super.onResume();
    }

    @Override
    public void onStart(){
        super.onStart();
    }

    @Override
    protected void onPause(){
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (null != mUniversityFeedAdapter)
            mUniversityFeedAdapter.stopListening();
        hidePDialog();
    }
}