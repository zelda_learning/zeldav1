package com.zeldav1;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.Timestamp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.storage.UploadTask;
import com.utils.CustomUtils;
import com.utils.FirebaseAnalyticsHelper;
import com.utils.UpdateUploadViewHolderListener;
import com.utils.adapters.ViewPagerAdapter;
import com.utils.dialogFragments.BackSaveDialogFragment;
import com.utils.dialogFragments.CBASubmitDialogFragment;
import com.utils.fragments.CBAFormUniversityFragment;
import com.utils.fragments.UserViewPersonalFragment;
import com.utils.fragments.UserViewSchoolFragment;
import com.utils.services.FirebaseFirestoreService;
import com.utils.services.FirebaseStorageService;
import com.zelda.object.UserProfilePersonal;
import com.zelda.object.UserProfileSchool;
import com.zelda.object.UserState;

import java.util.HashMap;
import java.util.Objects;
import java.util.UUID;

import leo.me.la.labeledprogressbarlib.LabeledProgressBar;

import static com.utils.Constants.ANALYTICS_CBA_PROGRESS;
import static com.utils.Constants.ANALYTICS_DELETE_CBA;
import static com.utils.Constants.ANALYTICS_EVENT_OPT_OUT;
import static com.utils.Constants.ANALYTICS_EVENT_OPT_OUT_DELETE;
import static com.utils.Constants.ANALYTICS_EVENT_RESUBMIT_CBA;
import static com.utils.Constants.ANALYTICS_EVENT_SUBMIT_CBA;
import static com.utils.Constants.ANALYTICS_EVENT_UPDATE_CBA;
import static com.utils.Constants.ANALYTICS_NO;
import static com.utils.Constants.ANALYTICS_OPT_IN_CBA;
import static com.utils.Constants.ANALYTICS_OPT_OUT_CBA;
import static com.utils.Constants.ANALYTICS_RESUBMIT_CBA;
import static com.utils.Constants.ANALYTICS_SUBMIT_CBA;
import static com.utils.Constants.ANALYTICS_UPDATE_CBA;
import static com.utils.Constants.ANALYTICS_YES;
import static com.utils.Constants.APPLICATION_FORM;
import static com.utils.Constants.BURSARY_ID_TAG;
import static com.utils.Constants.CBA_ID_APPLICATION_FORM;
import static com.utils.Constants.ELEMENT_UPLOAD;
import static com.utils.Constants.FALSE;
import static com.utils.Constants.FINAL_PROGRESS;
import static com.utils.Constants.FIRESTORE_DOCUMENTS_ID_COPY;
import static com.utils.Constants.FIRESTORE_DOCUMENTS_MATRIC_CERTIFICATE;
import static com.utils.Constants.FIRESTORE_DOCUMENTS_SCHOOL_REPORT;
import static com.utils.Constants.FIRESTORE_DOCUMENTS_UNIVERSITY_TRANSCRIPT;
import static com.utils.Constants.FIRESTORE_ID_COPY_UPLOADED;
import static com.utils.Constants.FIRESTORE_MC_UPLOADED;
import static com.utils.Constants.FIRESTORE_SCHOOL_REPORT_UPLOADED;
import static com.utils.Constants.FIRESTORE_UNIVERSITY_TRANSCRIPT_UPLOADED;
import static com.utils.Constants.FROM_BURSARY_TAG;
import static com.utils.Constants.FROM_CBA_TAG;
import static com.utils.Constants.INITIAL_PROGRESS;
import static com.utils.Constants.UNREAD_MESSAGE_TAG;
import static com.utils.Constants.UPP_EMAIL_PROGRESS;
import static com.utils.Constants.UPP_SIZE;
import static com.utils.Constants.UPS_CURRENT_GRADE_MATRICULATED;
import static com.utils.Constants.UPS_CURRENT_SIZE;
import static com.utils.Constants.UPS_MATRICULATED_SIZE;
import static com.utils.Constants.UPU_CURRENT_SIZE;
import static com.utils.Constants.UPU_PROSPECTIVE;
import static com.utils.Constants.UPU_PROSPECTIVE_SIZE;
import static com.utils.Constants.USER_OPT_OUT;
import static com.utils.Constants.USER_OPT_OUT_DELETE;
import static com.utils.Constants.USER_PROFILE_DOCUMENTS_UPLOAD_TAG;
import static com.utils.Constants.USER_PROFILE_PERSONAL;
import static com.utils.Constants.USER_PROFILE_SCHOOL;
import static com.utils.Constants.USER_STATE_FIRESTORE;
import static com.utils.Constants.USER_STATE_TAG;

public class UserViewActivity extends BaseActivity implements
        BackSaveDialogFragment.ApplicationBackSaveDialogFragmentListener,
        CBASubmitDialogFragment.CBASubmitListener
{
    private static final String TAG = UserViewActivity.class.getSimpleName();
    private static final int REQUEST_CAPTURE_IMAGE = 100;
    private final int PICK_IMAGE_REQUEST = 71;
    private final int PICK_FILE_REQUEST = 74;

    private final int PERSONAL_FRAGMENT_POS = 0;
    private final int SCHOOL_FRAGMENT_POS = 1;
    private final int UNIVERSITY_FRAGMENT_POS = 2;

    private final int PICK_ID_COPY = 75;
    private final int PICK_MATRIC_CERTIFICATE = 76;
    private final int PICK_SCHOOL_REPORT = 77;
    private final int PICK_UNIVERSITY_TRANSCRIPT = 78;

    private final int PERMISSIONS_REQUEST_READ = 55;
    private final int PERMISSIONS_REQUEST_WRITE = 56;

    private final Context mContext = this;

    private FirebaseUser mCurrentUser;
    private UserState mUserState, mSavedUserState;
    private UserProfilePersonal mUserProfilePersonal, mSavedUserProfilePersonal;
    private UserProfileSchool mUserProfileSchool, mSavedUserProfileSchool;
    private Boolean mUnreadBoolean, mFromBursary;

    private boolean schoolMatriculated = false, universityCurrent = false;

    private boolean personalLoaded = false, schoolLoaded = false, stateLoaded = false;
    private boolean personalChangesMade =  false, schoolChangesMade =  false, stateChangesMade =  false;

    private ViewPager mViewPager;
    private ViewPagerAdapter mViewPagerAdapter;
    private FirebaseFirestoreService mFirebaseUtil;
    private FirebaseStorageService mFirebaseStorageUtil;
    private FirebaseAnalyticsHelper mFirebaseAnalyticsHelper;

    Uri idImageFilePath, reportImageFilePath;

    private String requestedFileType;

    private TextView mCBATitleTextView;
    private LabeledProgressBar mUserStateProgress;
    private Button mSaveChangesButton, mSubmitButton;
    private Boolean mSaveClicked = false;

    private String mSubmitCache;
    private HashMap<String, Double> subjectHashmap;

    private void findUserState() {
        if (null != mCurrentUser) {
            mFirebaseUtil.assignUserListener(value -> {
                if (null != value) {
                    mUserState = value.toObject(UserState.class);
                    mUserState.setId(mCurrentUser.getUid());
                    mSavedUserState = new UserState(mUserState);

                    if (null != mUserState && mUserState.isOptedIn()) {
                        Log.d(TAG, "USer state found");
                        stateLoaded = true;

                        updateLoadingScreen();
                    }
                } else {
                    Log.d(TAG, "No such User, should check firebase");
                }
            }, mCurrentUser.getUid());
        }
    }

    private void findUserProfilePersonal() {
        if (null != mCurrentUser) {
            Log.d(TAG, "findUserProfilePersonal");
            mFirebaseUtil.fetchDocument(value -> {
                if (null != value) {
                    mUserProfilePersonal = value.toObject(UserProfilePersonal.class);
                } else {
                    mUserProfilePersonal = new UserProfilePersonal(mCurrentUser.getUid());
                }
                if (null != mUserProfilePersonal) {
                    mSavedUserProfilePersonal = new UserProfilePersonal(mUserProfilePersonal);
                    personalLoaded = true;
                    updateLoadingScreen();
                }
            }, USER_PROFILE_PERSONAL, mCurrentUser.getUid());
        }
    }

    private void findUserProfileSchool(){
        if (null != mCurrentUser) {
            Log.d(TAG, "findUserProfileSchool");
            mFirebaseUtil.fetchDocument(value -> {
                if (null != value) {
                    mUserProfileSchool = value.toObject(UserProfileSchool.class);
                } else {
                    Log.d(TAG, "No UPS found. Creating new.");
                    mUserProfileSchool = new UserProfileSchool(mCurrentUser.getUid());
                }
                if (null != mUserProfileSchool) {
                    mSavedUserProfileSchool = new UserProfileSchool(mUserProfileSchool);
                    schoolLoaded = true;
                    setTypeSchoolStudent();
                    setTypeUniStudent();
                    updateLoadingScreen();

                }
            }, USER_PROFILE_SCHOOL, mCurrentUser.getUid());
        }
    }

    private void setTypeUniStudent(){
        if (null != mUserProfileSchool && null != mUserProfileSchool.getUniversityYear()){
            universityCurrent = !mUserProfileSchool.getUniversityYear().equals(UPU_PROSPECTIVE);
        }
        Log.d(TAG, "universityCurrent: " + universityCurrent);
    }

    private void setTypeSchoolStudent(){
        if (null != mUserProfileSchool && null != mUserProfileSchool.getCurrentGrade()){
            schoolMatriculated = mUserProfileSchool.getCurrentGrade().equals(UPS_CURRENT_GRADE_MATRICULATED);
        }
        Log.d(TAG, "schoolMatriculated: " + schoolMatriculated);
    }

    private void optOutAndDelete(){

        if (null != mCurrentUser) {
            Log.d(TAG, "applicationForm to delete: " + mUserState.getOptIn());
            mFirebaseUtil.updateFirestoreDocumentField(APPLICATION_FORM, mUserState.getOptIn(), USER_OPT_OUT_DELETE, true);

            mUserState.setOptIn(FALSE);
            mUserState.setProgress(INITIAL_PROGRESS);
            mUserState.setSubmitted(false);

            logOptOutDeleteMetric();

            mFirebaseUtil.setFirestoreDocumentMergeUpdate(value -> {
                if (value) {
                    Log.d(TAG, "optOut deleted successfully: " + mUserState.getOptIn());
                    Toast.makeText(mContext, R.string.cba_opt_out_delete_toast, Toast.LENGTH_LONG).show();
                } else {
                    Log.d(TAG, "optOut delete failed");
                }
            }, USER_STATE_FIRESTORE, mCurrentUser.getUid(), mUserState);

            finish();
        }
    }

    private void optOut(){

        if (null != mCurrentUser) {
            Log.d(TAG, "applicationForm to opt out of: " + mUserState.getOptIn());
            mFirebaseUtil.updateFirestoreDocumentField(APPLICATION_FORM, mUserState.getOptIn(), USER_OPT_OUT, true);

            mUserState.setOptIn(FALSE);
            mUserState.setProgress(INITIAL_PROGRESS);
            mUserState.setSubmitted(false);

            logOptOutMetric();

            mFirebaseUtil.setFirestoreDocumentMergeUpdate(value -> {
                if (value) {
                    Log.d(TAG, "optOut deleted successfully: " + mUserState.getOptIn());
                    Toast.makeText(mContext, R.string.cba_opt_out_toast, Toast.LENGTH_LONG).show();
                } else {
                    Log.d(TAG, "optOut delete failed");
                }
            }, USER_STATE_FIRESTORE, mCurrentUser.getUid(), mUserState);

            finish();
        }
    }

    private void calculateProgress(){
        if (null != mCurrentUser && null != mUserState && mUserState.isOptedIn()) {

            Log.d(TAG, "US isOptedIn: " + mUserState.isOptedIn());
            int progress = 0, upsProgress = 0, uppProgress = 0, emailProgress = 0;
            int total = 0;

            setTypeSchoolStudent();
            setTypeUniStudent();

            UserViewSchoolFragment userViewSchoolFragment = null;
            CBAFormUniversityFragment cbaFormUniversityFragment = null;
            userViewSchoolFragment = (UserViewSchoolFragment) getCBAFragmentByPosition(SCHOOL_FRAGMENT_POS);
            cbaFormUniversityFragment = (CBAFormUniversityFragment) getCBAFragmentByPosition(UNIVERSITY_FRAGMENT_POS);

            if (null != userViewSchoolFragment){
                Log.d(TAG, "userViewSchoolFragment fragment found");
                userViewSchoolFragment.captureSubjectsMarks();
            }
            if (null != cbaFormUniversityFragment){
                Log.d(TAG, "cbaFormUniversityFragment fragment found");
                cbaFormUniversityFragment.captureSubjectsMarks();
            }

            if (null != mUserProfileSchool)
                upsProgress = mUserProfileSchool.getNumberFields();

            if (null != mUserProfilePersonal)
                uppProgress = mUserProfilePersonal.getNumberFields();

            if (null != mCurrentUser.getEmail()){
                emailProgress++;
            }

            if (mCurrentUser.isEmailVerified()) {
                emailProgress++;
            }

            progress += emailProgress;
            progress += upsProgress;
            progress += uppProgress;

            Log.d(TAG, "schoolMatriculated: " + schoolMatriculated);
            Log.d(TAG, "universityCurrent: " + universityCurrent);

            Log.d(TAG, "Email fields: " + emailProgress + "/" + UPP_EMAIL_PROGRESS);
            Log.d(TAG, "UPP fields: " + uppProgress + "/" + UPP_SIZE);

            //Current School student, and current university student - shouldn't be possible
            if (!schoolMatriculated && universityCurrent){
                Log.d(TAG, "Current School student, and current university student - shouldn't be possible");
                Log.d(TAG, "UPS fields: " + upsProgress + "/(" + UPS_CURRENT_SIZE + "+" + UPU_CURRENT_SIZE +")");
                total = UPP_EMAIL_PROGRESS + UPP_SIZE + UPS_CURRENT_SIZE + UPU_CURRENT_SIZE;
            }

            //Current School student, and prospective university student
            if (!schoolMatriculated && !universityCurrent){
                Log.d(TAG, "Current School student, and prospective university student");
                Log.d(TAG, "UPS fields: " + upsProgress + "/(" + UPS_CURRENT_SIZE + "+" + UPU_PROSPECTIVE_SIZE +")");
                total = UPP_EMAIL_PROGRESS + UPP_SIZE + UPS_CURRENT_SIZE + UPU_PROSPECTIVE_SIZE;
            }

            //Matriculated School student, and prospective university student
            if (schoolMatriculated && !universityCurrent){
                Log.d(TAG, "Matriculated School student, and prospective university student");
                Log.d(TAG, "UPS fields: " + upsProgress + "/(" + UPS_MATRICULATED_SIZE + "+" + UPU_PROSPECTIVE_SIZE +")");
                total = UPP_EMAIL_PROGRESS + UPP_SIZE + UPS_MATRICULATED_SIZE + UPU_PROSPECTIVE_SIZE;
            }

            //Matriculated School student, and current university student
            if (schoolMatriculated && universityCurrent){
                Log.d(TAG, "Matriculated School student, and current university student");
                Log.d(TAG, "UPS fields: " + upsProgress + "/(" + UPS_MATRICULATED_SIZE + "+" + UPU_CURRENT_SIZE +")");
                total = UPP_EMAIL_PROGRESS + UPP_SIZE + UPS_MATRICULATED_SIZE + UPU_CURRENT_SIZE;
            }

            Log.d(TAG, "Total Progress: " + progress + "/" + total);
            Log.d(TAG, "Progress %: " + progress*100/total);
            mUserState.setProgress(progress*100/total);
            mUserStateProgress.setValue(mUserState.getProgress());
            mUserStateProgress.setLabelText(mContext.getResources().getString(R.string.cba_progress_label, mUserState.getProgress()));

        }
    }

    private void onBackShowSaveDialog() {
        DialogFragment dialog = new BackSaveDialogFragment();
        dialog.show(getSupportFragmentManager(), "BackSaveDialogFragment");
    }

    private void checkChanges(){
        if (null != mUserProfilePersonal && !mUserProfilePersonal.equalsHash(mSavedUserProfilePersonal)) {
            Log.d(TAG, "mUserProfilePersonal changed: ");
            personalChangesMade = true;
        }
        else
            personalChangesMade = false;

        if (null != mUserProfileSchool && !mUserProfileSchool.equalsHash(mSavedUserProfileSchool)) {
            Log.d(TAG, "mUserProfileSchool changed: ");
            schoolChangesMade = true;
        }
        else
            schoolChangesMade = false;

        if (null != mUserState && !mUserState.equals(mSavedUserState)) {
            Log.d(TAG, "mUserState changed: ");
            stateChangesMade = true;
        }
        else
            stateChangesMade = false;
    }

    public void updateFirestoreUserObjects(){

        if (null != mCurrentUser && personalChangesMade || schoolChangesMade || stateChangesMade) {
            mSaveChangesButton.setText(R.string.updating_changes_text);

            FirebaseFirestoreService.BooleanCallback booleanCallback = value -> {
                if (value) {
                    Log.d(TAG, "Update completed successfully ");
                    Toast.makeText(mContext, mContext.getResources().getString(R.string.user_view_save_changes_success), Toast.LENGTH_SHORT).show();
                    logCBAUpdateMetric();
                } else {
                    Log.d(TAG, "Update failed");
                    Toast.makeText(mContext, R.string.user_view_save_changes_fail, Toast.LENGTH_LONG).show();
                }

                Log.d(TAG, "update complete, update analytics");
                mSaveChangesButton.setText(R.string.user_view_save_changes);
                updateLoadingScreen();
            };

            if (personalChangesMade) {
                mFirebaseUtil.setFirestoreDocument(booleanCallback, mContext, USER_PROFILE_PERSONAL, mCurrentUser.getUid(), mUserProfilePersonal.getUserProfilePersonalWrite());
                mSavedUserProfilePersonal = new UserProfilePersonal(mUserProfilePersonal);
                Log.d(TAG, "personalChangesMade");
                personalChangesMade = false;
            }
            if (schoolChangesMade) {
                mFirebaseUtil.setFirestoreDocument(booleanCallback, mContext, USER_PROFILE_SCHOOL, mCurrentUser.getUid(), mUserProfileSchool.getUserProfileSchoolWrite());
                mSavedUserProfileSchool = new UserProfileSchool(mUserProfileSchool);
                Log.d(TAG, "schoolChangesMade");
                schoolChangesMade = false;
            }
            if (stateChangesMade) {
                mUserState.setCbaEditTimeStamp(new Timestamp(CustomUtils.getDate()));
                mFirebaseUtil.setFirestoreDocument(booleanCallback, mContext, USER_STATE_FIRESTORE, mCurrentUser.getUid(), mUserState);
                mSavedUserState = new UserState(mUserState);
                Log.d(TAG, "stateChangesMade");
                stateChangesMade = false;
            }

        }
        else {
            if (mSaveClicked) {
                Toast.makeText(mContext, getResources().getString(R.string.user_view_save_changes_none), Toast.LENGTH_SHORT).show();
            }
            mSaveChangesButton.setText(R.string.user_view_save_changes);
        }
    }

    private void setUpLinks(){
        Log.d(TAG, "Set up links");
        Toolbar mToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        mViewPager = findViewById(R.id.cba_view_pager);
        mViewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager(), true);

        TabLayout tabLayout = findViewById(R.id.user_view_tab_layout);
        tabLayout.setupWithViewPager(mViewPager);

        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));

        mViewPager.setOffscreenPageLimit(2);
        mViewPager.setAdapter(mViewPagerAdapter);

        mCBATitleTextView = findViewById(R.id.cba_title_textview);
        mUserStateProgress = findViewById(R.id.cba_progress_bar);
        mSaveChangesButton = findViewById(R.id.cba_save_changes);
        mSubmitButton = findViewById(R.id.cba_submit_button);

        View rootView = getWindow().getDecorView().getRootView();
        rootView.getViewTreeObserver().addOnGlobalLayoutListener(() -> {

            Rect r = new Rect();
            rootView.getWindowVisibleDisplayFrame(r);
            int screenHeight = rootView.getRootView().getHeight();

            // r.bottom is the position above soft keypad or device button.
            // if keypad is shown, the r.bottom is smaller than that before.
            int keypadHeight = screenHeight - (r.bottom - r.top);

            if (keypadHeight > (mContext.getResources().getDimension(R.dimen._100sdp))) {
                // keyboard is opened
                mSubmitButton.setVisibility(View.GONE);
                mSaveChangesButton.setVisibility(View.GONE);
                mSaveClicked = false;
            }
            else if (!mSaveClicked){
                // keyboard is closed
                if (mSaveChangesButton.getVisibility() == View.GONE)
                    mSaveChangesButton.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.fade_in));

                mSaveChangesButton.setVisibility(View.VISIBLE);

                if(mUserState.getProgress() == FINAL_PROGRESS){
                    if (mSubmitButton.getVisibility() == View.GONE)
                        mSubmitButton.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.fade_in));
                    mSubmitButton.setVisibility(View.VISIBLE);
                }
            }
        });

        findUserState();
        findUserProfilePersonal();
        findUserProfileSchool();

    }

    public void submit(View view){
        Log.d(TAG, "Submit user: " + mUserState.getOptIn());

        calculateProgress();
        checkChanges();

        DialogFragment dialog = CBASubmitDialogFragment.newInstance(mUserState.getSubmitted());
        dialog.show(getSupportFragmentManager(), "CBASubmitDialogFragment");

    }

    @Override
    public void onSubmitPositiveClick(DialogFragment dialog) {

        Log.d(TAG, "getSubmitted user: " + mUserState.getSubmitted());

        Log.d(TAG, "CHANGES MADE, update values");

        calculateProgress();
        checkChanges();
        updateFirestoreUserObjects();
        startPDialog(mContext, mContext.getResources().getString(R.string.loading_submitting));
        mFirebaseUtil.submitCBAForm(value -> {
            if (value) {
                Log.d(TAG, "User submitted: " + mUserState.getSubmitted());
                findViewById(R.id.cba_submitted_textview).setVisibility(View.VISIBLE);
                Toast.makeText(mContext, R.string.cba_submitted_toast, Toast.LENGTH_LONG).show();
                logSubmitMetric();
            } else {
                Log.d(TAG, "Submit  failed");
                Toast.makeText(mContext, R.string.cba_submitted_failed_toast, Toast.LENGTH_LONG).show();
            }
            hidePDialog();
        }, APPLICATION_FORM, mUserState.getOptIn(), mCurrentUser.getUid());

    }

    @Override
    public void onResubmitPositiveClick(DialogFragment dialog) {

        Log.d(TAG, "CHANGES MADE, update values");

        calculateProgress();
        checkChanges();
        updateFirestoreUserObjects();
        startPDialog(mContext, mContext.getResources().getString(R.string.loading_resubmitting));
        mFirebaseUtil.resubmitCBAForm(value -> {
            if (value) {
                Log.d(TAG, "User resubmitted: " + mUserState.getSubmitted());
                findViewById(R.id.cba_submitted_textview).setVisibility(View.VISIBLE);

                ((TextView)findViewById(R.id.cba_submitted_textview)).setText(R.string.cba_resubmitted_text);
                Toast.makeText(mContext, R.string.cba_resubmitted_toast, Toast.LENGTH_LONG).show();
                logResubmitMetric();
            } else {
                Log.d(TAG, "resubmit  failed");
                Toast.makeText(mContext, R.string.cba_resubmitted_failed_toast, Toast.LENGTH_LONG).show();
            }
            hidePDialog();
        }, APPLICATION_FORM, mUserState.getOptIn());

    }

    @Override
    public void onSubmitNegativeClick(DialogFragment dialog) {
        Log.d(TAG, "onSubmitNegativeClick user: " + mUserState.getOptIn());
        dialog.dismiss();
    }

    @SuppressLint("NewApi")
    public void chooseFile(View view) {
        requestedFileType = view.getTag().toString();
        checkPermission(PERMISSIONS_REQUEST_READ);
        Log.d(TAG, "Choose file onclick: " + requestedFileType);
    }

    private void pickFile(){

        Log.d(TAG, "pickFile method");
        String[] mimeTypes = {"image/*", "application/pdf"};

        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            intent.setType(mimeTypes.length == 1 ? mimeTypes[0] : "*/*");
            if (mimeTypes.length > 0) {
                intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
            }
        } else {
            StringBuilder mimeTypesStr = new StringBuilder();
            for (String mimeType : mimeTypes) {
                mimeTypesStr.append(mimeType).append("|");
            }
            intent.setType(mimeTypesStr.substring(0, mimeTypesStr.length() - 1));
        }

        switch (requestedFileType){
            case FIRESTORE_ID_COPY_UPLOADED:
                startActivityForResult(Intent.createChooser(intent,"Select a file"), PICK_ID_COPY);
                break;

            case FIRESTORE_MC_UPLOADED:
                startActivityForResult(Intent.createChooser(intent,"Select a file"), PICK_MATRIC_CERTIFICATE);
                break;

            case FIRESTORE_SCHOOL_REPORT_UPLOADED:
                startActivityForResult(Intent.createChooser(intent,"Select a file"), PICK_SCHOOL_REPORT);
                break;

            case FIRESTORE_UNIVERSITY_TRANSCRIPT_UPLOADED:
                startActivityForResult(Intent.createChooser(intent,"Select a file"), PICK_UNIVERSITY_TRANSCRIPT);
                break;

            case ELEMENT_UPLOAD:
                Toast.makeText(mContext, R.string.item_input_upload_select_grade, Toast.LENGTH_SHORT).show();
                break;
        }

    }

    @SuppressLint("NewApi")
    public void downloadFile(View view) {
        if (null == mFirebaseStorageUtil){
            mFirebaseStorageUtil = new FirebaseStorageService(mContext);
        }
        requestedFileType = view.getTag().toString();
        checkPermission(PERMISSIONS_REQUEST_WRITE);
    }

    private void downloadFile(){

        switch (requestedFileType) {
            case FIRESTORE_ID_COPY_UPLOADED:
                Log.d(TAG, "Download ID  " + mUserProfilePersonal.getIdUploaded());
                mFirebaseStorageUtil.downloadFile((Uri uri) -> {
                    if (null != uri) {
                        Log.d(TAG, "Value: " + uri);
                        try {
                            openFile(uri);
                        } catch (ActivityNotFoundException e) {
                            // no Activity to handle this kind of files
                            Toast.makeText(mContext, R.string.file_not_found_open_exception_message, Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Log.d(TAG, "Value null: ");
                    }}, mUserProfilePersonal.getIdUploaded(), FIRESTORE_DOCUMENTS_ID_COPY);
                break;

            case FIRESTORE_MC_UPLOADED:
                Log.d(TAG, "Download MC  " + mUserProfileSchool.getMatricCertificateUploaded());
                mFirebaseStorageUtil.downloadFile(value -> {
                    if (null != value) {
                        Log.d(TAG, "Value: " + value);
                        try {
                            openFile(value);
                        } catch (ActivityNotFoundException e) {
                            // no Activity to handle this kind of files
                            Toast.makeText(mContext, R.string.file_not_found_open_exception_message, Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Log.d(TAG, "Value null: ");
                    }}, mUserProfileSchool.getMatricCertificateUploaded(), FIRESTORE_DOCUMENTS_MATRIC_CERTIFICATE);
                break;


            case FIRESTORE_SCHOOL_REPORT_UPLOADED:
                Log.d(TAG, "Download SR  " + mUserProfileSchool.getSchoolReportUploaded());
                mFirebaseStorageUtil.downloadFile(value -> {
                    if (null != value) {
                        Log.d(TAG, "Value: " + value);
                        try {
                            openFile(value);
                        } catch (ActivityNotFoundException e) {
                            // no Activity to handle this kind of files
                            Toast.makeText(mContext, R.string.file_not_found_open_exception_message, Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Log.d(TAG, "Value null: ");
                    }}, mUserProfileSchool.getSchoolReportUploaded(), FIRESTORE_DOCUMENTS_SCHOOL_REPORT);
                break;


            case FIRESTORE_UNIVERSITY_TRANSCRIPT_UPLOADED:
                Log.d(TAG, "Download UT  " + mUserProfileSchool.getUniversityTranscriptUploaded());
                mFirebaseStorageUtil.downloadFile(value -> {
                    if (null != value) {
                        Log.d(TAG, "Value: " + value);
                        try {
                            openFile(value);
                        } catch (ActivityNotFoundException e) {
                            // no Activity to handle this kind of files
                            Toast.makeText(mContext, R.string.file_not_found_open_exception_message, Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Log.d(TAG, "Value null: ");
                    }}, mUserProfileSchool.getUniversityTranscriptUploaded(), FIRESTORE_DOCUMENTS_UNIVERSITY_TRANSCRIPT);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK
                && data!= null && data.getData() != null) {
            if (null == mFirebaseStorageUtil){
                mFirebaseStorageUtil = new FirebaseStorageService(mContext);
            }

            idImageFilePath = data.getData();
            ContentResolver cR = mContext.getContentResolver();
            MimeTypeMap mime = MimeTypeMap.getSingleton();
            String type = mime.getExtensionFromMimeType(cR.getType(idImageFilePath));
            String randomUid = UUID.randomUUID().toString();
            Log.d(TAG, "PICK_FILE_REQUEST Id file filepth " +  idImageFilePath + "TYPE: " + type + "Element: " + data.getStringExtra(USER_PROFILE_DOCUMENTS_UPLOAD_TAG));
            Log.d(TAG, "Upload name: " + randomUid);

            UpdateUploadViewHolderListener uploadViewHolderListener = null;
            UploadTask uploadTask = null;

            switch (requestCode) {

                case PICK_ID_COPY:
                    Log.d(TAG, "Upload ID");

                    uploadViewHolderListener = new UpdateUploadViewHolderListener(){
                        @Override
                        public void updateViewHolder(double uploadProgress) {
                            UserViewPersonalFragment userViewPersonalFragment = null;
                            userViewPersonalFragment = (UserViewPersonalFragment) getCBAFragmentByPosition(PERSONAL_FRAGMENT_POS);

                            Log.d(TAG, "updateViewHolder " + uploadProgress);
                            if (null != userViewPersonalFragment)
                                userViewPersonalFragment.setUploadProgress(uploadProgress);
                        }

                        @Override
                        public void uploadingComplete(String filename) {
                            Log.d(TAG, "uploadingComplete " + filename);
                            UserViewPersonalFragment userViewPersonalFragment = null;
                            userViewPersonalFragment = (UserViewPersonalFragment) getCBAFragmentByPosition(PERSONAL_FRAGMENT_POS);

                            if (null != userViewPersonalFragment)
                                userViewPersonalFragment.setUploadComplete(filename);

                        }

                        @Override
                        public void uploadFailed() {
                            Log.d(TAG, "uploadFailed " );
                            UserViewPersonalFragment userViewPersonalFragment = null;
                            userViewPersonalFragment = (UserViewPersonalFragment) getCBAFragmentByPosition(PERSONAL_FRAGMENT_POS);

                            if (null != userViewPersonalFragment)
                                userViewPersonalFragment.setUploadFailed();

                        }
                    };

                     uploadTask = mFirebaseStorageUtil.uploadFile(idImageFilePath, randomUid, FIRESTORE_DOCUMENTS_ID_COPY, uploadViewHolderListener);

                    if (null != uploadTask) {
                        UpdateUploadViewHolderListener finalUploadViewHolderListener = uploadViewHolderListener;
                        uploadTask.addOnProgressListener(taskSnapshot -> {
                            double progress = (100.0 * taskSnapshot.getBytesTransferred() / taskSnapshot.getTotalByteCount());
                            finalUploadViewHolderListener.updateViewHolder(progress);
                        })
                                .addOnCompleteListener(task -> {
                                    if (task.isSuccessful()) {
                                        UploadTask.TaskSnapshot taskSnapshot = task.getResult();
                                        if (null != taskSnapshot) {
                                            finalUploadViewHolderListener.uploadingComplete(randomUid + "." + type);

                                            mUserProfilePersonal.setIdUploaded(randomUid + "." + type);
                                            calculateProgress();
                                            mViewPagerAdapter.updateUserProfilePersonal(mUserProfilePersonal);
                                        } else {
                                            Log.d(TAG, "Upload task not successful");
                                            finalUploadViewHolderListener.uploadFailed();
                                        }
                                    } else {
                                        Log.d(TAG, "Upload failed", task.getException());
                                        finalUploadViewHolderListener.uploadFailed();
                                    }
                                });
                    }
                    break;

                case PICK_MATRIC_CERTIFICATE:
                    Log.d(TAG, "Upload MC");

                    uploadViewHolderListener = new UpdateUploadViewHolderListener(){
                        @Override
                        public void updateViewHolder(double uploadProgress) {
                            UserViewSchoolFragment userViewSchoolFragment = null;
                            userViewSchoolFragment = (UserViewSchoolFragment) getCBAFragmentByPosition(SCHOOL_FRAGMENT_POS);

                            Log.d(TAG, "updateViewHolder " + uploadProgress);
                            if (null != userViewSchoolFragment)
                                userViewSchoolFragment.setUploadProgress(uploadProgress);
                        }

                        @Override
                        public void uploadingComplete(String filename) {
                            Log.d(TAG, "uploadingComplete " + filename);
                            UserViewSchoolFragment userViewSchoolFragment = null;

                            userViewSchoolFragment = (UserViewSchoolFragment) getCBAFragmentByPosition(SCHOOL_FRAGMENT_POS);

                            if (null != userViewSchoolFragment)
                                userViewSchoolFragment.setUploadComplete(filename);

                        }

                        @Override
                        public void uploadFailed() {
                            Log.d(TAG, "uploadFailed " );
                            UserViewSchoolFragment userViewSchoolFragment = null;

                            userViewSchoolFragment = (UserViewSchoolFragment) getCBAFragmentByPosition(SCHOOL_FRAGMENT_POS);

                            if (null != userViewSchoolFragment)
                                userViewSchoolFragment.setUploadFailed();

                        }
                    };

                    uploadTask = mFirebaseStorageUtil.uploadFile(idImageFilePath, randomUid, FIRESTORE_DOCUMENTS_MATRIC_CERTIFICATE, uploadViewHolderListener);

                    if (null != uploadTask) {
                        UpdateUploadViewHolderListener finalUploadViewHolderListener1 = uploadViewHolderListener;
                        uploadTask.addOnProgressListener(taskSnapshot -> {
                            double progress = (100.0 * taskSnapshot.getBytesTransferred() / taskSnapshot.getTotalByteCount());
                            finalUploadViewHolderListener1.updateViewHolder(progress);
                        })
                                .addOnCompleteListener(task -> {
                                    if (task.isSuccessful()) {
                                        UploadTask.TaskSnapshot taskSnapshot = task.getResult();
                                        if (null != taskSnapshot) {
                                            finalUploadViewHolderListener1.uploadingComplete(randomUid + "." + type);

                                            mUserProfileSchool.setMatricCertificateUploaded(randomUid + "." + type);

                                            calculateProgress();
                                            mViewPagerAdapter.updateUserProfileSchool(mUserProfileSchool);
                                        } else {
                                            Log.d(TAG, "Upload task not successful");
                                            finalUploadViewHolderListener1.uploadFailed();
                                        }
                                    } else {
                                        Log.d(TAG, "Upload failed", task.getException());
                                        finalUploadViewHolderListener1.uploadFailed();
                                    }
                                });
                    }
                    break;
                case PICK_SCHOOL_REPORT:
                    Log.d(TAG, "Upload SR");

                    uploadViewHolderListener = new UpdateUploadViewHolderListener(){
                        @Override
                        public void updateViewHolder(double uploadProgress) {
                            UserViewSchoolFragment userViewSchoolFragment = null;
                            userViewSchoolFragment = (UserViewSchoolFragment) getCBAFragmentByPosition(SCHOOL_FRAGMENT_POS);

                            Log.d(TAG, "updateViewHolder " + uploadProgress);
                            if (null != userViewSchoolFragment)
                                userViewSchoolFragment.setUploadProgress(uploadProgress);
                        }

                        @Override
                        public void uploadingComplete(String filename) {
                            Log.d(TAG, "uploadingComplete " + filename);
                            UserViewSchoolFragment userViewSchoolFragment = null;

                            userViewSchoolFragment = (UserViewSchoolFragment) getCBAFragmentByPosition(SCHOOL_FRAGMENT_POS);

                            if (null != userViewSchoolFragment)
                                userViewSchoolFragment.setUploadComplete(filename);

                        }

                        @Override
                        public void uploadFailed() {
                            Log.d(TAG, "uploadFailed " );
                            UserViewSchoolFragment userViewSchoolFragment = null;

                            userViewSchoolFragment = (UserViewSchoolFragment) getCBAFragmentByPosition(SCHOOL_FRAGMENT_POS);

                            if (null != userViewSchoolFragment)
                                userViewSchoolFragment.setUploadFailed();

                        }
                    };

                    uploadTask = mFirebaseStorageUtil.uploadFile(idImageFilePath, randomUid, FIRESTORE_DOCUMENTS_SCHOOL_REPORT, uploadViewHolderListener);

                    if (null != uploadTask) {
                        UpdateUploadViewHolderListener finalUploadViewHolderListener1 = uploadViewHolderListener;
                        uploadTask.addOnProgressListener(taskSnapshot -> {
                            double progress = (100.0 * taskSnapshot.getBytesTransferred() / taskSnapshot.getTotalByteCount());
                            finalUploadViewHolderListener1.updateViewHolder(progress);
                        })
                        .addOnCompleteListener(task -> {
                            if (task.isSuccessful()) {
                                UploadTask.TaskSnapshot taskSnapshot = task.getResult();
                                if (null != taskSnapshot) {
                                    finalUploadViewHolderListener1.uploadingComplete(randomUid + "." + type);

                                    mUserProfileSchool.setSchoolReportUploaded(randomUid + "." + type);

                                    calculateProgress();
                                    mViewPagerAdapter.updateUserProfileSchool(mUserProfileSchool);
                                } else {
                                    Log.d(TAG, "Upload task not successful");
                                    finalUploadViewHolderListener1.uploadFailed();
                                }
                            } else {
                                Log.d(TAG, "Upload failed", task.getException());
                                finalUploadViewHolderListener1.uploadFailed();
                            }
                        });
                    }
                    break;

                case PICK_UNIVERSITY_TRANSCRIPT:
                    Log.d(TAG, "Upload UT");

                    uploadViewHolderListener = new UpdateUploadViewHolderListener(){
                        @Override
                        public void updateViewHolder(double uploadProgress) {
                            CBAFormUniversityFragment cbaFormUniversityFragment = null;
                            cbaFormUniversityFragment = (CBAFormUniversityFragment) getCBAFragmentByPosition(UNIVERSITY_FRAGMENT_POS);

                            Log.d(TAG, "updateViewHolder " + uploadProgress);
                            if (null != cbaFormUniversityFragment)
                                cbaFormUniversityFragment.setUploadProgress(uploadProgress);
                        }

                        @Override
                        public void uploadingComplete(String filename) {
                            Log.d(TAG, "uploadingComplete " + filename);
                            CBAFormUniversityFragment cbaFormUniversityFragment = null;
                            cbaFormUniversityFragment = (CBAFormUniversityFragment) getCBAFragmentByPosition(UNIVERSITY_FRAGMENT_POS);

                            if (null != cbaFormUniversityFragment)
                                cbaFormUniversityFragment.setUploadComplete(filename);

                        }

                        @Override
                        public void uploadFailed() {
                            Log.d(TAG, "uploadFailed " );
                            CBAFormUniversityFragment cbaFormUniversityFragment = null;
                            cbaFormUniversityFragment = (CBAFormUniversityFragment) getCBAFragmentByPosition(UNIVERSITY_FRAGMENT_POS);

                            if (null != cbaFormUniversityFragment)
                                cbaFormUniversityFragment.setUploadFailed();

                        }
                    };

                    uploadTask = mFirebaseStorageUtil.uploadFile(idImageFilePath, randomUid, FIRESTORE_DOCUMENTS_UNIVERSITY_TRANSCRIPT, uploadViewHolderListener);

                    if (null != uploadTask) {
                        UpdateUploadViewHolderListener finalUploadViewHolderListener = uploadViewHolderListener;
                        uploadTask.addOnProgressListener(taskSnapshot -> {
                            double progress = (100.0 * taskSnapshot.getBytesTransferred() / taskSnapshot.getTotalByteCount());
                            finalUploadViewHolderListener.updateViewHolder(progress);
                        })
                                .addOnCompleteListener(task -> {
                                    if (task.isSuccessful()) {
                                        UploadTask.TaskSnapshot taskSnapshot = task.getResult();
                                        if (null != taskSnapshot) {
                                            finalUploadViewHolderListener.uploadingComplete(randomUid + "." + type);

                                            mUserProfileSchool.setUniversityTranscriptUploaded(randomUid + "." + type);
                                            calculateProgress();
                                            mViewPagerAdapter.updateUserProfileSchool(mUserProfileSchool);
                                        } else {
                                            Log.d(TAG, "Upload task not successful");
                                            finalUploadViewHolderListener.uploadFailed();
                                        }
                                    } else {
                                        Log.d(TAG, "Upload failed", task.getException());
                                        finalUploadViewHolderListener.uploadFailed();
                                    }
                                });
                    }
                    break;

            }
        }
    }

    private Fragment getCBAFragmentByPosition(int position){

        if (getSupportFragmentManager().getFragments().size() >= position)
            return getSupportFragmentManager().getFragments().get(position);

        else
            return null;
    }

    private void openFile(Uri uri){

        Intent pdfOpenintent = new Intent(Intent.ACTION_VIEW);
        pdfOpenintent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        pdfOpenintent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        pdfOpenintent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        pdfOpenintent.setDataAndType(uri, "application/pdf");
        try {
            startActivity(pdfOpenintent);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(mContext, R.string.file_open_exception_message, Toast.LENGTH_LONG).show();
        }
    }

    private void checkPermission(int permissionType){
        switch (permissionType){
            case PERMISSIONS_REQUEST_READ:
                if (ContextCompat.checkSelfPermission(mContext,
                        Manifest.permission.READ_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {

                    if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                            Manifest.permission.READ_EXTERNAL_STORAGE)) {
                        showPermissionExplanation();
                    } else {
                        ActivityCompat.requestPermissions(this,
                                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                PERMISSIONS_REQUEST_READ);
                    }
                } else {
                    pickFile();
                }
                break;
            case PERMISSIONS_REQUEST_WRITE:
                if (ContextCompat.checkSelfPermission(mContext,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {

                    if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        showPermissionExplanation();
                    } else {
                        ActivityCompat.requestPermissions(this,
                                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                PERMISSIONS_REQUEST_WRITE);
                    }
                } else {
                    downloadFile();
                }
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST_WRITE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    downloadFile();
                else
                    showPermissionDenied();
                return;
            }

            case PERMISSIONS_REQUEST_READ: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    pickFile();
                else
                    showPermissionDenied();
                return;
            }
        }
    }

    private void showPermissionExplanation(){
        Toast.makeText(mContext, R.string.permissions_required_text, Toast.LENGTH_LONG).show();
        Log.d(TAG, "showPermissionExplanation");
    }

    private void showPermissionDenied(){
        Toast.makeText(mContext, R.string.permissions_denied_write_text, Toast.LENGTH_LONG).show();
        Log.d(TAG, "showPermissionDenied");
    }

    @SuppressLint("NewApi")
    public void aboutCBA(View view) {
        Log.d(TAG, "Launch BursaryView for about CBA");

        Intent intent = new Intent(mContext, BursaryViewActivity.class);
        intent.putExtra(BURSARY_ID_TAG, CBA_ID_APPLICATION_FORM);
        intent.putExtra(USER_STATE_TAG, mUserState);
        intent.putExtra(UNREAD_MESSAGE_TAG, mUnreadBoolean);
        intent.putExtra(FROM_CBA_TAG, true);
        overridePendingTransition( 0, 0);
        startActivity(intent);
    }

    public void saveChanges(View view) {
        Log.d(TAG, "Save changes clicked");
        calculateProgress();
        checkChanges();
        updateFirestoreUserObjects();
        mSaveClicked = true;
    }

    public void createOptOutAlertDialog(){
        AlertDialog alertDialog = new AlertDialog.Builder(this, R.style.AlertDialogTheme).create();
        alertDialog.setTitle(getString(R.string.prompt_cba_opt_out_title));
        alertDialog.setMessage(getString(R.string.prompt_cba_opt_out_message));

        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, mContext.getResources().getString(R.string.prompt_cba_opt_out_yes_keep), (dialog, which) -> {
            startLoadingScreen(mContext.getResources().getString(R.string.cba_deleting_application_toast));
            optOut();
        });

        alertDialog.setButton(DialogInterface.BUTTON_NEUTRAL, mContext.getResources().getString(R.string.prompt_cba_opt_out_yes_delete), (dialog, which) -> {
            startLoadingScreen(mContext.getResources().getString(R.string.cba_deleting_toast));
            optOutAndDelete();
        });

        alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, mContext.getResources().getString(R.string.prompt_cba_opt_out_no), (dialog, which) -> {
            alertDialog.hide();
        });

        alertDialog.show();
    }

    private void startLoadingScreen(String loadingText){
        findViewById(R.id.cba_content_container).setVisibility(View.GONE);
        findViewById(R.id.cba_loading_container).setVisibility(View.VISIBLE);

        ((TextView)(findViewById(R.id.cba_loading_container).findViewById(R.id.item_loading_text))).setText(loadingText);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_view);
        hidePDialog();

        mCurrentUser = FirebaseAuth.getInstance().getCurrentUser();
        if (null != mCurrentUser) {
            mUserState = (UserState) getIntent().getSerializableExtra(USER_STATE_TAG);
            mUnreadBoolean = getIntent().getBooleanExtra(UNREAD_MESSAGE_TAG, false);
            mFromBursary = getIntent().getBooleanExtra(FROM_BURSARY_TAG, false);
            Log.d(TAG, "mFromBursary: " + mFromBursary);

            mFirebaseUtil = new FirebaseFirestoreService();
            mFirebaseAnalyticsHelper = new FirebaseAnalyticsHelper(mContext);

            setUpLinks();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.d(TAG, "Create toolbar options");
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_top_userview, menu);

        MenuItem messageIcon = menu.findItem(R.id.action_message);
        messageIcon.setIcon(mUnreadBoolean?R.drawable.chat_notification:R.drawable.chat_white);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_opt_out){
            Log.d(TAG, "opt out.");
            createOptOutAlertDialog();
        }

        else if (!CustomUtils.menuOnClick(item, mUnreadBoolean, mUserState, this)){
            onBackPressed();
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        calculateProgress();
        checkChanges();

        if (mUserState.getFieldPreference().size() == 0)
            Toast.makeText(mContext, getString(R.string.field_of_interest_nothing_selected), Toast.LENGTH_SHORT).show();
        else if (schoolChangesMade || personalChangesMade || stateChangesMade)
            onBackShowSaveDialog();
        else {
            if (isTaskRoot()) {
                Intent intent = new Intent(mContext, HomeActivity.class);
                intent.putExtra(USER_STATE_TAG, mUserState);
                startActivity(intent);
                finish();
            } else {
                super.onBackPressed();
            }
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mFirebaseUtil.removeUserListener();
    }

    @Override
    public void onBackDialogPositiveClick(DialogFragment dialog) {
        calculateProgress();
        checkChanges();
        updateFirestoreUserObjects();

        if (mFromBursary){
            Intent intent = new Intent(mContext, HomeActivity.class);
            intent.putExtra(USER_STATE_TAG, mUserState);
            intent.putExtra(UNREAD_MESSAGE_TAG, mUnreadBoolean);
            startActivity(intent);
            finish();
        }
        else
            finish();
    }

    @Override
    public void onBackDialogNegativeClick(DialogFragment dialog) {
        if (mFromBursary){
            Intent intent = new Intent(mContext, HomeActivity.class);
            intent.putExtra(USER_STATE_TAG, mUserState);
            intent.putExtra(UNREAD_MESSAGE_TAG, mUnreadBoolean);
            startActivity(intent);
            finish();
        }
        else
            finish();
    }

    private void updateLoadingScreen(){
        Log.d(TAG, "stateLoaded: " + stateLoaded);
        Log.d(TAG, "personalLoaded: " + personalLoaded);
        Log.d(TAG, "schoolLoaded: " + schoolLoaded);

        if (stateLoaded && personalLoaded && schoolLoaded) {
            calculateProgress();
            mViewPagerAdapter.updateUserState(mUserState);
            mViewPagerAdapter.updateUserProfilePersonal(mUserProfilePersonal);
            mViewPagerAdapter.updateUserProfileSchool(mUserProfileSchool);

            findViewById(R.id.cba_content_container).setVisibility(View.VISIBLE);
            findViewById(R.id.cba_loading_container).setVisibility(View.GONE);

            if (mUserState.getProgress() == FINAL_PROGRESS){
                mSubmitButton.setVisibility(View.VISIBLE);

                if (mUserState.getSubmitted()) {
                    findViewById(R.id.cba_submitted_textview).setVisibility(View.VISIBLE);
                    mSubmitButton.setText(mContext.getResources().getString(R.string.resubmit_all_caps));
                }
            }
            else
                mSubmitButton.setVisibility(View.GONE);
            hidePDialog();
        }

    }

    private void logCBAUpdateMetric(){
        mFirebaseAnalyticsHelper.addIntToBundle(ANALYTICS_UPDATE_CBA, 1);
        mFirebaseAnalyticsHelper.addUserProperty(ANALYTICS_CBA_PROGRESS, mUserState.getProgress() + "");
        mFirebaseAnalyticsHelper.logFirebaseBundleMetrics(ANALYTICS_EVENT_UPDATE_CBA);
    }

    private void logSubmitMetric() {
        mFirebaseAnalyticsHelper.addIntToBundle(ANALYTICS_SUBMIT_CBA, 1);
        mFirebaseAnalyticsHelper.addUserProperty(ANALYTICS_SUBMIT_CBA, ANALYTICS_YES);
        mFirebaseAnalyticsHelper.logFirebaseBundleMetrics(ANALYTICS_EVENT_SUBMIT_CBA);
    }

    private void logResubmitMetric() {
        mFirebaseAnalyticsHelper.addIntToBundle(ANALYTICS_RESUBMIT_CBA, 1);
        mFirebaseAnalyticsHelper.addUserProperty(ANALYTICS_RESUBMIT_CBA, ANALYTICS_YES);
        mFirebaseAnalyticsHelper.logFirebaseBundleMetrics(ANALYTICS_EVENT_RESUBMIT_CBA);
    }

    private void logOptOutMetric(){
        mFirebaseAnalyticsHelper.addIntToBundle(ANALYTICS_OPT_OUT_CBA, 1);
        mFirebaseAnalyticsHelper.addUserProperty(ANALYTICS_OPT_IN_CBA, ANALYTICS_NO);
        mFirebaseAnalyticsHelper.addUserProperty(ANALYTICS_OPT_OUT_CBA, ANALYTICS_YES);
        mFirebaseAnalyticsHelper.logFirebaseBundleMetrics(ANALYTICS_EVENT_OPT_OUT);
    }

    private void logOptOutDeleteMetric(){
        mFirebaseAnalyticsHelper.addIntToBundle(ANALYTICS_DELETE_CBA, 1);;
        mFirebaseAnalyticsHelper.addUserProperty(ANALYTICS_OPT_IN_CBA, ANALYTICS_NO);
        mFirebaseAnalyticsHelper.addUserProperty(ANALYTICS_OPT_OUT_CBA, ANALYTICS_YES);
        mFirebaseAnalyticsHelper.addUserProperty(ANALYTICS_DELETE_CBA, ANALYTICS_YES);
        mFirebaseAnalyticsHelper.logFirebaseBundleMetrics(ANALYTICS_EVENT_OPT_OUT_DELETE);
    }

}
