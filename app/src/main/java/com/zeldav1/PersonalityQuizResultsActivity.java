package com.zeldav1;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.MPPointF;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.utils.CustomUtils;
import com.utils.services.FirebaseFirestoreService;
import com.zelda.object.UserProfilePersonality;
import com.zelda.object.UserState;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import static com.github.mikephil.charting.animation.Easing.EasingOption.EaseInOutQuad;
import static com.utils.Constants.FIELD_ML_COMMERCE;
import static com.utils.Constants.FIELD_ML_EBE;
import static com.utils.Constants.FIELD_ML_HEALTH_SCIENCE;
import static com.utils.Constants.FIELD_ML_HUMANITIES;
import static com.utils.Constants.FIELD_ML_LAW;
import static com.utils.Constants.FIELD_ML_SCIENCE;
import static com.utils.Constants.UNREAD_MESSAGE_TAG;
import static com.utils.Constants.USER_FIELD_PREFERENCE_FIRESTORE;
import static com.utils.Constants.USER_PROFILE_PERSONALITY;
import static com.utils.Constants.USER_PROFILE_PERSONALITY_TAG;
import static com.utils.Constants.USER_STATE_FIELD_PREFERENCE;
import static com.utils.Constants.USER_STATE_FIRESTORE;
import static com.utils.Constants.USER_STATE_TAG;

public class PersonalityQuizResultsActivity extends AppCompatActivity implements OnChartValueSelectedListener {
    private static final String TAG = PersonalityQuizResultsActivity.class.getSimpleName();
    private Context mContext = this;

    private UserState mUserState;
    private UserProfilePersonality mUserProfilePersonality;
    private HashMap<String, Boolean> mUserFieldPreferences;

    private Boolean mUnreadBoolean;
    private Button mContinueButton, mAddFoIButton, mHideAddContainerButton;
    private ImageView mHideAddContainerIcon;
    private PieChart mPieChart;
    private ProgressBar mLoadingProgressBar;
    private TextView mFieldSelectedHeadingTextView, mFieldSelectedDescriptionTextView, mPersonalityResultsTextView, mPersonalityAddFoIHeadingTextView;
    private CardView mFieldDescriptionCardView, mFeedbackResultsCardView, mAddFieldOfInterestCardView;
    private ConstraintLayout mPieChartContainer, mNoResultsContainer, mAddFoIContainer, mUserStateUpdatedContainer;
    private List<PieEntry> mPieEntries;
    private int mCurrentAngle;
    private List<Integer> mSliceCentreAngle;
    private FirebaseUser mCurrentUser;
    boolean personalityResultFound = false;
    private final Handler mHandler = new Handler();
    private RatingBar mFeedbackRatingBar;

    private FirebaseFirestoreService mFirebaseUtil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personality_quiz_results);
        if (null != getIntent().getSerializableExtra(USER_STATE_TAG)) {
            mUserState = (UserState) getIntent().getSerializableExtra(USER_STATE_TAG);
            mUserFieldPreferences = new HashMap<>(mUserState.getFieldPreference());
        }
        if (null != getIntent().getSerializableExtra(USER_FIELD_PREFERENCE_FIRESTORE)){
            mUserFieldPreferences = (HashMap<String, Boolean>) getIntent().getSerializableExtra(USER_FIELD_PREFERENCE_FIRESTORE);
        }

        mUserProfilePersonality = (UserProfilePersonality)getIntent().getSerializableExtra(USER_PROFILE_PERSONALITY_TAG);

        mCurrentUser = FirebaseAuth.getInstance().getCurrentUser();
        mUnreadBoolean = getIntent().getBooleanExtra(UNREAD_MESSAGE_TAG, false);
        mFirebaseUtil = new FirebaseFirestoreService();

        setUpLinks();

        if (null == mUserProfilePersonality){
            findUserProfilePersonality();
        }
        else{
            personalityResultFound = true;
            setPieChartValues();
        }
    }

    private void findUserProfilePersonality(){
        if (mCurrentUser != null) {
            mFirebaseUtil.assignUserPersonalityResultsListener(value -> {
                if (null != value){
                    mUserProfilePersonality = value.toObject(UserProfilePersonality.class);
                    Log.d(TAG, "mUserProfilePersonality updated: " + mUserProfilePersonality);
                    personalityResultFound = mUserProfilePersonality.getTestCompleted();

                    if (personalityResultFound){
                        if (null != mUserProfilePersonality.getFieldML()) {
                            removeResultsListener();
                            setPieChartValues();
                        }
                        else {
                            Log.d(TAG, "Personality test taken. Still awaiting results. Set text to loading");
                            setNoPieChartValues(getResources().getString(R.string.user_view_profile_personality_calculating_results));
                            setThread();
                        }
                    }
                }
                else{
                    Log.d(TAG, "No Personality results found at all");
                    setNoPieChartValues(getResources().getString(R.string.user_view_profile_personality_calculating_results));
                    setThread();
                }
            },  Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid());
        }
    }

    private void setThread(){
        int delay = 30000; // 30 Seconds
        mHandler.postDelayed(() -> {
            if (mContext != null && !((Activity) mContext).isFinishing()) {
                Log.d(TAG, "results found " + personalityResultFound);
                if (!personalityResultFound){
                    Log.d(TAG, "Results still not found. Update screen telling user to check back later");
                    setNoPieChartValues(getResources().getString(R.string.user_view_profile_personality_no_results));
                }
            }
        }, delay);
    }

    private void setUpLinks() {
        Log.d(TAG, "Set up links");

        Toolbar mToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mPieChart = findViewById(R.id.personality_pie_chart);
        mSliceCentreAngle = new ArrayList<>();

        mPieChartContainer = findViewById(R.id.personality_pie_chart_container);
        mNoResultsContainer = findViewById(R.id.personality_no_results_container);
        mAddFoIContainer = findViewById(R.id.personality_field_selected_add_FoI_container);
        mUserStateUpdatedContainer = findViewById(R.id.personality_field_selected_user_state_updated);
        mFeedbackResultsCardView = findViewById(R.id.personality_rating_card_view);

        mFieldSelectedHeadingTextView = findViewById(R.id.personality_field_selected_heading);
        mFieldSelectedDescriptionTextView = findViewById(R.id.personality_field_selected_description_text_view);
        mFieldDescriptionCardView = findViewById(R.id.personality_description_card_view);
        mAddFieldOfInterestCardView = findViewById(R.id.personality_quiz_results_add_FoI_cardview);

        mPieChartContainer.setVisibility(View.GONE);
        mNoResultsContainer.setVisibility(View.VISIBLE);
        mFeedbackResultsCardView.setVisibility(View.GONE);
        mAddFieldOfInterestCardView.setVisibility(View.GONE);
        mAddFoIContainer.setVisibility(View.VISIBLE);
        mUserStateUpdatedContainer.setVisibility(View.GONE);

        mLoadingProgressBar = findViewById(R.id.personality_results_loading_bar);
        mPersonalityResultsTextView = findViewById(R.id.personality_results_text);
        mPersonalityAddFoIHeadingTextView = findViewById(R.id.personality_quiz_results_add_FoI_heading);
        mContinueButton = findViewById(R.id.personality_continue_button);

        mAddFoIButton = findViewById(R.id.personality_field_selected_add_FoI_button);
        mHideAddContainerButton = findViewById(R.id.personality_field_selected_hide_section);
        mHideAddContainerIcon = findViewById(R.id.personality_quiz_results_hide_add_FoI_icon);
        mFeedbackRatingBar = findViewById(R.id.rating_bar);
    }

    private void setNoPieChartValues(String resultsText){
        mPieChartContainer.setVisibility(View.GONE);
        mAddFieldOfInterestCardView.setVisibility(View.GONE);
        mNoResultsContainer.setVisibility(View.VISIBLE);
        mLoadingProgressBar.setVisibility(View.VISIBLE);
        mFeedbackResultsCardView.setVisibility(View.GONE);

        mPersonalityResultsTextView.setText(resultsText);
        mContinueButton.setOnClickListener(v -> finish());
    }

    private void setPieChartValues(){

        if (null != mUserProfilePersonality.getFieldML()){
            Log.d(TAG, "setPieChartValues");
            mPieChartContainer.setVisibility(View.VISIBLE);
            mNoResultsContainer.setVisibility(View.GONE);
            setFeedbackRating();

            HashMap<String, Integer> fieldMLHashmap = mUserProfilePersonality.getFieldML();
            mPieEntries = new ArrayList<>();

            for (String key: fieldMLHashmap.keySet()){
                double value = (double)fieldMLHashmap.get(key);
                Log.d(TAG, "Create slice " + key + " value: " + value);
                mPieEntries.add(new PieEntry((int)value, key));
                double centre = ((value/100)*360)/2;
                mSliceCentreAngle.add((int)centre);
                Log.d(TAG, "Total Angle: " + centre*2 + " centre " + centre );
            }
            mCurrentAngle = 0;
            updatePieChart();
        }
    }

    private void setAddFieldOfInterestContainer(String label) {
        mAddFieldOfInterestCardView.setVisibility(View.VISIBLE);
        mAddFieldOfInterestCardView.animate()
                .translationY(0)
                .alpha(1.0f)
                .setDuration(300);
        mAddFoIContainer.setVisibility(View.VISIBLE);
        mUserStateUpdatedContainer.setVisibility(View.GONE);
        mPersonalityAddFoIHeadingTextView.setText(getResources().getString(R.string.personality_field_selected_add_FoI_heading, label));

        mAddFoIButton.setOnClickListener(v -> {
            //TODO Better adding of FoI
            switch (label){
                case FIELD_ML_COMMERCE:
                    mUserFieldPreferences.put("Accounting", true);
                    mUserFieldPreferences.put("Commerce", true);
                    mUserFieldPreferences.put("Finance", true);
                    mUserFieldPreferences.put("Trade", true);
                    break;

                case FIELD_ML_EBE:
                    mUserFieldPreferences.put("Agriculture", true);
                    mUserFieldPreferences.put("Design", true);
                    mUserFieldPreferences.put("Engineering", true);
                    mUserFieldPreferences.put("Information Technology", true);
                    mUserFieldPreferences.put("Mathematics", true);
                    mUserFieldPreferences.put("Safety", true);
                    mUserFieldPreferences.put("Science", true);
                    break;

                case FIELD_ML_HEALTH_SCIENCE:
                    mUserFieldPreferences.put("Health Science", true);
                    mUserFieldPreferences.put("Nursing", true);
                    mUserFieldPreferences.put("Medicine", true);
                    break;

                case FIELD_ML_HUMANITIES:
                    mUserFieldPreferences.put("Education", true);
                    mUserFieldPreferences.put("Fashion", true);
                    mUserFieldPreferences.put("Humanities", true);
                    mUserFieldPreferences.put("Journalism", true);
                    mUserFieldPreferences.put("Medicine", true);
                    mUserFieldPreferences.put("Research", true);
                    mUserFieldPreferences.put("Services", true);
                    break;

                case FIELD_ML_LAW:
                    mUserFieldPreferences.put("Law", true);
                    mUserFieldPreferences.put("Humanities", true);
                    break;

                case FIELD_ML_SCIENCE:
                    mUserFieldPreferences.put("Agriculture", true);
                    mUserFieldPreferences.put("Geology", true);
                    mUserFieldPreferences.put("Mathematics", true);
                    mUserFieldPreferences.put("Research", true);
                    break;

                default:
                    Log.e(TAG, "somehting else selected");
                    break;
            }

            Log.d(TAG, "UserStateFOI: " + mUserFieldPreferences);
            mAddFoIContainer.setVisibility(View.GONE);
            mUserStateUpdatedContainer.setVisibility(View.VISIBLE);
            mFirebaseUtil.updateFirestoreDocumentField(USER_STATE_FIRESTORE, mCurrentUser.getUid(), USER_STATE_FIELD_PREFERENCE, mUserFieldPreferences);

            mAddFieldOfInterestCardView.postDelayed(() -> mAddFieldOfInterestCardView.animate()
                    .translationY(mAddFieldOfInterestCardView.getHeight())
                    .alpha(0.0f)
                    .setDuration(500), 3000);
        });

        mHideAddContainerButton.setOnClickListener(v -> {
            mAddFieldOfInterestCardView.animate()
                    .translationY(mAddFieldOfInterestCardView.getHeight())
                    .alpha(0.0f)
                    .setDuration(300);
            mAddFieldOfInterestCardView.postDelayed(() ->
                    mAddFieldOfInterestCardView.setVisibility(View.GONE), 300);
        });

        mHideAddContainerIcon.setOnClickListener(v -> {
            mAddFieldOfInterestCardView.animate()
                    .translationY(mAddFieldOfInterestCardView.getHeight())
                    .alpha(0.0f)
                    .setDuration(300);
            mAddFieldOfInterestCardView.postDelayed(() ->
                mAddFieldOfInterestCardView.setVisibility(View.GONE), 300);
        });
    }

    private void setFeedbackRating(){
        if (null == mUserProfilePersonality.getFeedbackRating()){
            mFeedbackResultsCardView.setVisibility(View.VISIBLE);

            mFeedbackRatingBar.setOnRatingBarChangeListener((ratingBar, rating, fromUser) -> {
                mUserProfilePersonality.setFeedbackRating(Math.round(rating));
                mFirebaseUtil.updateFirestoreDocumentField(USER_PROFILE_PERSONALITY, mCurrentUser.getUid(), "feedbackRating", mUserProfilePersonality.getFeedbackRating());
                mFeedbackResultsCardView.setVisibility(View.GONE);
                Toast.makeText(mContext, "Thanks! Your feedback will help us improve this service.", Toast.LENGTH_SHORT).show();
            });

        } else {
            mFeedbackResultsCardView.setVisibility(View.GONE);
        }
    }

    private void updatePieChart(){
        PieDataSet dataSet = new PieDataSet(mPieEntries, "Results of personality quiz");
        dataSet.setDrawIcons(false);

        dataSet.setSliceSpace(4f);
        dataSet.setIconsOffset(new MPPointF(0, 40));
        dataSet.setSelectionShift(10f);

        ArrayList<Integer> colors = new ArrayList<Integer>();

        for (int c : ColorTemplate.COLORFUL_COLORS)
            colors.add(c);

        colors.add(ColorTemplate.getHoloBlue());

        dataSet.setColors(colors);

        PieData data = new PieData(dataSet);
        data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(11f);
        data.setValueTextColor(Color.WHITE);

        mPieChart.setData(data);
        mPieChart.setUsePercentValues(true);
        mPieChart.getDescription().setEnabled(false);
        mPieChart.setDragDecelerationFrictionCoef(0.95f);

        mPieChart.setCenterText(generateCenterSpannableText());
        mPieChart.setDrawHoleEnabled(true);
        mPieChart.setHoleColor(Color.WHITE);

        mPieChart.setTransparentCircleColor(Color.WHITE);
        mPieChart.setTransparentCircleAlpha(110);

        mPieChart.setHoleRadius(58f);
        mPieChart.setTransparentCircleRadius(61f);

        mPieChart.setDrawCenterText(true);

        mPieChart.setRotationAngle(0);
        mPieChart.setRotationEnabled(true);
        mPieChart.setHighlightPerTapEnabled(true);

        mPieChart.setOnChartValueSelectedListener(this);
        mPieChart.invalidate(); // refresh

        Legend l = mPieChart.getLegend();
        l.setEnabled(false);

        mPieChart.spin(2000, 0, 360, EaseInOutQuad);
    }

    @Override
    public void onValueSelected(Entry e, Highlight h) {

        if (e == null)
            return;
        Log.i("VAL SELECTED",
                "Value: " + e.getY() + ", index: " + h.getX()
                        + ", DataSet index: " + h.getDataSetIndex());
        int entrySelectedPos = (int) h.getX();

        Log.d(TAG, "pie entry clicked: " + mPieEntries.get(entrySelectedPos).getLabel() + ": " + mPieEntries.get(entrySelectedPos).getValue());
        int spinAngle = mSliceCentreAngle.get(entrySelectedPos);
        Log.d(TAG, "Spinning to: " +spinAngle + " from " + mCurrentAngle);

        switch (mPieEntries.get(entrySelectedPos).getLabel()){
            case FIELD_ML_COMMERCE:
                setAddFieldOfInterestContainer(FIELD_ML_COMMERCE);
                mFieldSelectedHeadingTextView.setText(getResources().getString(R.string.field_ml_commerce_heading));
                mFieldSelectedDescriptionTextView.setText(Html.fromHtml(getResources().getString(R.string.field_ml_commerce_description)));
                break;

            case FIELD_ML_EBE:
                setAddFieldOfInterestContainer(FIELD_ML_EBE);
                mFieldSelectedHeadingTextView.setText(getResources().getString(R.string.field_ml_ebe_heading));
                mFieldSelectedDescriptionTextView.setText(Html.fromHtml(getResources().getString(R.string.field_ml_ebe_description)));
                break;

            case FIELD_ML_HEALTH_SCIENCE:
                setAddFieldOfInterestContainer(FIELD_ML_HEALTH_SCIENCE);
                mFieldSelectedHeadingTextView.setText(getResources().getString(R.string.field_ml_health_sciences_heading));
                mFieldSelectedDescriptionTextView.setText(Html.fromHtml(getResources().getString(R.string.field_ml_health_sciences_description)));
                break;

            case FIELD_ML_HUMANITIES:
                setAddFieldOfInterestContainer(FIELD_ML_HUMANITIES);
                mFieldSelectedHeadingTextView.setText(getResources().getString(R.string.field_ml_humanities_heading));
                mFieldSelectedDescriptionTextView.setText(Html.fromHtml(getResources().getString(R.string.field_ml_humanities_description)));
                break;

            case FIELD_ML_LAW:
                setAddFieldOfInterestContainer(FIELD_ML_LAW);
                mFieldSelectedHeadingTextView.setText(getResources().getString(R.string.field_ml_law_heading));
                mFieldSelectedDescriptionTextView.setText(Html.fromHtml(getResources().getString(R.string.field_ml_law_description)));
                break;

            case FIELD_ML_SCIENCE:
                setAddFieldOfInterestContainer(FIELD_ML_SCIENCE);
                mFieldSelectedHeadingTextView.setText(getResources().getString(R.string.field_ml_science_heading));
                mFieldSelectedDescriptionTextView.setText(Html.fromHtml(getResources().getString(R.string.field_ml_science_description)));
                break;

            default:
                Log.e(TAG, "somehting else selected");
                break;
        }

        mPieChart.spin(1000, mCurrentAngle, 360 + spinAngle, EaseInOutQuad);
        mCurrentAngle = spinAngle;
        mFieldDescriptionCardView.setVisibility(View.VISIBLE);

    }

    @Override
    public void onNothingSelected() {
        Log.i("PieChart", "nothing selected");
        mFieldDescriptionCardView.setVisibility(View.GONE);
        mAddFieldOfInterestCardView.setVisibility(View.GONE);
    }

    private SpannableString generateCenterSpannableText() {
        SpannableString s = new SpannableString(getResources().getString(R.string.user_view_profile_personality_chart_heading));
        s.setSpan(new ForegroundColorSpan(Color.BLACK), 0, 12, 0);
        s.setSpan(new RelativeSizeSpan(2f), 0, 12, 0);
        s.setSpan(new RelativeSizeSpan(.8f), 13, 45, 0);
        s.setSpan(new StyleSpan(Typeface.NORMAL), 13, 45, 0);
        return s;
    }

    private void removeResultsListener(){
        mFirebaseUtil.removeUserPersonalityListener();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.d(TAG, "create options");

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_top_userview, menu);
        MenuItem messageIcon = menu.findItem(R.id.action_message);
        messageIcon.setIcon(mUnreadBoolean?R.drawable.chat_notification:R.drawable.chat_white);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // navigate up on back button click
        if (!CustomUtils.menuOnClick(item, mUnreadBoolean, mUserState, this)) {
            onBackPressed();
        }

        return true;
    }

    @Override
    public void onPause() {
        super.onPause();
        mHandler.removeCallbacks(null);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Log.d(TAG, "BAck");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        removeResultsListener();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
