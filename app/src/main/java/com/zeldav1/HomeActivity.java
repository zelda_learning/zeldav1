package com.zeldav1;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewStub;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;
import com.utils.Constants;
import com.utils.CustomUtils;
import com.utils.FirebaseAnalyticsHelper;
import com.utils.services.FirebaseFirestoreService;
import com.utils.services.FirebaseStorageService;
import com.zelda.object.ApplicationForm;
import com.zelda.object.Bursary;
import com.zelda.object.CallToAction;
import com.zelda.object.UserProfilePersonality;
import com.zelda.object.UserState;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Random;

import leo.me.la.labeledprogressbarlib.LabeledProgressBar;

import static com.utils.Constants.APPLICATION_FORM;
import static com.utils.Constants.APPLICATION_FORM_CONTINUE_TAG;
import static com.utils.Constants.APPLICATION_FORM_SECTIONS_TAG;
import static com.utils.Constants.APPLICATION_FORM_SUBMITTED_TAG;
import static com.utils.Constants.APPLICATION_STATUS_IN_REVIEW;
import static com.utils.Constants.APPLICATION_STATUS_PENDING;
import static com.utils.Constants.APPLICATION_STATUS_RETURNED;
import static com.utils.Constants.APPLICATION_STATUS_SUBMITTED;
import static com.utils.Constants.BURSARIES;
import static com.utils.Constants.BURSARY_FIELD_ALL;
import static com.utils.Constants.BURSARY_ID_TAG;
import static com.utils.Constants.BURSARY_TAG;
import static com.utils.Constants.CALLS_TO_ACTION;
import static com.utils.Constants.CBA_ID_APPLICATION_FORM;
import static com.utils.Constants.CLOSING_DATE;
import static com.utils.Constants.CTA_TYPE_BURSARY;
import static com.utils.Constants.CTA_TYPE_SCHOOL;
import static com.utils.Constants.FROM_HOME_TAG;
import static com.utils.Constants.MESSAGES;
import static com.utils.Constants.MESSAGE_NEW_RECEIVED;
import static com.utils.Constants.RECOMMENDED_HOME_TAG;
import static com.utils.Constants.UNREAD_MESSAGE_TAG;
import static com.utils.Constants.USER_CALLS_TO_ACTION;
import static com.utils.Constants.USER_ID_METRIC;
import static com.utils.Constants.USER_PROFILE_PERSONALITY;
import static com.utils.Constants.USER_PROFILE_PERSONALITY_TAG;
import static com.utils.Constants.USER_STATE_FIRESTORE;
import static com.utils.Constants.USER_STATE_TAG;
import static com.utils.CustomUtils.getDate;


public class HomeActivity extends BaseActivity {
    private static final String TAG = HomeActivity.class.getSimpleName();
    private Context mContext;
    private FirebaseAnalyticsHelper mFirebaseAnalyticsHelper;

    private BottomNavigationViewEx mBottomNavigationBar;

    private FirebaseAuth mAuth;
    private FirebaseUser mCurrentUser;
    private UserState mUserState;
    private UserProfilePersonality mUserProfilePersonality;

    private FirebaseFirestoreService mFirebaseUtil;
    private Boolean mUnreadBoolean = false;
    private TextView mWelcomeUserText;
    private TextView mContinueApplicationTitle, mContinueApplicationClosingDate;
    private TextView mCallToActionText;
    private Button mCallToActionYesButton, mCallToActionNoButton;

    private LabeledProgressBar mProfileProgressBar;
    private MenuItem messageIcon;
    private Bursary mRecommendedBursary = null, mSubmittedBursary = null, mContinueBursary = null;
    private ApplicationForm mSubmittedApplicationForm;
    private boolean mSchoolNameUpdated = false, viewStubInit = false;
    private List<Bursary> bursaryList;

    private View rootView, callToActionView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        mContext = this;
        mFirebaseAnalyticsHelper = new FirebaseAnalyticsHelper(this);
        mFirebaseUtil = new FirebaseFirestoreService();
        startPDialog(mContext);

        mAuth = FirebaseAuth.getInstance();
        mCurrentUser = mAuth.getCurrentUser();

        if (null == mCurrentUser) {
            Log.d(TAG, "No user signed in. Open LoginActivity");
            Intent intent = new Intent(HomeActivity.this, LoginActivity.class);
            overridePendingTransition( 0, 0);
            finish();
            overridePendingTransition( 0, 0);
            startActivity(intent);
        }
        else {
            mFirebaseAnalyticsHelper.addFirebaseEventMetric(mCurrentUser.getUid(), mCurrentUser.getDisplayName(), USER_ID_METRIC);

            setUpLinks();
            Log.d(TAG, "User signed in: " + mCurrentUser.getUid());
        }
    }

    private void findUser(){
        if (null != mCurrentUser) {
            mFirebaseUtil.assignUserListener(value -> {
                if (null != mCurrentUser) {
                    if (null != value) {
                        mCurrentUser = mAuth.getCurrentUser();
                        if (null != mCurrentUser) {
                            mUserState = value.toObject(UserState.class);
                            mUserState.setId(mCurrentUser.getUid());
                            Log.d(TAG, "found:" + mUserState.getId());
                            findBursaryObjects(getDate());
                            HomeActivity.this.updateHomeScreen();
                        }
                    } else {
                        Log.d(TAG, "No UserState found. Go to NewUserActivity");
                        mFirebaseUtil.removeUserListener();
                        Intent intent = new Intent(HomeActivity.this, NewUserActivity.class);
                        HomeActivity.this.startActivity(intent);
                        HomeActivity.this.finish();
                    }
                }
            }, mCurrentUser.getUid());
        }
    }

    private void findCallToActionObject(){
        if (0 != mUserState.getCallsToAction().size()) {
            DocumentReference docRef;
            String cta = mUserState.getCallsToAction().get(mUserState.getCallsToAction().size() - 1);
            Log.d(TAG, "Found calls to action: " + cta);
            docRef = mFirebaseUtil.getFirestore().collection(CALLS_TO_ACTION).document(cta);

            docRef.get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        DocumentSnapshot documentSnapshot = task.getResult();
                        if (Objects.requireNonNull(documentSnapshot).exists()) {
                            Log.d(TAG, "DocumentSnapshot data: " + documentSnapshot.getData());
                            CallToAction callToAction = documentSnapshot.toObject(CallToAction.class);
                            if (null != callToAction && callToAction.isActive()) {
                                callToAction.setId(documentSnapshot.getId());
                                switch (callToAction.getType()){
                                    case CTA_TYPE_BURSARY:
                                        Log.d(TAG, "Call to action bursary found, active");
                                        updateBursaryCallToActionItem(callToAction);
                                        break;

                                    case CTA_TYPE_SCHOOL:
                                        Log.d(TAG, "Call to action schoolPromo found, active");
                                        updateSchoolPromoCard(callToAction);
                                        break;
                                }
                            }

                            else if (null != callToAction && !callToAction.isActive()){
                                Log.d(TAG, "Call to action found, inactive");
                            }
                        }

                        else {
                            Log.d(TAG, "CTA documentSnapshot doesnt exist");
                        }
                    } else {
                        Log.d(TAG, "get failed with ", task.getException());
                    }
            });
        }
    }

    private void findSubmittedApplicationObject(String submittedBursaryId, String submittedApplicationFormId){

        if (null == mUserState.getCompletedApplicationIds() || 0 == mUserState.getCompletedApplicationIds().size()) {
            findViewById(R.id.home_submitted_application_card_view).setVisibility(View.GONE);
        }

        else {
            if (mSubmittedBursary == null){
                Log.d(TAG, "find submitted Bursary");
                mFirebaseUtil.getFirestore()
                        .collection(BURSARIES)
                        .document(submittedBursaryId)
                        .get().addOnCompleteListener(documentSnapshot -> {
                            if (documentSnapshot.isSuccessful()) {
                                Bursary bursary = (Objects.requireNonNull(documentSnapshot.getResult())).toObject(Bursary.class);
                                assert bursary != null;
                                bursary.setId(submittedBursaryId);
                                mSubmittedBursary = bursary;
                                Log.d(TAG, "Submitted found: " + mSubmittedBursary);
                                updateSubmittedApplicationItem();
                            }
                            else
                                Log.d(TAG, "Submitted bursary couldn't be found: " + documentSnapshot.getException());
                        });
            }

            if (mSubmittedApplicationForm == null){
                Log.d(TAG, "find submitted Application");
                mFirebaseUtil.getFirestore()
                        .collection(APPLICATION_FORM)
                        .document(submittedApplicationFormId)
                        .get().addOnCompleteListener((Task<DocumentSnapshot> documentSnapshot) -> {
                            if (documentSnapshot.isSuccessful()) {
                                ApplicationForm applicationForm = (Objects.requireNonNull(documentSnapshot.getResult())).toObject(ApplicationForm.class);
                                assert applicationForm != null;
                                mSubmittedApplicationForm = applicationForm;
                                mSubmittedApplicationForm.setApplicationFormId(submittedApplicationFormId);
                                Log.d(TAG, "Submitted Applicaion form found: " + mSubmittedApplicationForm);
                                updateSubmittedApplicationItem();
                            }
                            else
                                Log.d(TAG, "Submitted application couldn't be found: " + documentSnapshot.getException());
                });
            }
        }
    }

    private void findContinueApplicationObject(String applicationId){
        if (null == mUserState.getStartedApplicationIds() || 0 == mUserState.getStartedApplicationIds().size()) {
            findViewById(R.id.home_continue_application_card_view).setVisibility(View.GONE);
        }
        else {
            DocumentReference docRef;

            docRef = mFirebaseUtil.getFirestore().collection(BURSARIES).document(applicationId);

            docRef.get().addOnCompleteListener(documentSnapshot -> {
                if (documentSnapshot.isSuccessful()) {
                    mContinueBursary = (Objects.requireNonNull(documentSnapshot.getResult())).toObject(Bursary.class);
                    if (mContinueBursary != null) {
                        mContinueBursary.setId(applicationId);
                    }
                    updateContinueApplicationItem();
                }
                else
                    Log.d(TAG, "Submitted application couldn't be found: " + documentSnapshot.getException());
            });
        }
    }

    private void findBursaryObjects(Date date){
        if (null != mUserState) {
            String field = mUserState.getFieldPreferenceStringArray().get(new Random().nextInt(mUserState.getFieldPreferenceStringArray().size()));

            if (field.equals(Constants.BURSARY_FIELD_DONT_KNOW))
                field = BURSARY_FIELD_ALL;

            String finalField = field;
            mFirebaseUtil.getFirestore()
                    .collection(BURSARIES)
                    .whereGreaterThan(CLOSING_DATE, date)
                    .whereEqualTo("field." + field, true)
                    .limit(5)
                    .get()
                    .addOnSuccessListener(task -> {
                        if (mContext != null && !((Activity) (mContext)).isFinishing()) {
                            bursaryList = new ArrayList<>();
                            List<String> bursaryIDList = new ArrayList<>();
                            for (DocumentSnapshot document : task.getDocuments()) {
                                Bursary bursary = document.toObject(Bursary.class);
                                bursaryList.add(bursary);
                                bursaryIDList.add(bursary.getId());
                            }
                            Log.d(TAG, "bursaries found for : " + finalField +"\n" + bursaryIDList.toString());

                            String submittedBursaryId = "", submittedApplicationFormId = "", continueApplicationId = "";
                            boolean foundSubmitted = false, foundContinue = false;

                            //Continue
                            if (mUserState.getStartedApplicationMapSize() > 0) {
                                continueApplicationId = mUserState.getStartedApplicationIds().keySet().toArray()[0].toString();

                                if (bursaryIDList.indexOf(continueApplicationId) != -1) {
                                    mContinueBursary = bursaryList.get(bursaryIDList.indexOf(continueApplicationId));
                                    mContinueBursary.setId(continueApplicationId);
                                    foundContinue = true;
                                    Log.d(TAG, "continueApp found " + mContinueBursary);
                                    updateContinueApplicationItem();
                                }
                            }

                            //Submitted
                            if (mUserState.getCompletedApplicationMapSize() > 0) {
                                submittedBursaryId = mUserState.getCompletedApplicationIds().keySet().toArray()[0].toString();
                                submittedApplicationFormId = mUserState.getCompletedApplicationIds().get(submittedBursaryId);

                                if (bursaryIDList.indexOf(submittedBursaryId) != -1) {
                                    mSubmittedBursary = bursaryList.get(bursaryIDList.indexOf(submittedBursaryId));
                                    foundSubmitted = true;
                                    Log.d(TAG, "mSubmittedBursary found " + mSubmittedBursary);
                                    HomeActivity.this.findSubmittedApplicationObject(submittedBursaryId, submittedApplicationFormId);
                                }
                            }

                            updateRecommendedBursary();

                            if (!foundContinue) {
                                Log.d(TAG, "No continue found in this list");
                                HomeActivity.this.findContinueApplicationObject(continueApplicationId);
                            }
                            if (!foundSubmitted) {
                                Log.d(TAG, "No submitted found in this list");
                                HomeActivity.this.findSubmittedApplicationObject(submittedBursaryId, submittedApplicationFormId);
                            }
                        }
                        hidePDialog();

                    })
                    .addOnFailureListener(task -> Log.d(TAG, "Get of bursary objects failed."));
        }
    }

    private void updateBursaryCallToActionItem(CallToAction callToAction) {
        Log.d(TAG, "updateBursaryCallToActionItem " + callToAction.toString());
        if (!viewStubInit) {
            ViewStub viewStub = findViewById(R.id.home_call_to_action_viewstub);
            viewStub.setLayoutResource(R.layout.item_home_call_to_action);

            viewStub.setOnInflateListener((stub, inflated) -> {
                Log.d(TAG, "inflated " + inflated.getId());
                CardView cardView = (CardView) inflated;

                mCallToActionText = findViewById(R.id.item_home_call_to_action_text);
                mCallToActionNoButton = findViewById(R.id.item_home_call_to_action_no_button);
                mCallToActionYesButton = findViewById(R.id.item_home_call_to_action_yes_button);

                mCallToActionText.setText(callToAction.getHeadingText());
                mCallToActionNoButton.setText(callToAction.getNo());
                mCallToActionYesButton.setText(callToAction.getYes());

                cardView.setOnClickListener(v -> {
                    Intent intent = new Intent(mContext, BursaryViewActivity.class);
                    mFirebaseUtil.fetchDocument(value -> {
                        if (null != value) {
                            Bursary bursary = value.toObject(Bursary.class);
                            bursary.setId(value.getId());
                            intent.putExtra(BURSARY_TAG, bursary);
                            launchActivity(intent);
                        } else {
                            Log.d(TAG, "No Bursary found.");
                        }
                    }, BURSARIES, callToAction.getId());
                });

                mCallToActionYesButton.setOnClickListener(v -> {
                    Intent intent = new Intent(mContext, BursaryViewActivity.class);
                    mFirebaseUtil.fetchDocument(value -> {
                        if (null != value) {
                            Bursary bursary = value.toObject(Bursary.class);
                            bursary.setId(value.getId());
                            intent.putExtra(BURSARY_TAG, bursary);
                            launchActivity(intent);
                        } else {
                            Log.d(TAG, "No Bursary found.");
                        }
                    }, BURSARIES, callToAction.getId());
                });

                mCallToActionNoButton.setOnClickListener(v -> {
                    mUserState.removeCallToActionFromList(callToAction.getId());
                    mFirebaseUtil.updateFirestoreDocumentField(USER_STATE_FIRESTORE, mUserState.getId(), USER_CALLS_TO_ACTION, mUserState.getCallsToAction());
                    cardView.setVisibility(View.GONE);
                });
            });
            viewStub.setVisibility(View.VISIBLE);
            viewStubInit = true;
        }
    }

    private void updateSubmittedApplicationItem() {
        if (null != mSubmittedBursary &&  null != mSubmittedApplicationForm){
            Log.d(TAG, "updatesubmittedBursary: " + mSubmittedBursary.toString() + "\nsubmitted Application: " + mSubmittedApplicationForm.toString() );

            findViewById(R.id.home_submitted_application_card_view).setVisibility(View.VISIBLE);
            findViewById(R.id.home_applications_heading).setVisibility(View.VISIBLE);

            ((TextView)findViewById(R.id.item_home_review_submitted_application_title)).setText(mSubmittedBursary.getTitle());
            ((TextView)findViewById(R.id.item_home_review_submitted_application_status_text)).setText(mSubmittedApplicationForm.getStatus());
            FirebaseStorageService.setGlideImage(findViewById(R.id.item_home_review_submitted_application_image), CustomUtils.setImageNameOnType(Bursary.class, mSubmittedBursary.getId()));

            switch (mSubmittedApplicationForm.getStatus()){
                case APPLICATION_STATUS_SUBMITTED:
                    findViewById(R.id.item_home_review_submitted_application_progress_bar_submitted_progress).setVisibility(View.VISIBLE);
                    findViewById(R.id.item_home_review_submitted_application_progress_bar_feedback_progress).setVisibility(View.GONE);
                    findViewById(R.id.item_home_review_submitted_application_progress_bar_in_review_progress).setVisibility(View.GONE);
                    break;

                case APPLICATION_STATUS_PENDING:
                    findViewById(R.id.item_home_review_submitted_application_progress_bar_submitted_progress).setVisibility(View.GONE);
                    findViewById(R.id.item_home_review_submitted_application_progress_bar_in_review_progress).setVisibility(View.VISIBLE);
                    findViewById(R.id.item_home_review_submitted_application_progress_bar_feedback_progress).setVisibility(View.GONE);
                    break;

                case APPLICATION_STATUS_IN_REVIEW:
                    findViewById(R.id.item_home_review_submitted_application_progress_bar_submitted_progress).setVisibility(View.GONE);
                    findViewById(R.id.item_home_review_submitted_application_progress_bar_in_review_progress).setVisibility(View.VISIBLE);
                    findViewById(R.id.item_home_review_submitted_application_progress_bar_feedback_progress).setVisibility(View.GONE);
                    break;

                case APPLICATION_STATUS_RETURNED:
                    findViewById(R.id.item_home_review_submitted_application_progress_bar_submitted_progress).setVisibility(View.GONE);
                    findViewById(R.id.item_home_review_submitted_application_progress_bar_in_review_progress).setVisibility(View.GONE);
                    findViewById(R.id.item_home_review_submitted_application_progress_bar_feedback_progress).setVisibility(View.VISIBLE);
                    findViewById(R.id.item_home_review_submitted_application_notification_icon).setVisibility(View.VISIBLE);
                    ((TextView)findViewById(R.id.item_home_review_submitted_application_action)).setText(getString(R.string.item_home_review_submitted_application_action_feedback));
                    break;
            }

            findViewById(R.id.home_submitted_application_card_view).setOnClickListener(v -> {
                Intent intent = new Intent(mContext, ApplicationFormReviewActivity.class);
                intent.putExtra(BURSARY_TAG, mSubmittedBursary);
                intent.putExtra(APPLICATION_FORM_SUBMITTED_TAG, mSubmittedApplicationForm);
                launchActivity(intent);
            });
        }

        else {
            Log.d(TAG, "submitted null: " + mSubmittedBursary + "\nsubmitted Application: " + mSubmittedApplicationForm);
            findViewById(R.id.home_submitted_application_card_view).setVisibility(View.GONE);
        }
    }

    private void updateContinueApplicationItem() {
        if (null != mContinueBursary){
            Log.d(TAG, "updateContinueApplication: " + mContinueBursary.toString());
            mContinueApplicationTitle.setText(mContinueBursary.getTitle());
            mContinueApplicationClosingDate.setText(getResources().getString(R.string.application_view_date_closed, mContinueBursary.getClosingDate()));
            findViewById(R.id.home_continue_application_card_view).setVisibility(View.VISIBLE);
            FirebaseStorageService.setGlideImage(findViewById(R.id.item_home_stats_continue_application_image), CustomUtils.setImageNameOnType(Bursary.class, mContinueBursary.getId()));

            ((TextView)findViewById(R.id.item_home_stats_continue_application_action)).setText(getString(R.string.item_home_stats_continue_application_action));

            findViewById(R.id.home_continue_application_card_view).setOnClickListener(v -> {
                Intent intent = new Intent(mContext, ApplicationFormActivity.class);
                intent.putExtra(USER_STATE_TAG, mUserState);
                intent.putExtra(UNREAD_MESSAGE_TAG, mUnreadBoolean);
                intent.putExtra(BURSARY_TAG, mContinueBursary);
                intent.putExtra(APPLICATION_FORM_SECTIONS_TAG, mContinueBursary.getSections());
                intent.putExtra(APPLICATION_FORM_CONTINUE_TAG, true);
                launchActivity(intent);
            });

            mContinueApplicationClosingDate.setVisibility(View.VISIBLE);
        }
        else
            mContinueApplicationClosingDate.setVisibility(View.GONE);
    }

    private void updateWelcomeItem(){

        Log.d(TAG, "DisplayName : " + mUserState.isOptedIn());
        if(null != mUserState.getUsername()){
            mWelcomeUserText.setText(getString(R.string.home_welcome_text, mUserState.getUsername()));
        } else {
            mWelcomeUserText.setText(getString(R.string.home_welcome_text, "There"));
        }

        if (mUserState.isOptedIn()){
            Log.d(TAG, "User logged in: " + mUserState.isOptedIn());

            ((TextView) findViewById(R.id.item_home_welcome_action)).setText(mContext.getResources().getString(R.string.item_home_welcome_action));
            findViewById(R.id.home_user_profile_progress).setVisibility(View.VISIBLE);
            findViewById(R.id.item_home_find_out_text).setVisibility(View.GONE);

            findViewById(R.id.home_welcome_card).setOnClickListener(v -> {
                Intent intent = new Intent(mContext, UserViewActivity.class);
                intent.putExtra(USER_STATE_TAG, mUserState);
                intent.putExtra(UNREAD_MESSAGE_TAG, mUnreadBoolean);
                mFirebaseUtil.removeUserListener();

                if(null != mUserProfilePersonality){
                    intent.putExtra(USER_PROFILE_PERSONALITY_TAG, mUserProfilePersonality);
                }
                launchActivity(intent);
            });
        }

        else {
            Log.d(TAG, "User not opted in: " + mUserState.isOptedIn());
            ((TextView) findViewById(R.id.item_home_welcome_action)).setText(mContext.getResources().getString(R.string.item_home_welcome_action_cba));
            findViewById(R.id.home_user_profile_progress).setVisibility(View.GONE);
            findViewById(R.id.item_home_find_out_text).setVisibility(View.VISIBLE);

            findViewById(R.id.home_welcome_card).setOnClickListener(v -> {
                Intent intent = new Intent(mContext, BursaryViewActivity.class);
                intent.putExtra(USER_STATE_TAG, mUserState);
                intent.putExtra(UNREAD_MESSAGE_TAG, mUnreadBoolean);
                intent.putExtra(BURSARY_ID_TAG, CBA_ID_APPLICATION_FORM);
                intent.putExtra(FROM_HOME_TAG, true);
                mFirebaseUtil.removeUserListener();
                launchActivity(intent);
                finish();
            });
        }

        if (mUserState.getSubmitted())
            ((TextView)findViewById(R.id.item_home_welcome_action)).setText(mContext.getResources().getString(R.string.item_home_welcome_action_edit));

        mProfileProgressBar.setValue(mUserState.getProgress());
        mProfileProgressBar.setLabelText(mContext.getResources().getString(R.string.cba_progress_label, mUserState.getProgress()));
    }

    private void updatePersonalityQuizItem(){
        mFirebaseUtil.getFirestore()
                .collection(USER_PROFILE_PERSONALITY)
                .document(mCurrentUser.getUid())
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        DocumentSnapshot document = task.getResult();
                        if ((null == document) || !document.exists()) {
                            Log.d(TAG, "No UserProfilePersonality object found.");
                            findViewById(R.id.home_personality_card_view).setOnClickListener(v -> {
                                Intent intent = new Intent(mContext, IntroQuizStartActivity.class);
                                intent.putExtra(USER_STATE_TAG, mUserState);
                                intent.putExtra(UNREAD_MESSAGE_TAG, mUnreadBoolean);
                                launchActivity(intent);
                            });
                        } else {
                            mUserProfilePersonality = document.toObject(UserProfilePersonality.class);
                            Log.d(TAG, "DocumentSnapshot data: " + document.getData());
                            ((TextView)findViewById(R.id.item_home_personality_quiz_action)).setText(getText(R.string.item_home_quiz_taken_action));
                            findViewById(R.id.home_personality_card_view).setOnClickListener(v -> {
                                Intent intent = new Intent(mContext, PersonalityQuizResultsActivity.class);
                                intent.putExtra(USER_STATE_TAG, mUserState);
                                intent.putExtra(UNREAD_MESSAGE_TAG, mUnreadBoolean);
                                intent.putExtra(USER_PROFILE_PERSONALITY_TAG, mUserProfilePersonality);
                                launchActivity(intent);
                            });
                        }
                    } else {
                        Log.d(TAG, "get failed with ", task.getException());
                    }
                });
    }

    private void updateHomeScreen(){
        if (null != mCurrentUser && null != mUserState) {
            Log.d(TAG, "update");
            setNewMessageIcon();
            updateWelcomeItem();
            updateRecommendedBursary();
            updatePersonalityQuizItem();
            findCallToActionObject();
            updateVerification();
        }

    }

    private void updateRecommendedBursary() {
        if (bursaryList != null) {
            int randomInteger;
            if (!bursaryList.isEmpty()) {
                randomInteger = new Random().nextInt(bursaryList.size());
                mRecommendedBursary = bursaryList.get(randomInteger);
            }

            if (null != mRecommendedBursary) {
                findViewById(R.id.home_recommended_bursary_card_view).setVisibility(View.VISIBLE);
                findViewById(R.id.home_bursaries_heading).setVisibility(View.VISIBLE);
                ((TextView) findViewById(R.id.item_home_recommended_bursary_title)).setText(mRecommendedBursary.getTitle());
                ((TextView) findViewById(R.id.item_home_recommended_bursary_closing_date)).setText(getResources().getString(R.string.application_view_date_closed, mRecommendedBursary.getClosingDate()));
                FirebaseStorageService.setGlideImage(findViewById(R.id.item_home_recommended_bursary_image), CustomUtils.setImageNameOnType(Bursary.class, mRecommendedBursary.getId()));

                findViewById(R.id.home_recommended_bursary_card_view).setOnClickListener(v -> {
                    Intent intent = new Intent(mContext, BursaryViewActivity.class);
                    intent.putExtra(USER_STATE_TAG, mUserState);
                    intent.putExtra(UNREAD_MESSAGE_TAG, mUnreadBoolean);
                    intent.putExtra(BURSARY_TAG, mRecommendedBursary);
                    intent.putExtra(RECOMMENDED_HOME_TAG, true);
                    mContext.startActivity(intent);
                });
            }

            else {
                findViewById(R.id.home_recommended_bursary_card_view).setVisibility(View.GONE);
                findViewById(R.id.home_bursaries_heading).setVisibility(View.GONE);
            }
        }
        else {
            findViewById(R.id.home_bursaries_heading).setVisibility(View.GONE);
            findViewById(R.id.home_recommended_bursary_card_view).setVisibility(View.GONE);
        }
    }

    private void updateVerification() {
        if (null != mCurrentUser && mCurrentUser.getProviders() != null) {
            if (null == mCurrentUser.getEmail() || mCurrentUser.getEmail().equals("")) {
                Log.d(TAG, "User has no Email");
                findViewById(R.id.home_verification_card_view).setVisibility(View.GONE);
            }

            else {
                Log.d(TAG, "User has Email");
                if (!mCurrentUser.isEmailVerified()) {
                    Log.d(TAG, "Email Not verified.");
                    findViewById(R.id.home_verification_card_view).setVisibility(View.VISIBLE);
                    findViewById(R.id.home_verification_card_view).setOnClickListener(v -> mCurrentUser.sendEmailVerification().addOnCompleteListener(task2 -> {
                        if (task2.isSuccessful()) {
                            Log.d(TAG, "Verification Email sent");
                            Toast.makeText(mContext, getResources().getString(R.string.item_email_verification_sent_text, mCurrentUser.getEmail()), Toast.LENGTH_SHORT).show();
                        } else {
                            Log.e(TAG, "sendEmailVerification", task2.getException());
                            Toast.makeText(mContext, getResources().getString(R.string.item_email_verification_failed_text), Toast.LENGTH_SHORT).show();
                        }
                        findViewById(R.id.home_verification_card_view).setVisibility(View.GONE);
                    }));
                }
                else {
                    Log.d(TAG, "Email verified");
                    findViewById(R.id.home_verification_card_view).setVisibility(View.GONE);
                }
            }

        }
    }

    private void updateSchoolPromoCard(CallToAction callToAction){
        if (!viewStubInit) {
            ViewStub viewStub = findViewById(R.id.home_call_to_action_viewstub);
            viewStub.setLayoutResource(R.layout.item_home_school_promo);

            viewStub.setOnInflateListener((stub, inflated) -> {
                Log.d(TAG, "inflated " + inflated.getId());
                CardView cardView = (CardView) inflated;
                ((TextView)cardView.findViewById(R.id.item_home_school_promo_action)).setText(callToAction.getYes());
                ((TextView)cardView.findViewById(R.id.item_school_promo_subtitle)).setText(callToAction.getSubText());
                ((TextView)cardView.findViewById(R.id.item_school_promo_title)).setText(callToAction.getHeadingText());
                ((EditText)cardView.findViewById(R.id.item_home_school_edittext)).setHint(callToAction.getHint());

                cardView.findViewById(R.id.item_school_promo_close_button).setOnClickListener(v -> {
                    cardView.setVisibility(View.GONE);
                    mUserState.removeCallToActionFromList(callToAction.getId());
                    mUserState.setSchoolName(callToAction.getNo());
                    Log.d(TAG, "Updating...");
                    mSchoolNameUpdated = true;
                    mFirebaseUtil.setFirestoreDocumentMerge(USER_STATE_FIRESTORE, mCurrentUser.getUid(), mUserState);

                });
                cardView.findViewById(R.id.item_home_school_promo_action).setOnClickListener(v -> {
                    String schoolName = ((EditText) findViewById(R.id.item_home_school_edittext)).getText().toString();
                    if (schoolName.length() > 0) {
                        mUserState.removeCallToActionFromList(callToAction.getId());
                        mUserState.setSchoolName(schoolName);
                        mSchoolNameUpdated = true;

                        findViewById(R.id.item_school_promo_container).setVisibility(View.GONE);
                        findViewById(R.id.item_home_school_promo_update_container).setVisibility(View.VISIBLE);
                        Log.d(TAG, "Updating...");

                        mFirebaseUtil.setFirestoreDocumentMergeUpdate(value -> {
                            if (value) {
                                Log.d(TAG, "Update written successfully");
                                Toast.makeText(mContext, getResources().getString(R.string.home_school_promocode_submitted), Toast.LENGTH_LONG).show();

                                findViewById(R.id.item_school_promo_submitted_text).setVisibility(View.VISIBLE);
                                findViewById(R.id.item_school_promo_container).setVisibility(View.GONE);
                                findViewById(R.id.item_home_school_promo_update_container).setVisibility(View.GONE);

                                cardView.setVisibility(View.VISIBLE);
                                TranslateAnimation anim = new TranslateAnimation(0, cardView.getWidth(), 0, 0);
                                anim.setDuration(2000);
                                anim.setStartOffset(500);
                                anim.setAnimationListener(new Animation.AnimationListener() {
                                    @Override
                                    public void onAnimationStart(Animation animation) {

                                    }

                                    @Override
                                    public void onAnimationEnd(Animation animation) {
                                        ConstraintLayout.LayoutParams params = (ConstraintLayout.LayoutParams) cardView.getLayoutParams();
                                        params.rightMargin -= cardView.getWidth();
                                        cardView.setLayoutParams(params);
                                        cardView.setVisibility(View.GONE);

                                    }

                                    @Override
                                    public void onAnimationRepeat(Animation animation) {

                                    }
                                });
                                cardView.startAnimation(anim);
                            } else {
                                Log.d(TAG, "Write update failed");
                                Toast.makeText(mContext, getResources().getString(R.string.home_school_promocode_update_error), Toast.LENGTH_LONG).show();
                                findViewById(R.id.item_school_promo_submitted_text).setVisibility(View.GONE);
                                findViewById(R.id.item_school_promo_container).setVisibility(View.VISIBLE);
                                findViewById(R.id.item_home_school_promo_update_container).setVisibility(View.GONE);
                            }
                        }, USER_STATE_FIRESTORE, mCurrentUser.getUid(), mUserState);
                    } else {
                        Toast.makeText(mContext, getResources().getString(R.string.home_school_promocode_null_error), Toast.LENGTH_LONG).show();
                    }
                });

                if (null == mUserState.getSchoolName()) {
                    Log.d(TAG, "School name null");
                    cardView.setVisibility(View.VISIBLE);
                    cardView.findViewById(R.id.item_school_promo_submitted_text).setVisibility(View.GONE);
                    cardView.findViewById(R.id.item_school_promo_container).setVisibility(View.VISIBLE);
                    cardView.findViewById(R.id.item_home_school_promo_update_container).setVisibility(View.GONE);

                } else if (null != mUserState.getSchoolName() && !mSchoolNameUpdated) {
                    Log.d(TAG, "School name " + mUserState.getSchoolName());
                    cardView.setVisibility(View.GONE);
                }

            });
            viewStub.setVisibility(View.VISIBLE);
            viewStubInit = true;
        }
    }

    private void setUpLinks() {
        Log.d(TAG, "Set up links");

        rootView = findViewById(R.id.home_main_container);
        Toolbar mToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(false);
        mBottomNavigationBar = findViewById(R.id.bottom_nav_bar);
        mBottomNavigationBar.setSelectedItemId(R.id.action_home);
        mBottomNavigationBar.enableAnimation(false);
        mBottomNavigationBar.enableItemShiftingMode(false);
        mBottomNavigationBar.enableShiftingMode(false);
        mBottomNavigationBar.setOnNavigationItemSelectedListener(item -> {
            Class startClass = null;
            switch (item.getItemId()) {
                case R.id.action_home:
                    break;

                case R.id.action_bursary:
                    startClass = BursaryFeedActivity.class;
                    break;

                case R.id.action_tips:
                    startClass = TipFeedActivity.class;
                    break;

                case R.id.action_university:
                    startClass = UniversityFeedActivity.class;
                    break;

                case R.id.action_quiz:
                    startClass = QuizFeedActivity.class;
                    break;

            }
            if (null != startClass){
                Intent intent = new Intent(getApplicationContext(), startClass);
                intent.putExtra(USER_STATE_TAG, mUserState);
                intent.putExtra(UNREAD_MESSAGE_TAG, mUnreadBoolean);
                startActivity(intent);
                finish();
            }
            return false;
        });

        mProfileProgressBar = findViewById(R.id.home_user_profile_progress);

        mContinueApplicationTitle = findViewById(R.id.item_home_stats_continue_application_title);
        mContinueApplicationClosingDate = findViewById(R.id.item_home_stats_continue_application_closing_date);

        mWelcomeUserText = findViewById(R.id.home_welcome_text);
    }

    @SuppressLint("RestrictedApi")
    public void setNewMessageIcon(){
        Log.d(TAG, "Set new message icon");
            DocumentReference documentReference = mFirebaseUtil.getFirestore()
                    .collection(MESSAGES)
                    .document(mCurrentUser.getUid());

            documentReference.addSnapshotListener((documentSnapshot, e) -> {
                if (e != null) {
                    Log.d(TAG, "listen failed " + e.getMessage());
                }

                if (documentSnapshot != null && documentSnapshot.exists()) {
                    if (documentSnapshot.getBoolean(MESSAGE_NEW_RECEIVED)) {
                        if (null != messageIcon) {
                            messageIcon.setIcon(getResources().getDrawable(R.drawable.chat_notification));
                            mUnreadBoolean = true;
                            Log.d(TAG, "new unread message");
                        }
                    } else {
                        if (null != messageIcon) {
                            messageIcon.setIcon(getResources().getDrawable(R.drawable.chat_white));
                            mUnreadBoolean = false;
                            Log.d(TAG, "no new messages");
                        }
                    }
                } else {
                    Log.d(TAG, "Current message: null");
                }
            });

    }

    private void toggleVisibility(View view) {
        if (view.getVisibility() == View.GONE) {
            view.setVisibility(View.VISIBLE);
        } else if (view.getVisibility() == View.VISIBLE) {
            view.setVisibility(View.GONE);
        }
    }

    private void launchActivity(Intent intent){
        intent.putExtra(USER_STATE_TAG, mUserState);
        intent.putExtra(UNREAD_MESSAGE_TAG, mUnreadBoolean);
        overridePendingTransition( 0, 0);
        startActivity(intent);
        overridePendingTransition( 0, 0);
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateHomeScreen();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.d(TAG, "create options");
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_top_toolbar, menu);
        messageIcon = menu.findItem(R.id.action_message);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.action_logout){
            Log.d(TAG, "logout");
            AuthUI.getInstance()
                    .signOut(this)
                    .addOnCompleteListener(task -> {
                        // user is now signed out
                        startActivity(new Intent(mContext, LoginActivity.class));
                        finish();
                    });
        }

        else if (!CustomUtils.menuOnClick(item, mUnreadBoolean, mUserState, this)){
            onBackPressed();
        }

        return true;
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public void onStart() {
        super.onStart();
        findUser();
    }

    @Override
    public void onDestroy() {
        mFirebaseUtil.removeUserListener();
        super.onDestroy();
    }

}