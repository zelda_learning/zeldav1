package com.zeldav1;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;

import com.utils.CustomUtils;
import com.utils.services.FirebaseFirestoreService;
import com.zelda.object.UserState;

import static com.utils.Constants.DISPLAY_TEXT_COLLECTION;
import static com.utils.Constants.DISPLAY_TEXT_FAQ;
import static com.utils.Constants.FIRESTORE_BR;
import static com.utils.Constants.FIRESTORE_NEWLINE;
import static com.utils.Constants.UNREAD_MESSAGE_TAG;
import static com.utils.Constants.USER_STATE_TAG;


public class FAQActivity extends BaseActivity {
    private static final String TAG = FAQActivity.class.getSimpleName();
    private final Context mContext = this;

    private UserState mUserState;
    private Boolean mUnreadBoolean;
    private FirebaseFirestoreService mFirebaseUtil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faq);

        mUnreadBoolean = getIntent().getBooleanExtra(UNREAD_MESSAGE_TAG, false);

        if (getIntent().hasExtra(USER_STATE_TAG)) {
            mUserState = (UserState) getIntent().getSerializableExtra(USER_STATE_TAG);
            Log.d(TAG, "FAQActivity with User: " + mUserState);

        }

        mFirebaseUtil = new FirebaseFirestoreService();
        setUpLinks();
        findFAQ();
    }


    private void findFAQ(){
        startPDialog(mContext);
        mFirebaseUtil.fetchDocumentField(value -> {
            if (null != value){
                Log.d(TAG, "faqText found.");
            }
            else{
                Log.d(TAG, "faqText not found, use outdated?.");
                value = mContext.getResources().getString(R.string.faq_page_text);
            }
            updateScreen(value);
        }, DISPLAY_TEXT_COLLECTION, DISPLAY_TEXT_FAQ);
    }

    private void setUpLinks() {
        Log.d(TAG, "Set up links");

        Toolbar mToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        ((TextView)findViewById(R.id.faq_text)).setText(CustomUtils.fromHtml(mContext.getResources().getString(R.string.faq_page_text)));

    }

    private void updateScreen(String faqText){
        faqText = faqText.replace(FIRESTORE_NEWLINE, FIRESTORE_BR);
        Log.d(TAG, "New FAQ: " + faqText);
        ((TextView)findViewById(R.id.faq_text)).setText(CustomUtils.fromHtml(faqText));

        hidePDialog();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.d(TAG, "create options");
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_faq_activity, menu);
        MenuItem messageIcon = menu.findItem(R.id.action_message);
        messageIcon.setIcon(mUnreadBoolean?R.drawable.chat_notification:R.drawable.chat_white);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.action_about_us){
            finish();
        }

        if (!CustomUtils.menuOnClick(item, mUnreadBoolean, mUserState, this)){
            onBackPressed();
        }

        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
    }
}