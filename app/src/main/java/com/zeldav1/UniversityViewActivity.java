package com.zeldav1;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.util.Linkify;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.utils.CustomUtils;
import com.utils.ListTagHandler;
import com.utils.adapters.ViewAdapter;
import com.zelda.object.University;
import com.zelda.object.UserState;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import static com.utils.Constants.UNIVERSITY_ID_TAG;
import static com.utils.Constants.UNREAD_MESSAGE_TAG;
import static com.utils.Constants.USER_STATE_TAG;
import static com.utils.CustomUtils.replaceStringChars;

public class UniversityViewActivity extends BaseActivity {
    private static final String TAG = UniversityViewActivity.class.getSimpleName();
    private final Context mContext = this;

    private RecyclerView mImageRecyclerView;
    private University mUniversity;
    private UserState mUserState;
    private Boolean mUnreadBoolean;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_university_view);
        startPDialog(mContext);

        mUserState = (UserState)getIntent().getSerializableExtra(USER_STATE_TAG);
        mUniversity = (University)getIntent().getSerializableExtra(UNIVERSITY_ID_TAG);
        mUnreadBoolean = getIntent().getBooleanExtra(UNREAD_MESSAGE_TAG, false);

        setUpLinks();

        makeUniversityView();
        setScrollImages();
    }

    private void setUpLinks() {
        Log.d(TAG, "Set up links");

        Toolbar mToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mImageRecyclerView = findViewById(R.id.university_view_recycler_images);

    }

    private void makeUniversityView(){
        Log.d(TAG, "Setting up UniversityView: " +  mUniversity.getTitle());

        try{
            ((TextView)findViewById(R.id.university_name)).setText(mUniversity.getTitle());
            ((TextView)findViewById(R.id.university_view_closing_date_text)).setText(mUniversity.getClosingDate());
            ((TextView)findViewById(R.id.university_description)).setText(mUniversity.getText());
            ((TextView)findViewById(R.id.university_location)).setText(replaceStringChars(mUniversity.getLocation()));
            ((TextView)findViewById(R.id.university_view_faculties_text)).setText(ListTagHandler.listFormatter(mUniversity.getQualifications()));

            ((TextView)findViewById(R.id.university_nbt_required_text)).setText(mUniversity.getNBTRequirement());
            Pattern pattern = Pattern.compile("developer.android.com");
            Linkify.addLinks((Button)findViewById(R.id.university_link_button), pattern, mUniversity.getLink());

            findViewById(R.id.university_link_button).setOnClickListener(view -> {
                try {
                    Log.d(TAG, "Try load university Link");

                    Uri uri = Uri.parse(mUniversity.getLink());
                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                    startActivity(intent);
                }
                catch(NullPointerException e) {
                    Log.d(TAG, "Company has no link.");
                }
            });
            hidePDialog();

        } catch (NullPointerException e){
            Toast.makeText(mContext, "Whoops, couldn't load this university! Please let us know in the messages tab if you'd like this fixed ASAP.", Toast.LENGTH_LONG).show();
            Log.e(TAG, "UniversityView binding error: " + e.getMessage());
        }
    }

    private void setScrollImages() {
        Log.d(TAG, "Setting image recycle view");
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        mImageRecyclerView.setLayoutManager(mLayoutManager);

        List<String> universityImages = new ArrayList<>();
        universityImages.add("1");
        universityImages.add("2");
        universityImages.add("3");

        ViewAdapter<String> mUniversityImageScrollAdapter = new ViewAdapter<>(universityImages,
                R.layout.item_university_image,
                mImageRecyclerView,
                mUniversity.getId());

        mImageRecyclerView.setAdapter(mUniversityImageScrollAdapter);
        mUniversityImageScrollAdapter.notifyDataSetChanged();
        mImageRecyclerView.scrollToPosition(1);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_top_toolbar, menu);
        MenuItem messageIcon = menu.findItem(R.id.action_message);
        messageIcon.setIcon(mUnreadBoolean?R.drawable.chat_notification:R.drawable.chat_white);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // navigate up on back button click
        if (!CustomUtils.menuOnClick(item, mUnreadBoolean, mUserState, this)){
            onBackPressed();
        }

        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
    }

}