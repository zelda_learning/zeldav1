package com.zeldav1;

import android.arch.paging.PagedList;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.firebase.ui.firestore.paging.FirestorePagingAdapter;
import com.firebase.ui.firestore.paging.FirestorePagingOptions;
import com.firebase.ui.firestore.paging.LoadingState;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.utils.CustomUtils;
import com.utils.FirebaseAnalyticsHelper;
import com.utils.adapters.ViewAdapter;
import com.utils.services.FirebaseFirestoreService;
import com.utils.viewholders.BursaryViewHolder;
import com.zelda.object.Bursary;
import com.zelda.object.UserState;

import java.util.ArrayList;
import java.util.Arrays;

import static com.utils.Constants.BURSARIES;
import static com.utils.Constants.BURSARY_FIELD_ALL;
import static com.utils.Constants.BURSARY_FIELD_PINNED;
import static com.utils.Constants.BURSARY_FIELD_PROMPT;
import static com.utils.Constants.BURSARY_TAG;
import static com.utils.Constants.CLOSING_DATE;
import static com.utils.Constants.SELECT_APPLICATION_METRIC;
import static com.utils.Constants.UNREAD_MESSAGE_TAG;
import static com.utils.Constants.USER_STATE_TAG;
import static com.utils.CustomUtils.getDate;

public class BursaryFeedActivity extends BaseActivity{
    private static final String TAG = BursaryFeedActivity.class.getSimpleName();
    private Context mContext;
    private FirebaseAnalyticsHelper mFirebaseAnalyticsHelper;

    private BottomNavigationViewEx mBottomNavigationBar;

    private FirebaseAuth mAuth;
    private FirebaseUser mCurrentUser;

    private RecyclerView mBursaryRecyclerView;
    private ArrayList<String> fieldSelectOptions = null;
    private ArrayList<Bursary> pinnedBursariesList = null;
    private FirestorePagingAdapter<Bursary, BursaryViewHolder> mBursaryFeedFirestoreAdapter;
    private ViewAdapter<Bursary> mPinnedBursaryFeedAdapter;
    private Query mPinnedQuery, mAllQuery, mOtherQuery;
    private UserState mUserState;
    private Boolean mUnreadBoolean;
    private CardView mNoResultsFoundCardView;
    private FirebaseFirestoreService mFirebaseUtil;
    private MaterialSpinner mSpinner;
    private RecyclerView.OnItemTouchListener mPinnedOnTouchListener;

    private int limit = 15, prefetchDistance = 10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mUserState = (UserState)getIntent().getSerializableExtra(USER_STATE_TAG);
        mUnreadBoolean = getIntent().getBooleanExtra(UNREAD_MESSAGE_TAG, false);
        mFirebaseAnalyticsHelper = new FirebaseAnalyticsHelper(this);

        mAuth = FirebaseAuth.getInstance();
        mCurrentUser = mAuth.getCurrentUser();

        mFirebaseUtil = new FirebaseFirestoreService();
        mContext = getApplicationContext();
        startPDialog(mContext);

        setContentView(R.layout.activity_bursary_feed);
        setUpLinks();
    }

    private void setUpLinks() {
        Log.d(TAG, "Set up links");

        Toolbar mToolbar = findViewById(R.id.toolbar_with_spinner);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        mBottomNavigationBar = findViewById(R.id.bottom_nav_bar);
        mBottomNavigationBar.setSelectedItemId(R.id.action_bursary);
        mBottomNavigationBar.enableAnimation(false);
        mBottomNavigationBar.enableItemShiftingMode(false);
        mBottomNavigationBar.enableShiftingMode(false);
        mBottomNavigationBar.setOnNavigationItemSelectedListener(item -> {
            Class startClass = null;
            switch (item.getItemId()) {
                case R.id.action_bursary:
                    break;

                case R.id.action_home:
                    startClass = HomeActivity.class;
                    break;

                case R.id.action_university:
                    startClass = UniversityFeedActivity.class;
                    break;

                case R.id.action_tips:
                    startClass = TipFeedActivity.class;
                    break;

                case R.id.action_quiz:
                    startClass = QuizFeedActivity.class;
                    break;

            }
            if (null != startClass){
                Intent intent = new Intent(BursaryFeedActivity.this, startClass);
                intent.putExtra(USER_STATE_TAG, mUserState);
                intent.putExtra(UNREAD_MESSAGE_TAG, mUnreadBoolean);
                startActivity(intent);
                finish();
            }
            return false;
        });

        final GestureDetector mGestureDetector = new GestureDetector(mContext, new GestureDetector.SimpleOnGestureListener(){
            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                return true;
            }
        });

        mPinnedOnTouchListener = new RecyclerView.OnItemTouchListener() {

            @Override
            public void onTouchEvent(RecyclerView rv, MotionEvent e) {

            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }

            @Override
            public boolean onInterceptTouchEvent(RecyclerView recyclerView, MotionEvent motionEvent) {
                View child = recyclerView.findChildViewUnder(motionEvent.getX(), motionEvent.getY());

                if (child != null && mGestureDetector.onTouchEvent(motionEvent)) {
                    Bursary bursary = null;
                    if(null != mPinnedBursaryFeedAdapter){
                        bursary = mPinnedBursaryFeedAdapter.getItem(recyclerView.getChildLayoutPosition(child));
                    }

                    if (null != bursary){
                        mFirebaseAnalyticsHelper.addFirebaseEventMetric(bursary.getId(), bursary.getTitle(), SELECT_APPLICATION_METRIC);

                        Intent intent = new Intent(BursaryFeedActivity.this, BursaryViewActivity.class);
                        intent.putExtra(BURSARY_TAG, bursary);
                        intent.putExtra(USER_STATE_TAG, mUserState);
                        intent.putExtra(UNREAD_MESSAGE_TAG, mUnreadBoolean);
                        startActivity(intent);
                    }
                }
                return false;

            }
        };

        mBursaryRecyclerView = findViewById(R.id.bursaries_recycler_view);
        mNoResultsFoundCardView = findViewById(R.id.item_feed_no_results_found_card_view);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mContext);
        mBursaryRecyclerView.setLayoutManager(mLayoutManager);

        findUser();

    }

    private void updateFeedBursaries(FirestorePagingOptions<Bursary> options) {

        mBursaryFeedFirestoreAdapter = new FirestorePagingAdapter<Bursary, BursaryViewHolder>(options) {
            @Override
            protected void onBindViewHolder(@NonNull BursaryViewHolder holder, int position, @NonNull Bursary bursary) {

                if (!bursary.hasFeedEssentials()) return;

                holder.bindData(bursary, mUserState);

            }

            @NonNull
            @Override
            public BursaryViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
                View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_feed_bursary, viewGroup, false);
                return new BursaryViewHolder(itemView);
            }
            @Override
            protected void onLoadingStateChanged(@NonNull LoadingState state) {
                switch (state) {
                    case LOADING_INITIAL:
                        Log.d(TAG, "Loading initial");
                        break;
                    case LOADING_MORE:
                        Log.d(TAG, "Loading more");
                        break;
                    case LOADED:
                        Log.d(TAG, "Loaded " + mBursaryFeedFirestoreAdapter.getItemCount() + " items");
                        if(mBursaryFeedFirestoreAdapter.getItemCount() == 0){
                            Log.d(TAG, "No bursary items found");
                            noBursariesFound();
                        }else {
                            Log.d(TAG, " bursary items found");
                            bursariesFound();
                        }
                        hidePDialog();
                        break;
                    case ERROR:
                        Log.d(TAG, "Error loading");
                        break;

                    case FINISHED:
                        Log.d(TAG, "Finished loading: " + mBursaryFeedFirestoreAdapter.getItemCount());
                        if(mBursaryFeedFirestoreAdapter.getItemCount() == 0){
                            Log.d(TAG, "No bursary items found");
                            noBursariesFound();
                        }
                        else
                            bursariesFound();
                        break;
                }
            }

        };

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        mBursaryRecyclerView.removeOnItemTouchListener(mPinnedOnTouchListener);
        mBursaryRecyclerView.setLayoutManager(mLayoutManager);
        mBursaryRecyclerView.setAdapter(mBursaryFeedFirestoreAdapter);
        mBursaryFeedFirestoreAdapter.notifyDataSetChanged();
        mBursaryRecyclerView.setHasFixedSize(true);
    }

    private void updateFeedPinnedBursaries(){
        Log.d(TAG, "Fetching pinned Bursary objects from Firestore " + mUserState.getPinnedBursariesSize());

        mPinnedBursaryFeedAdapter = new ViewAdapter<Bursary>(pinnedBursariesList, R.layout.item_feed_bursary, mBursaryRecyclerView, mUserState);
        if(pinnedBursariesList.size() == 0){
            Log.d(TAG, "No bursary items found");
            mBursaryRecyclerView.setVisibility(View.GONE);
            mNoResultsFoundCardView.setVisibility(View.VISIBLE);
        }else {
            Log.d(TAG, " bursary items found");
            mBursaryRecyclerView.setVisibility(View.VISIBLE);
            mNoResultsFoundCardView.setVisibility(View.GONE);
        }

        mBursaryRecyclerView.removeOnItemTouchListener(mPinnedOnTouchListener);
        mBursaryRecyclerView.addOnItemTouchListener(mPinnedOnTouchListener);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        mBursaryRecyclerView.setLayoutManager(mLayoutManager);
        mBursaryRecyclerView.setAdapter(mPinnedBursaryFeedAdapter);
        mPinnedBursaryFeedAdapter.notifyDataSetChanged();
        mBursaryRecyclerView.setHasFixedSize(true);

        hidePDialog();
    }

    private void noBursariesFound(){
        mBursaryRecyclerView.setVisibility(View.GONE);
        mNoResultsFoundCardView.setVisibility(View.VISIBLE);
        hidePDialog();
    }

    private void bursariesFound(){
        mBursaryRecyclerView.setVisibility(View.VISIBLE);
        mNoResultsFoundCardView.setVisibility(View.GONE);
        hidePDialog();
    }

    private void findUser(){
        if (null != mCurrentUser) {
            mFirebaseUtil.assignUserListener(value -> {
                if (null != value) {
                    mUserState = value.toObject(UserState.class);
                    mUserState.setId(value.getId());
                    Log.d(TAG, "BursaryFeed Users pinned: " + mUserState.getPinnedBursaries());

                    if (null != mBursaryFeedFirestoreAdapter)
                        mBursaryFeedFirestoreAdapter.notifyDataSetChanged();

                    if (null != mPinnedBursaryFeedAdapter) {
                        mPinnedBursaryFeedAdapter.updateUserState(mUserState);
                        mPinnedBursaryFeedAdapter.notifyDataSetChanged();
                    }

                } else {
                    Log.d(TAG, "No such document, should check firebase");
                }
            }, mCurrentUser.getUid());
        }
    }

    private void filterBursariesOnField(String selectedField){
        Log.d(TAG, "filterBursariesOnType: " + selectedField);
        startPDialog(mContext);

        mBursaryRecyclerView.setVisibility(View.VISIBLE);
        mNoResultsFoundCardView.setVisibility(View.GONE);

        PagedList.Config config;
        FirestorePagingOptions<Bursary> options;

        switch (selectedField) {
            case BURSARY_FIELD_PINNED:

                ((TextView)findViewById(R.id.item_feed_no_results_title))
                        .setText(getResources().getString(R.string.item_no_pinned_bursaries_found_text));

                if (null != pinnedBursariesList && pinnedBursariesList.size() > 0){
                    updateFeedPinnedBursaries();
                }

                else if (null == mPinnedQuery) {
                    mPinnedQuery = mFirebaseUtil.getFirestore()
                            .collection(BURSARIES)
                            .whereGreaterThan(CLOSING_DATE, getDate())
                            .orderBy(CLOSING_DATE);

                    mPinnedQuery.get().addOnSuccessListener( (QuerySnapshot task) ->  {
                        pinnedBursariesList = new ArrayList<>();
                        for (String bursaryId : mUserState.getPinnedBursaries()) {
                            for (Bursary bursary : task.toObjects(Bursary.class)) {
                                if (bursaryId.equals(bursary.getId())) {
                                    Log.d(TAG, "pinned found");
                                    pinnedBursariesList.add(bursary);
                                }
                            }
                        }
                        updateFeedPinnedBursaries();
                    })
                    .addOnFailureListener(command -> {
                        noBursariesFound();
                    });
                }

                else{
                    updateFeedPinnedBursaries();
                }
                break;

            case BURSARY_FIELD_PROMPT:
            case BURSARY_FIELD_ALL:
                if (null == mAllQuery) {
                    mAllQuery = mFirebaseUtil.getFirestore()
                            .collection(BURSARIES)
                            .whereGreaterThan(CLOSING_DATE, getDate())
                            .orderBy(CLOSING_DATE);
                }

                config = new PagedList.Config.Builder()
                        .setEnablePlaceholders(false)
                        .setPrefetchDistance(prefetchDistance)
                        .setPageSize(limit)
                        .build();

                options = new FirestorePagingOptions.Builder<Bursary>()
                        .setLifecycleOwner(this)
                        .setQuery(mAllQuery, config, Bursary.class)
                        .build();

                updateFeedBursaries(options);
                break;

            default:
                Log.d(TAG, "sel " + selectedField);
                ((TextView)findViewById(R.id.item_feed_no_results_title)).setText(getResources().getString(R.string.item_no_bursaries_found_text, selectedField));
                mOtherQuery = mFirebaseUtil.getFirestore()
                        .collection(BURSARIES)
                        .whereEqualTo("field." + selectedField, true)
                        .whereGreaterThan(CLOSING_DATE, getDate())
                        .orderBy(CLOSING_DATE);

                config = new PagedList.Config.Builder()
                        .setEnablePlaceholders(false)
                        .setPrefetchDistance(prefetchDistance)
                        .setPageSize(limit)
                        .build();

                options = new FirestorePagingOptions.Builder<Bursary>()
                        .setLifecycleOwner(this)
                        .setQuery(mOtherQuery, config, Bursary.class)
                        .build();

                updateFeedBursaries(options);

                break;
        }

    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.d(TAG, "create options");

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_top_toolbar, menu);

        MenuItem messageIcon = menu.findItem(R.id.action_message);
        if (mUnreadBoolean) messageIcon.setIcon(R.drawable.chat_notification);
        else messageIcon.setIcon(R.drawable.chat_white);

        Log.d(TAG, "mUnreadBoolean " + mUnreadBoolean + messageIcon.getIcon() + "white " + getResources().getDrawable(R.drawable.chat_white));

        mSpinner = findViewById(R.id.toolbar_spinner);

        if (null == fieldSelectOptions){
            fieldSelectOptions = new ArrayList<>();

            fieldSelectOptions.addAll(Arrays.asList(getResources().getStringArray(R.array.drop_down_fields)));

            fieldSelectOptions.add(BURSARY_FIELD_ALL);
            fieldSelectOptions.add(BURSARY_FIELD_PINNED);

        }
        Log.d(TAG, "items: " + fieldSelectOptions.toString());

        mSpinner.setItems(fieldSelectOptions);

        filterBursariesOnField(BURSARY_FIELD_PROMPT);
        mSpinner.setOnItemSelectedListener((view, position, id, item) -> {
            Log.d(TAG, "field select pos: " + fieldSelectOptions.toString() + " pos" + position);
            filterBursariesOnField(fieldSelectOptions.get(position));
        });

        mSpinner.setOnNothingSelectedListener(spinner -> Log.d(TAG, "Nothing selected"));

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // navigate up on back button click
        if (!CustomUtils.menuOnClick(item, mUnreadBoolean, mUserState, this)){
            onBackPressed();
        }

        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onResume(){
        super.onResume();
    }

    @Override
    public void onStart(){
        super.onStart();
    }


    @Override
    protected void onPause(){
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mFirebaseUtil.removeUserListener();
    }
}