package com.zeldav1;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.ErrorCodes;
import com.firebase.ui.auth.IdpResponse;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.utils.FirebaseAnalyticsHelper;

import java.util.Arrays;
import java.util.List;

import static com.utils.Constants.ANALYTICS_EVENT_CLOSE_LOGIN;
import static com.utils.Constants.ANALYTICS_FAIL_LOGIN;
import static com.utils.Constants.ANALYTICS_SUCCESSFUL_LOGIN;
import static com.utils.Constants.RC_SIGN_IN;

public class LoginActivity extends AppCompatActivity {
    private static final String TAG = LoginActivity.class.getSimpleName();

    private FirebaseUser mCurrentUser;
    private FirebaseAuth mAuth;
    private FirebaseAnalyticsHelper mFirebaseAnalyticsHelper;

    List<AuthUI.IdpConfig> providers = Arrays.asList(
            new AuthUI.IdpConfig.PhoneBuilder().setDefaultCountryIso("za").build(),
            new AuthUI.IdpConfig.EmailBuilder().build(),
            new AuthUI.IdpConfig.GoogleBuilder().build());

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAuth = FirebaseAuth.getInstance();
        mCurrentUser = mAuth.getCurrentUser();
        mFirebaseAnalyticsHelper = new FirebaseAnalyticsHelper(this);

        if (mCurrentUser != null){
            userStateFound();
        }
        else{
            setUpAuthUILogin();
        }
    }

    private void setUpAuthUILogin() {

        startActivityForResult(
                AuthUI.getInstance()
                        .createSignInIntentBuilder()
                        .setAvailableProviders(providers)
                        .setLogo(R.drawable.login_icon)
                        .setTheme(R.style.LoginTheme)
                        .setPrivacyPolicyUrl("https://www.zelda.org.za/privacypolicy.html")
                        .build(),
                RC_SIGN_IN);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            IdpResponse response = IdpResponse.fromResultIntent(data);

            if (resultCode == RESULT_CANCELED) {
                Log.d(TAG, "Result cancelled");
                close();
            }

            else if (resultCode == RESULT_OK) {
                Log.d(TAG, "Result ok");
                mCurrentUser = mAuth.getCurrentUser();

                if (null != response && response.isNewUser()){
                    Log.d(TAG, "new user");
                    Intent intent = new Intent(getApplicationContext(), NewUserActivity.class);
                    startActivity(intent);
                    finish();
                }
                else if (null != response && !response.isNewUser()){
                    Log.d(TAG, "Response non-null, not new user.");
                    userStateFound();
                }
                else {
                    Log.d(TAG, "Response null");
                }
            } else {
                if (null == response) {
                    Log.d(TAG, "close");
                    close();
                    return;
                }

                else if (response.getError() != null && ErrorCodes.NO_NETWORK == response.getError().getErrorCode()) {
                    Toast.makeText(getApplicationContext(),R.string.sign_in_no_network,Toast.LENGTH_SHORT).show();
                    return;
                }

                Toast.makeText(getApplicationContext(),R.string.sign_in_failed, Toast.LENGTH_SHORT).show();
                Log.e(TAG, "Sign-in error: ", response.getError());
            }
        }
    }

    private void userStateFound(){
        if (null != mCurrentUser) {
            Log.d(TAG, "User logged in: " + mCurrentUser.getUid() + " open home");

            mFirebaseAnalyticsHelper.addIntToBundle(ANALYTICS_SUCCESSFUL_LOGIN, 1);
            mFirebaseAnalyticsHelper.logFirebaseBundleMetrics(FirebaseAnalytics.Event.LOGIN);
            Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
            startActivity(intent);
            finish();
        }
    }

    private void close(){
        mFirebaseAnalyticsHelper.addIntToBundle(ANALYTICS_FAIL_LOGIN, 1);
        mFirebaseAnalyticsHelper.logFirebaseBundleMetrics(ANALYTICS_EVENT_CLOSE_LOGIN);
        finish();
    }
}