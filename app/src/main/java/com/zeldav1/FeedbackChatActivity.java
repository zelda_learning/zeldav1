package com.zeldav1;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.Query;
import com.stfalcon.chatkit.messages.MessagesList;
import com.stfalcon.chatkit.messages.MessagesListAdapter;
import com.utils.services.FirebaseFirestoreService;
import com.utils.CustomUtils;
import com.zelda.object.Message;
import com.zelda.object.UserState;

import static com.utils.Constants.MESSAGES;
import static com.utils.Constants.MESSAGE_NEW_RECEIVED;
import static com.utils.Constants.MESSAGE_NEW_SENT;
import static com.utils.Constants.MESSAGE_SEND_TIME;
import static com.utils.Constants.UNREAD_MESSAGE_TAG;
import static com.utils.Constants.USER_STATE_TAG;

public class FeedbackChatActivity extends BaseActivity {
    private static final String TAG = FeedbackChatActivity.class.getSimpleName();
    private final Context mContext = this;
    private FirebaseFirestoreService mFirebaseUtil;
    private DocumentReference firestoreMessagesDocument;
    private MenuItem mMessageMenuIcon;
    private Boolean mUnreadBoolean;

    private FirebaseUser mCurrentUser;
    private UserState mUserState;
    private EditText mMessageInput;

    private MessagesList mMessagesList;
    private MessagesListAdapter<Message> mMessagesListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mUserState = (UserState)getIntent().getSerializableExtra(USER_STATE_TAG);
        mUnreadBoolean = getIntent().getBooleanExtra(UNREAD_MESSAGE_TAG, false);
        Log.d(TAG, "FeedbackChatActivity with User: " + mUserState);

        setContentView(R.layout.activity_feedback_chat);
        setUpLinks();

        findMessages();

    }

    private void setUpLinks() {
        Log.d(TAG, "Set up links");
        startPDialog(mContext);

        Toolbar mToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mMessageInput = findViewById(R.id.message_input);
        mMessageInput.requestFocus();
        mFirebaseUtil = new FirebaseFirestoreService();

        mMessagesList = findViewById(R.id.messages_list);

        mCurrentUser = FirebaseAuth.getInstance().getCurrentUser();
        if (null != mCurrentUser) {

            firestoreMessagesDocument = mFirebaseUtil.getFirestore()
                    .collection(MESSAGES)
                    .document(mCurrentUser.getUid());

            mMessagesListAdapter = new MessagesListAdapter<>(mCurrentUser.getUid(), null);
            mMessagesList.setAdapter(mMessagesListAdapter);

            findViewById(R.id.send_button).setOnClickListener(view -> {
                submitMessage(mCurrentUser.getUid(),
                        new Message(mMessageInput.getText().toString(), false));
                mMessageInput.setText("");
            });
        }
        hidePDialog();
    }

    private void findMessages(){
        if (null != FirebaseAuth.getInstance().getCurrentUser()) {
            firestoreMessagesDocument
                    .collection(MESSAGES)
                    .orderBy(MESSAGE_SEND_TIME, Query.Direction.ASCENDING)
                    .addSnapshotListener((snapshots, e) -> {
                        if (e != null) {
                            Log.w(TAG, "listen:error", e);
                            return;
                        }
                        Message message;
                        if (null != snapshots) {
                            for (DocumentChange docChange : snapshots.getDocumentChanges()) {
                                switch (docChange.getType()) {
                                    case ADDED:
                                        message = docChange.getDocument().toObject(Message.class);
                                        if (!message.isMessageZeldaTeam()) {
                                            message.setUserId(mCurrentUser.getUid());
                                            if (mUserState != null)
                                                message.setName(mUserState.getUsername());
                                            else
                                                message.setName(mCurrentUser.getDisplayName());
                                        }
                                        mMessagesListAdapter.addToStart(message, true);
                                        (mMessagesList.getLayoutManager()).scrollToPosition(0);
                                        break;
                                    default:
                                        return;
                                }
                            }
                        }
                    });

            Log.d(TAG, "User viewed chat, take away notification");
            firestoreMessagesDocument.update(MESSAGE_NEW_RECEIVED, false);

            hidePDialog();
        }
    }

    public void submitMessage(String userId, Message message){
        Log.d(TAG, "Submitting message: " + message.getMessageText());
        CollectionReference collectionReference = mFirebaseUtil.getFirestore()
                .collection(MESSAGES).document(userId)
                .collection(MESSAGES);
        collectionReference.add(message);

        Log.d(TAG, "User submitted message, change newMessageSent notifier");
        firestoreMessagesDocument.update(MESSAGE_NEW_SENT, true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.d(TAG, "Create toolbar options");
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_top_toolbar, menu);

        mMessageMenuIcon = menu.findItem(R.id.action_message);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // navigate up on back button click
        if (!CustomUtils.menuOnClick(item, mUnreadBoolean, mUserState, this)){
            onBackPressed();
        }

        return true;
    }

    @Override
    public void onBackPressed() {
            super.onBackPressed();
            finish();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
    }
}