package com.zeldav1;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.SetOptions;
import com.utils.CustomUtils;
import com.utils.adapters.ViewPagerAdapter;
import com.utils.dialogFragments.BackSaveDialogFragment;
import com.utils.fragments.ApplicationFormOverviewFragment;
import com.utils.fragments.ApplicationFormSectionAFragment;
import com.utils.fragments.ApplicationFormSectionBFragment;
import com.utils.fragments.ApplicationFormSectionCFragment;
import com.utils.fragments.ApplicationFormSubmitFragment;
import com.utils.dialogFragments.ApplicationUploadDialogFragment;
import com.utils.services.FirebaseFirestoreService;
import com.zelda.object.ApplicationForm;
import com.zelda.object.ApplicationFormSectionA;
import com.zelda.object.ApplicationFormSectionB;
import com.zelda.object.ApplicationFormSectionC;
import com.zelda.object.Bursary;
import com.zelda.object.UserApplicationFormA;
import com.zelda.object.UserApplicationFormB;
import com.zelda.object.UserApplicationFormC;
import com.zelda.object.UserState;

import java.util.Objects;

import static com.utils.Constants.APPLICATION_FORM;
import static com.utils.Constants.APPLICATION_FORM_SECTION_A_TAG;
import static com.utils.Constants.APPLICATION_FORM_CONTINUE_TAG;
import static com.utils.Constants.APPLICATION_FORM_SECTIONS_TAG;
import static com.utils.Constants.APPLICATION_FORM_SECTION_A;
import static com.utils.Constants.APPLICATION_FORM_TAG;
import static com.utils.Constants.APPLICATION_STATUS_SUBMITTED;
import static com.utils.Constants.TAG_A;
import static com.utils.Constants.TAG_B;
import static com.utils.Constants.TAG_C;
import static com.utils.Constants.TAG_SUBMIT;
import static com.utils.Constants.USER_APPLICATION_FORM_A;
import static com.utils.Constants.BURSARY_TAG;
import static com.utils.Constants.UNREAD_MESSAGE_TAG;
import static com.utils.Constants.USER_APPLICATION_FORM_A_TAG;
import static com.utils.Constants.USER_STATE_FIRESTORE;
import static com.utils.Constants.USER_STATE_TAG;

public class ApplicationFormActivity extends BaseActivity implements
                                                            ApplicationFormSubmitFragment.ApplicationFormEventListener,
                                                            ApplicationUploadDialogFragment.ApplicationUploadDialogListener,
                                                            BackSaveDialogFragment.ApplicationBackSaveDialogFragmentListener{
    private static final String TAG = ApplicationFormActivity.class.getSimpleName();
    private final Context mContext = this;

    private FirebaseUser mCurrentUser;
    private UserState mUserState;
    private Boolean mUnreadBoolean;
    private int mApplicationFormSections;
    private ViewPagerAdapter mViewPagerAdapter;
    private ViewPager mViewPager;
    private FirebaseFirestoreService mFirebaseUtil;

    private Button mStartApplicationButton;

    private Bursary mBursary;
    private ApplicationForm mApplicationForm;
    private ApplicationFormSectionA mApplicationFormSectionA;
    private ApplicationFormSectionB mApplicationFormSectionB;
    private ApplicationFormSectionC mApplicationFormSectionC;

    private UserApplicationFormA mUserApplicationFormA, mSavedUserApplicationFormA;
    private UserApplicationFormB mUserApplicationFormB;
    private UserApplicationFormC mUserApplicationFormC;

    int containerId;

    private FragmentManager mFragmentManager;
    private ApplicationFormOverviewFragment applicationFormOverviewFragment;
    private ApplicationFormSectionAFragment applicationFormSectionAFragment;
    private ApplicationFormSectionBFragment applicationFormSectionBFragment;
    private ApplicationFormSectionCFragment applicationFormSectionCFragment;
    private ApplicationFormSubmitFragment applicationFormSubmitFragment;

    private static Bundle mBundle = new Bundle();

    private String applicationId = "";
    private String applicationFormId = "";
    private Boolean mChangesMade = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_application_form);
        containerId = R.id.application_form_fragment_container;
        hidePDialog();

        mUserState = (UserState) getIntent().getSerializableExtra(USER_STATE_TAG);
        mUnreadBoolean = getIntent().getBooleanExtra(UNREAD_MESSAGE_TAG, false);
        mBursary = (Bursary) getIntent().getSerializableExtra(BURSARY_TAG);
        mApplicationFormSections = getIntent().getIntExtra(APPLICATION_FORM_SECTIONS_TAG, 1);

        mFirebaseUtil = new FirebaseFirestoreService();

        Boolean continueApplicationForm = getIntent().getBooleanExtra(APPLICATION_FORM_CONTINUE_TAG, false);

        Log.d(TAG, "UserState: " + mUserState.getId());
        Log.d(TAG, "mUnreadBoolean: " + mUnreadBoolean);
        Log.d(TAG, "ApplicationId: " + mBursary.getId());
        Log.d(TAG, "mApplicationFormSections: " + mApplicationFormSections);
        Log.d(TAG, "Continue: " + continueApplicationForm);

        mFragmentManager = getSupportFragmentManager();
        applicationFormOverviewFragment = new ApplicationFormOverviewFragment();
        applicationFormOverviewFragment.setArguments(mBundle);
        setUpLinks();

        if (continueApplicationForm) {
            Log.d(TAG, "Application should continue. Open Manager and find application form");
            showApplicationFormSections();
        }

        else {
            Log.d(TAG, " applicationForm Should start");
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.add(containerId, applicationFormOverviewFragment);
            ft.commit();
        }
    }

    private void findApplicationForm() {
        Log.d(TAG, "find application form for applicationID: " +((Bursary)(Objects.requireNonNull(mBundle.get(BURSARY_TAG)))).getId() +
                "\nApplicationFormId:" + mUserState.getStartedApplicationIds().get(((Bursary)(Objects.requireNonNull(mBundle.get(BURSARY_TAG)))).getId()));

        applicationId = ((Bursary)(Objects.requireNonNull(mBundle.get(BURSARY_TAG)))).getId();
        applicationFormId = mUserState.getStartedApplicationIds().get(((Bursary)(Objects.requireNonNull(mBundle.get(BURSARY_TAG)))).getId());

        if (null != applicationFormId) {
            mFirebaseUtil.fetchDocument(value -> {
                if (null != value) {
                    mApplicationForm = value.toObject(ApplicationForm.class);
                    if (null != mApplicationForm) {
                        mApplicationForm.setApplicationFormId(applicationFormId);
                        applicationFormSectionAFragment.updateApplicationForm(mApplicationForm);
                        findApplicationFormSectionA();
                        findUserApplicationFormA();
                    }
                } else {
                    Log.d(TAG, "No Application Form found. Create new object: ");
                    createNewApplicationFormObject();
                }
            }, APPLICATION_FORM, applicationFormId);
        }
        else {
            Log.d(TAG, "No applicationFormId  found. Create new object: ");
            createNewApplicationFormObject();
        }
    }

    private void findApplicationFormSectionA() {
        mFirebaseUtil.fetchDocument(value -> {
            if (null != value){
                mApplicationFormSectionA = value.toObject(ApplicationFormSectionA.class);
                if (null != mApplicationFormSectionA) {
                    Log.d(TAG, "AppFormA found " + mApplicationFormSectionA.toString());
                    applicationFormSectionAFragment.updateApplicationFormSectionA(mApplicationFormSectionA);
                    applicationFormSubmitFragment.updateApplicationFormSectionA(mApplicationFormSectionA);
                }
            }
            else{
                Log.d(TAG, "No Application Form A found for ID: " + applicationId);
            }
        }, APPLICATION_FORM_SECTION_A, applicationId);
    }

    private void findUserApplicationFormA() {
        if (null != applicationFormId) {
            mFirebaseUtil.fetchDocument(value -> {
                if (null != value) {
                    Log.d(TAG, "UserFormA found");
                    mUserApplicationFormA = value.toObject(UserApplicationFormA.class);
                    if (null != mUserApplicationFormA) {
                        mSavedUserApplicationFormA = new UserApplicationFormA(mUserApplicationFormA);
                        applicationFormSectionAFragment.updateUserApplicationFormA(mUserApplicationFormA);
                        applicationFormSubmitFragment.updateUserApplicationFormA(mUserApplicationFormA);
                    }
                } else {
                    Log.d(TAG, "No UserApplicationFormA found. Create new");
                    createNewUserApplicationFormAObject();
                }
            }, USER_APPLICATION_FORM_A, applicationFormId);
        }
        else {
            Log.d("findApplicationForm", "No User Application Form . Create new object: ");
            createNewUserApplicationFormAObject();
        }
    }

    private void setUpLinks(){
        Log.d(TAG, "Set up links");

        mBundle.putSerializable(BURSARY_TAG, mBursary);
        mBundle.putSerializable(APPLICATION_FORM_TAG, mApplicationForm);
        mBundle.putSerializable(UNREAD_MESSAGE_TAG, mUnreadBoolean);
        mBundle.putSerializable(USER_STATE_TAG, mUserState);
        mBundle.putSerializable(APPLICATION_FORM_SECTIONS_TAG, mBursary.getSections());

        Toolbar mToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mViewPager = findViewById(R.id.application_form_view_pager);

        applicationFormSectionAFragment = new ApplicationFormSectionAFragment();
        applicationFormSectionAFragment.setArguments(mBundle);
        applicationFormSectionBFragment = new ApplicationFormSectionBFragment();
        applicationFormSectionBFragment.setArguments(mBundle);
        applicationFormSectionCFragment = new ApplicationFormSectionCFragment();
        applicationFormSectionCFragment.setArguments(mBundle);
        applicationFormSubmitFragment = new ApplicationFormSubmitFragment();
        applicationFormSubmitFragment.setArguments(mBundle);

        mViewPagerAdapter = loadDynamicApplicationTabs();

        mViewPager.setAdapter(mViewPagerAdapter);
        TabLayout tabLayout = findViewById(R.id.application_form_tab_layout);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                //do stuff here
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout){
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                int pageNumber = position + 1;
                if (pageNumber == mViewPagerAdapter.getCount() && !mUserApplicationFormA.equals(mSavedUserApplicationFormA)){
                    makeToast();
                    Log.d(TAG, "Not equal, upload.");
                    uploadUserApplicationFormSectionA();
                }
            }
        });
        tabLayout.setupWithViewPager(mViewPager);

        mStartApplicationButton = findViewById(R.id.application_form_start_button);

        mStartApplicationButton.setOnClickListener(arg0 -> {
            showApplicationFormSections();
        });

        updateVerification();

        hidePDialog();

    }

    private void updateVerification() {
        mCurrentUser = FirebaseAuth.getInstance().getCurrentUser();
        if (mCurrentUser != null && mCurrentUser.getProviders() != null) {
            if (mCurrentUser.getProviders().get(0).contains("password") || mCurrentUser.getProviders().get(0).contains("google")) {
                Log.d(TAG, "Password user found.");
                Log.d(TAG, "Verified: " + mCurrentUser.isEmailVerified());
                if (!mCurrentUser.isEmailVerified()) {
                    Log.d(TAG, "email not verified");
                    mStartApplicationButton.setVisibility(View.GONE);
                    ((TextView)findViewById(R.id.item_verification_text_textview)).setText(R.string.item_email_verification_application_text);
                    findViewById(R.id.application_verification_card_view).setVisibility(View.VISIBLE);
                    findViewById(R.id.application_verification_card_view).setOnClickListener(v -> mCurrentUser.sendEmailVerification().addOnCompleteListener(task -> {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "Verification email sent");
                            Toast.makeText(mContext, getResources().getString(R.string.item_email_verification_sent_text, mCurrentUser.getEmail()), Toast.LENGTH_SHORT).show();
                        } else {
                            Log.e(TAG, "sendEmailVerification", task.getException());
                            Toast.makeText(mContext, getResources().getString(R.string.item_email_verification_failed_text), Toast.LENGTH_SHORT).show();
                        }
                        findViewById(R.id.application_verification_card_view).setVisibility(View.GONE);
                    }));
                }
            }
            else if (mCurrentUser.getProviders().get(0).contains("facebook")) {
                Log.d(TAG, "facebook user found");
            }
            else if (mCurrentUser.getProviders().get(0).contains("phone")) {
                Log.d(TAG, "phone user found");
            }
            else if (mCurrentUser.getProviders().get(0).contains("google")) {
                Log.d(TAG, "google user found");
            }
        }
    }

    private void makeToast() {
        Toast.makeText(mContext, getResources().getString(R.string.application_form_application_saved_toast_text), Toast.LENGTH_SHORT).show();
    }

    private void removeOverviewFragment(){
        Log.d(TAG, "removing overviewFragment");
        getSupportFragmentManager().beginTransaction().remove(applicationFormOverviewFragment).commit();
        getSupportFragmentManager()
                .beginTransaction()
                .commit();
    }

    private void showApplicationFormSections(){
        removeOverviewFragment();
        findApplicationForm();
        mViewPager.setVisibility(View.VISIBLE);
        mStartApplicationButton.setVisibility(View.GONE);
    }

    public ViewPagerAdapter loadDynamicApplicationTabs(){
        Log.d(TAG, "Load dynamic tabs " + mApplicationFormSections);
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager(), false);

        switch(mApplicationFormSections){
            case(1):
                adapter.addFragment(applicationFormSectionAFragment, TAG_A);
                break;
            case(2):
                adapter.addFragment(applicationFormSectionAFragment, TAG_A);
                adapter.addFragment(applicationFormSectionBFragment, TAG_B);
                break;
            case(3):
                adapter.addFragment(applicationFormSectionAFragment, TAG_A);
                adapter.addFragment(applicationFormSectionBFragment, TAG_B);
                adapter.addFragment(applicationFormSectionCFragment, TAG_C);
                break;
        }

        adapter.addFragment(applicationFormSubmitFragment, TAG_SUBMIT);
        return adapter;
    }

    private void updateUserState(){
        mFirebaseUtil.setFirestoreDocument(USER_STATE_FIRESTORE, mUserState.getId(), mUserState);
    }

    private void createNewApplicationFormObject(){
        mApplicationForm = new ApplicationForm(mBursary.getId(), false, mUserState.getId());

        mFirebaseUtil.getFirestore().collection(APPLICATION_FORM)
                .add(mApplicationForm)
                .addOnSuccessListener(documentReference -> {
                    Log.d(TAG, "DocumentSnapshot written with ID: " + documentReference.getId());
                    applicationFormId = documentReference.getId();
                    mUserState.addApplicationForm(mBursary.getId(), documentReference.getId());
                    mApplicationForm.setApplicationFormId(documentReference.getId());
                    Log.d(TAG, "New application form created: " + mApplicationForm.toString());
                    updateUserState();
                    findApplicationFormSectionA();
                    findUserApplicationFormA();
                    applicationFormSectionAFragment.updateApplicationForm(mApplicationForm);
                })
                .addOnFailureListener(e -> Log.w(TAG, "Error writing document " + e));
    }

    private void createNewUserApplicationFormAObject(){

        if (applicationFormId != null) {
            mUserApplicationFormA = new UserApplicationFormA();
            Log.d(TAG, " creating new user application A: " + applicationFormId);

            applicationFormSectionAFragment.updateUserApplicationFormA(mUserApplicationFormA);
            applicationFormSubmitFragment.updateUserApplicationFormA(mUserApplicationFormA);
        }
        else
            Log.d(TAG, "applicationFormId null " );
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.d(TAG, "Create toolbar options");
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_top_toolbar, menu);

        MenuItem messageIcon = menu.findItem(R.id.action_message);
        messageIcon.setIcon(mUnreadBoolean?R.drawable.chat_notification:R.drawable.chat_white);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // navigate up on back button click
        if (!CustomUtils.menuOnClick(item, mUnreadBoolean, mUserState, this)){
            onBackPressed();
        }

        return true;
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        if (mUserApplicationFormA != null && mSavedUserApplicationFormA != null) {
            Log.d(TAG, mUserApplicationFormA.toString() + "\nSaved: " + mSavedUserApplicationFormA.toString());
            if (!mUserApplicationFormA.equals(mSavedUserApplicationFormA))
                saveChangesOnBack();
            else
                super.onBackPressed();
        }
        else {
            super.onBackPressed();
        }

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void uploadUserApplicationFormSectionA() {
        ApplicationFormSectionAFragment applicationFormSectionAFragment = (ApplicationFormSectionAFragment) getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.application_form_view_pager + ":" + 0);

        if (null != applicationFormSectionAFragment && mUserApplicationFormA.getFormValues().size() > 0){
            mUserApplicationFormA = applicationFormSectionAFragment.captureApplicationFormSectionA();
            mUserApplicationFormA.addAddressFormValue();
            Log.d(TAG, "Uploading " + mUserApplicationFormA.getFormValues());
            mSavedUserApplicationFormA = new UserApplicationFormA(mUserApplicationFormA);
            applicationFormSubmitFragment.updateUserApplicationFormA(mUserApplicationFormA);
            mFirebaseUtil.setFirestoreDocumentMerge(USER_APPLICATION_FORM_A, applicationFormId, mUserApplicationFormA);
        }
    }

    @Override
    public void submitApplicationForm(int progressValue) {
        DialogFragment dialog = ApplicationUploadDialogFragment.newInstance(progressValue);
        dialog.show(getSupportFragmentManager(), "ApplicationUploadDialogFragment");
    }

    private void saveChangesOnBack() {
        DialogFragment dialog = new BackSaveDialogFragment();
        dialog.show(getSupportFragmentManager(), "BackSaveDialogFragment");
    }

    @Override
    public void onUploadDialogPositiveClick(DialogFragment dialog) {
        applicationFormSubmitFragment.updateScreenUploadStarting();
        mApplicationForm.setSubmitted(true);
        mApplicationForm.setStatus(APPLICATION_STATUS_SUBMITTED);
        mUserState.addCompletedApplicationIds(applicationId, applicationFormId);
        updateUserState();

        mFirebaseUtil.getFirestore().collection(APPLICATION_FORM)
                .document(applicationFormId)
                .set(mApplicationForm, SetOptions.merge())
                .addOnSuccessListener(documentReference -> {
                    Log.d(TAG, "Uploaded/Updated new doc: " + mApplicationForm );
                    launchReviewActivity();
                    applicationFormSubmitFragment.updateScreenUploadFinished();
                })
                .addOnFailureListener(e -> Log.w(TAG, "Error writing document " + e));

    }

    private void launchReviewActivity() {
        Intent intent = new Intent(ApplicationFormActivity.this, ApplicationFormReviewActivity.class);
        intent.putExtra(USER_STATE_TAG, mUserState);
        intent.putExtra(UNREAD_MESSAGE_TAG, mUnreadBoolean);
        intent.putExtra(APPLICATION_FORM_TAG, mApplicationForm);
        intent.putExtra(APPLICATION_FORM_SECTION_A_TAG, mApplicationFormSectionA);
        intent.putExtra(USER_APPLICATION_FORM_A_TAG, mUserApplicationFormA);
        intent.putExtra(BURSARY_TAG, mBursary);
        startActivity(intent);
        finish();
    }

    @Override
    public void onUploadDialogNegativeClick(DialogFragment dialog) {
        Log.d(TAG, "negative click, do nothing.");
        applicationFormSubmitFragment.updateScreenUploadCancelled();
    }

    @Override
    public void onBackDialogPositiveClick(DialogFragment dialog) {
        Log.d(TAG, "positive. save, exit");
        uploadUserApplicationFormSectionA();
        Toast.makeText(mContext, getResources().getString(R.string.application_form_application_saved_toast_text), Toast.LENGTH_SHORT).show();
        finish();
    }

    @Override
    public void onBackDialogNegativeClick(DialogFragment dialog) {
        Log.d(TAG, "Negative. Dont save, exit");
        finish();
    }
}
