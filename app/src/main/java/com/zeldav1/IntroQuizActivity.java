package com.zeldav1;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageButton;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.mindorks.placeholderview.SwipeDecor;
import com.mindorks.placeholderview.SwipeDirectionalView;
import com.mindorks.placeholderview.Utils;
import com.utils.SwipeCardHelper;
import com.utils.services.FirebaseFirestoreService;
import com.zelda.object.IntroQuizQuestion;
import com.zelda.object.IntroQuizQuestions;
import com.zelda.object.UserProfilePersonality;
import com.zelda.object.UserState;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static com.utils.Constants.INTRO_QUIZ_ID;
import static com.utils.Constants.INTRO_QUIZ_LABEL;
import static com.utils.Constants.INTRO_QUIZ_TEXT;
import static com.utils.Constants.QUESTIONS;
import static com.utils.Constants.QUIZZES;
import static com.utils.Constants.USER_PROFILE_PERSONALITY;


public class IntroQuizActivity extends BaseActivity {
    private static final String TAG = IntroQuizActivity.class.getSimpleName();

    private SwipeDirectionalView mSwipeView;
    private Context mContext = this;
    private UserState mUserState;
    private boolean mUnreadBoolean;
    private FirebaseUser mCurrentUser;
    private FirebaseFirestoreService mFirebaseUtil;
    private Intent mCarriedIntent;

    SwipeDecor mSwipeDecor;

    IntroQuizQuestions mQuestions;
    static int[] mResultScore = {0, 0, 0, 0, 0};
    private static int prevScore = 0;
    private static String prevLabel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCarriedIntent = getIntent();
        setContentView(R.layout.activity_intro_quiz);
        mCurrentUser = FirebaseAuth.getInstance().getCurrentUser();
        mFirebaseUtil = new FirebaseFirestoreService();

        mSwipeView = findViewById(R.id.intro_quiz_swipe_view);
        mContext = getApplicationContext();
        loadQuestions();

        ImageButton undoButton = findViewById(R.id.intro_quiz_undo_button);
        undoButton.setOnClickListener(v -> {
            if(prevScore != 0) {
                mSwipeView.undoLastSwipe();
                undoResult();
                undoButton.setEnabled(false);
            }
        });
    }

    private void loadQuestions() {
        mFirebaseUtil.getFirestore().collection(QUIZZES).document(INTRO_QUIZ_ID)
                .get().addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        Map<String, Object> map = task.getResult().getData();
                        for (Map.Entry<String, Object> entry : map.entrySet()) {
                        if (entry.getKey().equals(QUESTIONS)) {
                                mQuestions = new IntroQuizQuestions();
                                for(Object a: (ArrayList)entry.getValue()){
                                    mQuestions.getQuestions().add(new IntroQuizQuestion(((HashMap)a).get(INTRO_QUIZ_LABEL).toString(), ((HashMap)a).get(INTRO_QUIZ_TEXT).toString()));
                                }
                            }
                        }
                        for(String question: task.getResult().getData().keySet()){
                            task.getResult().get(question);
                        }
                        setQuestionCards();
                    } else {
                        Log.d(TAG, "get failed with ", task.getException());
                    }
                });
    }

    private void setQuestionCards() {
        mSwipeView.getBuilder()
                .setDisplayViewCount(3)
                .setIsUndoEnabled(true)
                .setSwipeVerticalThreshold(Utils.dpToPx(50))
                .setSwipeHorizontalThreshold(Utils.dpToPx(50))
                .setHeightSwipeDistFactor(10)
                .setWidthSwipeDistFactor(5);

        mQuestions.shuffle();
        for (IntroQuizQuestion question: mQuestions.getQuestions()) {
            mSwipeView.addView(new SwipeCardHelper(mContext, this, question, mSwipeView));
        }

        mSwipeView.addItemRemoveListener(count -> {
            if (count == 0) {
                UserProfilePersonality upPersonality = new UserProfilePersonality();
                upPersonality.setTestCompleted(true);
                mFirebaseUtil.setFirestoreDocumentMerge(USER_PROFILE_PERSONALITY, mCurrentUser.getUid(), upPersonality);
                mFirebaseUtil.submitIntroQuizResults(mContext, mCurrentUser.getUid(), mResultScore);
                outputPage();
            }
        });

        mSwipeView.bringToFront();
        mSwipeView.invalidate();
    }

    public static void sumResults(String label, int result){
        switch (label) {
            case "A":
                mResultScore[0] += result;
                break;
            case "B":
                mResultScore[1] += result;
                break;
            case "C":
                mResultScore[2] += result;
                break;
            case "D":
                mResultScore[3] += result;
                break;
            case "E":
                mResultScore[4] += result;
                break;
        }
        prevScore = result;
        prevLabel = label;

        Log.d(TAG, "Results: " + Arrays.toString(mResultScore));

    }

    public static void undoResult(){
        switch (prevLabel) {
            case "A":
                mResultScore[0] -= prevScore;
                break;
            case "B":
                mResultScore[1] -= prevScore;
                break;
            case "C":
                mResultScore[2] -= prevScore;
                break;
            case "D":
                mResultScore[3] -= prevScore;
                break;
            case "E":
                mResultScore[4] -= prevScore;
                break;
        }
        Log.d(TAG, "Results: " + Arrays.toString(mResultScore));
    }

    private void outputPage() {
            Intent intent = new Intent(IntroQuizActivity.this, PersonalityQuizResultsActivity.class);
            intent.putExtras(mCarriedIntent);
            startActivity(intent);
            finish();
    }
}
