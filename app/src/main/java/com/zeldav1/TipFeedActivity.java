package com.zeldav1;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.Query;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;
import com.utils.CustomUtils;
import com.utils.FirebaseAnalyticsHelper;
import com.utils.services.FirebaseFirestoreService;
import com.utils.viewholders.TipViewHolder;
import com.zelda.object.Tip;
import com.zelda.object.UserState;

import static com.utils.Constants.SELECT_TIP_METRIC;
import static com.utils.Constants.TIPS;
import static com.utils.Constants.TIP_ID_TAG;
import static com.utils.Constants.UNREAD_MESSAGE_TAG;
import static com.utils.Constants.USER_STATE_TAG;

public class TipFeedActivity extends BaseActivity{
    private static final String TAG = TipFeedActivity.class.getSimpleName();
    private final Context mContext = this;
    private FirebaseAnalyticsHelper mFirebaseAnalyticsHelper;

    private BottomNavigationViewEx mBottomNavigationBar;

    private RecyclerView mRecyclerView;
    private UserState mUserState;
    private Boolean mUnreadBoolean;

    private FirestoreRecyclerAdapter<Tip, RecyclerView.ViewHolder> mTipFeedAdapter;

    private FirebaseFirestoreService mFirebaseUtil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mUserState = (UserState)getIntent().getSerializableExtra(USER_STATE_TAG);
        mUnreadBoolean = getIntent().getBooleanExtra(UNREAD_MESSAGE_TAG, false);
        mFirebaseAnalyticsHelper = new FirebaseAnalyticsHelper(this);
        Log.d(TAG, "TipsFeed with User: " + mUserState.getId());
        mFirebaseUtil = new FirebaseFirestoreService();

        setContentView(R.layout.activity_tips_feed);
        startPDialog(mContext);
        setUpLinks();

        hidePDialog();

    }

    private void setUpLinks() {
        Log.d(TAG, "Set up links");

        Toolbar mToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        mBottomNavigationBar = findViewById(R.id.bottom_nav_bar);
        mBottomNavigationBar.setSelectedItemId(R.id.action_tips);
        mBottomNavigationBar.enableAnimation(false);
        mBottomNavigationBar.enableItemShiftingMode(false);
        mBottomNavigationBar.enableShiftingMode(false);

        mRecyclerView = findViewById(R.id.tips_recycler_view);

        createFeedAdapter();

        final GestureDetector mGestureDetector = new GestureDetector(mContext, new GestureDetector.SimpleOnGestureListener(){
            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                return true;
            }
        });

        mRecyclerView.addOnItemTouchListener(
                new RecyclerView.OnItemTouchListener() {

                    @Override
                    public void onTouchEvent(RecyclerView rv, MotionEvent e) {

                    }

                    @Override
                    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

                    }

                    @Override
                    public boolean onInterceptTouchEvent(RecyclerView recyclerView, MotionEvent motionEvent) {
                        View child = recyclerView.findChildViewUnder(motionEvent.getX(), motionEvent.getY());

                        if (child != null && mGestureDetector.onTouchEvent(motionEvent)) {
                            Tip tip = mTipFeedAdapter.getItem(recyclerView.getChildLayoutPosition(child));
                            Log.d(TAG, "touch intercept Tip Feed:" + tip);
                            mFirebaseAnalyticsHelper.addFirebaseEventMetric(tip.getId(), tip.getTitle(), SELECT_TIP_METRIC);

                            Intent intent = new Intent(TipFeedActivity.this, TipViewActivity.class);
                            intent.putExtra(TIP_ID_TAG, tip);
                            intent.putExtra(USER_STATE_TAG, mUserState);
                            intent.putExtra(UNREAD_MESSAGE_TAG, mUnreadBoolean);
                            startActivity(intent);
                        }
                        return false;
                    }
                });
        mBottomNavigationBar.setSelectedItemId(R.id.action_tips);

        mBottomNavigationBar.setOnNavigationItemSelectedListener(item -> {
            Class startClass = null;
            switch (item.getItemId()) {
                case R.id.action_bursary:
                    startClass = BursaryFeedActivity.class;
                    break;

                case R.id.action_home:
                    startClass = HomeActivity.class;
                    break;

                case R.id.action_tips:
                    break;

                case R.id.action_university:
                    startClass = UniversityFeedActivity.class;
                    break;

                case R.id.action_quiz:
                    startClass = QuizFeedActivity.class;
                    break;

            }
            if (null != startClass) {
                Intent intent = new Intent(TipFeedActivity.this, startClass);
                intent.putExtra(USER_STATE_TAG, mUserState);
                intent.putExtra(UNREAD_MESSAGE_TAG, mUnreadBoolean);
                startActivity(intent);
                finish();
            }
            return false;
        });
    }


    private void createFeedAdapter(){
        Log.d(TAG, "Fetching Tip objects from Firestore");

        Query query = mFirebaseUtil.getFirestore()
                .collection(TIPS)
                .limit(50);

        FirestoreRecyclerOptions<Tip> options = new FirestoreRecyclerOptions.Builder<Tip>()
                .setQuery(query, Tip.class)
                .build();

        mTipFeedAdapter = new FirestoreRecyclerAdapter<Tip, RecyclerView.ViewHolder>(options) {
            @Override
            protected void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position, @NonNull Tip tip) {
                ((TipViewHolder)holder).bindData(tip);
            }

            @Override
            public TipViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
                View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_feed_tips, viewGroup, false);
                return new TipViewHolder(itemView);
            }

            @NonNull
            @Override
            public Tip getItem(int position){
                return super.getItem(position);
            }
        };

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mTipFeedAdapter);
        mTipFeedAdapter.notifyDataSetChanged();
        mRecyclerView.setHasFixedSize(true);
        hidePDialog();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.d(TAG, "create options");
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_top_toolbar, menu);
        MenuItem messageIcon = menu.findItem(R.id.action_message);
        messageIcon.setIcon(mUnreadBoolean?R.drawable.chat_notification:R.drawable.chat_white);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // navigate up on back button click
        if (!CustomUtils.menuOnClick(item, mUnreadBoolean, mUserState, this)){
            onBackPressed();
        }

        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void onStart(){
        super.onStart();
        mTipFeedAdapter.startListening();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mTipFeedAdapter.stopListening();
    }
}