package com.zelda.object;

public class NBTQuizReport {
    private String quizId;
    private String problem;
    private int questionNumber;

    public NBTQuizReport(){

    }

    public NBTQuizReport(String quizId, int questionNumber){
        this.quizId = quizId;
        this.questionNumber = questionNumber;
    }

    public NBTQuizReport(String quizId, String problem, int questionNumber){
        this.quizId = quizId;
        this.problem = problem;
        this.questionNumber = questionNumber;
    }

    public String getProblem() {
        return problem;
    }

    public String getQuizId() {
        return quizId;
    }

    public void setProblem(String problem) {
        this.problem = problem;
    }

    public void setQuizId(String quizId) {
        this.quizId = quizId;
    }

    @Override
    public String toString() {
        return "QuizID: " + quizId + "\n" +
                "Question: " + questionNumber + "\n" +
                "Problem " + problem;
    }

    public int getQuestionNumber() {
        return questionNumber;
    }

    public void setQuestionNumber(int questionNumber) {
        this.questionNumber = questionNumber;
    }
}
