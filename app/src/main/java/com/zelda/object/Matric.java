package com.zelda.object;

import java.io.Serializable;

public class Matric implements Serializable {

    private String currentGrade;
    private String matricCertificate;
    private String matricCompletedYear;
    private String matricCertificateUploaded;

    public Matric(){
    }

    public Matric(Matric matric) {
        this.currentGrade = matric.currentGrade;
        this.matricCertificate = matric.matricCertificate;
        this.matricCompletedYear = matric.matricCompletedYear;
        this.matricCertificateUploaded = matric.matricCertificateUploaded;
    }

    public String getCurrentGrade() {
        if(null != currentGrade){
            return currentGrade;
        } else {
            return "";
        }
    }

    public String getMatricCertificate() {
        return matricCertificate;
    }

    public String getMatricCompletedYear() {
        return matricCompletedYear;
    }

    public void setCurrentGrade(String currentGrade) {
        this.currentGrade = currentGrade;
    }

    public void setMatricCertificate(String matricCertificate) {
        this.matricCertificate = matricCertificate;
    }

    public void setMatricCompletedYear(String matricCompletedYear) {
        this.matricCompletedYear = matricCompletedYear;
    }

    public String getMatricCertificateUploaded() {
        return matricCertificateUploaded;
    }

    public void setMatricCertificateUploaded(String matricCertificateUploaded) {
        this.matricCertificateUploaded = matricCertificateUploaded;
    }

    @Override
    public boolean equals(Object object){
        if (object == this)
            return true;

        if (!(object instanceof Matric))
            return false;

        Matric matric = new Matric((Matric) object);

        boolean returnValue = false;
        if (this.currentGrade != null){
            if (matric.currentGrade == null || !this.currentGrade.equals(matric.currentGrade))
                return false;
            else
                returnValue = true;
        }
        if (this.matricCertificate != null){
            if (matric.matricCertificate == null || !this.matricCertificate.equals(matric.matricCertificate))
                return false;
            else
                returnValue = true;
        }
        if (this.matricCompletedYear != null){
            if (matric.matricCompletedYear == null || !this.matricCompletedYear.equals(matric.matricCompletedYear))
                return false;
            else
                returnValue = true;
        }
        if (this.matricCertificateUploaded != null){
            if (matric.matricCertificateUploaded == null || !this.matricCertificateUploaded.equals(matric.matricCertificateUploaded))
                return false;
            else
                returnValue = true;
        }
        return returnValue;
    }

    @Override
    public String toString() {
        return "getCurrentGrade: " + getCurrentGrade() + "\n"
                + " getMatricCertificate: " + getMatricCertificate() + "\n"
                + " getMatricCompletedYear: " + getMatricCompletedYear() + "\n"
                + " getMatricCertificateUploaded: " + getMatricCertificateUploaded() + "\n";
    }
}
