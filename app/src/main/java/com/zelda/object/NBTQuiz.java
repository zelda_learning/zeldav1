package com.zelda.object;

import com.google.firebase.database.IgnoreExtraProperties;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@IgnoreExtraProperties
public class NBTQuiz extends RootObject implements Serializable {
    private String field;
    private int type;
    private String subtitle;
    private int correct;
    private List<String> answers, workedSolutions;

    public NBTQuiz() {
        correct = 0;
        answers = new ArrayList<>();
    }

    public void setId(String id){
        super.setId(id);
    }

    public String getId(){
        return super.getId();
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public void setImageName(String imageName){
        super.setImageName(imageName);
    }

    @Override
    public String toString(){
        return "\nNBTQuiz:\n" +
                "\tid: " + super.getId() + "\n" +
                "\timage: " + super.getImageName() + "\n" +
                "\ttitle: " + super.getTitle() + "\n" +
                "\ttype: " + getType() + "\n" +
                "\tsubtitle: " + getSubtitle() + "\n" +
                "\tanswers: " + getAnswers().toString() + "\n" +
                "\tworkedSolutions: " + getWorkedSolutionsToString() + "\n" +
                "\tfield: " + getField();
    }

    public int getCorrect() {
        return correct;
    }

    public void setCorrect(int correct) {
        this.correct = correct;
    }

    public List<String> getAnswers() {
        return answers;
    }

    public void setAnswers(List<String> answers) {
        this.answers = answers;
    }

    public int getNumberQuestions(){
        return answers.size();
    }

    public List<String> getWorkedSolutions() {
        return workedSolutions;
    }

    private String getWorkedSolutionsToString() {
        if (null != workedSolutions)
            return workedSolutions.toString();
        return "";
    }

    public String getWorkedSolutionString(int pos) {
        if (null != workedSolutions)
            return workedSolutions.get(pos);
        return null;
    }

    public void setWorkedSolutions(List<String> workedSolutions) {
        this.workedSolutions = workedSolutions;
    }
}
