package com.zelda.object;

import com.google.firebase.firestore.IgnoreExtraProperties;
import com.google.firebase.firestore.PropertyName;
import com.google.firebase.firestore.Exclude;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.utils.Constants.ELEMENT_AREA;
import static com.utils.Constants.ELEMENT_GENDER;
import static com.utils.Constants.ELEMENT_INCOME;

@IgnoreExtraProperties
public class ApplicationFormSectionA implements Serializable{
    private HashMap<String, String> formValues;
    private Address homeAddress;

    @PropertyName("area")
    public List<String> area;

    @PropertyName("gender")
    public List<String> gender;

    @PropertyName("income")
    public List<String> income;

    private List<String> spinnerList;

    public ApplicationFormSectionA(HashMap<String, String> hashMap, List<String> area, List<String> gender, List<String> income){
        this.formValues = hashMap;
        this.area = area;
        this.gender = gender;
        this.income = income;
    }

    public ApplicationFormSectionA() {
        formValues = new HashMap<>();
        area = new ArrayList<>();
        gender = new ArrayList<>();
        income = new ArrayList<>();
        spinnerList = new ArrayList<>();
    }

    public Address getHomeAddress() {
        return homeAddress;
    }

    @PropertyName("address")
    public void setHomeAddress(Address homeAddress) {
        this.homeAddress = homeAddress;
    }

    public void setHomeAddressLine1(String line1) {
        this.homeAddress.setLine1(line1);
    }

    public void setHomeAddressLine2(String line2) {
        this.homeAddress.setLine2(line2);
    }

    public void setHomeAddressCity(String city) {
        this.homeAddress.setCity(city);
    }

    public void setHomeAddressProvince(String province) {
        this.homeAddress.setProvince(province);
    }

    public void setHomeAddressZip(String zip) {
        this.homeAddress.setZip(zip);
    }

    public String getFormValue(String keyValue){
        return formValues.get(keyValue);
    }

    public HashMap<String, String> getFormValues() {
        return formValues;
    }

    public void setFormValues(HashMap<String, String> formValues) {
        this.formValues = formValues;
    }

    @Exclude
    public List<String> getArea() {
        return area;
    }

    @Exclude
    public void setArea(List<String> area) {
        this.area = area;
        spinnerList.add(ELEMENT_AREA);
    }

    @Exclude
    public List<String> getGender() {
        return gender;
    }

    @Exclude
    public void setGender(List<String> gender) {
        this.gender = gender;
        spinnerList.add(ELEMENT_GENDER);
    }

    @Exclude
    public List<String> getIncome() {
        return income;
    }

    @Exclude
    public void setIncome(List<String> income) {
        this.income = income;
        spinnerList.add(ELEMENT_INCOME);
    }

    @Exclude
    public int getNumberElements(){
        return formValues.size();
    }

    @Override
    public String toString() {
        return "Form Values: " + formValues.toString() + "\n" +
                "Area: " + area.toString() + "\n" +
                "Gender: " + gender.toString() + "\n" +
                "Income: " + income.toString() + "\n";
    }

}
