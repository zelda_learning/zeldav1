package com.zelda.object;

import java.util.HashMap;

public class UserApplicationFormB {
    private HashMap<String, String> formValues;

    public UserApplicationFormB(){}

    public UserApplicationFormB(HashMap<String, String> formValues){
        this.formValues = formValues;
    }

    public HashMap<String, String> getFormValues() {
        return formValues;
    }

    public void setFormValues(HashMap<String, String> formValues) {
        this.formValues = formValues;
    }

    @Override
    public String toString() {
        return "UserApplicationFormB:" + formValues.toString();
    }
}
