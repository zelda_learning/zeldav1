package com.zelda.object;

import com.google.firebase.firestore.IgnoreExtraProperties;

import java.io.Serializable;
import java.util.HashMap;

@IgnoreExtraProperties
public class UserProfilePersonality implements Serializable {

    private HashMap<String, Integer> fieldML;
    private Boolean testCompleted;
    private Integer feedbackRating;

    public UserProfilePersonality(){

    }

    public UserProfilePersonality(UserProfilePersonality userProfilePersonality){
        if (userProfilePersonality != null){
            this.testCompleted = userProfilePersonality.testCompleted;

            if (null != userProfilePersonality.feedbackRating) {
                this.feedbackRating = userProfilePersonality.feedbackRating;
            }

            if (userProfilePersonality.fieldML != null)
                this.fieldML = new HashMap<>(userProfilePersonality.fieldML);
            else {
                this.fieldML = new HashMap<>();
            }
        }
        else {
            this.fieldML = new HashMap<>();
        }
    }

    public HashMap<String, Integer> getFieldML() {
        return fieldML;
    }

    public void setFieldML(HashMap<String, Integer> fieldML) {
        this.fieldML = fieldML;
    }

    public Integer getFeedbackRating() {
        if ( null != feedbackRating) {
            return feedbackRating;
        } else {
            return 0;
        }
    }

    public void setFeedbackRating(Integer feedbackRating) {
        this.feedbackRating = feedbackRating;
    }

    @Override
    public String toString() {
        String UP = "";
        if (fieldML != null)
            UP += "FieldML: " + getFieldML().toString() + "\n";
        UP += "testCompleted: " + testCompleted + "\n" +
                "FeedbackRating: " + feedbackRating;
        return UP;
    }

    public Boolean getTestCompleted() {
        if (null != testCompleted)
            return testCompleted;
        else
            return false;
    }

    public void setTestCompleted(Boolean testCompleted) {
        this.testCompleted = testCompleted;
    }
}
