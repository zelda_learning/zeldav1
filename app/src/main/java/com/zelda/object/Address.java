package com.zelda.object;

import android.text.TextUtils;
import android.util.Log;

import com.google.firebase.firestore.Exclude;

import java.io.Serializable;

import static com.utils.Constants.BLANK_ADDRESS_DELIMITER;
import static com.utils.Constants.STRING_ADDRESS_DELIMITER;

public class Address implements Serializable{
    private String line1;
    private String line2;
    private String city;
    private String province;
    private String zip;

    public Address(String line1, String line2, String city, String province, String zip) {
        this.line1 = line1;
        this.line2 = line2;
        this.city = city;
        this.province = province;
        this.zip = zip;
    }

    public Address() {

    }

    public Address(String addressString){
        createAddress(addressString);
    }

    public Address(Address address){
        this.line1 = address.line1;
        this.line2 = address.line2;
        this.city = address.city;
        this.province = address.province;
        this.zip = address.zip;
    }

    public String getLine1() {
        return line1;
    }

    public void setLine1(String line1) {
        this.line1 = line1;
    }

    public String getLine2() {
        return line2;
    }

    public void setLine2(String line2) {
        this.line2 = line2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    @Override
    public String toString() {
        return "L1: " + getLine1() + "\n" +
                "L2: " + getLine2() + "\n" +
                "City: " + getCity() + "\n" +
                "Province: " + getProvince() + "\n" +
                "Zip: " + getZip();
    }

    @Exclude
    public String getFirestoreString() {
        String firestoreString  = "";

        if (null != line1)
            firestoreString += line1;
        firestoreString += STRING_ADDRESS_DELIMITER;
        if (null != line2)
            firestoreString += line2;
        firestoreString += STRING_ADDRESS_DELIMITER;
        if (null != city)
            firestoreString += city;
        firestoreString += STRING_ADDRESS_DELIMITER;
        if (null != province)
            firestoreString += province;
        firestoreString += STRING_ADDRESS_DELIMITER;
        if (null != zip)
            firestoreString += zip;
        firestoreString += STRING_ADDRESS_DELIMITER;

        if (!firestoreString.equals(BLANK_ADDRESS_DELIMITER))
            return firestoreString;
        else
            return null;
    }

    public void createAddress(String addressString){
        if (addressString != null) {
            String escapeDelimiter = "\\" + STRING_ADDRESS_DELIMITER;
            String lines[] = TextUtils.split(addressString, escapeDelimiter);
            for (int lineNumber = 0; lineNumber < lines.length; lineNumber++) {
                switch (lineNumber) {
                    case 0:
                        this.setLine1(lines[lineNumber]);
                        break;

                    case 1:
                        this.setLine2(lines[lineNumber]);
                        break;

                    case 2:
                        this.setCity(lines[lineNumber]);
                        break;

                    case 3:
                        this.setProvince(lines[lineNumber]);
                        break;

                    case 4:
                        this.setZip(lines[lineNumber]);
                        break;
                }
            }
        }
    }


    @Override
    public boolean equals(Object object){
        if (object == this)
            return true;

        if (!(object instanceof Address))
            return false;

        Address address = new Address((Address) object);

        boolean returnValue = false;
        if (this.line1 != null){
            if (address.line1 == null || !this.line1.equals(address.line1))
                return false;
            else
                returnValue = true;
        }
        if (this.line2 != null){
            if (address.line2 == null || !this.line2.equals(address.line2))
                return false;
            else
                returnValue = true;
        }
        if (this.city != null){
            if (address.city == null || !this.city.equals(address.city))
                return false;
            else
                returnValue = true;
        }
        if (this.province != null){
            if (address.province == null || !this.province.equals(address.province))
                return false;
            else
                returnValue = true;
        }
        if (this.zip != null){
            if (address.zip == null || !this.zip.equals(address.zip))
                return false;
            else
                returnValue = true;
        }

        return returnValue;
    }
}
