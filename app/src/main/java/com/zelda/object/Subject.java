package com.zelda.object;

import android.os.Parcel;

import java.io.Serializable;

import static com.utils.Constants.SUBJECT_NAME_HINT;

public class Subject implements Serializable {

    private String subjectName;
    private double mark;

    public Subject() {
        subjectName = SUBJECT_NAME_HINT;
        mark = 0.0;
    }

    public Subject(Subject subject) {
        subjectName = subject.subjectName;
        mark = subject.mark;
    }

    Subject(String name, int mark) {
        this.subjectName = name;
        this.mark = mark;
    }

    public Subject(String name, double mark) {
        this.subjectName = name;
        this.mark = mark;
    }


    public void setMark(double mark) {
        this.mark = mark;
    }

    public double getMark() {
        return this.mark;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }


    @com.google.firebase.firestore.Exclude
    public boolean isHint() {
        return subjectName.equals(SUBJECT_NAME_HINT);
    }

    @Override
    public String toString() {
        return "\n\t\tSubject: " + subjectName + "   mark:  " + mark;
    }

    @Override
    public boolean equals(Object object) {
        if (object == this)
            return true;

        if (!(object instanceof Subject))
            return false;

        Subject subject = (Subject) object;

        return this.subjectName.equals(subject.subjectName) &&
                this.mark == subject.mark;

    }
}
