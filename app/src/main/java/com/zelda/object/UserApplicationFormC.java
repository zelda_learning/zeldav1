package com.zelda.object;

import java.util.HashMap;

public class UserApplicationFormC {
    private HashMap<String, String> formValues;

    public UserApplicationFormC(){}

    public UserApplicationFormC(HashMap<String, String> formValues){
        this.formValues = formValues;
    }

    public HashMap<String, String> getFormValues() {
        return formValues;
    }

    public void setFormValues(HashMap<String, String> formValues) {
        this.formValues = formValues;
    }

    @Override
    public String toString() {
        return "UserApplicationFormC:" + formValues.toString();
    }
}
