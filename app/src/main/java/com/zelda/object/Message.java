package com.zelda.object;

import com.google.firebase.Timestamp;
import com.google.firebase.firestore.Exclude;
import com.google.firebase.firestore.IgnoreExtraProperties;
import com.stfalcon.chatkit.commons.models.IMessage;
import com.stfalcon.chatkit.commons.models.IUser;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

@IgnoreExtraProperties
public class Message implements IMessage{
    private Date messageSendTime;
    private String messageText;
    private boolean messageZeldaTeam;

    public Message(){
    }

    public Message(String messageText, boolean messageZeldaTeam) {
        this.messageText = messageText;
        // false if message is from user, true if from Us
        this.messageZeldaTeam = messageZeldaTeam;

        Long timeLong = System.currentTimeMillis();
        Calendar cal = Calendar.getInstance();
        TimeZone tz = cal.getTimeZone();//get your local time zone.
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.ENGLISH);
        sdf.setTimeZone(tz);//set time zone.
        String localTime = sdf.format(new Date(timeLong));

        try {
            messageSendTime = sdf.parse(localTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        this.messageSendTime = messageSendTime;
    }

    public String getMessageText() {
        return messageText;
    }

    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }

    public boolean isMessageZeldaTeam() {
        return messageZeldaTeam;
    }

    public void setMessageZeldaTeam(boolean messageZeldaTeam) {
        this.messageZeldaTeam = messageZeldaTeam;
    }

    public Date getMessageSendTime() {
        return messageSendTime;
    }

    public void setMessageSendTime(Timestamp messageSendTime) {
        this.messageSendTime = messageSendTime.toDate();
    }

    @Override
    public String toString() {
        return "Message: " + "{\n" + messageText + "\n"
                + "fromZelda: " + messageZeldaTeam + "\n"
                + "sendTime: " + messageSendTime + "\n}";
    }


    /*
        Override methods for ChatKit
     */

    private String userId;
    private String userName;

    @Exclude
    public void setUserId(String userId){
        this.userId = userId;
    }

    @Exclude
    public void setName(String userName){
        this.userName = userName;
    }

    @Exclude
    @Override
    public String getText() {
        return this.messageText;
    }

    @Exclude
    @Override
    public IUser getUser() {
        if(this.isMessageZeldaTeam()){
            return new IUser() {
                @Override
                public String getId() {
                    return "ZELDA";
                }

                @Override
                public String getName() {
                    return "Zelda Team";
                }

                @Override
                public String getAvatar() {
                    return null;
                }
            };
        } else {
            return new IUser() {
                @Override
                public String getId() {
                    return userId;
                }

                @Override
                public String getName() {
                    return userName;
                }

                @Override
                public String getAvatar() {
                    return null;
                }
            };
        }
    }

    @Exclude
    @Override
    public String getId() {
        return this.getUser().getId();
    }

    @Exclude
    @Override
    public Date getCreatedAt() {
        return this.messageSendTime;
    }
}