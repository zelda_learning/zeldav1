package com.zelda.object;

import java.util.Comparator;

public class SubjectComparator implements Comparator<Subject> {
    @Override
    public int compare(Subject o1, Subject o2) {
        return o1.getSubjectName().compareTo(o2.getSubjectName());
    }
}