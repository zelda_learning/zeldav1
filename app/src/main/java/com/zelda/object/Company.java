package com.zelda.object;

import android.text.Html;
import android.text.Spanned;

import com.utils.ListTagHandler;

public class Company extends RootObject {
    private String field;
    private String link;
    private Spanned text;
    private String blurb;

    public Company() {}

    public String getName(){
        return super.getTitle();
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public Spanned getText() {
        return text;
    }

    public void setText(String text) {
        this.text = Html.fromHtml(text, null, new ListTagHandler());
    }

    public String getBlurb() {
        return blurb;
    }

    public void setBlurb(String blurb) {
        this.blurb = blurb;
    }

    public String toString(){
        return "" +
                getId() + ",\n" +
                getName() + ",\n" +
                getBlurb() + ",\n" +
                getField() + ",\n" +
                getLink() + ",\n" +
                getText() + ",\n" +
                getImageName();

    }
}
