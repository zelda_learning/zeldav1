package com.zelda.object;

import android.os.Build;
import android.text.Html;
import android.text.Spanned;

import com.google.firebase.Timestamp;
import com.google.firebase.firestore.IgnoreExtraProperties;
import com.utils.CustomUtils;
import com.utils.ListTagHandler;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

@IgnoreExtraProperties
public class Bursary extends RootObject implements Serializable {
    private String companyId;
    private String companyName;
    private String subtitle;
    private Date closingDate;
    private List<String> applicableFields;
    private HashMap<String, Boolean> field;
    private String supportProvided;
    private String requirements;
    private String applicationProcess;
    private String bursaryUrl;
    private boolean internalApplicationForm;
    private int sections;

    public Bursary(){}

    private String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public Spanned getSubtitle() {
        if(null != subtitle) {
            return CustomUtils.fromHtml(subtitle);
        }else{
            return null;
        }
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getClosingDate() {
        return closingDate.toString().substring(0, 10);
    }

    public void setClosingDate(Timestamp closingDate) {
        this.closingDate = closingDate.toDate();
    }

    public String getApplicableFields() {
        if(null != applicableFields){
            return ListTagHandler.listFormatter(applicableFields);
        } else {
            return null;
        }
    }

    public void setApplicableFields(List<String> applicableFields) {
        this.applicableFields = applicableFields;
    }

    public Spanned getSupportProvided() {
        if(null != supportProvided)
            return CustomUtils.fromHtml(supportProvided);
        else
            return null;
    }

    public void setSupportProvided(String supportProvided) {
        this.supportProvided = supportProvided;
    }

    public Spanned getRequirements() {
        if(null != requirements)
            return CustomUtils.fromHtml(requirements);
        else
            return null;
    }

    public void setRequirements(String requirements) {
        this.requirements = requirements;
    }

    public Spanned getApplicationProcess() {
        if(null != applicationProcess) {
            return CustomUtils.fromHtml(applicationProcess);
        } else {
            return null;
        }
    }

    public void setApplicationProcess(String applicationProcess) {
        this.applicationProcess = applicationProcess;
    }

    public String getBursaryUrl() {
        return bursaryUrl;
    }

    public void setBursaryUrl(String bursaryUrl) {
        this.bursaryUrl = bursaryUrl;
    }

    @Override
    public String toString() {
        return "Bursary:\n{\t" +
                "ID:\t\t " + getId() + "\n" +
                "Company:\t\t " + getCompanyName() + "\n" +
                "Company ID:\t " + getCompanyId() + "\n" +
                "Field:\t\t " + getField() + "\n" +
                "Applicable Fields:\t " + getApplicableFields() + "\n" +
                "URL:\t\t " + getBursaryUrl() + "\n" +
                "Closing Date:\t " + getClosingDate() + "\n}";
    }

    public HashMap<String, Boolean> getField() {
        return field;
    }

    public String getFieldString() {
        StringBuilder fieldString = new StringBuilder();
        int c = 0;
        for(String f: field.keySet()){
            fieldString.append(f);
            if (c < field.size()-1){
                fieldString.append(", ");
            }
        }
        return fieldString.toString();
    }

    public void setField(HashMap<String, Boolean> field) {
        this.field = field;
    }

    public boolean getInternalApplicationForm() {
        return internalApplicationForm;
    }

    public void setInternalApplicationForm(Boolean internalApplicationForm) {
        this.internalApplicationForm = internalApplicationForm;
    }

    public int getSections() {
        return sections;
    }

    public void setSections(int sections) {
        this.sections = sections;
    }

    public boolean hasFeedEssentials(){
        return ((null != this.closingDate) && (null != this.getTitle()) && (null != this.subtitle));
    }
}
