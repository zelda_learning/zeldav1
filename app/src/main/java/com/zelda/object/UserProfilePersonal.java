package com.zelda.object;

import android.text.TextUtils;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.Exclude;
import com.google.firebase.firestore.PropertyName;

import java.io.Serializable;
import java.util.HashMap;

import static com.utils.Constants.ELEMENT_ADDRESS;
import static com.utils.Constants.FIRESTORE_ALT_CONTACT_NUMBER;
import static com.utils.Constants.FIRESTORE_ALT_CONTACT_RELATIONSHIP;
import static com.utils.Constants.FIRESTORE_AWARDS;
import static com.utils.Constants.FIRESTORE_CAREER;
import static com.utils.Constants.FIRESTORE_COMBINED_YEARLY_INCOME;
import static com.utils.Constants.FIRESTORE_CONTACT_NUMBER;
import static com.utils.Constants.FIRESTORE_CRIMES;
import static com.utils.Constants.FIRESTORE_CULTURAL;
import static com.utils.Constants.FIRESTORE_DATE_BIRTH;
import static com.utils.Constants.FIRESTORE_DISABILITY;
import static com.utils.Constants.FIRESTORE_FIRST_NAME;
import static com.utils.Constants.FIRESTORE_GENDER;
import static com.utils.Constants.FIRESTORE_GUARDIAN_OCCUPATION;
import static com.utils.Constants.FIRESTORE_HOBBIES;
import static com.utils.Constants.FIRESTORE_HOME_LANGUAGE;
import static com.utils.Constants.FIRESTORE_HOME_ORIGIN;
import static com.utils.Constants.FIRESTORE_ID_COPY_UPLOADED;
import static com.utils.Constants.FIRESTORE_ID_NUMBER;
import static com.utils.Constants.FIRESTORE_LAST_NAME;
import static com.utils.Constants.FIRESTORE_MARITAL_STATUS;
import static com.utils.Constants.FIRESTORE_NATIONALITY;
import static com.utils.Constants.FIRESTORE_OTHER_LANGUAGE;
import static com.utils.Constants.FIRESTORE_PLACE_BIRTH;
import static com.utils.Constants.FIRESTORE_RACE;
import static com.utils.Constants.FIRESTORE_SA_CITIZEN;
import static com.utils.Constants.STRING_ADDRESS_DELIMITER;

public class UserProfilePersonal implements Serializable{
    private static final String TAG = UserProfilePersonal.class.getSimpleName();

    private String id;
    private Address address;
    private String addressString;
    private String firstName;
    private String lastName;
    private String emailAddress;
    private String gender;
    private String homeOrigin;
    private String nationality;
    private String race;
    private String awards;
    private String careerIntentions;
    private String culturalActivities;
    private String hobbies;

    private String idNumber;
    private String idUploaded;
    private String placeOfBirth;
    private String dateOfBirth;
    private String homeLanguage;
    private String otherLanguage;
    private String maritalStatus;
    private String contactNumber;
    private String guardianOccupation;
    private String altContactNumber;
    private String altContactRelationship;
    private String combinedYearlyIncome;
    private String disability;
    private String crimes;
    private String saCitizen;

    private int numberFields;
    private HashMap<String, Object> userProfilePersonalWrite;

    public UserProfilePersonal(){
        userProfilePersonalWrite = new HashMap<>();
        address = new Address();
    }

    public UserProfilePersonal(String id){
        this.id = id;
        userProfilePersonalWrite = new HashMap<>();
        address = new Address();
    }

    public UserProfilePersonal(UserProfilePersonal userProfilePersonal){
        this.id = userProfilePersonal.id;
        this.firstName = userProfilePersonal.firstName;
        this.lastName = userProfilePersonal.lastName;
        this.gender = userProfilePersonal.gender;
        this.homeOrigin = userProfilePersonal.homeOrigin;
        this.nationality = userProfilePersonal.nationality;
        this.race = userProfilePersonal.race;
        this.awards = userProfilePersonal.awards;
        this.careerIntentions = userProfilePersonal.careerIntentions;
        this.culturalActivities = userProfilePersonal.culturalActivities;
        this.hobbies = userProfilePersonal.hobbies;
        this.numberFields = userProfilePersonal.numberFields;
        this.emailAddress = userProfilePersonal.emailAddress;

        this.idNumber = userProfilePersonal.idNumber;
        this.idUploaded = userProfilePersonal.idUploaded;
        this.placeOfBirth = userProfilePersonal.placeOfBirth;
        this.dateOfBirth = userProfilePersonal.dateOfBirth;
        this.homeLanguage = userProfilePersonal.homeLanguage;
        this.otherLanguage = userProfilePersonal.otherLanguage;
        this.maritalStatus = userProfilePersonal.maritalStatus;
        this.contactNumber = userProfilePersonal.contactNumber;
        this.guardianOccupation = userProfilePersonal.guardianOccupation;
        this.altContactNumber = userProfilePersonal.altContactNumber;
        this.altContactRelationship = userProfilePersonal.altContactRelationship;
        this.combinedYearlyIncome = userProfilePersonal.combinedYearlyIncome;
        this.disability = userProfilePersonal.disability;
        this.crimes = userProfilePersonal.crimes;
        this.saCitizen = userProfilePersonal.saCitizen;

        userProfilePersonalWrite = new HashMap<>(userProfilePersonal.userProfilePersonalWrite);
        if (null != userProfilePersonal.address)
            this.address = new Address(userProfilePersonal.address);
    }

    @Exclude
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
        addNonNullValue(FIRESTORE_FIRST_NAME, firstName);
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
        addNonNullValue(FIRESTORE_LAST_NAME, lastName);
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
        addNonNullValue(FIRESTORE_GENDER, gender);
    }

    public String getHomeOrigin() {
        return homeOrigin;
    }

    public void setHomeOrigin(String homeOrigin) {
        this.homeOrigin = homeOrigin;
        addNonNullValue(FIRESTORE_HOME_ORIGIN, homeOrigin);
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
        addNonNullValue(FIRESTORE_NATIONALITY, nationality);
    }

    public String getRace() {
        return race;
    }

    public void setRace(String race) {
        this.race = race;
        addNonNullValue(FIRESTORE_RACE, race);
    }

    public String getAwards() {
        return awards;
    }

    public void setAwards(String awards) {
        this.awards = awards;
        addNonNullValue(FIRESTORE_AWARDS, awards);
    }

    public String getCareerIntentions() {
        return careerIntentions;
    }

    public void setCareerIntentions(String careerIntentions) {
        this.careerIntentions = careerIntentions;
        addNonNullValue(FIRESTORE_CAREER, careerIntentions);
    }

    public String getCulturalActivities() {
        return culturalActivities;
    }

    public void setCulturalActivities(String culturalActivities) {
        this.culturalActivities = culturalActivities;
        addNonNullValue(FIRESTORE_CULTURAL, culturalActivities);
    }

    public String getHobbies() {
        return hobbies;
    }

    public void setHobbies(String hobbies) {
        this.hobbies = hobbies;
        addNonNullValue(FIRESTORE_HOBBIES, hobbies);
    }

    @Override
    public String toString(){
        return "UPPersonal: " + "\n{" +
                "fields: " + getNumberFields() + "\n" +
                "getUserProfilePersonalWrite: " + getUserProfilePersonalWrite() + "}\n";
    }

    @Override
    public boolean equals(Object object){
        if (object == this)
            return true;

        if (!(object instanceof UserProfilePersonal))
            return false;

        UserProfilePersonal upp = (UserProfilePersonal) object;

        boolean returnValue = false;

        if (this.userProfilePersonalWrite != null){
            if (upp.userProfilePersonalWrite == null || this.userProfilePersonalWrite.size() != upp.userProfilePersonalWrite.size())
                return false;
            else if (this.userProfilePersonalWrite.equals(upp.userProfilePersonalWrite))
                return false;
            else
                returnValue = true;
        }

        if (this.id != null){
            if (upp.id == null || !this.id.equals(upp.id))
                return false;
            else
                returnValue = true;
        }
        else if (upp.id == null){
            returnValue = true;
        }

        if (this.awards != null){
            if (upp.awards == null || !this.awards.equals(upp.awards))
                return false;
            else
                returnValue = true;
        }
        else if (upp.awards == null){
            returnValue = true;
        }
        if (this.firstName != null){
            if (upp.firstName == null || !this.firstName.equals(upp.firstName))
                return false;
            else
                returnValue = true;
        }
        else if (upp.firstName == null){
            returnValue = true;
        }
        if (this.lastName != null){
            if (upp.lastName == null || !this.lastName.equals(upp.lastName))
                return false;
            else
                returnValue = true;
        }
        else if (upp.lastName == null){
            returnValue = true;
        }
        if (this.gender != null){
            if (upp.gender == null || !this.gender.equals(upp.gender))
                return false;
            else
                returnValue = true;
        }
        else if (upp.gender == null){
            returnValue = true;
        }
        if (this.race != null){
            if (upp.race == null || !this.race.equals(upp.race))
                return false;
            else
                returnValue = true;
        }
        else if (upp.race == null){
            returnValue = true;
        }
        if (this.nationality != null){
            if (upp.nationality == null || !this.nationality.equals(upp.nationality))
                return false;
            else
                returnValue = true;
        }
        else if (upp.nationality == null){
            returnValue = true;
        }
        if (this.hobbies != null){
            if (upp.hobbies == null || !this.hobbies.equals(upp.hobbies))
                return false;
            else
                returnValue = true;
        }
        else if (upp.hobbies == null){
            returnValue = true;
        }
        if (this.homeOrigin != null){
            if (upp.homeOrigin == null || !this.homeOrigin.equals(upp.homeOrigin))
                return false;
            else
                returnValue = true;
        }
        else if (upp.homeOrigin == null){
            returnValue = true;
        }
        if (this.culturalActivities != null){
            if (upp.culturalActivities == null || !this.culturalActivities.equals(upp.culturalActivities))
                return false;
            else
                returnValue = true;
        }
        else if (upp.culturalActivities == null){
            returnValue = true;
        }
        if (this.careerIntentions != null){
            if (upp.careerIntentions == null || !this.careerIntentions.equals(upp.careerIntentions))
                return false;
            else
                returnValue = true;
        }
        else if (upp.careerIntentions == null){
            returnValue = true;
        }

        return returnValue;

    }

    public boolean equalsHash(Object object) {
        if (object == this)
            return true;

        if (!(object instanceof UserProfilePersonal))
            return false;

        UserProfilePersonal upp = (UserProfilePersonal) object;

        boolean returnValue = false;

        if (this.userProfilePersonalWrite != null) {
            if (upp.userProfilePersonalWrite == null || this.userProfilePersonalWrite.size() != upp.userProfilePersonalWrite.size())
                return false;
            else if (!this.userProfilePersonalWrite.equals(upp.userProfilePersonalWrite))
                return false;
            else
                returnValue = true;
        }

        return returnValue;
    }

    private void addNonNullValue(String key, String value){
        if (value != null && !value.equals("")) {
            userProfilePersonalWrite.put(key, value);
        }
        else
            userProfilePersonalWrite.remove(key);
    }

    public HashMap<String, Object> getUserProfilePersonalWrite() {
        return userProfilePersonalWrite;
    }

    @Exclude
    public int getNumberFields() {
        return userProfilePersonalWrite.size();
    }

    @Exclude
    public String getEmailAddress() {
        return emailAddress;
    }

    @Exclude
    public String getRegisteredEmailAddress() {
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        if (null !=  currentUser && null != currentUser.getEmail())
            return currentUser.getEmail();

        return null;
    }

    @Exclude
    public FirebaseUser getCurrentUser() {
        return FirebaseAuth.getInstance().getCurrentUser();
    }

    public void put(String key, String value){
        addNonNullValue(key, value);
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    @PropertyName("address")
    public String getAddressString(){
        return address.getFirestoreString();
    }

    public void setAddress(String addressFirestoreString) {
        this.address = new Address(addressFirestoreString);
        addNonNullValue(ELEMENT_ADDRESS, addressFirestoreString);
    }

    @Exclude
    public Address getAddress() {
        return address;
    }

    @Exclude
    public void setAddress(Address address) {
        if (null != address)
            this.address = new Address(address);
    }

    public void createAddress(String addressString){
        if (addressString != null) {
            String escapeDelimiter = "\\" + STRING_ADDRESS_DELIMITER;
            String lines[] = TextUtils.split(addressString, escapeDelimiter);
            for (int lineNumber = 0; lineNumber < lines.length; lineNumber++) {
                switch (lineNumber) {
                    case 0:
                        this.address.setLine1(lines[lineNumber]);
                        break;

                    case 1:
                        this.address.setLine2(lines[lineNumber]);
                        break;

                    case 2:
                        this.address.setCity(lines[lineNumber]);
                        break;

                    case 3:
                        this.address.setProvince(lines[lineNumber]);
                        break;

                    case 4:
                        this.address.setZip(lines[lineNumber]);
                        break;
                }
            }
        }
    }

    public void setAddressLine1(String line1){
        if (address == null)
            address = new Address();
        address.setLine1(line1);
        addNonNullValue(ELEMENT_ADDRESS, address.getFirestoreString());
    }

    public void setAddressLine2(String line2){
        if (address == null)
            address = new Address();
        address.setLine2(line2);
        addNonNullValue(ELEMENT_ADDRESS, address.getFirestoreString());
    }

    public void setAddressCity(String city){
        if (address == null)
            address = new Address();
        address.setCity(city);
        addNonNullValue(ELEMENT_ADDRESS, address.getFirestoreString());
    }

    public void setAddressProvince(String province){
        if (address == null)
            address = new Address();
        address.setProvince(province);
        addNonNullValue(ELEMENT_ADDRESS, address.getFirestoreString());
    }

    public void setAddressZip(String zip){
        if (address == null)
            address = new Address();
        address.setZip(zip);
        addNonNullValue(ELEMENT_ADDRESS, address.getFirestoreString());
    }

    public String getIdUploaded() {
        return idUploaded;
    }

    public void setIdUploaded(String idUploaded) {
        this.idUploaded = idUploaded;
        addNonNullValue(FIRESTORE_ID_COPY_UPLOADED, idUploaded);
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
        addNonNullValue(FIRESTORE_ID_NUMBER, idNumber);
    }

    public String getPlaceOfBirth() {
        return placeOfBirth;
    }

    public void setPlaceOfBirth(String placeOfBirth) {
        this.placeOfBirth = placeOfBirth;
        addNonNullValue(FIRESTORE_PLACE_BIRTH, placeOfBirth);
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
        addNonNullValue(FIRESTORE_DATE_BIRTH, dateOfBirth);
    }

    public String getHomeLanguage() {
        return homeLanguage;
    }

    public void setHomeLanguage(String homeLanguage) {
        this.homeLanguage = homeLanguage;
        addNonNullValue(FIRESTORE_HOME_LANGUAGE, homeLanguage);
    }

    public String getOtherLanguage() {
        return otherLanguage;
    }

    public void setOtherLanguage(String otherLanguage) {
        this.otherLanguage = otherLanguage;
        addNonNullValue(FIRESTORE_OTHER_LANGUAGE, otherLanguage);
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
        addNonNullValue(FIRESTORE_MARITAL_STATUS, maritalStatus);
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
        addNonNullValue(FIRESTORE_CONTACT_NUMBER, contactNumber);
    }

    public String getGuardianOccupation() {
        return guardianOccupation;
    }

    public void setGuardianOccupation(String guardianOccupation) {
        this.guardianOccupation = guardianOccupation;
        addNonNullValue(FIRESTORE_GUARDIAN_OCCUPATION, guardianOccupation);
    }

    public String getAltContactNumber() {
        return altContactNumber;
    }

    public void setAltContactNumber(String altContactNumber) {
        this.altContactNumber = altContactNumber;
        addNonNullValue(FIRESTORE_ALT_CONTACT_NUMBER, altContactNumber);
    }

    public String getAltContactRelationship() {
        return altContactRelationship;
    }

    public void setAltContactRelationship(String altContactRelationship) {
        this.altContactRelationship = altContactRelationship;
        addNonNullValue(FIRESTORE_ALT_CONTACT_RELATIONSHIP, altContactRelationship);
    }

    public String getCombinedYearlyIncome() {
        return combinedYearlyIncome;
    }

    public void setCombinedYearlyIncome(String combinedYearlyIncome) {
        this.combinedYearlyIncome = combinedYearlyIncome;
        addNonNullValue(FIRESTORE_COMBINED_YEARLY_INCOME, combinedYearlyIncome);
    }

    public String getDisability() {
        return disability;
    }

    public void setDisability(String disability) {
        this.disability = disability;
        addNonNullValue(FIRESTORE_DISABILITY, disability);
    }

    public String getCrimes() {
        return crimes;
    }

    public void setCrimes(String crimes) {
        this.crimes = crimes;
        addNonNullValue(FIRESTORE_CRIMES, crimes);
    }

    public String getSaCitizen() {
        return saCitizen;
    }

    public void setSaCitizen(String saCitizen) {
        this.saCitizen = saCitizen;
        addNonNullValue(FIRESTORE_SA_CITIZEN, saCitizen);
    }

}
