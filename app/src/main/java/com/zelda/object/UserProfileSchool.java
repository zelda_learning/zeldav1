package com.zelda.object;

import android.util.Log;

import com.google.firebase.firestore.Exclude;
import com.google.firebase.firestore.PropertyName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.utils.Constants.ELEMENT_CURRENT_GRADE;
import static com.utils.Constants.ELEMENT_MATRIC_CERTIFICATE;
import static com.utils.Constants.ELEMENT_MATRIC_CERTIFICATE_UPLOADED;
import static com.utils.Constants.ELEMENT_MATRIC_COMPLETED_YEAR;
import static com.utils.Constants.ELEMENT_SCHOOL_CITY;
import static com.utils.Constants.ELEMENT_SCHOOL_ENDED;
import static com.utils.Constants.ELEMENT_SCHOOL_LINE1;
import static com.utils.Constants.ELEMENT_SCHOOL_LINE2;
import static com.utils.Constants.ELEMENT_SCHOOL_NAME;
import static com.utils.Constants.ELEMENT_SCHOOL_PROVINCE;
import static com.utils.Constants.ELEMENT_SCHOOL_STARTED;
import static com.utils.Constants.ELEMENT_SCHOOL_ZIP;
import static com.utils.Constants.FIRESTORE_CURRENT_DEGREE_STARTED_YEAR;
import static com.utils.Constants.FIRESTORE_CURRENT_GRADE;
import static com.utils.Constants.FIRESTORE_CURRENT_UNIVERSITY_DEGREE_NAME;
import static com.utils.Constants.FIRESTORE_CURRENT_UNIVERSITY_NAME;
import static com.utils.Constants.FIRESTORE_DEGREE_EXPECTED_DURATION;
import static com.utils.Constants.FIRESTORE_MATRIC_CERTIFICATE;
import static com.utils.Constants.FIRESTORE_MATRIC_CERTIFICATE_SUBJECTS;
import static com.utils.Constants.FIRESTORE_MATRIC_COMPLETED_YEAR;
import static com.utils.Constants.FIRESTORE_MC_UPLOADED;
import static com.utils.Constants.FIRESTORE_POSTMATRIC_STUDIES;
import static com.utils.Constants.FIRESTORE_PROSPECTIVE_DEGREE_APPLICATION_YEAR;
import static com.utils.Constants.FIRESTORE_SCHOOL_ADDRESS;
import static com.utils.Constants.FIRESTORE_SCHOOL_NAME;
import static com.utils.Constants.FIRESTORE_SCHOOL_REPORT_UPLOADED;
import static com.utils.Constants.FIRESTORE_SCHOOL_SUBJECTS;
import static com.utils.Constants.FIRESTORE_SCHOOL_YEAR_ENDED;
import static com.utils.Constants.FIRESTORE_SCHOOL_YEAR_STARTED;
import static com.utils.Constants.FIRESTORE_SUBJECTS_MARKS_MOST_RECENT;
import static com.utils.Constants.FIRESTORE_UNIVERSITY_SUBJECTS;
import static com.utils.Constants.FIRESTORE_UNIVERSITY_TRANSCRIPT_UPLOADED;
import static com.utils.Constants.FIRESTORE_UNIVERSITY_YEAR;
import static com.utils.Constants.UPS_CURRENT_GRADE_MATRICULATED;
import static com.utils.Constants.UPU_PROSPECTIVE;

public class UserProfileSchool implements Serializable {
    private static final String TAG = UserProfileSchool.class.getSimpleName();
    private String id;
    private List<Subject> matricCertificateSubjects, schoolSubjects, universitySubjects;
    private HashMap<String, Double> mcHashmap, sHashmap, uHashmap;
    private HashMap<String, Object> userProfileSchoolWrite;
    private int numberFields;

    private List<Subject> repeatingExamSubjectList;
    private HashMap<String, Double> repeatingExamSubjectMap;

    private School school;
    private Matric matric;
    private UserProfileUniversity userProfileUniversity;

    public UserProfileSchool() {
        userProfileSchoolWrite = new HashMap<>();
        school = new School();
        matric = new Matric();
        userProfileUniversity = new UserProfileUniversity();
    }

    public UserProfileSchool(String id) {
        this.id = id;
        userProfileSchoolWrite = new HashMap<>();
        school = new School();
        matric = new Matric();
        userProfileUniversity = new UserProfileUniversity();
    }

    public UserProfileSchool(UserProfileSchool userProfileSchool) {
        this.id = userProfileSchool.id;
        this.numberFields = userProfileSchool.numberFields;

        if (null != userProfileSchool.userProfileSchoolWrite)
            this.userProfileSchoolWrite = new HashMap<>(userProfileSchool.userProfileSchoolWrite);
        else
            this.userProfileSchoolWrite = new HashMap<>();

        if (null != userProfileSchool.matricCertificateSubjects)
            this.matricCertificateSubjects = new ArrayList<>(userProfileSchool.matricCertificateSubjects);
        else
            this.matricCertificateSubjects = new ArrayList<>();

        if (null != userProfileSchool.mcHashmap)
            this.mcHashmap = new HashMap<>(userProfileSchool.mcHashmap);
        else
            this.mcHashmap = new HashMap<>();

        if (null != userProfileSchool.schoolSubjects)
            this.schoolSubjects = new ArrayList<>(userProfileSchool.schoolSubjects);
        else
            this.schoolSubjects = new ArrayList<>();

        if (null != userProfileSchool.sHashmap)
            this.sHashmap = new HashMap<>(userProfileSchool.sHashmap);
        else
            this.sHashmap = new HashMap<>();

        if (null != userProfileSchool.universitySubjects)
            this.schoolSubjects = new ArrayList<>(userProfileSchool.universitySubjects);
        else
            this.schoolSubjects = new ArrayList<>();

        if (null != userProfileSchool.uHashmap)
            this.uHashmap = new HashMap<>(userProfileSchool.uHashmap);
        else
            this.uHashmap = new HashMap<>();

        if (null != userProfileSchool.school)
            this.school = new School(userProfileSchool.school);
        else
            this.school = new School();

        if (null != userProfileSchool.matric)
            this.matric = new Matric(userProfileSchool.matric);
        else
            this.matric = new Matric();

        if (null != userProfileSchool.userProfileUniversity)
            this.userProfileUniversity = new UserProfileUniversity(userProfileSchool.userProfileUniversity);
        else
            userProfileUniversity = new UserProfileUniversity();
    }

    @Exclude
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @com.google.firebase.firestore.Exclude
    public List<Subject> getMatricCertificateSubjects() {
        return matricCertificateSubjects;
    }

    @com.google.firebase.firestore.Exclude
    public List<Subject> getSchoolSubjects() {
        return schoolSubjects;
    }

    @com.google.firebase.firestore.Exclude
    public List<Subject> getUniversitySubjects() {
        return universitySubjects;
    }

    @PropertyName(FIRESTORE_MATRIC_CERTIFICATE_SUBJECTS)
    public HashMap<String, Double> getMcHashmap(){
        this.mcHashmap = new HashMap<>();
        if (null != matricCertificateSubjects) {
            for (Subject subject : matricCertificateSubjects) {
                this.mcHashmap.put(subject.getSubjectName(), subject.getMark());
            }
            addNonNullValue(FIRESTORE_MATRIC_CERTIFICATE_SUBJECTS, this.mcHashmap);
        }

        Log.d(TAG, "get mcHashmap: " + this.mcHashmap.toString());
        return this.mcHashmap;
    }

    @PropertyName(FIRESTORE_SCHOOL_SUBJECTS)
    public HashMap<String, Double> getsHashmap(){
        this.sHashmap = new HashMap<>();
        if (null != schoolSubjects) {
            for (Subject subject : schoolSubjects) {
                this.sHashmap.put(subject.getSubjectName(), subject.getMark());
            }
            addNonNullValue(FIRESTORE_SCHOOL_SUBJECTS, this.sHashmap);
        }

        Log.d(TAG, "get subject hashmap: " + this.sHashmap.toString());
        return this.sHashmap;
    }

    @PropertyName(FIRESTORE_UNIVERSITY_SUBJECTS)
    public HashMap<String, Double> getuHashmap(){
        this.uHashmap = new HashMap<>();
        if (null != universitySubjects) {
            for (Subject subject : universitySubjects) {
                this.uHashmap.put(subject.getSubjectName(), subject.getMark());
            }
            addNonNullValue(FIRESTORE_UNIVERSITY_SUBJECTS, this.uHashmap);
        }

        Log.d(TAG, "get uHashmap: " + this.uHashmap.toString());
        return this.uHashmap;
    }

    public void setMatricCertificateSubjects(HashMap<String, Double> mcHashmap) {
        if (null != mcHashmap){
            this.matricCertificateSubjects = new ArrayList<>();
            this.mcHashmap = new HashMap<>();
            for(String key: mcHashmap.keySet()){
                this.matricCertificateSubjects.add(new Subject(key, mcHashmap.get(key)));
                this.mcHashmap.put(key, mcHashmap.get(key));
            }

            if (matricCertificateSubjects.size() != 0)
                addNonNullValue(FIRESTORE_MATRIC_CERTIFICATE_SUBJECTS, this.mcHashmap);
        }
        else {
            addNonNullValue(FIRESTORE_SCHOOL_SUBJECTS, null);
            addNonNullValue(FIRESTORE_SUBJECTS_MARKS_MOST_RECENT, null);
        }
    }

    public void setSchoolSubjects(HashMap<String, Double> schoolSubjectsHashmap) {
        if (null != schoolSubjectsHashmap){
            this.schoolSubjects = new ArrayList<>();
            this.sHashmap = new HashMap<>();
            for(String key: schoolSubjectsHashmap.keySet()){
                this.schoolSubjects.add(new Subject(key, schoolSubjectsHashmap.get(key)));
                this.sHashmap.put(key, schoolSubjectsHashmap.get(key));
            }

            if (schoolSubjects.size() != 0)
                addNonNullValue(FIRESTORE_SCHOOL_SUBJECTS, this.sHashmap);
        }
        else {
            addNonNullValue(FIRESTORE_MATRIC_CERTIFICATE_SUBJECTS, null);
            addNonNullValue(FIRESTORE_SUBJECTS_MARKS_MOST_RECENT, null);
        }
    }

    public void setUniversitySubjects(HashMap<String, Double> universitySubjectsHashmap) {
        if (null != universitySubjectsHashmap){
            this.universitySubjects = new ArrayList<>();
            this.uHashmap = new HashMap<>();
            for(String key: universitySubjectsHashmap.keySet()){
                this.universitySubjects.add(new Subject(key, universitySubjectsHashmap.get(key)));
                this.uHashmap.put(key, universitySubjectsHashmap.get(key));
            }

            if (universitySubjects.size() != 0)
                addNonNullValue(FIRESTORE_UNIVERSITY_SUBJECTS, this.uHashmap);
        }
    }

    private void addNonNullValue(String key, Object value){
        if (value != null)
            userProfileSchoolWrite.put(key, value);
        else
            userProfileSchoolWrite.remove(key);
    }

    public HashMap<String, Object> getUserProfileSchoolWrite() {
        return userProfileSchoolWrite;
    }

    @Override
    public boolean equals(Object object){
        if (object == this)
            return true;

        if (!(object instanceof UserProfileSchool))
            return false;

        UserProfileSchool userProfileSchool = new UserProfileSchool((UserProfileSchool) object);

        boolean returnValue = false;
        if (this.school != null){
            if (userProfileSchool.school == null || !this.school.equals(userProfileSchool.school))
                return false;
            else
                returnValue = true;
        }
        if (this.matric != null){
            if (userProfileSchool.matric == null || !this.matric.equals(userProfileSchool.matric))
                return false;
            else
                returnValue = true;
        }

        return returnValue;
    }

    public boolean equalsHash(Object object){
        if (object == this)
            return true;

        if (!(object instanceof UserProfileSchool))
            return false;

        UserProfileSchool userProfileSchool = new UserProfileSchool((UserProfileSchool) object);

        boolean returnValue = false;
        if (this.userProfileSchoolWrite != null){
            if (userProfileSchool.userProfileSchoolWrite == null || !this.userProfileSchoolWrite.equals(userProfileSchool.userProfileSchoolWrite))
                return false;
            else
                returnValue = true;
        }

        return returnValue;
    }

    @Override
    public String toString() {
        StringBuilder returnValue = new StringBuilder("UPSchool: " + "\n{" +
                "ID: " + getId() + "\n" +
                "Matric: " + getMatric().toString() + "\n" +
                "Uni: " + getUserProfileUniversity() + "\n" +
                "school: " + getSchool().toString() + "\n");

        returnValue.append("matricCertificateSubjects").append("\n");
        if(null != matricCertificateSubjects) {
            for (Subject s : matricCertificateSubjects) {
                returnValue.append(s.getSubjectName()).append(" - ").append(s.getMark()).append("\n");
            }
        }

        returnValue.append("schoolSubjects").append("\n");
        if(null != schoolSubjects) {
            for (Subject s : schoolSubjects) {
                returnValue.append(s.getSubjectName()).append(" - ").append(s.getMark()).append("\n");
            }
        }

        returnValue.append("universitySubjects").append("\n");
        if(null != universitySubjects) {
            for (Subject s : universitySubjects) {
                returnValue.append(s.getSubjectName()).append(" - ").append(s.getMark()).append("\n");
            }
        }

        returnValue.append("}\n");
        return returnValue.toString();
    }

    public String getCurrentGrade() {
        return matric.getCurrentGrade();
    }

    public String getMatricCertificate() {
        return matric.getMatricCertificate();
    }

    public String getMatricCompletedYear() {
        return matric.getMatricCompletedYear();
    }

    public String getMatricCertificateUploaded() {
        return matric.getMatricCertificateUploaded();
    }

    public void setCurrentGrade(String currentGrade) {
        addMatricValue(ELEMENT_CURRENT_GRADE, currentGrade);
    }

    public void setMatricCertificate(String matricCertificate) {
        addMatricValue(ELEMENT_MATRIC_CERTIFICATE, matricCertificate);
    }

    public void setMatricCompletedYear(String matricCompleted) {
        addMatricValue(ELEMENT_MATRIC_COMPLETED_YEAR, matricCompleted);
    }

    public String getUniversityYear() {
        return userProfileUniversity.getUniversityYear();
    }

    public void setUniversityYear(String universityYear) {
        addUniversityValue(FIRESTORE_UNIVERSITY_YEAR, universityYear);
    }

    public String getPostMatricStudies() {
        return userProfileUniversity.getPostMatricStudies();
    }

    public void setPostMatricStudies(String postMatricStudies) {
        addUniversityValue(FIRESTORE_POSTMATRIC_STUDIES, postMatricStudies);
    }

    public String getDegreeDuration(){
        return userProfileUniversity.getDegreeDuration();
    }

    public void setDegreeDuration(String degreeDuration) {
        addUniversityValue(FIRESTORE_DEGREE_EXPECTED_DURATION, degreeDuration);
    }

    public String getDegreeStartedYear() {
        return userProfileUniversity.getDegreeStartedYear();
    }

    public void setDegreeStartedYear(String degreeStartedYear) {
        addUniversityValue(FIRESTORE_CURRENT_DEGREE_STARTED_YEAR, degreeStartedYear);
    }

    public String getDegreeApplicationYear() {
        return userProfileUniversity.getDegreeApplicationYear();
    }

    public void setDegreeApplicationYear(String degreeApplicationYear) {
        addUniversityValue(FIRESTORE_PROSPECTIVE_DEGREE_APPLICATION_YEAR, degreeApplicationYear);
    }

    public String getUniversityName() {
        return userProfileUniversity.getUniversityName();
    }

    public void setUniversityName(String universityName) {
        addUniversityValue(FIRESTORE_CURRENT_UNIVERSITY_NAME, universityName);
    }

    public String getDegreeName() {
        return userProfileUniversity.getDegreeName();
    }

    public void setDegreeName(String degreeName){
        addUniversityValue(FIRESTORE_CURRENT_UNIVERSITY_DEGREE_NAME, degreeName);
    }

    public String getUniversityTranscriptUploaded() {
        return userProfileUniversity.getUniversityTranscriptUploaded();
    }

    public void setSchoolReportUploaded(String schoolReportUploaded) {
        addSchoolValue(FIRESTORE_SCHOOL_REPORT_UPLOADED, schoolReportUploaded);
        addMatricValue(FIRESTORE_MC_UPLOADED, null);
    }

    public void setMatricCertificateUploaded(String matricCertificateUploaded) {
        addMatricValue(ELEMENT_MATRIC_CERTIFICATE_UPLOADED, matricCertificateUploaded);
        addSchoolValue(FIRESTORE_SCHOOL_REPORT_UPLOADED, null);
    }

    public void setUniversityTranscriptUploaded(String universityTranscriptUploaded){
        addUniversityValue(FIRESTORE_UNIVERSITY_TRANSCRIPT_UPLOADED, universityTranscriptUploaded);
    }

    @Exclude
    public int getNumberFields() {
        return userProfileSchoolWrite.size();
    }

    public void put(String key, Object value){
        addNonNullValue(key, value);
    }

    public String getSchoolName() {
        return school.getSchoolName();
    }

    public void setSchoolName(String schoolName) {
        addSchoolValue(ELEMENT_SCHOOL_NAME, schoolName);
    }

    public String getSchoolYearStarted() {
        return school.getSchoolYearStarted();
    }

    public void setSchoolYearStarted(String schoolYearStarted) {
        addSchoolValue(ELEMENT_SCHOOL_STARTED, schoolYearStarted);
    }

    public String getSchoolYearEnded() {
        return school.getSchoolYearEnded();
    }

    public void setSchoolYearEnded(String schoolYearEnded) {
        addSchoolValue(ELEMENT_SCHOOL_ENDED, schoolYearEnded);
    }

    @PropertyName("schoolAddress")
    public String getShoolAddressString(){
        return school.getSchoolAddress().getFirestoreString();
    }

    @PropertyName("schoolAddress")
    public void setSchoolAddress(String addressFirestoreString) {
        addSchoolValue(FIRESTORE_SCHOOL_ADDRESS, addressFirestoreString);
    }

    @Exclude
    public Address getSchoolAddress() {
        return this.school.getSchoolAddress();
    }

    @Exclude
    public void setSchoolAddress(Address schoolAddress) {
        if (null != schoolAddress)
            this.school.setSchoolAddress(schoolAddress);
    }

    public String getLine1() {
        return school.getSchoolAddress().getLine1();
    }

    public void setLine1(String line1) {
        addSchoolValue(ELEMENT_SCHOOL_LINE1, line1);
    }

    public String getLine2() {
        return school.getSchoolAddress().getLine2();
    }

    public void setLine2(String line2) {
        addSchoolValue(ELEMENT_SCHOOL_LINE2, line2);
    }

    public String getCity() {
        return school.getSchoolAddress().getCity();
    }

    public void setCity(String city) {
        addSchoolValue(ELEMENT_SCHOOL_CITY, city);
    }

    public String getProvince() {
        return school.getSchoolAddress().getProvince();
    }

    public void setProvince(String province) {
        addSchoolValue(ELEMENT_SCHOOL_PROVINCE, province);
    }

    public String getZip() {
        return school.getSchoolAddress().getZip();
    }

    public void setZip(String zip) {
        addSchoolValue(ELEMENT_SCHOOL_ZIP, zip);
    }

    public String getSchoolReportUploaded() {
        return school.getSchoolReportUploaded();
    }

    @Exclude
    public School getSchool() {
        return school;
    }

    @Exclude
    public Matric getMatric() {
        return matric;
    }

    @Exclude
    public UserProfileUniversity getUserProfileUniversity(){
        return userProfileUniversity;
    }

    public void addSchoolValue(String key, String value){
        if (school == null)
            school = new School();

        switch (key){
            case ELEMENT_SCHOOL_NAME:
                this.school.setSchoolName(value);
                addNonNullValue(FIRESTORE_SCHOOL_NAME, this.school.getSchoolName());
                break;
            case ELEMENT_SCHOOL_STARTED:
                this.school.setSchoolYearStarted(value);
                addNonNullValue(FIRESTORE_SCHOOL_YEAR_STARTED, this.school.getSchoolYearStarted());
                break;
            case ELEMENT_SCHOOL_ENDED:
                this.school.setSchoolYearEnded(value);
                addNonNullValue(FIRESTORE_SCHOOL_YEAR_ENDED, this.school.getSchoolYearEnded());
                break;
            case FIRESTORE_SCHOOL_ADDRESS:
                this.school.setSchoolAddress(value);
                addNonNullValue(FIRESTORE_SCHOOL_ADDRESS, this.school.getSchoolAddress().getFirestoreString());
                break;
            case ELEMENT_SCHOOL_LINE1:
                this.school.setLine1(value);
                addNonNullValue(FIRESTORE_SCHOOL_ADDRESS, this.school.getSchoolAddress().getFirestoreString());
                break;
            case ELEMENT_SCHOOL_LINE2:
                this.school.setLine2(value);
                addNonNullValue(FIRESTORE_SCHOOL_ADDRESS, this.school.getSchoolAddress().getFirestoreString());
                break;
            case ELEMENT_SCHOOL_CITY:
                this.school.setCity(value);
                addNonNullValue(FIRESTORE_SCHOOL_ADDRESS, this.school.getSchoolAddress().getFirestoreString());
                break;
            case ELEMENT_SCHOOL_PROVINCE:
                this.school.setProvince(value);
                addNonNullValue(FIRESTORE_SCHOOL_ADDRESS, this.school.getSchoolAddress().getFirestoreString());
                break;
            case ELEMENT_SCHOOL_ZIP:
                this.school.setZip(value);
                addNonNullValue(FIRESTORE_SCHOOL_ADDRESS, this.school.getSchoolAddress().getFirestoreString());
                break;

            case FIRESTORE_SCHOOL_REPORT_UPLOADED:
                this.school.setSchoolReportUploaded(value);
                addNonNullValue(FIRESTORE_SCHOOL_REPORT_UPLOADED, this.school.getSchoolReportUploaded());
                break;
        }
    }

    public void addMatricValue(String key, String value){
        if (matric == null)
            matric = new Matric();

        switch (key){
            case ELEMENT_CURRENT_GRADE:
                this.matric.setCurrentGrade(value);
                addNonNullValue(FIRESTORE_CURRENT_GRADE, this.matric.getCurrentGrade());

                if (!value.equals(UPS_CURRENT_GRADE_MATRICULATED)) {
                    Log.d(TAG, "current, remove non-matric fields");
                    addNonNullValue(FIRESTORE_MATRIC_CERTIFICATE_SUBJECTS, null);
                    addNonNullValue(FIRESTORE_MC_UPLOADED, null);
                    addNonNullValue(FIRESTORE_MATRIC_CERTIFICATE, null);
                    addNonNullValue(FIRESTORE_MATRIC_COMPLETED_YEAR, null);
                    addNonNullValue(FIRESTORE_POSTMATRIC_STUDIES, null);
                }
                else {
                    Log.d(TAG, "matriculated, remove school fields");
                    addNonNullValue(FIRESTORE_SCHOOL_SUBJECTS, null);
                    addNonNullValue(FIRESTORE_SCHOOL_REPORT_UPLOADED, null);
                }

                    break;
            case ELEMENT_MATRIC_CERTIFICATE:
                this.matric.setMatricCertificate(value);
                addNonNullValue(FIRESTORE_MATRIC_CERTIFICATE, this.matric.getMatricCertificate());
                break;
            case ELEMENT_MATRIC_COMPLETED_YEAR:
                this.matric.setMatricCompletedYear(value);
                addNonNullValue(FIRESTORE_MATRIC_COMPLETED_YEAR, this.matric.getMatricCompletedYear());
                break;
            case ELEMENT_MATRIC_CERTIFICATE_UPLOADED:
                this.matric.setMatricCertificateUploaded(value);
                addNonNullValue(FIRESTORE_MC_UPLOADED, this.matric.getMatricCertificateUploaded());
                break;
        }
    }

    public void addUniversityValue(String key, String value){
        if (userProfileUniversity == null)
            userProfileUniversity = new UserProfileUniversity();

        switch (key){
            case FIRESTORE_UNIVERSITY_YEAR:
                this.userProfileUniversity.setUniversityYear(value);
                addNonNullValue(FIRESTORE_UNIVERSITY_YEAR, this.userProfileUniversity.getUniversityYear());

                //Prospective uni student - clear matriculated fields
                if (value.equals(UPU_PROSPECTIVE)) {
                    Log.d(TAG, "Prospective, remove non-current fields");
                    addNonNullValue(FIRESTORE_UNIVERSITY_TRANSCRIPT_UPLOADED, null);
                    addNonNullValue(FIRESTORE_UNIVERSITY_SUBJECTS, null);
                    addNonNullValue(FIRESTORE_CURRENT_DEGREE_STARTED_YEAR, null);
                }
                else {
                    Log.d(TAG, "current, remove non-prosp fields");
                    addNonNullValue(FIRESTORE_PROSPECTIVE_DEGREE_APPLICATION_YEAR, null);
                }

                break;

            case FIRESTORE_POSTMATRIC_STUDIES:
                this.userProfileUniversity.setPostMatricStudies(value);
                addNonNullValue(FIRESTORE_POSTMATRIC_STUDIES, this.userProfileUniversity.getPostMatricStudies());
                break;

            case FIRESTORE_CURRENT_UNIVERSITY_NAME:
                this.userProfileUniversity.setUniversityName(value);
                addNonNullValue(FIRESTORE_CURRENT_UNIVERSITY_NAME, this.userProfileUniversity.getUniversityName());
                break;

            case FIRESTORE_CURRENT_UNIVERSITY_DEGREE_NAME:
                this.userProfileUniversity.setDegreeName(value);
                addNonNullValue(FIRESTORE_CURRENT_UNIVERSITY_DEGREE_NAME, this.userProfileUniversity.getDegreeName());
                break;

            case FIRESTORE_DEGREE_EXPECTED_DURATION:
                this.userProfileUniversity.setDegreeDuration(value);
                addNonNullValue(FIRESTORE_DEGREE_EXPECTED_DURATION, this.userProfileUniversity.getDegreeDuration());
                break;

            case FIRESTORE_PROSPECTIVE_DEGREE_APPLICATION_YEAR:
                this.userProfileUniversity.setDegreeApplicationYear(value);
                addNonNullValue(FIRESTORE_PROSPECTIVE_DEGREE_APPLICATION_YEAR, this.userProfileUniversity.getDegreeApplicationYear());
                break;

            case FIRESTORE_CURRENT_DEGREE_STARTED_YEAR:
                this.userProfileUniversity.setDegreeStartedYear(value);
                addNonNullValue(FIRESTORE_CURRENT_DEGREE_STARTED_YEAR, this.userProfileUniversity.getDegreeStartedYear());
                break;

            case FIRESTORE_UNIVERSITY_TRANSCRIPT_UPLOADED:
                this.userProfileUniversity.setUniversityTranscriptUploaded(value);
                addNonNullValue(FIRESTORE_UNIVERSITY_TRANSCRIPT_UPLOADED, this.userProfileUniversity.getUniversityTranscriptUploaded());
                break;
        }
    }
}
