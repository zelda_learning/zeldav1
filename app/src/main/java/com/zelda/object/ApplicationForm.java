package com.zelda.object;

import com.google.firebase.firestore.Exclude;

import java.io.Serializable;

public class ApplicationForm implements Serializable {
    private String applicationId;
    private boolean submitted;
    private String userId;
    private String applicationFormId;
    private String status;

    public ApplicationForm(String applicationId, boolean submitted, String userId) {
        this.applicationId = applicationId;
        this.submitted = submitted;
        this.userId = userId;
    }

    public ApplicationForm(String applicationFormId, String applicationId, boolean submitted, String userId) {
        this.applicationId = applicationId;
        this.submitted = submitted;
        this.userId = userId;
        this.applicationFormId = applicationFormId;
    }

    public ApplicationForm() {
    }

    public String getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

    public boolean isSubmitted() {
        return submitted;
    }

    public void setSubmitted(boolean submitted) {
        this.submitted = submitted;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "ApplicationForm: {\n" +
                "applicationFormId: " + applicationFormId + "\n" +
                "applicationId: " + applicationId + "\n" +
                "submitted: " + submitted + "\n" +
                "status: " + status + "\n" +
                "userId: " + userId + "\n" +
                "} ";
    }

    @Exclude
    public String getApplicationFormId() {
        return applicationFormId;
    }

    @Exclude
    public void setApplicationFormId(String applicationFormId) {
        this.applicationFormId = applicationFormId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
