package com.zelda.object;

import android.text.Html;
import android.text.Spanned;

import com.utils.ListTagHandler;

import java.io.Serializable;

public class Tip extends RootObject implements Serializable{
    private String field;
    private String text;
    private String subtitle;

    public Tip() {}

    public Spanned getText() {
        return Html.fromHtml(text, null, new ListTagHandler());
    }

    public String getStringText(){
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String toString(){
        return "Tip:\n{" +
                "UUID: " + super.getId() + "\n" +
                "Title: " + super.getTitle() + "\n" +
                "Subtitle: " + getSubtitle() + "\n" +
                "Text: " + getText() + "\n" +
                "Image Name: " + super.getImageName() + "\n" +
                "Field: " + getField() + "}";
    }
}
