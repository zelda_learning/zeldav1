package com.zelda.object;

import android.util.Log;

import com.google.firebase.Timestamp;
import com.google.firebase.database.IgnoreExtraProperties;
import com.google.firebase.database.PropertyName;
import com.utils.Constants;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

@IgnoreExtraProperties
public class UserState extends RootObject implements Serializable {
    private static final String TAG = UserState.class.getSimpleName();

    private boolean submitted;
    private int progress;
    private String optIn;
    private String username;
    private String schoolName;
    private Date cbaEditTimeStamp;
    private HashMap<String, Boolean> fieldPreference;
    private List<String> pinnedBursaries = null;
    private HashMap<String, String> startedApplicationIds = null, completedApplicationIds = null;
    private HashMap<String, Integer> completedNBTQuizzes;
    private List<String> callsToAction = null;

    public UserState(){
        fieldPreference = new HashMap<>();
        completedNBTQuizzes = new HashMap<>();
        pinnedBursaries = new ArrayList<>();
        startedApplicationIds = new HashMap<>();
        callsToAction = new ArrayList<>();
        progress = Constants.INITIAL_PROGRESS;
        optIn = Constants.FALSE;
    }

    public UserState(UserState userState){
        this.setId(userState.getId());
        this.submitted = userState.submitted;
        this.username = userState.getUsername();
        this.fieldPreference = new HashMap<>(userState.fieldPreference);
        this.progress = userState.getProgress();
        this.optIn = userState.getOptIn();
        this.schoolName = userState.schoolName;
        this.cbaEditTimeStamp = userState.cbaEditTimeStamp;
    }

    public UserState(String UUID, String username, HashMap<String, Boolean> fieldPreference){
        super(UUID, username);
        this.fieldPreference = fieldPreference;
        completedNBTQuizzes = new HashMap<>();
    }

    public UserState(String UUID, String username, HashMap<String, Boolean> fieldPreference, String optIn){
        super(UUID, username);
        this.fieldPreference = fieldPreference;
        completedNBTQuizzes = new HashMap<>();
        this.optIn = optIn;
    }

    public String getOptIn() {
        if(this.optIn != null)
            return this.optIn;
        else
            return Constants.FALSE;
    }

    @com.google.firebase.firestore.Exclude
    public boolean isOptedIn(){
        return this.optIn != null && !this.optIn.equals(Constants.FALSE);
    }

    public void setOptIn(String optIn) {
        this.optIn = optIn;
    }

    public int getProgress() {
        return progress;
    }

    public void setProgress(int progress) {
        this.progress = progress;
    }

    public void incrementProgress(){
        this.progress += 15;
        if(this.progress > 100) {
            this.progress = 100;
        }
    }

    @PropertyName("username")
    public String getUsername() {
        return this.username;
    }

    @PropertyName("username")
    public void setUsername(String username) {
        this.username = username;
    }

    @com.google.firebase.firestore.Exclude
    public String getTitle(){
        return super.getTitle();
    }

    @com.google.firebase.firestore.Exclude
    public void setTitle(String username) {
        super.setTitle(username);
    }

    @com.google.firebase.firestore.Exclude
    public String getId(){
        return super.getId();
    }

    @com.google.firebase.firestore.Exclude
    public void setId(String id){
        super.setId(id);
    }

    public HashMap<String, Boolean> getFieldPreference(){
        if (fieldPreference != null) {
            return fieldPreference;
        }
        return null;
    }

    @com.google.firebase.firestore.Exclude
    public ArrayList<String> getFieldPreferenceStringArray(){
        return new ArrayList<>(fieldPreference.keySet());
    }

    @com.google.firebase.firestore.Exclude
    public int getFieldPreferenceSize(){
        return fieldPreference.size();
    }

    public void setFieldPreference(HashMap<String, Boolean> fieldPreference) {
        this.fieldPreference = fieldPreference;
    }

    public HashMap<String, Integer> getCompletedNBTQuizzes() {
        return completedNBTQuizzes;
    }

    @com.google.firebase.firestore.Exclude
    public int getCompletedNBTQuizzesSize(){
        if (null != completedNBTQuizzes){
            return completedNBTQuizzes.size();
        }
        return 0;
    }

    public void setCompletedNBTQuizzes(HashMap<String, Integer> completedNBTQuizzes) {
        this.completedNBTQuizzes = completedNBTQuizzes;
    }

    @com.google.firebase.firestore.Exclude
    public String getLastPinnedBursary(){
        if (null != pinnedBursaries){
            if (pinnedBursaries.size() > 0)
                return pinnedBursaries.get(pinnedBursaries.size()-1);
            return null;
        }
        return null;
    }

    public List<String> getPinnedBursaries() {
        return pinnedBursaries;
    }

    public void addBursaryToPinned(String bursaryID){
        pinnedBursaries.add(bursaryID);
    }

    public void removeBursaryFromPinnedList(String bursaryID){
        for(int c = 0; c < pinnedBursaries.size(); c++){
            if (bursaryID.equals(pinnedBursaries.get(c))){
                Log.d("removeBursaryFromPList", ": Match. Remove.");
                pinnedBursaries.remove(c);
            }
        }
        Log.d("removeBursaryFromPList", pinnedBursaries.toString());
    }

    @com.google.firebase.firestore.Exclude
    public int getPinnedBursariesSize() {
        if (null != pinnedBursaries)
            return pinnedBursaries.size();
        else
            return 0;
    }

    public void setPinnedBursaries(List<String> pinnedBursaries) {
        this.pinnedBursaries = pinnedBursaries;
    }

    public HashMap<String, String> getStartedApplicationIds() {
        return startedApplicationIds;
    }

    @com.google.firebase.firestore.Exclude
    public int getStartedApplicationMapSize(){
        if (null != startedApplicationIds){
            return startedApplicationIds.size();
        }
        return 0;
    }

    public void setStartedApplicationIds(HashMap<String, String> startedApplicationIds) {
        this.startedApplicationIds = startedApplicationIds;
    }

    public void addApplicationForm(String applicationId, String applicationFormId){
        if (startedApplicationIds != null && !startedApplicationIds.keySet().contains(applicationId)){
            startedApplicationIds.put(applicationId, applicationFormId);
        }
    }

    @com.google.firebase.firestore.Exclude
    public int getApplicationFormSize() {
        if (null != startedApplicationIds)
            return startedApplicationIds.size();
        else
            return 0;
    }

    public void removeApplicationFormList(String applicationFormId){
        for(int c = 0; c < startedApplicationIds.size(); c++){
            if (applicationFormId.equals(startedApplicationIds.get(c))){
                Log.d("removeApplicationForm", ": Match. Remove.");
                startedApplicationIds.remove(c);
            }
        }
        Log.d("removeApplicationForm", startedApplicationIds.toString());
    }

    public List<String> getCallsToAction() {
        return callsToAction;
    }

    public void removeCallToActionFromList(String callToActionId){
        for(int c = 0; c < callsToAction.size(); c++){
            if (callToActionId.equals(callsToAction.get(c))){
                Log.d("removeCallToAction", ": Match. Remove.");
                callsToAction.remove(c);
            }
        }
    }

    @com.google.firebase.firestore.Exclude
    public int getCallsToActionSize() {
        if (null != callsToAction)
            return callsToAction.size();
        else
            return 0;
    }

    public void setCallsToAction(List<String> callsToAction) {
        if(null == callsToAction){
            this.callsToAction = new ArrayList<>();
        } else {
            this.callsToAction = callsToAction;
        }
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    @Override
    public boolean equals(Object object){
        if (object == this)
            return true;

        if (!(object instanceof UserState))
            return false;

        UserState userState = (UserState) object;

        return this.getUsername().equals(userState.getUsername()) &&
                this.getProgress() == userState.getProgress() &&
                this.getOptIn().equals(userState.getOptIn()) &&
                this.getFieldPreference().equals(userState.getFieldPreference());
    }

    @Override
    public String toString() {
        return "UserState:\n{"
                + "progress:\t" + getProgress() + "\n"
                + "Username:\t " + getUsername() + "\n"
                + "Submitted:\t " + getSubmitted()+ "\n"
                + "id:\t\t" + super.getId() + "\n"
                + "pinnedBursaries:\t\t" + getPinnedBursaries() + "\n"
                + "startedApplicationForm:\t\t" + getStartedApplicationIds() + "\n"
                + "completedApplicationForm:\t\t" + getCompletedApplicationIds() + "\n"
                + "quizResults:\t\t" + getCompletedNBTQuizzes() + "\n"
                + "callsToAction:\t\t" + getCallsToAction() + "\n"
                + "fieldPreference:\t" + getFieldPreference() + "\n"
                + "optInField:\t" + getOptIn() + "\n"
                + "schoolName:\t" + getSchoolName() + "\n"
                + "}";
    }

    public void setSubmitted(boolean submitted){
        this.submitted = submitted;
    }

    public boolean getSubmitted() {
        return submitted;
    }

    public HashMap<String, String> getCompletedApplicationIds() {
        return completedApplicationIds;
    }

    @com.google.firebase.firestore.Exclude
    public int getCompletedApplicationMapSize(){
        if (null != completedApplicationIds){
            return completedApplicationIds.size();
        }
        return 0;
    }

    public void setCompletedApplicationIds(HashMap<String, String> completedApplicationIds) {
        this.completedApplicationIds = completedApplicationIds;
    }

    public void addCompletedApplicationIds(String keyValue, String mapValue ) {
        if (null ==completedApplicationIds)
            completedApplicationIds = new HashMap<>();

        this.completedApplicationIds.put(keyValue, mapValue);
        this.startedApplicationIds.remove(keyValue);
    }

    public void addFieldOfInterest(String fieldKey){
        fieldPreference.put(fieldKey, true);
    }

    public Date getCbaEditTimeStamp() {
        return cbaEditTimeStamp;
    }

    public void setCbaEditTimeStamp(Timestamp cbaEditTimeStamp) {
        if (null != cbaEditTimeStamp)
            this.cbaEditTimeStamp = cbaEditTimeStamp.toDate();
        else
            this.cbaEditTimeStamp = null;
    }
}
