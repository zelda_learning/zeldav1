package com.zelda.object;

import java.io.Serializable;

public class UniversityDetails implements Serializable {
    private String universityName;
    private String degreeName;
    private String degreeDuration;
    private String degreeCurrentYear;
    private String degreeStartedYear;
    private String degreeApplicationYear;

    public UniversityDetails(){

    }

    public UniversityDetails(UniversityDetails universityDetails){
        this.universityName = universityDetails.universityName;
        this.degreeName = universityDetails.degreeName;
        this.degreeDuration = universityDetails.degreeDuration;
        this.degreeCurrentYear = universityDetails.degreeCurrentYear;
        this.degreeStartedYear = universityDetails.degreeStartedYear;
        this.degreeApplicationYear = universityDetails.degreeApplicationYear;
    }


    public String getUniversityName() {
        return universityName;
    }

    public void setUniversityName(String universityName) {
        this.universityName = universityName;
    }

    public String getDegreeName() {
        return degreeName;
    }

    public void setDegreeName(String degreeName) {
        this.degreeName = degreeName;
    }

    public String getDegreeDuration() {
        return degreeDuration;
    }

    public void setDegreeDuration(String degreeDuration) {
        this.degreeDuration = degreeDuration;
    }

    public String getDegreeCurrentYear() {
        return degreeCurrentYear;
    }

    public void setDegreeCurrentYear(String degreeCurrentYear) {
        this.degreeCurrentYear = degreeCurrentYear;
    }

    public String getDegreeStartedYear() {
        return degreeStartedYear;
    }

    public void setDegreeStartedYear(String degreeStartedYear) {
        this.degreeStartedYear = degreeStartedYear;
    }

    public String getDegreeApplicationYear() {
        return degreeApplicationYear;
    }

    public void setDegreeApplicationYear(String degreeApplicationYear) {
        this.degreeApplicationYear = degreeApplicationYear;
    }

}
