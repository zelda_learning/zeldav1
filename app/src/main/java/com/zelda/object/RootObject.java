package com.zelda.object;

import java.io.Serializable;

public class RootObject implements Serializable {
    private String id;
    private String title;
    private String imageName;

    RootObject() {
    }

    RootObject(String id, String title) {
        this.id = id;
        this.title = title;
    }

    RootObject(String id, String title, String imageName) {
        this.id = id;
        this.title = title;
        this.imageName = imageName;
    }

    public String getId() {
        return id;
    }

    @com.google.firebase.firestore.Exclude
    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @com.google.firebase.firestore.Exclude
    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }
}
