package com.zelda.object;

import android.text.TextUtils;
import android.util.Log;

import com.google.firebase.firestore.Exclude;
import com.google.firebase.firestore.PropertyName;

import java.io.Serializable;
import java.util.Arrays;
import java.util.HashMap;

import static com.utils.Constants.ELEMENT_ADDRESS;
import static com.utils.Constants.ELEMENT_NO_ADDRESS;
import static com.utils.Constants.STRING_ADDRESS_DELIMITER;

public class UserApplicationFormA implements Serializable{
    private static final String TAG = UserApplicationFormA.class.getSimpleName();

    private HashMap<String, String> formValues;
    private Address address;
    private String addressFirestoreString;
    private String numberedAddressField;

    public UserApplicationFormA(){
        formValues = new HashMap<>();
        address = new Address();
    }

    public UserApplicationFormA(UserApplicationFormA userApplicationFormA){
        this.formValues = new HashMap<>(userApplicationFormA.formValues);
        if (null!= userApplicationFormA.address)
            this.address = new Address(userApplicationFormA.address);
        if (null!= userApplicationFormA.addressFirestoreString)
            this.addressFirestoreString = userApplicationFormA.addressFirestoreString;
    }

    public UserApplicationFormA(HashMap<String, String> formValues){
        this.formValues = formValues;
    }

    public String getFormValue(String keyValue){
        return formValues.get(keyValue);
    }

    public HashMap<String, String> getFormValues() {
        if (null != formValues)
            return formValues;
        else
            return new HashMap<>();
    }

    public boolean containsKey(String key) {
        return formValues.containsKey(key);
    }

    public boolean containsValue(String value){
        return formValues.containsValue(value);
    }

    public void setFormValues(HashMap<String, String> formValues) {
        this.formValues = formValues;
    }

    public void createAddress(String addressString){
        if (addressString != null) {
            Log.d(TAG, addressString);
            String escapeDelimiter = "\\" + STRING_ADDRESS_DELIMITER;
            String lines[] = TextUtils.split(addressString, escapeDelimiter);
            Log.d(TAG, Arrays.toString(lines));
            for (int lineNumber = 0; lineNumber < lines.length; lineNumber++) {
                switch (lineNumber) {
                    case 0:
                        this.address.setLine1(lines[lineNumber]);
                        break;

                    case 1:
                        this.address.setLine2(lines[lineNumber]);
                        break;

                    case 2:
                        this.address.setCity(lines[lineNumber]);
                        break;

                    case 3:
                        this.address.setProvince(lines[lineNumber]);
                        break;

                    case 4:
                        this.address.setZip(lines[lineNumber]);
                        break;
                }
            }
        }
    }

    @Override
    public String toString() {
        return "UserApplicationFormA Form Values:" + getFormValues().toString() + "\n"
                + "Address: " + getAddressString();
    }

    public void put(String userValue, String s) {
        formValues.put(userValue, s);
    }

    @Exclude
    public int getNumberElements() {
        return formValues.size();
    }

    @Exclude
    public Address getAddress() {
        return address;
    }

    @PropertyName("address")
    private String getAddressString(){
        return address.getFirestoreString();
    }

    @Exclude
    public String getNumberedAddressString(){
        return this.formValues.get(numberedAddressField);
    }

    public void setAddressLine1(String line1){
        if (address == null)
            address = new Address();

        address.setLine1(line1);
        addAddressFormValue();
    }

    public void setAddressLine2(String line2){
        if (address == null)
            address = new Address();

        address.setLine2(line2);
        addAddressFormValue();
    }

    public void setAddressCity(String city){
        if (address == null)
            address = new Address();

        address.setCity(city);
        addAddressFormValue();
    }

    public void setAddressProvince(String province){
        if (address == null)
            address = new Address();

        address.setProvince(province);
        addAddressFormValue();
    }

    public void setAddressZip(String zip){
        if (address == null)
            address = new Address();

        address.setZip(zip);
        addAddressFormValue();
    }

    @Exclude
    public void setAddress(Address address) {
        this.address = address;
    }

    @Exclude
    public String getAddressFirestoreString() {
        return addressFirestoreString;
    }

    @PropertyName("address")
    public void setAddressFirestoreString(String addressFirestoreString) {
        Log.d(TAG, "FirestoreAddress: " + addressFirestoreString);
        this.addressFirestoreString = addressFirestoreString;
        put(ELEMENT_ADDRESS, addressFirestoreString);
    }

    public void addAddressFormValue(){
        if (numberedAddressField != null && !getAddressString().equals(ELEMENT_NO_ADDRESS)) {
            formValues.put(numberedAddressField, getAddressString());
        }
    }

    @Override
    public boolean equals(Object o){
        if (this == o)
            return true;

        if (o == null || getClass() != o.getClass())
            return false;

        UserApplicationFormA comparisonUserApplicationFormA = (UserApplicationFormA) o;
        return this.formValues.equals(comparisonUserApplicationFormA.formValues);
    }

    @Exclude
    public String getNumberedAddressField() {
        return numberedAddressField;
    }

    @Exclude
    public void setNumberedAddressField(String numberedAddressField) {
        this.numberedAddressField = numberedAddressField;
        createAddress(getFormValue(numberedAddressField));
    }
}
