package com.zelda.object;

import static com.utils.Constants.FIRESTORE_BR;
import static com.utils.Constants.FIRESTORE_NEWLINE;

public class About {
    private String aboutText;
    private String privacyPolicyText;
    private String termsConditionsText;

    public About(){

    }

    public String getAboutText() {
        return aboutText;
    }

    public void setAboutText(String aboutText) {
        this.aboutText = aboutText.replace(FIRESTORE_NEWLINE, FIRESTORE_BR);
    }

    public String getPrivacyPolicyText() {
        return privacyPolicyText;
    }

    public void setPrivacyPolicyText(String privacyPolicyText) {
        this.privacyPolicyText = privacyPolicyText.replace(FIRESTORE_NEWLINE, FIRESTORE_BR);
    }

    public String getTermsConditionsText() {
        return termsConditionsText;
    }

    public void setTermsConditionsText(String termsConditionsText) {
        this.termsConditionsText = termsConditionsText.replace(FIRESTORE_NEWLINE, FIRESTORE_BR);
    }

    @Override
    public String toString() {
        return "About: " + getAboutText() + "\n" +
               "Terms: " + getTermsConditionsText() + "\n" +
               "Privacy: " + getPrivacyPolicyText() + "\n";
    }
}
