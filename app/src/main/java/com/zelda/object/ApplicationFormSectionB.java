package com.zelda.object;

import com.google.firebase.firestore.PropertyName;

import java.util.HashMap;

public class ApplicationFormSectionB {
    private String matricYear;
    private Address schoolAddress;
    private String schoolName;

    public ApplicationFormSectionB(String matricYear, Address schoolAddress, String schoolName) {
        this.matricYear = matricYear;
        this.schoolAddress = schoolAddress;
        this.schoolName = schoolName;
    }

    public ApplicationFormSectionB() {
    }

    public String getMatricYear() {
        return matricYear;
    }

    public void setMatricYear(String matricYear) {
        this.matricYear = matricYear;
    }

    public Address getSchoolAddress() {
        return schoolAddress;
    }

    public void setSchoolAddress(Address schoolAddress) {
        this.schoolAddress = schoolAddress;
    }

    public void setSchoolAddressLine1(String line1) {
        this.schoolAddress.setLine1(line1);
    }

    public void setSchoolAddressLine2(String line2) {
        this.schoolAddress.setLine2(line2);
    }

    public void setSchoolAddressCity(String city) {
        this.schoolAddress.setCity(city);
    }

    public void setSchoolAddressProvince(String province) {
        this.schoolAddress.setProvince(province);
    }

    public void setSchoolAddressZip(String zip) {
        this.schoolAddress.setZip(zip);
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

}
