package com.zelda.object;

public class ApplicationFormSectionC {
    private String awards;
    private String careerIntentions;
    private String culturalActivities;
    private String guardianName;
    private String guardianNo;
    private String hobbies;
    private String otherInfo;
    private String relativePetroSA;

    public ApplicationFormSectionC(String awards, String careerIntentions, String culturalActivities, String guardianName, String guardianNo, String hobbies, String otherInfo, String relativePetroSA) {
        this.awards = awards;
        this.careerIntentions = careerIntentions;
        this.culturalActivities = culturalActivities;
        this.guardianName = guardianName;
        this.guardianNo = guardianNo;
        this.hobbies = hobbies;
        this.otherInfo = otherInfo;
        this.relativePetroSA = relativePetroSA;
    }

    public ApplicationFormSectionC() {
    }

    public String getAwards() {
        return awards;
    }

    public void setAwards(String awards) {
        this.awards = awards;
    }

    public String getCareerIntentions() {
        return careerIntentions;
    }

    public void setCareerIntentions(String careerIntentions) {
        this.careerIntentions = careerIntentions;
    }

    public String getCulturalActivities() {
        return culturalActivities;
    }

    public void setCulturalActivities(String culturalActivities) {
        this.culturalActivities = culturalActivities;
    }

    public String getGuardianName() {
        return guardianName;
    }

    public void setGuardianName(String guardianName) {
        this.guardianName = guardianName;
    }

    public String getGuardianNo() {
        return guardianNo;
    }

    public void setGuardianNo(String guardianNo) {
        this.guardianNo = guardianNo;
    }

    public String getHobbies() {
        return hobbies;
    }

    public void setHobbies(String hobbies) {
        this.hobbies = hobbies;
    }

    public String getOtherInfo() {
        return otherInfo;
    }

    public void setOtherInfo(String otherInfo) {
        this.otherInfo = otherInfo;
    }

    public String getRelativePetroSA() {
        return relativePetroSA;
    }

    public void setRelativePetroSA(String relativePetroSA) {
        this.relativePetroSA = relativePetroSA;
    }
}
