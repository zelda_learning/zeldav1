package com.zelda.object;


import android.text.Html;
import android.text.Spanned;

import com.google.firebase.Timestamp;
import com.utils.ListTagHandler;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class University extends RootObject implements Serializable{
    private String text;
    private String location;
    private Date closingDate;
    private String link;
    private List<String> qualifications;
    private String NBTRequirement;
    private String type;

    public University(String UUID, String title, String text, String location, Date closingDate, String link, List<String> qualifications, String NBTRequirement, String type) {
        super(UUID, title);
        this.text = text;
        this.location = location;
        this.closingDate = closingDate;
        this.link = link;
        this.qualifications = qualifications;
        this.NBTRequirement = NBTRequirement;
        this.type = type;
    }

    public University() {
    }

    public University(University university) {
        super(university.getId(), university.getTitle(), university.getImageName());
        this.text = university.text;
        this.location = university.location;
        this.closingDate = university.closingDate;
        this.link = university.link;
        this.qualifications = university.qualifications;
        this.NBTRequirement = university.NBTRequirement;
        this.type = university.type;
    }

    public Spanned getText() {
        return Html.fromHtml(text, null, new ListTagHandler());
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String universityLocation) {
        this.location = universityLocation;
    }

    public String getClosingDate() {
        return closingDate.toString().substring(0, 10);
    }

    public void setClosingDate(Timestamp universityApplicationOpenDate) {
        this.closingDate = universityApplicationOpenDate.toDate();
    }

    public void setLink(String universityLink) {
        this.link = universityLink;
    }

    public String getLink() {
        return link;
    }

    public List<String> getQualifications() {
        return qualifications;
    }

    public void setQualifications(List<String> qualifications) {
        this.qualifications = qualifications;
    }

    public String getNBTRequirement() {
        return NBTRequirement;
    }

    public void setNBTRequirement(String NBTRequirement) {
        this.NBTRequirement = NBTRequirement;
    }

    public String getType() {
        return type;
    }

    public boolean hasType(String type){
        return this.type.contains(type);
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString(){
        return "\nUniversity: " + super.getId();

    }
}
