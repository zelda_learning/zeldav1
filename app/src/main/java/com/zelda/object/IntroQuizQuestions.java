package com.zelda.object;

import com.google.firebase.database.IgnoreExtraProperties;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@IgnoreExtraProperties
public class IntroQuizQuestions implements Serializable {
    private List<IntroQuizQuestion> questions;

    public IntroQuizQuestions() {
        this.questions = new ArrayList<>();
    }

    @com.google.firebase.firestore.Exclude
    public List<IntroQuizQuestion> getQuestions() {
        return questions;
    }

    public void setQuestions(List<IntroQuizQuestion> questions) {
        this.questions = questions;
    }

    public IntroQuizQuestion get(int position){
        return this.questions.get(position);
    }

    @com.google.firebase.firestore.Exclude
    public int getSize(){
        return questions.size();
    }

    public void shuffle(){
        Collections.shuffle(this.questions);
    }
}
