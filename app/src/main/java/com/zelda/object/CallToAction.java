package com.zelda.object;

import com.google.firebase.firestore.Exclude;
import com.google.firebase.firestore.IgnoreExtraProperties;

@IgnoreExtraProperties
public class CallToAction {
    private String id;
    private boolean active;
    private String headingText;
    private String yes;
    private String no;

    private String type;
    private String hint;
    private String subText;

    public CallToAction(String id, boolean active, String headingText, String yes, String no) {
        this.id = id;
        this.active = active;
        this.headingText = headingText;
        this.yes = yes;
        this.no = no;
    }

    public CallToAction() {
    }

    @Exclude
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getHeadingText() {
        return headingText;
    }

    public void setHeadingText(String headingText) {
        this.headingText = headingText;
    }

    public String getYes() {
        return yes;
    }

    public void setYes(String yes) {
        this.yes = yes;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getHint() {
        return hint;
    }

    public void setHint(String hint) {
        this.hint = hint;
    }

    public String getSubText() {
        return subText;
    }

    public void setSubText(String subText) {
        this.subText = subText;
    }

    @Override
    public String toString() {
        return "ID: " + getId() + "\n" +
                "headingText: " + getHeadingText() + "\n";
    }
}
