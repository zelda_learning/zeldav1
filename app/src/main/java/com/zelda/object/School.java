package com.zelda.object;

import java.io.Serializable;

public class School implements Serializable {
    private String schoolName;
    private String schoolYearStarted;
    private String schoolYearEnded;
    private Address schoolAddress;
    private String schoolReportUploaded;

    public School() {
        this.schoolAddress = new Address();
    }

    public School(String stringAddress) {
        this.schoolAddress = new Address(stringAddress);
    }

    public School(School school){
        this.schoolName = school.schoolName;
        this.schoolYearStarted = school.schoolYearStarted;
        this.schoolYearEnded = school.schoolYearEnded;
        this.schoolReportUploaded = school.schoolReportUploaded;
        if (null != school.schoolAddress)
            this.schoolAddress = new Address(school.schoolAddress);
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getSchoolYearStarted() {
        return schoolYearStarted;
    }

    public void setSchoolYearStarted(String schoolYearStarted) {
        this.schoolYearStarted = schoolYearStarted;
    }

    public String getSchoolYearEnded() {
        return schoolYearEnded;
    }

    public void setSchoolYearEnded(String schoolYearEnded) {
        this.schoolYearEnded = schoolYearEnded;
    }

    public Address getSchoolAddress() {
        return schoolAddress;
    }

    public void setSchoolAddress(Address address){
        if (null != address)
            this.schoolAddress = new Address(address);
    }

    public void setSchoolAddress(String address){
        this.schoolAddress = new Address(address);
    }

    public String getLine1() {
        return schoolAddress.getLine1();
    }

    public void setLine1(String line1) {
        if (schoolAddress == null)
            schoolAddress = new Address();
        this.schoolAddress.setLine1(line1);
    }

    public String getLine2() {
        return schoolAddress.getLine2();
    }

    public void setLine2(String line2) {
        if (schoolAddress == null)
            schoolAddress = new Address();
        this.schoolAddress.setLine2(line2);
    }

    public String getCity() {
        return schoolAddress.getCity();
    }

    public void setCity(String city) {
        if (schoolAddress == null)
            schoolAddress = new Address();
        this.schoolAddress.setCity(city);
    }

    public String getProvince() {
        return schoolAddress.getProvince();
    }

    public void setProvince(String province) {
        if (schoolAddress == null)
            schoolAddress = new Address();
        this.schoolAddress.setProvince(province);
    }

    public String getZip() {
        return schoolAddress.getZip();
    }

    public void setZip(String zip) {
        if (schoolAddress == null)
            schoolAddress = new Address();
        this.schoolAddress.setZip(zip);
    }

    public String getSchoolReportUploaded() {
        return schoolReportUploaded;
    }

    public void setSchoolReportUploaded(String schoolReportUploaded) {
        this.schoolReportUploaded = schoolReportUploaded;
    }

    @Override
    public String toString() {
        return "School Name: " + getSchoolName() + "\n"
                        + " start: " + getSchoolYearStarted() + "\n"
                        + " end: " + getSchoolYearEnded() + "\n"
                        + " addr: " + getSchoolAddress().toString() + "\n"
                        + " report: " + getSchoolReportUploaded() + "\n";
    }

    @Override
    public boolean equals(Object object){
        if (object == this)
            return true;

        if (!(object instanceof School))
            return false;

        School school = new School((School) object);

        boolean returnValue = false;
        if (this.schoolName != null){
            if (school.schoolName == null || !this.schoolName.equals(school.schoolName))
                return false;
            else
                returnValue = true;
        }
        if (this.schoolYearStarted != null){
            if (school.schoolYearStarted == null || !this.schoolYearStarted.equals(school.schoolYearStarted))
                return false;
            else
                returnValue = true;
        }
        if (this.schoolYearEnded != null){
            if (school.schoolYearEnded == null || !this.schoolYearEnded.equals(school.schoolYearEnded))
                return false;
            else
                returnValue = true;
        }
        if (this.schoolAddress != null){
            if (school.schoolAddress == null || !this.schoolAddress.equals(school.schoolAddress))
                return false;
            else
                returnValue = true;
        }
        if (this.schoolReportUploaded != null){
            if (school.schoolReportUploaded == null || !this.schoolReportUploaded.equals(school.schoolReportUploaded))
                return false;
            else
                returnValue = true;
        }

        return returnValue;
    }
}
