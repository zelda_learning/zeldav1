package com.zelda.object;

import java.io.Serializable;

public class UserProfileUniversity implements Serializable {
    private static final String TAG = UserProfileUniversity.class.getSimpleName();

    private String universityYear;
    private String universityName;
    private String degreeName;
    private String degreeStartedYear;
    private String degreeApplicationYear;

    private String universityTranscriptUploaded;
    private String degreeDuration;
    private String postMatricStudies;


    public UserProfileUniversity(){

    }

    public UserProfileUniversity(UserProfileUniversity userProfileUniversity){
        this.universityYear = userProfileUniversity.universityYear;
        this.postMatricStudies = userProfileUniversity.postMatricStudies;
        this.universityName = userProfileUniversity.universityName;
        this.degreeName = userProfileUniversity.degreeName;
        this.degreeDuration = userProfileUniversity.degreeDuration;
        this.degreeStartedYear = userProfileUniversity.degreeStartedYear;
        this.degreeApplicationYear = userProfileUniversity.degreeApplicationYear;
        this.universityTranscriptUploaded = userProfileUniversity.universityTranscriptUploaded;
    }

    public String getUniversityYear() {
        if(null != this.universityYear){
            return universityYear;
        }
        return "";
    }

    public void setUniversityYear(String universityYear) {
        this.universityYear = universityYear;
    }

    public String getPostMatricStudies() {
        return postMatricStudies;
    }

    public void setPostMatricStudies(String postMatricStudies) {
        this.postMatricStudies = postMatricStudies;
    }

    public String getUniversityName() {
        return universityName;
    }

    public void setUniversityName(String universityName) {
        this.universityName = universityName;
    }

    public String getDegreeName() {
        return degreeName;
    }

    public void setDegreeName(String degreeName) {
        this.degreeName = degreeName;
    }

    public String getDegreeDuration() {
        return degreeDuration;
    }

    public void setDegreeDuration(String degreeDuration) {
        this.degreeDuration = degreeDuration;
    }

    public String getDegreeStartedYear() {
        return degreeStartedYear;
    }

    public void setDegreeStartedYear(String degreeStartedYear) {
        this.degreeStartedYear = degreeStartedYear;
    }

    public String getDegreeApplicationYear() {
        return degreeApplicationYear;
    }

    public void setDegreeApplicationYear(String degreeApplicationYear) {
        this.degreeApplicationYear = degreeApplicationYear;
    }

    public String getUniversityTranscriptUploaded() {
        return universityTranscriptUploaded;
    }

    public void setUniversityTranscriptUploaded(String universityTranscriptUploaded) {
        this.universityTranscriptUploaded = universityTranscriptUploaded;
    }

    @Override
    public String toString() {
        return "\nUni Year: " + universityYear + "\n" +
                        "PostMatric " + postMatricStudies + "\n" +
                "universityName: " + universityName + "\n" +
                "degreeName " + degreeName + "\n" +
                "degreeDuration " + degreeDuration + "\n" +
                "degreeStartedYear " + degreeStartedYear + "\n" +
                "universityTranscriptUploaded " + universityTranscriptUploaded + "\n" +
                "degreeApplicationYear " + degreeApplicationYear;
    }
}
