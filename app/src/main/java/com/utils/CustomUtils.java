package com.utils;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.Nullable;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ImageView;

import com.firebase.ui.auth.AuthUI;
import com.zelda.object.Bursary;
import com.zelda.object.Company;
import com.zelda.object.NBTQuiz;
import com.zelda.object.Tip;
import com.zelda.object.University;
import com.zelda.object.UserState;
import com.zeldav1.AboutActivity;
import com.zeldav1.FAQActivity;
import com.zeldav1.FeedbackChatActivity;
import com.zeldav1.LoginActivity;
import com.zeldav1.R;

import java.lang.reflect.Type;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static com.utils.Constants.BURSARIES;
import static com.utils.Constants.COMPANIES;
import static com.utils.Constants.FIRESTORE_BOLD;
import static com.utils.Constants.FIRESTORE_BOLD_CLOSE;
import static com.utils.Constants.FIRESTORE_BR;
import static com.utils.Constants.FIRESTORE_BR_CLOSE;
import static com.utils.Constants.FIRESTORE_MDASH;
import static com.utils.Constants.FIRESTORE_NDASH;
import static com.utils.Constants.FIRESTORE_NEWLINE;
import static com.utils.Constants.FIRESTORE_STRONG;
import static com.utils.Constants.FIRESTORE_STRONG_CLOSE;
import static com.utils.Constants.QUIZZES;
import static com.utils.Constants.RESOURCES;
import static com.utils.Constants.STRING_BOLD;
import static com.utils.Constants.STRING_BOLD_CLOSE;
import static com.utils.Constants.STRING_MDASH;
import static com.utils.Constants.STRING_NDASH;
import static com.utils.Constants.STRING_NEWLINE;
import static com.utils.Constants.UNIVERSITIES;
import static com.utils.Constants.UNREAD_MESSAGE_TAG;
import static com.utils.Constants.USER_STATES;
import static com.utils.Constants.USER_STATE_TAG;
import static com.utils.services.FirebaseStorageService.setGlideImage;

public class CustomUtils {
    private static final String TAG = CustomUtils.class.getSimpleName();

    public CustomUtils() {

    }

    private static final Map<Class,String> typeMap = new HashMap<>();

    static {
        typeMap.put(Bursary.class, BURSARIES);
        typeMap.put(Company.class, COMPANIES);
        typeMap.put(University.class, UNIVERSITIES);
        typeMap.put(UserState.class, USER_STATES);
        typeMap.put(Tip.class, RESOURCES);
        typeMap.put(NBTQuiz.class, QUIZZES);
    }

    public static boolean menuOnClick(MenuItem item, boolean mUnreadBoolean, UserState mUserState, Activity activity){
        Log.d(TAG, "menu on click custom");
        Intent intent = null;
        // Handle item selection
        switch (item.getItemId()) {
            case android.R.id.home: {
                return false;
            }

            case R.id.action_message:
                Log.d(TAG, "Message Page!");
                intent = new Intent(activity, FeedbackChatActivity.class);
                break;
            case R.id.action_logout:
                Log.d(TAG, "Logout!");
                AuthUI.getInstance()
                        .signOut(activity)
                        .addOnCompleteListener(task -> {
                            // user is now signed out
                            activity.startActivity(new Intent(activity, LoginActivity.class));
                            activity.finish();
                        });

                break;
            case R.id.action_about_us:
                Log.d(TAG, "About Us!");
                intent = new Intent(activity, AboutActivity.class);
                break;
            case R.id.action_faq:
                Log.d(TAG, "FAQ Page!");
                intent = new Intent(activity, FAQActivity.class);
                break;
            default:
                Log.d(TAG, "Other button pressed?");
                break;
        }

        if (null != intent){
            intent.putExtra(UNREAD_MESSAGE_TAG, mUnreadBoolean);

            if(null != mUserState)
                intent.putExtra(USER_STATE_TAG, mUserState);

            launchActivity(intent, activity);
        }
        return true;
    }

    private static void launchActivity(Intent intent, Activity activity){
        activity.overridePendingTransition( 0, 0);
        activity.startActivity(intent);
        activity.overridePendingTransition( 0, 0);
    }

    public static String setImageNameOnType(Type type, String id){
        if (type.equals(UserState.class)) {
            id = "1";
        }
        else if(type.equals(NBTQuiz.class)) {
            return QUIZZES + "/" + id + "/" + appendSuffix(id);
        }
        return typeMap.get(type) + "/" + appendSuffix(id);
    }

    public static String setImageNameOnType(Type type, String id, String image){
        if(type.equals(NBTQuiz.class)){
            return QUIZZES + "/" + id + "/" + appendSuffix(id);
        } else {
            return typeMap.get(type) + "/" + appendSuffix(image);
        }
    }

    private static String appendSuffix(String imageName){
        String fileExtension = ".png";
        String fileExtension2 = ".jpg";
        if(null != imageName) {
            imageName = imageName.replace(".JPG", fileExtension2);
            if (!imageName.contains(fileExtension) && !imageName.contains(fileExtension2)) {
                imageName = imageName + ".jpg";
            }
        }
        return imageName;
    }

    public static void setUniversityScrollImage(final ImageView imageView, String imageId, String universityId){
        String imageUid = UNIVERSITIES + "/" + universityId + "/" + imageId + ".jpg";
        setGlideImage(imageView, imageUid);
    }

    public static Date getDate() {
        return Calendar.getInstance(Locale.ENGLISH).getTime();
    }

    public static String replaceStringChars(String s) {
        String returnString = s.replace(FIRESTORE_BR_CLOSE, STRING_NEWLINE);
        returnString = returnString.replace(FIRESTORE_NEWLINE, STRING_NEWLINE);
        returnString = returnString.replace(FIRESTORE_BR, STRING_NEWLINE);
        returnString = returnString.replace(FIRESTORE_NDASH, STRING_NDASH);
        returnString = returnString.replace(FIRESTORE_MDASH, STRING_MDASH);
        returnString = returnString.replace(FIRESTORE_BOLD, STRING_BOLD);
        returnString = returnString.replace(FIRESTORE_BOLD_CLOSE, STRING_BOLD_CLOSE);
        returnString = returnString.replace(FIRESTORE_STRONG, STRING_BOLD);
        returnString = returnString.replace(FIRESTORE_STRONG_CLOSE, STRING_BOLD_CLOSE);
        returnString = returnString.replace("</ul>", "");
        returnString = returnString.replace("<p>", "");
        returnString = returnString.replace("</p>", "");
        returnString = returnString.replace("<div>", "");
        returnString = returnString.replace("</div>", "");
        returnString = returnString.replace("&amp;", "&");
        returnString = returnString.replace("&nbsp;", STRING_NEWLINE);
        returnString =  returnString.replace("<ul>", "");
        returnString =  returnString.replace("<li>", "•  ");
        returnString = returnString.replace("</li>", "\n");

        return returnString;
    }

    @SuppressWarnings("deprecation")
    public static Spanned fromHtml(String html){
        html = customizeListTags(html);
        if(html == null){
            // return an empty spannable if the html is null
            return new SpannableString("");
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY, null, new ListTagHandler());
        } else {
            return Html.fromHtml(html, null, new ListTagHandler());
        }
    }

    private static String customizeListTags(@Nullable String html) {
        if (html == null) {
            return null;
        }
        html = html.replace("<ul>", "<UNORDERED>");
        html = html.replace("</ul>", "</UNORDERED>");
        html = html.replace("<ol>", "<OL>");
        html = html.replace("</ol>", "</OL>");
        html = html.replace("<li>", "<LISTITEM>");
        html = html.replace("</li>", "</LISTITEM>");
        return html;
    }

    public static SpannableStringBuilder getRedStar(String s){
        SpannableStringBuilder spannable = new SpannableStringBuilder (s);
        spannable.append(" " + Constants.USER_PROFILE_ESSENTIAL_STAR);
        spannable.setSpan(new ForegroundColorSpan(Color.RED), spannable.length()-2, spannable.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        return spannable;
    }
}
