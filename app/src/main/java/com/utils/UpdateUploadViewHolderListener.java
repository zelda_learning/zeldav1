package com.utils;

public interface UpdateUploadViewHolderListener {
    public void updateViewHolder(double uploadProgress);
    public void uploadingComplete(String filename);
    public void uploadFailed();
}
