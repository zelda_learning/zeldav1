package com.utils;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.mindorks.placeholderview.SwipeDecor;
import com.mindorks.placeholderview.SwipeDirection;
import com.mindorks.placeholderview.SwipePlaceHolderView;
import com.mindorks.placeholderview.annotations.Layout;
import com.mindorks.placeholderview.annotations.Resolve;
import com.mindorks.placeholderview.annotations.View;
import com.mindorks.placeholderview.annotations.swipe.SwipeCancelState;
import com.mindorks.placeholderview.annotations.swipe.SwipeInDirectional;
import com.mindorks.placeholderview.annotations.swipe.SwipeOutDirectional;
import com.mindorks.placeholderview.annotations.swipe.SwipeTouch;
import com.zelda.object.IntroQuizQuestion;
import com.zeldav1.IntroQuizActivity;
import com.zeldav1.R;


@Layout(R.layout.card_view_intro_quiz)
public class SwipeCardHelper {
    public static final int LEFT_SCORE = 1;
    public static final int RIGHT_SCORE = 5;
    public static final int UP_SCORE = 3;

    @View(R.id.statement_text)
    TextView statementTextView;

    @View(R.id.card_id)
    CardView cardView;

    @View(R.id.in_card)
    TextView inCard;

    @View(R.id.out_card)
    TextView outCard;

    private IntroQuizQuestion mQuestion;
    private Context mContext;
    private Activity mActivity;
    private SwipePlaceHolderView mSwipeView;

    public SwipeCardHelper(Context context, Activity activity, IntroQuizQuestion question, SwipePlaceHolderView swipeView) {
        mContext = context;
        mActivity = activity;
        mQuestion = question;
        mSwipeView = swipeView;
        mSwipeView.getBuilder()
                .setSwipeDecor(new SwipeDecor()
                        .setPaddingTop(20)
                        .setRelativeScale(0.01f)
                        .setSwipeOutMsgLayoutId(R.layout.card_view_swipe_out)
                        .setSwipeInMsgLayoutId(R.layout.card_view_swipe_in));
    }

    @Resolve
    void onResolved() {
        statementTextView.setText(mQuestion.getText());
    }

    SwipeDirection mDirection;

    @SwipeTouch
     void onSwipeTouch(float xStart, float yStart, float xCurrent, float yCurrent) {
        float deltaX = xCurrent - xStart;
        float deltaY = yCurrent - yStart;
        double rad = Math.atan2(deltaY, deltaX);
        double deg = -1 * rad * (180 / Math.PI);

        if((deg < 45 && deg < -90) || deg > 135) {
            inCard.setVisibility(android.view.View.VISIBLE);
            inCard.setText(mContext.getString(R.string.intro_quiz_agree));
            inCard.setTextColor(mContext.getResources().getColor(R.color.md_light_green_800));
            outCard.setText(mContext.getString(R.string.intro_quiz_disagree));
            outCard.setTextColor(mContext.getResources().getColor(R.color.md_red_800));
        } else if (deg >= 45 && deg <= 135){
            outCard.setText(mContext.getString(R.string.intro_quiz_maybe));
            outCard.setTextColor(mContext.getResources().getColor(R.color.md_amber_800));
            inCard.setText(mContext.getString(R.string.intro_quiz_maybe));
            inCard.setTextColor(mContext.getResources().getColor(R.color.md_amber_800));
        } else {
            mDirection = SwipeDirection.RIGHT;

            inCard.setText(mContext.getString(R.string.intro_quiz_agree));
            inCard.setTextColor(mContext.getResources().getColor(R.color.md_light_green_800));
            outCard.setText(mContext.getString(R.string.intro_quiz_disagree));
            outCard.setTextColor(mContext.getResources().getColor(R.color.md_red_800));
        }
    }

    @SwipeOutDirectional
     void onSwipeOutDirectional(SwipeDirection direction) {
        Log.d("DEBUG", "SwipeOutDirectional " + direction.name());
        if(direction == SwipeDirection.LEFT){
            IntroQuizActivity.sumResults(mQuestion.getLabel(), LEFT_SCORE);
            mActivity.findViewById(R.id.intro_quiz_undo_button).setEnabled(true);
        }else{
            IntroQuizActivity.sumResults(mQuestion.getLabel(), UP_SCORE);
            mActivity.findViewById(R.id.intro_quiz_undo_button).setEnabled(true);
        }
    }

    @SwipeInDirectional
    void onSwipeInDirectional(SwipeDirection direction) {
        Log.d("DEBUG", "SwipeInDirectional " + direction.name());
        if(direction == SwipeDirection.RIGHT) {
            IntroQuizActivity.sumResults(mQuestion.getLabel(), RIGHT_SCORE);
            mActivity.findViewById(R.id.intro_quiz_undo_button).setEnabled(true);
        }
    }


    @SwipeCancelState
    void onSwipeCancelState() {
        Log.d("DEBUG", "onSwipeCancelState");
        mSwipeView.setAlpha(1);
    }

}