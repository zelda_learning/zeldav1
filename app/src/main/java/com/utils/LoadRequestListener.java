package com.utils;

import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

public class LoadRequestListener implements RequestListener {
private static final String TAG = "LoadRequestListener";

    private ImageView imageView;
    private String imageName;
    private ProgressBar progressBar;
    private int failImageId;

    LoadRequestListener() {

    }

    public LoadRequestListener(ImageView imageView, String imageName, int failImageId) {
        this.imageName = imageName;
        this.imageView = imageView;
        this.failImageId = failImageId;
    }

    LoadRequestListener(ImageView imageView, String imageName, ProgressBar progressBar, int failImageId) {
        this.imageName = imageName;
        this.imageView = imageView;
        this.progressBar = progressBar;
        this.failImageId = failImageId;
    }

    @Override
    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target target, boolean isFirstResource) {
        Log.d(TAG, "Load failed: " + imageName);
        if (null != progressBar)
            progressBar.setVisibility(View.GONE);
        imageView.setImageResource(failImageId);
        imageView.setVisibility(View.VISIBLE);
        return false;
    }

    @Override
    public boolean onResourceReady(Object resource, Object model, Target target, DataSource dataSource, boolean isFirstResource) {
        Log.d(TAG, "Load success: " + imageName);
        if (null != progressBar)
            progressBar.setVisibility(View.GONE);
        imageView.setVisibility(View.VISIBLE);
        return false;
    }

}
