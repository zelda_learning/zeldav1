package com.utils;

import java.util.HashMap;

public interface UserStateListener {

    public interface UserStateUpdate {
        void updateUserState();
    }
}
