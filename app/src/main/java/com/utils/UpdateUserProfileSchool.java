package com.utils;

import com.zelda.object.UserProfileSchool;

public interface UpdateUserProfileSchool {
    void updateUserProfileSchool(UserProfileSchool userProfileSchool);
}
