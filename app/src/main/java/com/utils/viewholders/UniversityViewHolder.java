package com.utils.viewholders;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.utils.CustomUtils;
import com.utils.services.FirebaseStorageService;
import com.zeldav1.R;
import com.zelda.object.University;


public class UniversityViewHolder extends RecyclerView.ViewHolder {
    private static final String TAG = UniversityViewHolder.class.getSimpleName();
    private TextView universityNameTextView, applicationsClosingDateTextView;
    private ImageView universityImageView;

    public UniversityViewHolder(View view) {
        super(view);
        universityNameTextView = view.findViewById(R.id.item_university_name);
        applicationsClosingDateTextView = view.findViewById(R.id.item_university_application_close_date_heading);
        universityImageView = view.findViewById(R.id.item_university_image);
    }

    public void bindData(final University university) {
        try{
            universityNameTextView.setText(university.getTitle());
            applicationsClosingDateTextView.setText(university.getClosingDate());
            applicationsClosingDateTextView.setText(applicationsClosingDateTextView.getContext().getResources().getString(R.string.item_university_application_close_date, university.getClosingDate()));

            if(null == university.getImageName()){
                FirebaseStorageService.setGlideImage(universityImageView, CustomUtils.setImageNameOnType(University.class, university.getId()));
            } else {
                FirebaseStorageService.setGlideImage(universityImageView, CustomUtils.setImageNameOnType(University.class, university.getId(), university.getImageName()));
            }

        }
        catch (NullPointerException e){
            Log.e(TAG, "University bind error: " + e.getMessage());
        }
    }
}