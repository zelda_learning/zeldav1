package com.utils.viewholders;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.TextView;

import com.utils.adapters.HintSpinnerAdapter;
import com.zelda.object.Matric;
import com.zeldav1.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.utils.Constants.ELEMENT_CURRENT_GRADE;
import static com.utils.Constants.ELEMENT_MATRIC_CERTIFICATE;
import static com.utils.Constants.ELEMENT_MATRIC_COMPLETED_YEAR;
import static com.utils.Constants.UPS_CURRENT_GRADE;
import static com.utils.Constants.UPS_MATRIC_CERTIFICATE;
import static com.utils.Constants.UPS_MATRIC_COMPLETED_YEAR;
import static com.utils.CustomUtils.getRedStar;

public class InputMatricViewHolder extends RecyclerView.ViewHolder {
    private static final String TAG = InputMatricViewHolder.class.getSimpleName();

    private TextView currentGradeHeadingTextView, matricCertificateHeadingTextView, matricCompletedYearHeadingTextView;
    private Spinner currentGradeSpinner, matricCertificateSpinner, matricCompletedYearSpinner;
    private View current, certificate, year;
    private Context mContext;

    public InputMatricViewHolder(View view) {
        super(view);
        current = view.findViewById(R.id.item_input_current_year_container);
        currentGradeHeadingTextView = current.findViewById(R.id.item_input_spinner_field_heading);
        currentGradeSpinner = current.findViewById(R.id.item_input_spinner);

        certificate = view.findViewById(R.id.item_input_matric_certificate_container);
        matricCertificateHeadingTextView = certificate.findViewById(R.id.item_input_spinner_field_heading);
        matricCertificateSpinner = certificate.findViewById(R.id.item_input_spinner);

        year = view.findViewById(R.id.item_input_matric_completed_year_container);
        matricCompletedYearHeadingTextView = year.findViewById(R.id.item_input_spinner_field_heading);
        matricCompletedYearSpinner = year.findViewById(R.id.item_input_spinner);

        mContext = view.getContext();
    }

    public void bindData(final Matric matric,
                         AdapterView.OnItemSelectedListener adapterItemSelectedListener) {

        List<String> arrayList = new ArrayList<>();
        Collections.addAll(arrayList, mContext.getResources().getStringArray(R.array.drop_down_current_grade));
        String hint = mContext.getResources().getString(R.string.item_spinner_prompt);
        arrayList.add(hint);

        HintSpinnerAdapter spinnerAdapter =  new HintSpinnerAdapter(mContext, R.layout.item_spinner, arrayList);
        spinnerAdapter.setDropDownViewResource(R.layout.item_spinner);
        currentGradeSpinner.setTag(ELEMENT_CURRENT_GRADE);
        currentGradeSpinner.setAdapter(spinnerAdapter);
        currentGradeSpinner.setOnItemSelectedListener(adapterItemSelectedListener);

        List<String> arrayList1 = new ArrayList<>();
        Collections.addAll(arrayList1, mContext.getResources().getStringArray(R.array.drop_down_matric_certificate));
        arrayList1.add(hint);

        HintSpinnerAdapter spinnerAdapter1 = new HintSpinnerAdapter(mContext, R.layout.item_spinner, arrayList1);
        spinnerAdapter1.setDropDownViewResource(R.layout.item_spinner);
        matricCertificateSpinner.setTag(ELEMENT_MATRIC_CERTIFICATE);
        matricCertificateSpinner.setAdapter(spinnerAdapter1);
        matricCertificateSpinner.setOnItemSelectedListener(adapterItemSelectedListener);

        List<String> arrayList2 = new ArrayList<>();
        Collections.addAll(arrayList2, mContext.getResources().getStringArray(R.array.drop_down_years));
        arrayList2.add(hint);

        HintSpinnerAdapter spinnerAdapter2 = new HintSpinnerAdapter(mContext, R.layout.item_spinner, arrayList2);
        spinnerAdapter2.setDropDownViewResource(R.layout.item_spinner);
        matricCompletedYearSpinner.setTag(ELEMENT_MATRIC_COMPLETED_YEAR);
        matricCompletedYearSpinner.setAdapter(spinnerAdapter2);
        matricCompletedYearSpinner.setOnItemSelectedListener(adapterItemSelectedListener);

        if (null != matric) {
            if (null != matric.getCurrentGrade() && matric.getCurrentGrade().length() > 0) {
                currentGradeSpinner.setSelection(arrayList.indexOf(matric.getCurrentGrade()));
                currentGradeHeadingTextView.setText(UPS_CURRENT_GRADE);
            }
            else {
                currentGradeSpinner.setSelection(spinnerAdapter.getCount());
                currentGradeHeadingTextView.setText(getRedStar(UPS_CURRENT_GRADE), TextView.BufferType.SPANNABLE);
            }

            if (currentGradeSpinner.getSelectedItem().toString().equals(hint))
                hideContainers();
            else if (null != matric.getCurrentGrade() && matric.getCurrentGrade().equals(mContext.getResources().getStringArray(R.array.drop_down_current_grade)[2]))
                hideContainers();
            else
                showContainers();

            if (null != matric.getMatricCertificate() && matric.getMatricCertificate().length() > 0) {
                matricCertificateSpinner.setSelection(arrayList1.indexOf(matric.getMatricCertificate()));
                matricCertificateHeadingTextView.setText(UPS_MATRIC_CERTIFICATE);
            }
            else {
                matricCertificateSpinner.setSelection(spinnerAdapter1.getCount());
                matricCertificateHeadingTextView.setText(getRedStar(UPS_MATRIC_CERTIFICATE), TextView.BufferType.SPANNABLE);
            }

            if (null != matric.getMatricCompletedYear() && matric.getMatricCompletedYear().length() > 0) {
                matricCompletedYearSpinner.setSelection(arrayList2.indexOf(matric.getMatricCompletedYear()));
                matricCompletedYearHeadingTextView.setText(UPS_MATRIC_COMPLETED_YEAR);
            }
            else {
                matricCompletedYearSpinner.setSelection(spinnerAdapter2.getCount());
                matricCompletedYearHeadingTextView.setText(getRedStar(UPS_MATRIC_COMPLETED_YEAR), TextView.BufferType.SPANNABLE);
            }
        }
        else {
            Log.e(TAG, "Error, matric is null: ");
            currentGradeSpinner.setSelection(spinnerAdapter.getCount());
            matricCertificateSpinner.setSelection(spinnerAdapter1.getCount());
            matricCompletedYearSpinner.setSelection(spinnerAdapter2.getCount());

            currentGradeHeadingTextView.setText(UPS_CURRENT_GRADE);
            matricCertificateHeadingTextView.setText(UPS_MATRIC_CERTIFICATE);
            matricCompletedYearHeadingTextView.setText(UPS_MATRIC_COMPLETED_YEAR);
        }

    }

    public void hideContainers(){
        certificate.setVisibility(View.GONE);
        year.setVisibility(View.GONE);
    }

    public void showContainers(){
        certificate.setVisibility(View.VISIBLE);
        year.setVisibility(View.VISIBLE);
    }
}