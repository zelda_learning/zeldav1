package com.utils.viewholders;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.utils.services.FirebaseStorageService;
import com.utils.fragments.NBTQuizQuestionPageFragment;
import com.utils.services.FirebaseFirestoreService;
import com.zelda.object.NBTQuiz;
import com.zelda.object.NBTQuizReport;
import com.zeldav1.FullScreenImageActivity;
import com.zeldav1.NBTQuizActivity;
import com.zeldav1.R;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

import static com.utils.Constants.IMAGE_ID_TAG;
import static com.utils.Constants.NBT_QUIZZES_PROBLEMS;
import static com.utils.Constants.NBT_QUIZ_RETRY_TAG;
import static com.utils.Constants.QUESTION_NUMBER_TAG;
import static com.utils.Constants.QUIZZES;
import static com.utils.Constants.USER_CHOICE_RETRY_PAST_TAG;

public class QuizNBTResultsViewHolder extends RecyclerView.ViewHolder {
    private static final String TAG = QuizNBTResultsViewHolder.class.getSimpleName();
    private RecyclerView mRecyclerView;

    private List<String> mPastUserChoice;
    private Context context;
    private TextView nbtQuizHeadingTextView, mUserChoiceTextView, mAnswerTextView, mReportTextView;
    private ConstraintLayout mWorkingContainer, mAnswerContainer, mReportContainer;
    private ProgressBar mLoadingProgressBar;
    private ImageView mQuestionImageView, mZoomQuestionImageView,
                        mUserChoiceAnswerImageView, mResultImageView, mNBTQuizOptionsImageView,
                        mWorkingImageView, mZoomWorkingImageView,
                        mAnswerImageView, mHideAnswerImageView, mHideReportImageView;
    private Button mReportSubmitButton;
    private View itemView;
    private NBTQuizReport mReport;

    public RadioGroup reportGroup, lastCheckedRadioGroup;

    public QuizNBTResultsViewHolder(View view, RecyclerView recyclerView, List<String> pastUserChoice) {
        super(view);
        mRecyclerView = recyclerView;
        itemView = view;
        context = mRecyclerView.getContext();

        nbtQuizHeadingTextView = view.findViewById(R.id.item_nbt_quiz_result_question_heading);
        mQuestionImageView = view.findViewById(R.id.item_nbt_quiz_question_image);
        mZoomQuestionImageView = view.findViewById(R.id.item_nbt_quiz_zoom_question_image_view);
        mResultImageView = view.findViewById(R.id.item_nbt_quiz_result_image);

        mUserChoiceTextView = view.findViewById(R.id.item_nbt_quiz_user_choice_text_view);
        mUserChoiceAnswerImageView = view.findViewById(R.id.item_nbt_quiz_user_choice);

        mLoadingProgressBar = view.findViewById(R.id.item_nbt_quiz_progress_loading_bar);
        mNBTQuizOptionsImageView = view.findViewById(R.id.item_nbt_quiz_menu_options);

        mWorkingImageView = view.findViewById(R.id.item_nbt_quiz_working_image);
        mWorkingContainer = view.findViewById(R.id.item_nbt_quiz_working_container);
        mZoomWorkingImageView = view.findViewById(R.id.item_nbt_quiz_zoom_working_image_view);

        mReportContainer = view.findViewById(R.id.item_nbt_quiz_report_container);
        mReportTextView = view.findViewById(R.id.item_nbt_quiz_report_heading_text_view);

        mAnswerImageView = view.findViewById(R.id.item_nbt_quiz_answer_image);
        mAnswerContainer = view.findViewById(R.id.item_nbt_quiz_answer_container);
        mAnswerTextView = view.findViewById(R.id.item_nbt_quiz_answer_text_view);
        mHideAnswerImageView = view.findViewById(R.id.item_nbt_quiz_hide_answer_image);
        mHideReportImageView = view.findViewById(R.id.item_nbt_quiz_hide_report_container);
        mPastUserChoice = pastUserChoice;

        reportGroup = view.findViewById(R.id.item_nbt_quiz_report_radio_group);

        reportGroup.setOnCheckedChangeListener((radioGroup, i) -> {
            Log.d(TAG, "Checked ID: " + radioGroup.getCheckedRadioButtonId());
            if (lastCheckedRadioGroup != null)
                Log.d(TAG, "Last Checked ID: " + lastCheckedRadioGroup.getCheckedRadioButtonId());

            if (lastCheckedRadioGroup != null &&
                    lastCheckedRadioGroup.getCheckedRadioButtonId() != radioGroup.getCheckedRadioButtonId() &&
                    lastCheckedRadioGroup.getCheckedRadioButtonId() != -1) {

                lastCheckedRadioGroup.clearCheck();

                Log.d(TAG, "Radio group checked: " + radioGroup.getCheckedRadioButtonId());

            }
            lastCheckedRadioGroup = radioGroup;

        });

        mReportSubmitButton = view.findViewById(R.id.item_nbt_quiz_report_submit_button);

    }

    public void bindData(final String userChoice, final NBTQuiz nbtQuiz, int position) {
        try {
            int questionNumber = position + 1;
            SpannableString nbtQuizHeadingString = new SpannableString(context.getString(R.string.item_nbt_quiz_result_title, questionNumber + ""));
            nbtQuizHeadingString.setSpan(new UnderlineSpan(), 0, nbtQuizHeadingString.length(), 0);
            nbtQuizHeadingTextView.setText(nbtQuizHeadingString);

            String questionStoragePrefix = QUIZZES + "/" + nbtQuiz.getId() + "/" + questionNumber + "/" ;
            String questionImageName = questionStoragePrefix + questionNumber;
            String userChoiceImageName = questionStoragePrefix + questionNumber + userChoice;
            String answerText = nbtQuiz.getAnswers().get(position);
            String answerImageName = questionStoragePrefix + questionNumber + answerText;

            mUserChoiceTextView.setText(context.getResources().getString(R.string.tag_element, userChoice));
            mReportTextView.setText(context.getResources().getString(R.string.item_nbt_quiz_report_heading, questionNumber + ""));

            mLoadingProgressBar.setVisibility(View.VISIBLE);
            mQuestionImageView.setVisibility(View.INVISIBLE);

            FirebaseStorageService.setGlideImageJPG(context, questionImageName, mQuestionImageView,
                    new RequestListener() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target target, boolean isFirstResource) {
                            FirebaseStorageService.setGlideImagePNG(mQuestionImageView, questionImageName);
                            mLoadingProgressBar.setVisibility(View.GONE);
                            mQuestionImageView.setVisibility(View.VISIBLE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Object resource, Object model, Target target, DataSource dataSource, boolean isFirstResource) {
                            Log.d(TAG, "Load success: " + questionImageName);
                            mLoadingProgressBar.setVisibility(View.GONE);
                            mQuestionImageView.setVisibility(View.VISIBLE);
                            return false;
                        }

                    });

            FirebaseStorageService.setGlideImageJPG(context, userChoiceImageName, mUserChoiceAnswerImageView,
                    new RequestListener() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target target, boolean isFirstResource) {
                            FirebaseStorageService.setGlideImagePNG(mUserChoiceAnswerImageView, userChoiceImageName);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Object resource, Object model, Target target, DataSource dataSource, boolean isFirstResource) {
                            return false;
                        }

                    });

            FirebaseStorageService.setGlideImageJPG(context, answerImageName, mAnswerImageView,
                    new RequestListener() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target target, boolean isFirstResource) {
                            FirebaseStorageService.setGlideImagePNG(mUserChoiceAnswerImageView, userChoiceImageName);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Object resource, Object model, Target target, DataSource dataSource, boolean isFirstResource) {
                            Log.d(TAG, "Load success: " + userChoiceImageName);
                            return false;
                        }

                    });

            mNBTQuizOptionsImageView.setOnClickListener(v -> {
                //creating a popup menu
                PopupMenu popup = new PopupMenu(context, mNBTQuizOptionsImageView);
                //inflating menu from xml resource
                popup.inflate(R.menu.menu_nbt_quiz_results_menu);
                //adding click listener
                popup.setOnMenuItemClickListener(item -> {
                    switch (item.getItemId()) {
                        case R.id.nbt_quiz_results_menu_retry:
                            //handle menu1 click
                            Log.d(TAG, "retry!!");
                            Bundle bundle = NBTQuizActivity.getBundle();
                            bundle.putSerializable(QUESTION_NUMBER_TAG, questionNumber);
                            bundle.putSerializable(USER_CHOICE_RETRY_PAST_TAG, (Serializable) mPastUserChoice);
                            bundle.putSerializable(NBT_QUIZ_RETRY_TAG, true);
                            NBTQuizQuestionPageFragment nextFrag= new NBTQuizQuestionPageFragment();
                            nextFrag.setArguments(bundle);
                            Objects.requireNonNull((NBTQuizActivity)context).getSupportFragmentManager().beginTransaction()
                                    .replace(R.id.nbt_quiz_frame_layout_container, nextFrag,"findThisFragment")
                                    .addToBackStack(null)
                                    .commit();
                            return true;

                        case R.id.nbt_quiz_results_menu_view_answer:
                            showAnswer(answerText);
                            return true;

                        case R.id.nbt_quiz_results_menu_report:
                            Log.d(TAG, "report!! create a NBTProblem item.");
                            showReportOptions(nbtQuiz.getId(), questionNumber);
                            return true;
                        default:
                            return false;
                    }
                });
                popup.show();
            });

            if (userChoice.equals( nbtQuiz.getAnswers().get(position))){
                Log.d(TAG, "User is correct");
                mResultImageView.setImageResource(R.drawable.btn_check_buttonless_on);
                mWorkingContainer.setVisibility(View.GONE);
            } else {
                Log.d(TAG, "user is incorrect");
                mResultImageView.setImageResource(R.drawable.ic_delete);
            }

            Intent intent = new Intent(context, FullScreenImageActivity.class);

            if (null == nbtQuiz.getWorkedSolutionString(position)){
                Log.d(TAG, "Working image not found: hide option to show.");
                mWorkingContainer.setVisibility(View.GONE);
            } else{
                Log.d(TAG, "Working image url found: show options.");
                FirebaseStorageService.setGlideImage(mWorkingImageView, questionStoragePrefix + nbtQuiz.getWorkedSolutionString(position));

                mWorkingImageView.setOnClickListener(v -> {
                    Log.d(TAG, "mWorkingImageView image clicked");
                    intent.putExtra(IMAGE_ID_TAG, questionStoragePrefix + nbtQuiz.getWorkedSolutionString(position));
                    context.startActivity(intent);
                });

                mZoomWorkingImageView.setOnClickListener(v -> {
                    Log.d(TAG, "mZoomWorkingImageView image clicked");
                    intent.putExtra(IMAGE_ID_TAG, questionStoragePrefix + nbtQuiz.getWorkedSolutionString(position));
                    context.startActivity(intent);
                });

            }

            mHideReportImageView.setOnClickListener(v -> {
                mReportContainer.setVisibility(View.GONE);
            });

            mHideAnswerImageView.setOnClickListener(v -> {
                mAnswerContainer.setVisibility(View.GONE);
            });

            mQuestionImageView.setOnClickListener(v -> {
                intent.putExtra(IMAGE_ID_TAG, questionImageName);
                context.startActivity(intent);
            });

            mZoomQuestionImageView.setOnClickListener(v -> {
                intent.putExtra(IMAGE_ID_TAG, questionImageName);
                context.startActivity(intent);
            });

            mAnswerImageView.setOnClickListener(v -> {
                intent.putExtra(IMAGE_ID_TAG, answerImageName);
                context.startActivity(intent);
            });

            mReportSubmitButton.setOnClickListener(v -> {
                int selectedId = lastCheckedRadioGroup.getCheckedRadioButtonId();

                if (selectedId == -1){
                    Toast.makeText(context, R.string.item_nbt_quiz_report_question_nothing_selected, Toast.LENGTH_LONG).show();
                }

                else{
                    mReportContainer.setVisibility(View.GONE);
                    mReport.setProblem( ((RadioButton)( itemView.findViewById(selectedId) ) ).getText().toString());
                    Toast.makeText(context, R.string.item_nbt_quiz_menu_report_toast, Toast.LENGTH_SHORT).show();
                    Log.d(TAG, "Submit report: " + mReport);
                    new FirebaseFirestoreService().addFirestoreDocument(NBT_QUIZZES_PROBLEMS, mReport);
                }

            });

        }
        catch (NullPointerException e){
            Log.e(TAG, "nbtQuiz bind error: " + e.getMessage());
        }
    }

    private void showAnswer(String answerText) {
        mWorkingContainer.setVisibility(View.GONE);
        mAnswerContainer.setVisibility(View.VISIBLE);
        mReportContainer.setVisibility(View.GONE);
        mAnswerTextView.setText(context.getResources().getString(R.string.tag_element, answerText));
    }

    private void showReportOptions(String quizId, int questionNumber) {
        mWorkingContainer.setVisibility(View.GONE);
        mAnswerContainer.setVisibility(View.GONE);
        mReportContainer.setVisibility(View.VISIBLE);

        mReport = new NBTQuizReport(quizId, questionNumber);

    }

    private void showQuestionWorking() {
        mWorkingContainer.setVisibility(View.VISIBLE);
        mAnswerContainer.setVisibility(View.GONE);
        mReportContainer.setVisibility(View.GONE);
    }

}