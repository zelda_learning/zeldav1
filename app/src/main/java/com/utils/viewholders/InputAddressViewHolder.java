package com.utils.viewholders;

import android.support.v7.widget.RecyclerView;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.utils.Constants;
import com.zelda.object.Address;
import com.zelda.object.UserApplicationFormA;
import com.zeldav1.R;

import static com.utils.CustomUtils.getRedStar;

public class InputAddressViewHolder extends RecyclerView.ViewHolder {

    private static final String TAG = InputAddressViewHolder.class.getSimpleName();

    private TextView fieldHeadingTextView;
    private EditText fieldInputLine1, fieldInputLine2, fieldInputCity, fieldInputProvince, fieldInputZip;

    public InputAddressViewHolder(View view) {
        super(view);
        fieldHeadingTextView = view.findViewById(R.id.item_input_address_field_heading);
        fieldInputLine1 = view.findViewById(R.id.item_input_address_line_1_edittext);
        fieldInputLine2 = view.findViewById(R.id.item_input_address_line_2_edittext);
        fieldInputCity = view.findViewById(R.id.item_input_address_city_edittext);
        fieldInputProvince = view.findViewById(R.id.item_input_address_province_edittext);
        fieldInputZip = view.findViewById(R.id.item_input_address_zip_code_edittext);
    }

    public void bindData(final String headingText, final Address address) {
        try {
            fieldHeadingTextView.setText(headingText);
            fieldInputLine1.setText(address.getLine1());
            fieldInputLine2.setText(address.getLine2());
            fieldInputCity.setText(address.getCity());
            fieldInputProvince.setText(address.getProvince());
            fieldInputZip.setText(address.getZip());
        }
        catch (NullPointerException e){
            Log.e(TAG, "InputAddressViewHolder: " + e.getMessage());
        }
    }

    public void bindData(final String headingText, final TextWatcher textWatcherLine1,
                         final TextWatcher textWatcherLine2, final TextWatcher textWatcherCity,
                         final TextWatcher textWatcherProvince, final TextWatcher textWatcherZip,
                         final UserApplicationFormA userApplicationFormA) {
        try {
            Log.d(TAG, userApplicationFormA.toString());
            fieldHeadingTextView.setText(headingText);
            fieldInputLine1.addTextChangedListener(textWatcherLine1);
            fieldInputLine2.addTextChangedListener(textWatcherLine2);
            fieldInputCity.addTextChangedListener(textWatcherCity);
            fieldInputProvince.addTextChangedListener(textWatcherProvince);
            fieldInputZip.addTextChangedListener(textWatcherZip);
        }
        catch (NullPointerException e){
            Log.e(TAG, "InputAddressViewHolder: " + e.getMessage());
        }
    }


    public void bindData(final String headingText, final TextWatcher textWatcherLine1,
                         final TextWatcher textWatcherLine2, final TextWatcher textWatcherCity,
                         final TextWatcher textWatcherProvince, final TextWatcher textWatcherZip,
                         final Address address) {
        try {

            if (address != null && null != address.getFirestoreString() && !address.getFirestoreString().equals(Constants.BLANK_ADDRESS_DELIMITER)){
                Log.d(TAG, "Address: " + headingText + "getAddress " + address.getFirestoreString());
                fieldInputLine1.setText(address.getLine1());
                fieldInputLine2.setText(address.getLine2());
                fieldInputCity.setText(address.getCity());
                fieldInputProvince.setText(address.getProvince());
                fieldInputZip.setText(address.getZip());
                fieldHeadingTextView.setText(headingText);
            }else {
                fieldInputLine1.setText("");
                fieldInputLine2.setText("");
                fieldInputCity.setText("");
                fieldInputProvince.setText("");
                fieldInputZip.setText("");
                fieldHeadingTextView.setText(getRedStar(headingText), TextView.BufferType.SPANNABLE);
            }
            fieldInputLine1.addTextChangedListener(textWatcherLine1);
            fieldInputLine2.addTextChangedListener(textWatcherLine2);
            fieldInputCity.addTextChangedListener(textWatcherCity);
            fieldInputProvince.addTextChangedListener(textWatcherProvince);
            fieldInputZip.addTextChangedListener(textWatcherZip);
        }
        catch (NullPointerException e){
            Log.e(TAG, "InputAddressViewHolder: " + e.getMessage());
        }
    }

    public Address captureAddressData(){
        Address address = new Address();
        address.setLine1(fieldInputLine1.getText().toString());
        address.setLine2(fieldInputLine2.getText().toString());
        address.setCity(fieldInputCity.getText().toString());
        address.setProvince(fieldInputProvince.getText().toString());
        address.setZip(fieldInputZip.getText().toString());
        return address;
    }

}
