package com.utils.viewholders;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.zeldav1.R;

import static com.utils.Constants.FIRESTORE_TOGGLE_CS;
import static com.utils.Constants.FIRESTORE_TOGGLE_HSC;
import static com.utils.Constants.FIRESTORE_TOGGLE_OI;
import static com.utils.Constants.FIRESTORE_TOGGLE_PI;
import static com.utils.Constants.FIRESTORE_TOGGLE_PS;
import static com.utils.Constants.UPS_CURRENT_GRADE_11;
import static com.utils.Constants.UPS_CURRENT_GRADE_12;
import static com.utils.Constants.UPS_CURRENT_GRADE_MATRICULATED;

public class ToggleViewHolder  extends RecyclerView.ViewHolder{

    private static final String TAG = ToggleViewHolder.class.getSimpleName();

    private Context mContext;
    private TextView fieldHeadingTextView;
    private TextView fieldDisplayTextView;
    private ToggleButton toggleButton;
    private RecyclerView recyclerView;

    public ToggleViewHolder(View view, RecyclerView recyclerView) {
        super(view);
        mContext = view.getContext();
        fieldHeadingTextView = view.findViewById(R.id.item_toggle_heading_textview);
        fieldDisplayTextView = view.findViewById(R.id.item_toggle_text_textview);
        fieldHeadingTextView.setText("");
        fieldDisplayTextView.setText("");
        this.recyclerView = recyclerView;
        this.toggleButton = view.findViewById(R.id.item_toggle_image);

        fieldHeadingTextView.setOnClickListener(v -> {
            if (fieldDisplayTextView.getVisibility() == View.VISIBLE) {
                fieldDisplayTextView.setVisibility(View.GONE);
                toggleButton.setChecked(false);
            }
            else {
                fieldDisplayTextView.setVisibility(View.VISIBLE);
                toggleButton.setChecked(true);
            }
            recyclerView.smoothScrollToPosition(getAdapterPosition());
        });

        toggleButton.setOnClickListener(v -> {
            if (fieldDisplayTextView.getVisibility() == View.VISIBLE)
                fieldDisplayTextView.setVisibility(View.GONE);
            else
                fieldDisplayTextView.setVisibility(View.VISIBLE);
            recyclerView.smoothScrollToPosition(getAdapterPosition());
        });
    }

    public void bindData(final String keyValue) {
        Log.d(TAG, "KeyValue: " + keyValue);
        switch (keyValue){
            case FIRESTORE_TOGGLE_PI:
                fieldHeadingTextView.setText(mContext.getResources().getString(R.string.cba_sensitive_data_heading));
                fieldDisplayTextView.setText(mContext.getResources().getString(R.string.cba_sensitive_data_text));
                break;

            case FIRESTORE_TOGGLE_OI:
                fieldHeadingTextView.setText(mContext.getResources().getString(R.string.cba_other_data_heading));
                fieldDisplayTextView.setText(mContext.getResources().getString(R.string.cba_other_data_text));
                break;

            case FIRESTORE_TOGGLE_CS:
                fieldHeadingTextView.setText(mContext.getResources().getString(R.string.cba_university_current_heading));
                fieldDisplayTextView.setText(mContext.getResources().getString(R.string.cba_university_current_text));
                break;

            case FIRESTORE_TOGGLE_PS:
                fieldHeadingTextView.setText(mContext.getResources().getString(R.string.cba_university_prospective_heading));
                fieldDisplayTextView.setText(mContext.getResources().getString(R.string.cba_university_prospective_text));
                break;

            case FIRESTORE_TOGGLE_HSC:
                fieldHeadingTextView.setText(mContext.getResources().getString(R.string.cba_high_school_heading));
                fieldDisplayTextView.setText(mContext.getResources().getString(R.string.cba_high_school_current_text));
                break;

            default:
                fieldHeadingTextView.setText("Default");
                fieldDisplayTextView.setText("Default");
                break;
        }
    }

    public void bindData(final String keyValue, String currentGrade){
        fieldHeadingTextView.setText(mContext.getResources().getString(R.string.cba_high_school_heading));

        if (null != currentGrade) {
            switch (currentGrade) {
                case UPS_CURRENT_GRADE_MATRICULATED:
                    fieldDisplayTextView.setText(mContext.getResources().getString(R.string.cba_high_school_matriculated_text));
                    break;

                case UPS_CURRENT_GRADE_11:
                case UPS_CURRENT_GRADE_12:
                    fieldDisplayTextView.setText(mContext.getResources().getString(R.string.cba_high_school_current_text));
                    break;

                default:
                    fieldDisplayTextView.setText("Default");
                    break;
            }
        }

        else
            fieldDisplayTextView.setText(mContext.getResources().getString(R.string.cba_high_school_current_text));

    }

}
