package com.utils.viewholders;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.utils.CustomUtils;
import com.utils.services.FirebaseStorageService;
import com.zelda.object.UserState;
import com.zeldav1.BursaryViewActivity;
import com.zeldav1.R;
import com.zelda.object.Bursary;

import static com.utils.Constants.BURSARY_TAG;
import static com.utils.Constants.USER_STATE_TAG;

public class BursaryViewHolder extends RecyclerView.ViewHolder{
    private static final String TAG = BursaryViewHolder.class.getSimpleName();

    private Context mContext;
    private View view;
    private CardView bursaryFeedCardView;
    private TextView bursaryTitleTextView;
    private TextView bursarySubtitleTextView;
    private TextView bursaryClosingDateTextView;
    private ImageView bursaryImageView, bursaryPinnedIcon;

    public BursaryViewHolder(View view) {
        super(view);
        this.view = view;
        mContext = view.getContext();
        bursaryFeedCardView = view.findViewById(R.id.item_feed_bursary_card_view);
        bursaryTitleTextView = view.findViewById(R.id.bursary_feed_item_title);
        bursarySubtitleTextView = view.findViewById(R.id.bursary_feed_item_subtitle);
        bursaryClosingDateTextView = view.findViewById(R.id.bursary_feed_item_closing_date);
        bursaryImageView = view.findViewById(R.id.bursary_feed_item_image);
        bursaryPinnedIcon = view.findViewById(R.id.item_bursary_pinned_icon);
    }

    public void bindData(final Bursary bursary, final UserState userState) {
        try {
            bursaryFeedCardView.setVisibility(View.VISIBLE);
            bursaryTitleTextView.setText(bursary.getTitle());
            bursaryClosingDateTextView.setText(bursaryClosingDateTextView.getContext().getResources().getString(R.string.application_view_date_closed, bursary.getClosingDate()));
            bursarySubtitleTextView.setText(bursary.getSubtitle());
            FirebaseStorageService.setGlideImage(bursaryImageView, CustomUtils.setImageNameOnType(Bursary.class, bursary.getId()));

            if (userState.getPinnedBursaries().contains(bursary.getId())){
                bursaryPinnedIcon.setVisibility(View.VISIBLE);
            }
            else {
                bursaryPinnedIcon.setVisibility(View.GONE);
            }

            view.setOnClickListener(v -> {

                final Intent intent;
                intent =  new Intent(mContext, BursaryViewActivity.class);
                intent.putExtra(BURSARY_TAG, bursary);
                intent.putExtra(USER_STATE_TAG, userState);
                mContext.startActivity(intent);
            });
        } catch (NullPointerException e){
            Log.e(TAG, "Bursary bind error: " + e.getMessage());
        }
    }

    public void hideBursaryViewHolder(){
        bursaryFeedCardView.setVisibility(View.GONE);
    }
}