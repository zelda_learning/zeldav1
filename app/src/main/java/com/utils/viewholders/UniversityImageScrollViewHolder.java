package com.utils.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.utils.CustomUtils;
import com.zeldav1.R;


public class UniversityImageScrollViewHolder extends RecyclerView.ViewHolder {
    private RecyclerView mRecyclerView;
    private ImageView universityImage;
    private String universityId;

    public UniversityImageScrollViewHolder(View view, RecyclerView recyclerView, String universityId) {
        super(view);
        universityImage = view.findViewById(R.id.item_university_view_scroll_image);
        mRecyclerView = recyclerView;
        this.universityId = universityId;
    }

    public void bindData(final String position) {
        CustomUtils.setUniversityScrollImage(universityImage, position, universityId);
    }
}