package com.utils.viewholders;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.utils.adapters.HintSpinnerAdapter;
import com.zelda.object.UserApplicationFormA;
import com.zeldav1.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import static com.utils.CustomUtils.getRedStar;

public class InputSpinnerViewHolder extends RecyclerView.ViewHolder {
    private static final String TAG = InputSpinnerViewHolder.class.getSimpleName();

    private TextView fieldHeadingTextView;
    private Spinner fieldInputSpinner;
    private Context mContext;

    public InputSpinnerViewHolder(View view) {
        super(view);
        fieldHeadingTextView = view.findViewById(R.id.item_input_spinner_field_heading);
        fieldInputSpinner = view.findViewById(R.id.item_input_spinner);
        mContext = view.getContext();
    }

    public void bindData(final String headingText, List<String> arrayList,
                         String spinnerTopic, AdapterView.OnItemSelectedListener adapterItemSelectedListener,
                         UserApplicationFormA userApplicationFormA) {
        try {
            if (null != arrayList){
                fieldHeadingTextView.setText(headingText);
                List<String> list = new ArrayList<>(arrayList);
                list.add(mContext.getResources().getString(R.string.item_spinner_prompt));
                HintSpinnerAdapter spinnerAdapter =  new HintSpinnerAdapter(mContext, R.layout.item_spinner, list);
                spinnerAdapter.setDropDownViewResource(R.layout.item_spinner);
                fieldInputSpinner.setTag(spinnerTopic);
                fieldInputSpinner.setAdapter(spinnerAdapter);

                if (null != userApplicationFormA && userApplicationFormA.containsKey(spinnerTopic))
                    fieldInputSpinner.setSelection(arrayList.indexOf(userApplicationFormA.getFormValue(spinnerTopic)));
                else
                    fieldInputSpinner.setSelection(spinnerAdapter.getCount());

                fieldInputSpinner.setOnItemSelectedListener(adapterItemSelectedListener);
            }
        }
        catch (NullPointerException e){
            Log.e(TAG, "InputEditTextViewHolder: " + e.getMessage());
        }
    }


    public void bindData(final String headingText, final String spinnerTopic, List<String> optionsArray,
                         AdapterView.OnItemSelectedListener adapterItemSelectedListener,
                         int indexOf) {
        try {
            if (optionsArray != null && optionsArray.size() > 0){

                Log.d(TAG, "Spinner: " + headingText + " spinnerTopic " + spinnerTopic + "arrayList " + optionsArray);

                List<String> list = new ArrayList<>(optionsArray);
                list.add(mContext.getResources().getString(R.string.item_spinner_prompt));
                
                HintSpinnerAdapter spinnerAdapter =  new HintSpinnerAdapter(mContext, R.layout.item_spinner, list);
                spinnerAdapter.setDropDownViewResource(R.layout.item_spinner);
                fieldInputSpinner.setTag(spinnerTopic);
                fieldInputSpinner.setAdapter(spinnerAdapter);

                fieldInputSpinner.setOnItemSelectedListener(adapterItemSelectedListener);

                if (indexOf != -1) {
                    fieldInputSpinner.setSelection(indexOf);
                    fieldHeadingTextView.setText(headingText);
                }
                else {
                    fieldInputSpinner.setSelection(spinnerAdapter.getCount());
                    fieldHeadingTextView.setText(getRedStar(headingText), TextView.BufferType.SPANNABLE);
                }
            }
        }
        catch (NullPointerException e){
            Log.e(TAG, "InputEditTextViewHolder: " + e.getMessage());
        }
    }
}