package com.utils.viewholders;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.utils.adapters.HintSpinnerAdapter;
import com.zelda.object.UserProfileUniversity;
import com.zeldav1.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.utils.Constants.FIRESTORE_CURRENT_DEGREE_STARTED_YEAR;
import static com.utils.Constants.FIRESTORE_DEGREE_EXPECTED_DURATION;
import static com.utils.Constants.FIRESTORE_PROSPECTIVE_DEGREE_APPLICATION_YEAR;
import static com.utils.Constants.FIRESTORE_UNIVERSITY_YEAR;
import static com.utils.Constants.UPU_CURRENT_DEGREE_NAME;
import static com.utils.Constants.UPU_CURRENT_DEGREE_STARTED_YEAR;
import static com.utils.Constants.UPU_CURRENT_UNIVERSITY_NAME;
import static com.utils.Constants.UPU_DEGREE_DURATION;
import static com.utils.Constants.UPU_PROSPECTIVE;
import static com.utils.Constants.UPU_PROSPECTIVE_DEGREE_APPLICATION_YEAR;
import static com.utils.Constants.UPU_PROSPECTIVE_DEGREE_NAME;
import static com.utils.Constants.UPU_PROSPECTIVE_UNIVERSITY_NAME;
import static com.utils.Constants.UPU_UNIVERSITY_YEAR;
import static com.utils.CustomUtils.getRedStar;

public class InputUniversityViewHolder extends RecyclerView.ViewHolder {
    private static final String TAG = InputUniversityViewHolder.class.getSimpleName();

    private TextView universityCurrentYearHeadingTextView, universityNameHeadingTextView, degreeNameHeadingTextView, leftSpinnerHeadingTextView, rightSpinnerHeadingTextView;
    private EditText universityNameEditText, degreeNameEditText;
    private Spinner universityCurrentYearSpinner, leftSpinner, rightSpinner;
    private View universityCurrentYearViewContainer, universityNameViewContainer, degreeNameViewContainer, leftSpinnerViewContainer, rightSpinnerViewContainer;
    private Context mContext;
    private HintSpinnerAdapter universityCurrentYearAdapter, leftSpinnerAdapter, rightSpinnerAdapter;

    public InputUniversityViewHolder(View view) {
        super(view);
        mContext = view.getContext();
        universityCurrentYearViewContainer = view.findViewById(R.id.item_input_current_university_spinner_container);
        universityCurrentYearHeadingTextView = universityCurrentYearViewContainer.findViewById(R.id.item_input_spinner_field_heading);
        universityCurrentYearSpinner = universityCurrentYearViewContainer.findViewById(R.id.item_input_spinner);

        leftSpinnerViewContainer = view.findViewById(R.id.item_input_left_spinner_container);
        leftSpinnerHeadingTextView = leftSpinnerViewContainer.findViewById(R.id.item_input_spinner_field_heading);
        leftSpinner = leftSpinnerViewContainer.findViewById(R.id.item_input_spinner);

        rightSpinnerViewContainer = view.findViewById(R.id.item_input_right_spinner_container);
        rightSpinnerHeadingTextView = rightSpinnerViewContainer.findViewById(R.id.item_input_spinner_field_heading);
        rightSpinner = rightSpinnerViewContainer.findViewById(R.id.item_input_spinner);

        universityNameViewContainer = view.findViewById(R.id.item_input_university_name_container);
        universityNameHeadingTextView = universityNameViewContainer.findViewById(R.id.item_input_edittext_field_heading);
        universityNameEditText = universityNameViewContainer.findViewById(R.id.item_input_edittext);

        degreeNameViewContainer = view.findViewById(R.id.item_input_degree_name_container);
        degreeNameHeadingTextView = degreeNameViewContainer.findViewById(R.id.item_input_edittext_field_heading);
        degreeNameEditText = degreeNameViewContainer.findViewById(R.id.item_input_edittext);

    }

    public void bindData(final UserProfileUniversity userProfileUniversity,
                         AdapterView.OnItemSelectedListener adapterItemSelectedListener,
                         TextWatcher universityNameWatcher, TextWatcher degreeNameWatcher) {
        try {
            Log.d(TAG, " userProfileUniversity" + userProfileUniversity.toString());
            String fieldValue = "";
            String spinnerValue = userProfileUniversity.getUniversityYear();

            if (null != userProfileUniversity.getUniversityYear() && !userProfileUniversity.getUniversityYear().equals("")) {
                fieldValue = userProfileUniversity.getUniversityYear();
                universityCurrentYearHeadingTextView.setText(UPU_UNIVERSITY_YEAR);
            }
            else
                universityCurrentYearHeadingTextView.setText(getRedStar(UPU_UNIVERSITY_YEAR), TextView.BufferType.SPANNABLE);

            if (fieldValue.equals(UPU_PROSPECTIVE))
                setProspectiveUniversityStudentView(userProfileUniversity);
            else
                setCurrentUniversityStudentView(userProfileUniversity);

            fieldValue = "";
            if (null != userProfileUniversity.getUniversityName() && !userProfileUniversity.getUniversityName().equals("")) {
                fieldValue = userProfileUniversity.getUniversityName();
            }

            universityNameEditText.setText(fieldValue);
            universityNameEditText.addTextChangedListener(universityNameWatcher);

            fieldValue = "";
            if (null != userProfileUniversity.getDegreeName() && !userProfileUniversity.getDegreeName().equals(""))
                fieldValue = userProfileUniversity.getDegreeName();

            degreeNameEditText.setText(fieldValue);
            degreeNameEditText.addTextChangedListener(degreeNameWatcher);

            List<String> arrayList = new ArrayList<>();
            Collections.addAll(arrayList, mContext.getResources().getStringArray(R.array.drop_down_university_year));
            arrayList.add(mContext.getResources().getString(R.string.item_spinner_prompt));

            universityCurrentYearAdapter =  new HintSpinnerAdapter(mContext, R.layout.item_spinner, arrayList);
            universityCurrentYearAdapter.setDropDownViewResource(R.layout.item_spinner);
            universityCurrentYearSpinner.setTag(FIRESTORE_UNIVERSITY_YEAR);
            universityCurrentYearSpinner.setAdapter(universityCurrentYearAdapter);
            universityCurrentYearSpinner.setOnItemSelectedListener(adapterItemSelectedListener);

            if (null != spinnerValue && spinnerValue.length() > 0)
                universityCurrentYearSpinner.setSelection(arrayList.indexOf(spinnerValue));
            else
                universityCurrentYearSpinner.setSelection(universityCurrentYearAdapter.getCount());

            leftSpinner.setOnItemSelectedListener(adapterItemSelectedListener);


            List<String> arrayList2 = new ArrayList<>();
            Collections.addAll(arrayList2, mContext.getResources().getStringArray(R.array.drop_down_expected_degree_years));
            arrayList2.add(mContext.getResources().getString(R.string.item_spinner_prompt));

            rightSpinnerAdapter =  new HintSpinnerAdapter(mContext, R.layout.item_spinner, arrayList2);
            rightSpinnerAdapter.setDropDownViewResource(R.layout.item_spinner);
            rightSpinner.setAdapter(rightSpinnerAdapter);
            rightSpinner.setTag(FIRESTORE_DEGREE_EXPECTED_DURATION);
            rightSpinner.setOnItemSelectedListener(adapterItemSelectedListener);

            if (null != userProfileUniversity.getDegreeDuration() && userProfileUniversity.getDegreeDuration().length() > 0)
                rightSpinner.setSelection(arrayList2.indexOf(userProfileUniversity.getDegreeDuration()));
            else
                rightSpinner.setSelection(rightSpinnerAdapter.getCount());

        }
        catch (NullPointerException e){
            Log.e(TAG, "InputEditTextViewHolder: " + e.getMessage());
        }
    }

    public void setProspectiveUniversityStudentView(UserProfileUniversity userProfileUniversity){

        List<String> arrayList1 = new ArrayList<>();
        Collections.addAll(arrayList1, mContext.getResources().getStringArray(R.array.drop_down_years_future));
        arrayList1.add(mContext.getResources().getString(R.string.item_spinner_prompt));

        leftSpinnerAdapter =  new HintSpinnerAdapter(mContext, R.layout.item_spinner, arrayList1);
        leftSpinnerAdapter.setDropDownViewResource(R.layout.item_spinner);
        leftSpinner.setTag(FIRESTORE_CURRENT_DEGREE_STARTED_YEAR);
        leftSpinner.setAdapter(leftSpinnerAdapter);

        leftSpinner.setTag(FIRESTORE_PROSPECTIVE_DEGREE_APPLICATION_YEAR);

        if (null != userProfileUniversity && null != userProfileUniversity.getUniversityName() && userProfileUniversity.getUniversityName().length() > 0)
            universityNameHeadingTextView.setText(UPU_PROSPECTIVE_UNIVERSITY_NAME);
        else
            universityNameHeadingTextView.setText(getRedStar(UPU_PROSPECTIVE_UNIVERSITY_NAME), TextView.BufferType.SPANNABLE);

        if (null != userProfileUniversity && null != userProfileUniversity.getDegreeName() && userProfileUniversity.getDegreeName().length() > 0)
            degreeNameHeadingTextView.setText(UPU_PROSPECTIVE_DEGREE_NAME);
        else
            degreeNameHeadingTextView.setText(getRedStar(UPU_PROSPECTIVE_DEGREE_NAME), TextView.BufferType.SPANNABLE);

        if (null != userProfileUniversity && null != userProfileUniversity.getDegreeApplicationYear() && userProfileUniversity.getDegreeApplicationYear().length() > 0) {
            leftSpinnerHeadingTextView.setText(UPU_PROSPECTIVE_DEGREE_APPLICATION_YEAR);
            leftSpinner.setSelection(leftSpinnerAdapter.getObsList().indexOf(userProfileUniversity.getDegreeApplicationYear()));
        }
        else {
            leftSpinnerHeadingTextView.setText(getRedStar(UPU_PROSPECTIVE_DEGREE_APPLICATION_YEAR), TextView.BufferType.SPANNABLE);
            leftSpinner.setSelection(leftSpinnerAdapter.getCount());
        }

        if (null != userProfileUniversity && null != userProfileUniversity.getDegreeDuration() && userProfileUniversity.getDegreeDuration().length() > 0)
            rightSpinnerHeadingTextView.setText(UPU_DEGREE_DURATION);
        else
            rightSpinnerHeadingTextView.setText(getRedStar(UPU_DEGREE_DURATION), TextView.BufferType.SPANNABLE);

    }

    public void setCurrentUniversityStudentView(UserProfileUniversity userProfileUniversity){

        List<String> arrayList1 = new ArrayList<>();
        Collections.addAll(arrayList1, mContext.getResources().getStringArray(R.array.drop_down_years));
        arrayList1.add(mContext.getResources().getString(R.string.item_spinner_prompt));

        leftSpinnerAdapter =  new HintSpinnerAdapter(mContext, R.layout.item_spinner, arrayList1);
        leftSpinnerAdapter.setDropDownViewResource(R.layout.item_spinner);
        leftSpinner.setAdapter(leftSpinnerAdapter);
        leftSpinner.setTag(FIRESTORE_CURRENT_DEGREE_STARTED_YEAR);

        if (null != userProfileUniversity && null != userProfileUniversity.getUniversityName() && userProfileUniversity.getUniversityName().length() > 0)
            universityNameHeadingTextView.setText(UPU_CURRENT_UNIVERSITY_NAME);
        else
            universityNameHeadingTextView.setText(getRedStar(UPU_CURRENT_UNIVERSITY_NAME), TextView.BufferType.SPANNABLE);

        if (null != userProfileUniversity && null != userProfileUniversity.getDegreeName() && userProfileUniversity.getDegreeName().length() > 0)
            degreeNameHeadingTextView.setText(UPU_CURRENT_DEGREE_NAME);
        else
            degreeNameHeadingTextView.setText(getRedStar(UPU_CURRENT_DEGREE_NAME), TextView.BufferType.SPANNABLE);

        if (null != userProfileUniversity && null != userProfileUniversity.getDegreeStartedYear() && userProfileUniversity.getDegreeStartedYear().length() > 0) {
            leftSpinnerHeadingTextView.setText(UPU_CURRENT_DEGREE_STARTED_YEAR);
            leftSpinner.setSelection(leftSpinnerAdapter.getObsList().indexOf(userProfileUniversity.getDegreeStartedYear()));
        }
        else {
            leftSpinnerHeadingTextView.setText(getRedStar(UPU_CURRENT_DEGREE_STARTED_YEAR), TextView.BufferType.SPANNABLE);
            leftSpinner.setSelection(leftSpinnerAdapter.getCount());
        }

        if (null != userProfileUniversity && null != userProfileUniversity.getDegreeDuration() && userProfileUniversity.getDegreeDuration().length() > 0)
            rightSpinnerHeadingTextView.setText(UPU_DEGREE_DURATION);
        else
            rightSpinnerHeadingTextView.setText(getRedStar(UPU_DEGREE_DURATION), TextView.BufferType.SPANNABLE);

    }

}