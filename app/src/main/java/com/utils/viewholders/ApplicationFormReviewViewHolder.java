package com.utils.viewholders;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.zeldav1.R;

import static com.utils.Constants.STRING_ADDRESS_DELIMITER;
import static com.utils.Constants.STRING_NEWLINE;

public class ApplicationFormReviewViewHolder extends RecyclerView.ViewHolder {
    private static final String TAG = ApplicationFormReviewViewHolder.class.getSimpleName();

    private TextView fieldHeadingTextView;
    private TextView fieldReviewTextView;

    public ApplicationFormReviewViewHolder(View view) {
        super(view);
        fieldHeadingTextView = view.findViewById(R.id.item_application_form_review_field_heading);
        fieldReviewTextView = view.findViewById(R.id.item_application_form_review_textview);
    }

    public void bindData(final String headingText, final String headingField) {
        try {
            String fieldValue =  headingField;
            Log.d(TAG, "headingText value: " + headingText + "\nfield value: " + fieldValue);
            if (null != fieldValue && fieldValue.contains(STRING_ADDRESS_DELIMITER)){
                fieldValue = fieldValue.replace(STRING_ADDRESS_DELIMITER, STRING_NEWLINE);
                fieldValue = fieldValue.substring(0, fieldValue.length()-1);
            }

            fieldHeadingTextView.setText(headingText);
            fieldReviewTextView.setText(fieldValue);
        }
        catch (NullPointerException e){
            Log.e(TAG, "ApplicationFormReviewViewHolder: " + e.getMessage());
        }
    }
}
