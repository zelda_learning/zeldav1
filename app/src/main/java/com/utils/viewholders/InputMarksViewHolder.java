package com.utils.viewholders;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.utils.adapters.SubjectsAdapter;
import com.zelda.object.Subject;
import com.zeldav1.R;

import java.util.List;

import static com.utils.Constants.UPS_11_SUBJECT_MAP;
import static com.utils.Constants.UPS_12_SUBJECT_MAP;
import static com.utils.Constants.UPS_CURRENT_GRADE_11;
import static com.utils.Constants.UPS_CURRENT_GRADE_12;
import static com.utils.Constants.UPS_CURRENT_GRADE_MATRICULATED;
import static com.utils.Constants.UPS_MATRIC_SUBJECT_MAP;
import static com.utils.Constants.UPS_SUBJECT_MAP;
import static com.utils.Constants.UPS_UNIVERSITY_SUBJECT_MAP;
import static com.utils.CustomUtils.getRedStar;

public class InputMarksViewHolder extends RecyclerView.ViewHolder {
    private static final String TAG = InputMarksViewHolder.class.getSimpleName();
    private RecyclerView recyclerView;
    private Context mContext;
    private ImageView mAddSubjectButton;
    private TextView mMarksHeadingTextView;
    private SubjectsAdapter mSubjectsAdapter;
    private List<Subject> subjectList = null;

    public InputMarksViewHolder(View view, SubjectsAdapter subjectsAdapter) {
        super(view);
        mContext = view.getContext();
        this.mSubjectsAdapter = subjectsAdapter;
        mMarksHeadingTextView = view.findViewById(R.id.item_input_marks_heading_textview);
        recyclerView = view.findViewById(R.id.item_input_marks_recyclerview);
        mAddSubjectButton = view.findViewById(R.id.item_input_marks_add_subjects_icon);

        mAddSubjectButton.setOnClickListener(v -> {
            mSubjectsAdapter.addSubject(new Subject());
            mSubjectsAdapter.notifyDataSetChanged();
        });

        recyclerView.setAdapter(mSubjectsAdapter);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mContext);
        recyclerView.setLayoutManager(mLayoutManager);
    }

    public void bindData(List<Subject> subjectList){
        Log.d(TAG, "Bind: " + subjectList);
        mSubjectsAdapter.updateSubjectList(subjectList);
        this.subjectList = subjectList;

        if (null != subjectList && subjectList.size() > 0)
            mMarksHeadingTextView.setText(UPS_UNIVERSITY_SUBJECT_MAP);
        else
            mMarksHeadingTextView.setText(getRedStar(UPS_UNIVERSITY_SUBJECT_MAP), TextView.BufferType.SPANNABLE);
    }

    public void bindData(List<Subject> subjectList, final String currentGrade) {
        Log.d(TAG, "Bind: " + subjectList + "currentGrade: " + currentGrade);
        mSubjectsAdapter.updateSubjectList(subjectList);
        this.subjectList = subjectList;
        setHighSchoolHeading(currentGrade);
    }

    public void setHighSchoolHeading(String currentGrade){
        String headingText = "";
        if(null != currentGrade) {
            switch (currentGrade) {
                case UPS_CURRENT_GRADE_MATRICULATED:
                    headingText = UPS_MATRIC_SUBJECT_MAP;
                    break;
                case UPS_CURRENT_GRADE_11:
                    headingText = UPS_11_SUBJECT_MAP;
                    break;
                case UPS_CURRENT_GRADE_12:
                    headingText = UPS_12_SUBJECT_MAP;
                    break;
                default:
                    headingText = UPS_SUBJECT_MAP;
                    break;
            }
        }
        else
            headingText = UPS_SUBJECT_MAP;

        Log.d(TAG, "setHighSchoolHeading: " + subjectList + "headingText " + headingText);

        if (null != subjectList && subjectList.size() > 0)
            mMarksHeadingTextView.setText(headingText);
        else
            mMarksHeadingTextView.setText(getRedStar(headingText), TextView.BufferType.SPANNABLE);
    }

    public SubjectsAdapter getSubjectsAdapter(){
        return mSubjectsAdapter;
    }
}
