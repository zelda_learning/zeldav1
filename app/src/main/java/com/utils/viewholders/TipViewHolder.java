package com.utils.viewholders;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.utils.CustomUtils;
import com.utils.services.FirebaseStorageService;
import com.zeldav1.R;
import com.zelda.object.Tip;

public class TipViewHolder extends RecyclerView.ViewHolder {
    private static final String TAG = TipViewHolder.class.getSimpleName();

    private TextView tipTitle;
    private TextView tipSubtitle;
    private ImageView tipImage;

    public TipViewHolder(View view) {
        super(view);

        tipTitle = view.findViewById(R.id.item_tip_title);
        tipSubtitle = view.findViewById(R.id.item_tip_subtitle);
        tipImage = view.findViewById(R.id.item_tip_image);
    }

    public void bindData(final Tip tip) {
        try {
            tipTitle.setText(tip.getTitle());
            tipSubtitle.setText(tip.getSubtitle());

            FirebaseStorageService.setGlideImage(tipImage, CustomUtils.setImageNameOnType(Tip.class, tip.getId()));
        }
        catch (NullPointerException e){
            Log.e(TAG, "Tip bind error: " + e.getMessage());
        }
    }
}