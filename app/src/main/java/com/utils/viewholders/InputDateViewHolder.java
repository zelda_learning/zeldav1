package com.utils.viewholders;

import android.app.DatePickerDialog;
import android.support.v7.widget.RecyclerView;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.zeldav1.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import static com.utils.Constants.DOB_HINT;
import static com.utils.CustomUtils.getRedStar;

public class InputDateViewHolder extends RecyclerView.ViewHolder {
    private static final String TAG = InputDateViewHolder.class.getSimpleName();

    private TextView fieldHeadingTextView;
    private TextView fieldInputTextView;
    private View container;
    private Calendar myCalendar = null;
    private DatePickerDialog.OnDateSetListener date;

    public InputDateViewHolder(View view) {
        super(view);
        container = view;
        fieldHeadingTextView = view.findViewById(R.id.item_input_date_field_heading);
        fieldInputTextView = view.findViewById(R.id.item_input_date_textview);
        fieldInputTextView.setText("");

        date = (view1, year, monthOfYear, dayOfMonth) -> {
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            setDate();
        };
    }

    private void setDate() {
        String myFormat = "yyyy-MM-dd"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.ENGLISH);

        fieldInputTextView.setText(sdf.format(myCalendar.getTime()));
    }

    public void bindData(final String headingText, final String fieldValue, final TextWatcher t) {
        try {
            if (null != fieldValue && !fieldValue.equals("")) {
                fieldInputTextView.setText(fieldValue);
                fieldHeadingTextView.setText(headingText);
            } else {
                fieldInputTextView.setText(DOB_HINT);
                fieldHeadingTextView.setText(getRedStar(headingText), TextView.BufferType.SPANNABLE);
            }
            fieldInputTextView.addTextChangedListener(t);

            myCalendar = Calendar.getInstance();
            fieldInputTextView.setFocusable(false);
            fieldInputTextView.setOnClickListener(v -> {
                new DatePickerDialog(container.getContext(), R.style.DatePickerTheme, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            });

        } catch (NullPointerException e) {
            Log.e(TAG, "" + e.getMessage());
        }
    }
}
