package com.utils.viewholders;

import android.support.v7.widget.RecyclerView;
import android.text.util.Linkify;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.utils.CustomUtils;
import com.utils.services.FirebaseStorageService;
import com.zelda.object.NBTQuiz;
import com.zeldav1.R;

import java.util.regex.Pattern;

public class QuizViewHolder extends RecyclerView.ViewHolder {
    private static final String TAG = QuizViewHolder.class.getSimpleName();
    private RecyclerView mRecyclerView;

    private TextView quizTitle;
    private TextView quizSubtitle;
    private TextView quizResults;
    private ImageView quizImage;

    public QuizViewHolder(View view, RecyclerView recyclerView) {
        super(view);
        quizTitle = view.findViewById(R.id.item_quiz_title);
        quizSubtitle = view.findViewById(R.id.item_quiz_subtitle);
        quizImage = view.findViewById(R.id.item_quiz_image);
        quizResults = view.findViewById(R.id.item_quiz_results);
        mRecyclerView = recyclerView;
    }

    public void bindData(final NBTQuiz NBTQuiz, final int userCorrect) {
        try {
            quizTitle.setText(NBTQuiz.getTitle());
            quizSubtitle.setText(NBTQuiz.getSubtitle());
            quizResults.setVisibility(View.VISIBLE);
            quizResults.setText(mRecyclerView.getContext().getString(R.string.item_quiz_results, userCorrect, NBTQuiz.getNumberQuestions()) );
            FirebaseStorageService.setGlideImage(quizImage, CustomUtils.setImageNameOnType(NBTQuiz.class, NBTQuiz.getId()));
        }
        catch (NullPointerException e){
            Log.e(TAG, "NBTQuiz bind error: " + e.getMessage());
        }
    }

    public void bindData(final NBTQuiz NBTQuiz) {
        try {
            quizTitle.setText(NBTQuiz.getTitle());
            quizSubtitle.setText(NBTQuiz.getSubtitle());
            quizResults.setVisibility(View.GONE);
            FirebaseStorageService.setGlideImage(quizImage, CustomUtils.setImageNameOnType(NBTQuiz.class, NBTQuiz.getId()));
        }
        catch (NullPointerException e){
            Log.e(TAG, "NBTQuiz bind error: " + e.getMessage());
        }
    }

    public void setNBTButton() {
        try {
            quizTitle.setText(itemView.getContext().getResources().getString(R.string.nbt_apply));
            quizTitle.setGravity(Gravity.CENTER);
            Pattern pattern = Pattern.compile("developer.android.com");
            Linkify.addLinks(quizTitle, pattern, "https://nbtests.uct.ac.za/");
        }
        catch (NullPointerException e){
            Log.e(TAG, "NBTQuiz bind error: " + e.getMessage());
        }
    }
}