package com.utils.viewholders;

import android.app.DatePickerDialog;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.zeldav1.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import static android.view.View.FOCUS_DOWN;
import static com.utils.Constants.UPP_ALT_CONTACT_NUMBER_TAG;
import static com.utils.Constants.UPP_CONTACT_NUMBER_TAG;
import static com.utils.Constants.UPP_DATE_BIRTH_TAG;
import static com.utils.Constants.UPP_ID_NUMBER_TAG;
import static com.utils.CustomUtils.getRedStar;

public class InputEditTextViewHolder extends RecyclerView.ViewHolder {
    private static final String TAG = InputEditTextViewHolder.class.getSimpleName();

    private TextView fieldHeadingTextView;
    private EditText fieldInputEditText;
    private View container;
    private Calendar myCalendar = null;
    private DatePickerDialog.OnDateSetListener date;

    public InputEditTextViewHolder(View view) {
        super(view);
        container = view;
        fieldHeadingTextView = view.findViewById(R.id.item_input_edittext_field_heading);
        fieldInputEditText = view.findViewById(R.id.item_input_edittext);
        fieldInputEditText.setText("");

        date = (view1, year, monthOfYear, dayOfMonth) -> {
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            setDate();
        };
    }

    private void setDate() {
        String myFormat = "yyyy-MM-dd"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.ENGLISH);

        fieldInputEditText.setText(sdf.format(myCalendar.getTime()));
    }

    public void bindData(final String headingText, final String fieldValue, final TextWatcher textWatcherListener) {
        try {
            if (null != fieldValue && !fieldValue.equals("")) {
                fieldInputEditText.setText(fieldValue);
                fieldHeadingTextView.setText(headingText);
            }else {
                fieldInputEditText.setText("");
                fieldHeadingTextView.setText(getRedStar(headingText), TextView.BufferType.SPANNABLE);
            }
            fieldInputEditText.setClickable(true);
            fieldInputEditText.setFocusable(true);
            switch (headingText){
                case UPP_ALT_CONTACT_NUMBER_TAG:
                case UPP_CONTACT_NUMBER_TAG:
                    fieldInputEditText.setInputType(InputType.TYPE_CLASS_PHONE);
                    break;
                case UPP_ID_NUMBER_TAG:
                    fieldInputEditText.setInputType(InputType.TYPE_CLASS_NUMBER);
                    break;
                case UPP_DATE_BIRTH_TAG:
                    myCalendar = Calendar.getInstance();
                    fieldInputEditText.setFocusable(false);
                    fieldInputEditText.setOnClickListener(v -> {
                        new DatePickerDialog(container.getContext(), date, myCalendar
                                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                                myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                    });
                    break;
                default:
                    fieldInputEditText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES );
                    break;
            }

            fieldInputEditText.addTextChangedListener(textWatcherListener);
            fieldInputEditText.setOnEditorActionListener((v, actionId, event) -> {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    View v1 = v.focusSearch(FOCUS_DOWN);
                    if (v1 != null) {
                        return !v.requestFocus(FOCUS_DOWN);
                    }
                    return false;
                } else return false;
            });
        }
        catch (NullPointerException e){
            Log.e(TAG, "InputEditTextViewHolder: " + e.getMessage());
        }
    }

    public void showViewContainer() {
        Log.e(TAG, "show: ");
        if (null != container) {
            ViewGroup.LayoutParams params = container.getLayoutParams();
            params.width = ViewGroup.LayoutParams.MATCH_PARENT;
            params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
            container.setLayoutParams(params);
            container.requestLayout();
        }
    }

    public void hideViewContainer() {
        Log.e(TAG, "hide: ");
        if (null != container) {
            ViewGroup.LayoutParams params = container.getLayoutParams();
            params.width = ViewGroup.LayoutParams.MATCH_PARENT;
            params.height = 0;
            container.setLayoutParams(params);
            container.requestLayout();
        }
    }
}
