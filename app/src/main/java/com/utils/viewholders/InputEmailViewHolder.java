package com.utils.viewholders;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.tolstykh.textviewrichdrawable.TextViewRichDrawable;
import com.zelda.object.UserProfilePersonal;
import com.zeldav1.R;

import static com.utils.Constants.UPP_EMAIL_TAG;
import static com.utils.CustomUtils.getRedStar;

public class InputEmailViewHolder extends RecyclerView.ViewHolder {
    private static final String TAG = InputEmailViewHolder.class.getSimpleName();

    private FirebaseUser currentU = FirebaseAuth.getInstance().getCurrentUser();
    TextViewRichDrawable verifiedTextView;
    private Context mContext;
    private String newEmail = "";

    private TextView fieldHeadingTextView, mEmailSentTextView;
    private ProgressBar mSendEmailLoadingProgressBar;
    private CardView mEmailAddressUpdateCardView, mResendVerificationEmailCardView, mEmailSendingProgressCardView;
    private EditText mEmailAddressEditText;
    private TextView mEmailChangedTextView, mUpdateEmail, mCancelEmailUpdate;

    public InputEmailViewHolder(View view) {
        super(view);
        this.mContext = view.getContext();
        fieldHeadingTextView = view.findViewById(R.id.item_input_email_textview);
        verifiedTextView = view.findViewById(R.id.item_input_verified_textview);
        mEmailAddressEditText = view.findViewById(R.id.item_input_email_edittext);

        mResendVerificationEmailCardView = view.findViewById(R.id.item_input_send_verification_card_view);

        mEmailSendingProgressCardView = view.findViewById(R.id.item_input_email_loading_card_view);
        mEmailSentTextView = view.findViewById(R.id.item_send_email_text_view);
        mSendEmailLoadingProgressBar = view.findViewById(R.id.item_send_email_progress_bar);

        mEmailAddressUpdateCardView = view.findViewById(R.id.item_input_email_update_email_card_view);
        mEmailChangedTextView = view.findViewById(R.id.item_email_changed_text_view);
        mUpdateEmail = view.findViewById(R.id.item_verification_action_yes);
        mCancelEmailUpdate = view.findViewById(R.id.item_verification_action_no);

        currentU.reload();

    }

    public void bindData(final UserProfilePersonal userProfilePersonal) {
        try {
            if (null != currentU && null != currentU.getEmail() && !currentU.getEmail().equals("")) {
                updateEmailAddressNotBlank();
            }else {
                updateEmailAddressBlank();
            }

            mUpdateEmail.setOnClickListener(v -> {
                Log.d(TAG, "mUpdateEmail clicked");
                sendingEmailLoading();
                currentU.updateEmail(userProfilePersonal.getEmailAddress())
                        .addOnCompleteListener(task -> {
                                if (task.isSuccessful()) {
                                    Log.d(TAG, "User email address updated to : " + userProfilePersonal.getEmailAddress());
                                    mEmailSentTextView.setText(mContext.getResources().getString(R.string.item_email_update_text));
                                    sendVerificationEmail();
                                } else {
                                    Log.d(TAG, "Exception: " + task.getException());
                                    if (null != task.getException() && task.getException().getMessage().equals(mContext.getResources().getString(R.string.item_email_changed_login_exception))) {
                                        mEmailSentTextView.setText(mContext.getResources().getString(R.string.item_email_changed_login_exception_text));
                                        userProfilePersonal.setEmailAddress(null);
                                        mEmailAddressEditText.setText(currentU.getEmail());
                                    } else if (null != task.getException())
                                        mEmailSentTextView.setText(task.getException().getMessage());
                                    sendEmailFailed();
                                }
                            });
            });

            mCancelEmailUpdate.setOnClickListener(v -> {
                Log.d(TAG, "mCancelEmailUpdate clicked");
                userProfilePersonal.setEmailAddress(null);
                mEmailAddressEditText.setText(currentU.getEmail());
                mEmailAddressUpdateCardView.setVisibility(View.GONE);

                if (currentU.isEmailVerified())
                    verifiedTextView.setVisibility(View.VISIBLE);
            });

            mEmailAddressEditText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    String newValue = editable.toString();
                    if (!newValue.equals("")) {
                        if (!newValue.equals(currentU.getEmail())) {
                            userProfilePersonal.setEmailAddress(newValue);
                            updateEmailAddressCard();
                        }
                        else {
                            mEmailAddressUpdateCardView.setVisibility(View.GONE);

                            if (currentU.isEmailVerified())
                                verifiedTextView.setVisibility(View.VISIBLE);
                        }
                    }
                }
            });
        }
        catch (NullPointerException e){
            Log.e(TAG, "InputEditTextViewHolder: " + e.getMessage());
        }
    }

    public void bindData(FirebaseUser user) {
        try {
            currentU = user;
            if (null != currentU && null != currentU.getEmail() && !currentU.getEmail().equals("")) {
                updateEmailAddressNotBlank();
            }else {
                updateEmailAddressBlank();
            }
            mUpdateEmail.setOnClickListener(v -> {
                Log.d(TAG, "mUpdateEmail clicked");
                sendingEmailLoading();
                currentU.updateEmail(newEmail)
                        .addOnCompleteListener(task -> {
                            if (task.isSuccessful()) {
                                Log.d(TAG, "User email address updated to : " + newEmail);
                                mEmailSentTextView.setText(mContext.getResources().getString(R.string.item_email_update_text));
                                sendVerificationEmail();
                            } else {
                                Log.d(TAG, "Exception: " + task.getException());
                                if (null != task.getException() && task.getException().getMessage().equals(mContext.getResources().getString(R.string.item_email_changed_login_exception))) {
                                    mEmailSentTextView.setText(mContext.getResources().getString(R.string.item_email_changed_login_exception_text));
                                    mEmailAddressEditText.setText(currentU.getEmail());
                                } else if (null != task.getException())
                                    mEmailSentTextView.setText(task.getException().getMessage());
                                sendEmailFailed();
                            }
                        });
            });

            mCancelEmailUpdate.setOnClickListener(v -> {
                Log.d(TAG, "mCancelEmailUpdate clicked");
                mEmailAddressEditText.setText(currentU.getEmail());
                mEmailAddressUpdateCardView.setVisibility(View.GONE);

                if (currentU.isEmailVerified())
                    verifiedTextView.setVisibility(View.VISIBLE);
            });

            mEmailAddressEditText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    String newValue = editable.toString();
                    if (!newValue.equals("")) {
                        if (!newValue.equals(currentU.getEmail())) {
                            newEmail = newValue;
                            updateEmailAddressCard();
                        }
                        else {
                            mEmailAddressUpdateCardView.setVisibility(View.GONE);

                            if (currentU.isEmailVerified())
                                verifiedTextView.setVisibility(View.VISIBLE);
                        }
                    }
                }
            });
        }
        catch (NullPointerException e){
            Log.e(TAG, "InputEditTextViewHolder: " + e.getMessage());
        }
    }

    private void updateEmailAddressNotBlank(){
        Log.d(TAG, "User email is non-null: " + currentU.getEmail());

        mEmailAddressEditText.setText(currentU.getEmail());

        if (currentU.isEmailVerified()) {
            updateEmailVerified();
        }
        else {
            updateEmailUnverified();
        }
    }

    private void updateEmailAddressBlank(){
        Log.d(TAG, "User email is null/blank");

        fieldHeadingTextView.setText(getRedStar(UPP_EMAIL_TAG), TextView.BufferType.SPANNABLE);
        mEmailAddressEditText.setText("");

        verifiedTextView.setVisibility(View.GONE);
        mResendVerificationEmailCardView.setVisibility(View.GONE);

        mEmailAddressUpdateCardView.setVisibility(View.GONE);
        mEmailSendingProgressCardView.setVisibility(View.GONE);
    }

    private void updateEmailAddressCard(){
        mEmailAddressUpdateCardView.setVisibility(View.VISIBLE);
        mUpdateEmail.setVisibility(View.VISIBLE);
        mCancelEmailUpdate.setVisibility(View.VISIBLE);
        mResendVerificationEmailCardView.setVisibility(View.GONE);
        mEmailSendingProgressCardView.setVisibility(View.GONE);
        verifiedTextView.setVisibility(View.GONE);
    }

    private void updateEmailVerified(){
        Log.d(TAG, "user Verified");
        fieldHeadingTextView.setText(UPP_EMAIL_TAG);
        verifiedTextView.setVisibility(View.VISIBLE);

        mResendVerificationEmailCardView.setVisibility(View.GONE);
    }

    private void updateEmailUnverified(){
        Log.d(TAG, "User email is not verified: ");

        fieldHeadingTextView.setText(getRedStar(UPP_EMAIL_TAG), TextView.BufferType.SPANNABLE);
        verifiedTextView.setVisibility(View.GONE);

        mResendVerificationEmailCardView.setVisibility(View.VISIBLE);
        mResendVerificationEmailCardView.setOnClickListener(v ->{
            sendingEmailLoading();
            sendVerificationEmail();
        });
    }

    private void sendVerificationEmail(){
        if (null != currentU) {
            if (null != currentU.getEmail()) {
                currentU.sendEmailVerification().addOnCompleteListener(task2 -> {
                    if (task2.isSuccessful()) {
                        sendingEmailComplete();
                    }

                    else {
                        Log.e(TAG, "sendEmailVerification", task2.getException());
                        if (null != task2.getException() && task2.getException().getMessage().equals(mContext.getResources().getString(R.string.item_email_changed_login_exception))) {
                            mEmailSentTextView.setText(task2.getException().getMessage());
                        }
                        else if (null != task2.getException())
                            mEmailSentTextView.setText(task2.getException().getMessage());

                        sendEmailFailed();
                    }
                });
            }
            else {
                Log.d(TAG, "User has no email.");
                Toast.makeText(mContext, mContext.getResources().getString(R.string.item_email_no_email_address), Toast.LENGTH_SHORT).show();
            }
        }
        else
            Log.d(TAG, "No current User .");

    }

    private void sendingEmailLoading(){
        Log.d(TAG, "Resending Email");
        mEmailSendingProgressCardView.setVisibility(View.VISIBLE);
        mSendEmailLoadingProgressBar.setVisibility(View.VISIBLE);
        mEmailSentTextView.setVisibility(View.GONE);
        mEmailAddressUpdateCardView.setVisibility(View.GONE);
        mResendVerificationEmailCardView.setVisibility(View.GONE);
    }

    private void sendingEmailComplete(){
        Log.d(TAG, "Sending Email complete successfully");
        Toast.makeText(mContext, mContext.getResources().getString(R.string.item_email_verification_sent_text, currentU.getEmail()), Toast.LENGTH_SHORT).show();

        mResendVerificationEmailCardView.setVisibility(View.GONE);
        mEmailSentTextView.setVisibility(View.VISIBLE);
        mSendEmailLoadingProgressBar.setVisibility(View.GONE);
        mEmailSendingProgressCardView.setVisibility(View.VISIBLE);

        TranslateAnimation anim = new TranslateAnimation(0, mEmailSendingProgressCardView.getWidth(), 0, 0);
        anim.setDuration(2000);
        anim.setStartOffset(3000);
        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                ConstraintLayout.LayoutParams params = (ConstraintLayout.LayoutParams) mEmailSendingProgressCardView.getLayoutParams();
                params.rightMargin   -= mEmailSendingProgressCardView.getWidth();
                mEmailSendingProgressCardView.setLayoutParams(params);
                mEmailSendingProgressCardView.setVisibility(View.GONE);

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        mEmailSendingProgressCardView.startAnimation(anim);
    }

    private void sendEmailFailed(){
        Log.d(TAG, "Sending Email failed");
        Toast.makeText(mContext, mContext.getResources().getString(R.string.item_email_verification_failed_text), Toast.LENGTH_SHORT).show();

        mResendVerificationEmailCardView.setVisibility(View.GONE);
        mEmailSentTextView.setVisibility(View.VISIBLE);
        mSendEmailLoadingProgressBar.setVisibility(View.GONE);
        mEmailSendingProgressCardView.setVisibility(View.VISIBLE);

        TranslateAnimation anim = new TranslateAnimation(0, mEmailSendingProgressCardView.getWidth(), 0, 0);
        anim.setDuration(200);
        anim.setStartOffset(3000);
        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                ConstraintLayout.LayoutParams params = (ConstraintLayout.LayoutParams) mEmailSendingProgressCardView.getLayoutParams();
                params.rightMargin   -= mEmailSendingProgressCardView.getWidth();
                mEmailSendingProgressCardView.setLayoutParams(params);
                mEmailSendingProgressCardView.setVisibility(View.GONE);

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        mEmailSendingProgressCardView.startAnimation(anim);

    }

}