package com.utils.viewholders;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;

import com.zelda.object.Subject;
import com.zeldav1.R;

import java.util.List;

public class SubjectsViewHolder extends RecyclerView.ViewHolder{
    private static final String TAG = SubjectsViewHolder.class.getSimpleName();
    private EditText percent, mTitleEditText;
    private Context mContext;

    public SubjectsViewHolder(View view) {
        super(view);
        percent = view.findViewById(R.id.item_subject_percent_edittext);
        mTitleEditText = view.findViewById(R.id.item_subject_name_edittext);
        mContext = view.getContext();
    }

    public void bindData(final Subject subject, final TextWatcher subjectNameWatcher, final TextWatcher subjectMarkWatcher) {
        String subjectHint = mContext.getResources().getString(R.string.user_state_subject_name_hint);
        if (subject.getSubjectName().equals(subjectHint)){
            mTitleEditText.setHint(subjectHint);
            mTitleEditText.setText("");
        }
        else {
            mTitleEditText.setText(subject.getSubjectName());
            percent.setText(String.format("%s", subject.getMark()));
        }
        if (subject.getMark() != 0.0)
            percent.setText(String.format("%s", subject.getMark()));
        else {
            percent.setHint("" + 0.0);
            percent.setText("");
        }

        percent.addTextChangedListener(subjectMarkWatcher);
        mTitleEditText.addTextChangedListener(subjectNameWatcher);
    }

    public EditText getmTitleEditText() {
        return mTitleEditText;
    }

    public EditText getPercent() {
        return percent;
    }
}
