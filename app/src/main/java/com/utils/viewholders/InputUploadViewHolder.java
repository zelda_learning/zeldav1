package com.utils.viewholders;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.utils.Constants;
import com.utils.services.FirebaseStorageService;
import com.zeldav1.FullScreenImageActivity;
import com.zeldav1.R;

import static com.utils.Constants.ELEMENT_N_A;
import static com.utils.Constants.ELEMENT_UPLOAD;
import static com.utils.Constants.FILE_TYPE_JPEG;
import static com.utils.Constants.FILE_TYPE_JPG;
import static com.utils.Constants.FILE_TYPE_MSWORD;
import static com.utils.Constants.FILE_TYPE_PDF;
import static com.utils.Constants.FILE_TYPE_PNG;
import static com.utils.Constants.FIRESTORE_DOCUMENTS_ID_COPY;
import static com.utils.Constants.FIRESTORE_DOCUMENTS_MATRIC_CERTIFICATE;
import static com.utils.Constants.FIRESTORE_DOCUMENTS_SCHOOL_REPORT;
import static com.utils.Constants.FIRESTORE_DOCUMENTS_UNIVERSITY_TRANSCRIPT;
import static com.utils.Constants.FIRESTORE_ID_COPY_UPLOADED;
import static com.utils.Constants.FIRESTORE_MC_UPLOADED;
import static com.utils.Constants.FIRESTORE_SCHOOL_REPORT_UPLOADED;
import static com.utils.Constants.FIRESTORE_UNIVERSITY_TRANSCRIPT_UPLOADED;
import static com.utils.Constants.IMAGE_ID_TAG;
import static com.utils.Constants.USER_PROFILE_DOCUMENTS_IMG;
import static com.utils.CustomUtils.getRedStar;

public class InputUploadViewHolder extends RecyclerView.ViewHolder  {

    private static final String TAG = InputUploadViewHolder.class.getSimpleName();

    private Context mContext;
    private View container;
    private ConstraintLayout contentContainer, uploadProgressContainer;
    private TextView uploadHeadingTextView, uploadStatusTextView, uploadingTextView;
    private ImageView uploadIconImageView, uploadAttachmentImageView, downloadIconImageView, thumbnailImageView, zoomImageView;
    private String type;

    public InputUploadViewHolder(View view) {
        super(view);
        this.mContext = view.getContext();
        container = view;
        uploadHeadingTextView = view.findViewById(R.id.item_input_upload_heading_textview);
        uploadIconImageView = view.findViewById(R.id.item_input_upload_icon_imageview);
        downloadIconImageView = view.findViewById(R.id.item_input_download_file);
        uploadStatusTextView = view.findViewById(R.id.item_input_upload_text_textview);
        uploadAttachmentImageView = view.findViewById(R.id.item_input_upload_attachment_file);
        thumbnailImageView = view.findViewById(R.id.item_input_download_thumbnail);
        zoomImageView = view.findViewById(R.id.item_input_zoom_thumbnail);

        contentContainer = view.findViewById(R.id.item_input_upload_content_container);

        uploadProgressContainer = view.findViewById(R.id.item_input_upload_progress_container);
        uploadingTextView = uploadProgressContainer.findViewById(R.id.item_loading_text);

    }

    public void bindData(final String headingText, final String uploaded, final String uploadType) {
        type = uploadType;
        contentContainer.setVisibility(View.VISIBLE);
        uploadProgressContainer.setVisibility(View.GONE);
        uploadIconImageView.setTag(type);
        uploadAttachmentImageView.setTag(type);
        downloadIconImageView.setTag(type);

        Log.d(TAG, "firestore: " + type + "  Uploaded: " + uploaded);
        if (null != uploaded && !uploaded.equals(ELEMENT_N_A)) {
            uploadingComplete(uploaded);
            uploadHeadingTextView.setText(headingText);
        }
        else {
            setNotUploaded();
            uploadHeadingTextView.setText(getRedStar(headingText), TextView.BufferType.SPANNABLE);
        }
    }

    private void setNotUploaded(){

        uploadStatusTextView.setVisibility(View.VISIBLE);
        uploadIconImageView.setVisibility(View.VISIBLE);
        downloadIconImageView.setVisibility(View.GONE);
        uploadIconImageView.setClickable(true);
        thumbnailImageView.setVisibility(View.GONE);
        zoomImageView.setVisibility(View.GONE);
        uploadIconImageView.setImageResource(R.drawable.baseline_cloud_upload_24);

        switch (type){
            case FIRESTORE_ID_COPY_UPLOADED:
                uploadStatusTextView.setText(mContext.getResources().getString(R.string.item_input_upload_id_not_complete_text));
                break;

            case FIRESTORE_MC_UPLOADED:
                uploadStatusTextView.setText(mContext.getResources().getString(R.string.item_input_upload_mc_not_complete_text));
                break;

            case FIRESTORE_SCHOOL_REPORT_UPLOADED:
                uploadStatusTextView.setText(mContext.getResources().getString(R.string.item_input_upload_sr_not_complete_text));
                break;

            case FIRESTORE_UNIVERSITY_TRANSCRIPT_UPLOADED:
                uploadStatusTextView.setText(mContext.getResources().getString(R.string.item_input_upload_ut_not_complete_text));
                break;

            case ELEMENT_UPLOAD:
                uploadStatusTextView.setText(mContext.getResources().getString(R.string.item_input_upload_not_complete_text));
                break;
        }
    }

    public void setUploading(double progress){
        Log.d(TAG, "Uploading progress " + progress);
        contentContainer.setVisibility(View.GONE);
        uploadProgressContainer.setVisibility(View.VISIBLE);
        uploadIconImageView.setClickable(false);
        uploadingTextView.setText(mContext.getResources().getString(R.string.loading_uploading_file, progress));
    }

    public void uploadingComplete(String uploadFileName){
        Log.d(TAG, "Upload completed: " + uploadFileName);
        uploadIconImageView.setVisibility(View.VISIBLE);
        uploadIconImageView.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_tick_green_48dp));
        uploadIconImageView.setClickable(false);

        String filepath;
        String postFix = uploadFileName.substring(uploadFileName.lastIndexOf(".") + 1);

        if (postFix.equals(FILE_TYPE_PDF) || postFix.equals(FILE_TYPE_MSWORD)){
            Log.d(TAG, "PDF FILE, enable download");
            downloadIconImageView.setVisibility(View.VISIBLE);
            thumbnailImageView.setVisibility(View.GONE);
            zoomImageView.setVisibility(View.GONE);
            uploadStatusTextView.setText(mContext.getResources().getString(R.string.item_input_upload_complete_pdf_text));
        }
        else if (postFix.equals(FILE_TYPE_JPG) || postFix.equals(FILE_TYPE_JPEG) || postFix.equals(FILE_TYPE_PNG)) {
            Log.d(TAG, "IMAGE FILE, disable downlaod");
            filepath = USER_PROFILE_DOCUMENTS_IMG;
            downloadIconImageView.setVisibility(View.VISIBLE);
            uploadStatusTextView.setText(mContext.getResources().getString(R.string.item_input_upload_complete_image_text));
            switch (type){
                case Constants.FIRESTORE_ID_COPY_UPLOADED:
                    filepath += "/" + FIRESTORE_DOCUMENTS_ID_COPY + "/" + uploadFileName;
                    FirebaseStorageService.setGlideImageWithDimens(createLoggerListener(uploadFileName), thumbnailImageView, filepath, 200, 200);
                    break;
                case Constants.FIRESTORE_MC_UPLOADED:
                    filepath += "/" + FIRESTORE_DOCUMENTS_MATRIC_CERTIFICATE + "/" + uploadFileName;
                    FirebaseStorageService.setGlideImageWithDimens(createLoggerListener(uploadFileName), thumbnailImageView, filepath, 200, 200);
                    break;
                case Constants.FIRESTORE_SCHOOL_REPORT_UPLOADED:
                    filepath += "/" + FIRESTORE_DOCUMENTS_SCHOOL_REPORT + "/" + uploadFileName;
                    FirebaseStorageService.setGlideImageWithDimens(createLoggerListener(uploadFileName), thumbnailImageView, filepath, 200, 200);
                    break;

                case Constants.FIRESTORE_UNIVERSITY_TRANSCRIPT_UPLOADED:
                    filepath += "/" + FIRESTORE_DOCUMENTS_UNIVERSITY_TRANSCRIPT + "/" + uploadFileName;
                    FirebaseStorageService.setGlideImageWithDimens(createLoggerListener(uploadFileName), thumbnailImageView, filepath, 200, 200);
                    break;
            }

            String finalFilepath = filepath;
            thumbnailImageView.setOnClickListener(v -> {
                Intent intent = new Intent(mContext, FullScreenImageActivity.class);
                intent.putExtra(IMAGE_ID_TAG, finalFilepath);
                mContext.startActivity(intent);
            });

            zoomImageView.setOnClickListener(v -> {
                Intent intent = new Intent(mContext, FullScreenImageActivity.class);
                intent.putExtra(IMAGE_ID_TAG, finalFilepath);
                mContext.startActivity(intent);
            });

        }

        contentContainer.setVisibility(View.VISIBLE);
        uploadProgressContainer.setVisibility(View.GONE);
    }

    public void uploadFailed(){
        Log.d(TAG, "Upload failed");
        downloadIconImageView.setVisibility(View.GONE);
        uploadIconImageView.setVisibility(View.VISIBLE);
        thumbnailImageView.setVisibility(View.GONE);
        zoomImageView.setVisibility(View.GONE);
        uploadIconImageView.setImageResource(R.drawable.baseline_cloud_upload_24);
        uploadStatusTextView.setText(mContext.getResources().getString(R.string.item_input_upload_failed_text));
    }

    private RequestListener<Drawable> createLoggerListener(final String name) {

        return new RequestListener<Drawable>(){

            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target target, boolean isFirstResource) {
                Log.d(TAG, "failed " + name);
                thumbnailImageView.setVisibility(View.GONE);
                zoomImageView.setVisibility(View.GONE);
                return false;
            }

            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target target, DataSource dataSource, boolean isFirstResource) {
                if (resource instanceof BitmapDrawable) {
                    thumbnailImageView.setVisibility(View.VISIBLE);
                    zoomImageView.setVisibility(View.VISIBLE);
                    Bitmap bitmap = ((BitmapDrawable) resource).getBitmap();
                    Log.d(TAG, String.format("Ready %s bitmap %,d bytes, size: %d x %d", name, bitmap.getByteCount(), bitmap.getWidth(), bitmap.getHeight()));
                }
                return false;
            }
        };
    }
}