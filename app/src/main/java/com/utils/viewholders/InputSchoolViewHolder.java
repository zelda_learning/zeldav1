package com.utils.viewholders;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import com.utils.Constants;
import com.utils.adapters.HintSpinnerAdapter;
import com.zelda.object.Address;
import com.zelda.object.School;
import com.zeldav1.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.utils.Constants.ELEMENT_SCHOOL_ENDED;
import static com.utils.Constants.ELEMENT_SCHOOL_STARTED;
import static com.utils.Constants.UPS_CURRENT_GRADE_11;
import static com.utils.Constants.UPS_CURRENT_GRADE_12;
import static com.utils.Constants.UPS_CURRENT_GRADE_MATRICULATED;
import static com.utils.Constants.UPS_LAST_SCHOOL_ADDRESS;
import static com.utils.Constants.UPS_LAST_SCHOOL_NAME;
import static com.utils.Constants.UPS_LAST_SCHOOL_YEAR_ENDED;
import static com.utils.Constants.UPS_LAST_SCHOOL_YEAR_STARTED;
import static com.utils.Constants.UPS_SCHOOL_ADDRESS;
import static com.utils.Constants.UPS_SCHOOL_NAME;
import static com.utils.Constants.UPS_SCHOOL_YEAR_ENDED;
import static com.utils.Constants.UPS_SCHOOL_YEAR_STARTED;
import static com.utils.CustomUtils.getRedStar;

public class InputSchoolViewHolder extends RecyclerView.ViewHolder {
    private static final String TAG = InputSchoolViewHolder.class.getSimpleName();

    private TextView schoolNameHeadingTextView, schoolMatricStartedHeadingTextView, schoolMatricEndedHeadingTextView, schoolAddressHeadingTextView;
    private EditText fieldInputSchoolName, fieldInputLine1, fieldInputLine2, fieldInputCity, fieldInputProvince, fieldInputZip;
    private Spinner schoolMatricStartedSpinner, schoolMatricEndedSpinner;
    private Context mContext;
    private Boolean mViewInitialized;

    public InputSchoolViewHolder(View view) {
        super(view);
        schoolNameHeadingTextView = view.findViewById(R.id.item_input_edittext_field_heading);
        fieldInputSchoolName = view.findViewById(R.id.item_input_edittext);

        View started = view.findViewById(R.id.item_input_year_started_container);
        schoolMatricStartedHeadingTextView = started.findViewById(R.id.item_input_spinner_field_heading);
        schoolMatricStartedSpinner = started.findViewById(R.id.item_input_spinner);

        View ended = view.findViewById(R.id.item_input_year_ended_container);
        schoolMatricEndedHeadingTextView = ended.findViewById(R.id.item_input_spinner_field_heading);
        schoolMatricEndedSpinner = ended.findViewById(R.id.item_input_spinner);

        schoolAddressHeadingTextView = view.findViewById(R.id.item_input_address_field_heading);
        fieldInputLine1 = view.findViewById(R.id.item_input_address_line_1_edittext);
        fieldInputLine2 = view.findViewById(R.id.item_input_address_line_2_edittext);
        fieldInputCity = view.findViewById(R.id.item_input_address_city_edittext);
        fieldInputProvince = view.findViewById(R.id.item_input_address_province_edittext);
        fieldInputZip = view.findViewById(R.id.item_input_address_zip_code_edittext);
        mContext = view.getContext();

        mViewInitialized = true;
    }

    public void bindData(final School school, final String currentGrade,
                         final TextWatcher textWatcherName, final TextWatcher textWatcherLine1,
                         final TextWatcher textWatcherLine2, final TextWatcher textWatcherCity,
                         final TextWatcher textWatcherProvince, final TextWatcher textWatcherZip,
                         AdapterView.OnItemSelectedListener adapterItemSelectedListener) {
        try {

            schoolMatricStartedHeadingTextView.setText(UPS_SCHOOL_YEAR_STARTED);
            schoolMatricEndedHeadingTextView.setText(UPS_SCHOOL_YEAR_ENDED);
            schoolAddressHeadingTextView.setText(UPS_SCHOOL_ADDRESS);

            List<String> arrayList1 = new ArrayList<>();
            Collections.addAll(arrayList1, mContext.getResources().getStringArray(R.array.drop_down_years));
            arrayList1.add(mContext.getResources().getString(R.string.item_spinner_prompt));

            HintSpinnerAdapter spinnerAdapter1 =  new HintSpinnerAdapter(mContext, R.layout.item_spinner, arrayList1);
            spinnerAdapter1.setDropDownViewResource(R.layout.item_spinner);
            schoolMatricStartedSpinner.setTag(ELEMENT_SCHOOL_STARTED);
            schoolMatricStartedSpinner.setAdapter(spinnerAdapter1);
            schoolMatricStartedSpinner.setOnItemSelectedListener(adapterItemSelectedListener);

            List<String> arrayList2 = new ArrayList<>();
            Collections.addAll(arrayList2, mContext.getResources().getStringArray(R.array.drop_down_years));
            arrayList2.add(mContext.getResources().getString(R.string.item_spinner_prompt));

            HintSpinnerAdapter spinnerAdapter2 =  new HintSpinnerAdapter(mContext, R.layout.item_spinner, arrayList2);
            spinnerAdapter2.setDropDownViewResource(R.layout.item_spinner);
            schoolMatricEndedSpinner.setTag(ELEMENT_SCHOOL_ENDED);
            schoolMatricEndedSpinner.setAdapter(spinnerAdapter2);
            schoolMatricEndedSpinner.setOnItemSelectedListener(adapterItemSelectedListener);

            fieldInputSchoolName.addTextChangedListener(textWatcherName);
            fieldInputLine1.addTextChangedListener(textWatcherLine1);
            fieldInputLine2.addTextChangedListener(textWatcherLine2);
            fieldInputCity.addTextChangedListener(textWatcherCity);
            fieldInputProvince.addTextChangedListener(textWatcherProvince);
            fieldInputZip.addTextChangedListener(textWatcherZip);

            if (null != school) {
                Log.d(TAG, " School" + school.toString() + "Current grade: " + currentGrade);

                setHighSchoolHeading(currentGrade, school);

                String fieldValue = school.getSchoolName();
                if (null != fieldValue && !fieldValue.equals(""))
                    fieldInputSchoolName.setText(fieldValue);
                else
                    fieldInputSchoolName.setText("");

                if (null != school.getSchoolYearStarted() && school.getSchoolYearStarted().length() > 0)
                    schoolMatricStartedSpinner.setSelection(arrayList1.indexOf(school.getSchoolYearStarted()));
                else
                    schoolMatricStartedSpinner.setSelection(spinnerAdapter1.getCount());

                if (null != school.getSchoolYearEnded() && school.getSchoolYearEnded().length() > 0)
                    schoolMatricEndedSpinner.setSelection(arrayList2.indexOf(school.getSchoolYearEnded()));
                else
                    schoolMatricEndedSpinner.setSelection(spinnerAdapter2.getCount());

                Address address = school.getSchoolAddress();

                if (address != null && null != address.getFirestoreString() && !address.getFirestoreString().equals(Constants.BLANK_ADDRESS_DELIMITER)) {
                    fieldInputLine1.setText(address.getLine1());
                    fieldInputLine2.setText(address.getLine2());
                    fieldInputCity.setText(address.getCity());
                    fieldInputProvince.setText(address.getProvince());
                    fieldInputZip.setText(address.getZip());
                } else {
                    fieldInputLine1.setText("");
                    fieldInputLine2.setText("");
                    fieldInputCity.setText("");
                    fieldInputProvince.setText("");
                    fieldInputZip.setText("");
                }
            }
        }
        catch (NullPointerException e){
            Log.e(TAG, "Error: " + e.getMessage());
        }
    }

    public void setHighSchoolHeading(String currentGrade, School school){
        if (mViewInitialized) {
            if (null != currentGrade && currentGrade.equals(UPS_CURRENT_GRADE_MATRICULATED)) {

                if (null != school && null != school.getSchoolName() && !school.getSchoolName().equals("")) {
                    schoolNameHeadingTextView.setText(UPS_LAST_SCHOOL_NAME);
                }else {
                    schoolNameHeadingTextView.setText(getRedStar(UPS_LAST_SCHOOL_NAME), TextView.BufferType.SPANNABLE);
                }

                if (null != school && null != school.getSchoolName() && !school.getSchoolName().equals("")) {
                    schoolMatricStartedHeadingTextView.setText(UPS_LAST_SCHOOL_YEAR_STARTED);
                }else {
                    schoolMatricStartedHeadingTextView.setText(getRedStar(UPS_LAST_SCHOOL_YEAR_STARTED), TextView.BufferType.SPANNABLE);
                }

                if (null != school && null != school.getSchoolName() && !school.getSchoolName().equals("")) {
                    schoolMatricEndedHeadingTextView.setText(UPS_LAST_SCHOOL_YEAR_ENDED);
                }else {
                    schoolMatricEndedHeadingTextView.setText(getRedStar(UPS_LAST_SCHOOL_YEAR_ENDED), TextView.BufferType.SPANNABLE);
                }

                if (null != school && null != school.getSchoolName() && !school.getSchoolName().equals("")) {
                    schoolAddressHeadingTextView.setText(UPS_LAST_SCHOOL_ADDRESS);
                }else {
                    schoolAddressHeadingTextView.setText(getRedStar(UPS_LAST_SCHOOL_ADDRESS), TextView.BufferType.SPANNABLE);
                }

            } else {

                if (null != school && null != school.getSchoolName() && !school.getSchoolName().equals("")) {
                    schoolNameHeadingTextView.setText(UPS_SCHOOL_NAME);
                }else {
                    schoolNameHeadingTextView.setText(getRedStar(UPS_SCHOOL_NAME), TextView.BufferType.SPANNABLE);
                }

                if (null != school && null != school.getSchoolName() && !school.getSchoolName().equals("")) {
                    schoolMatricStartedHeadingTextView.setText(UPS_SCHOOL_YEAR_STARTED);
                }else {
                    schoolMatricStartedHeadingTextView.setText(getRedStar(UPS_SCHOOL_YEAR_STARTED), TextView.BufferType.SPANNABLE);
                }

                if (null != school && null != school.getSchoolName() && !school.getSchoolName().equals("")) {
                    schoolMatricEndedHeadingTextView.setText(UPS_SCHOOL_YEAR_ENDED);
                }else {
                    schoolMatricEndedHeadingTextView.setText(getRedStar(UPS_SCHOOL_YEAR_ENDED), TextView.BufferType.SPANNABLE);
                }

                if (null != school && null != school.getSchoolName() && !school.getSchoolName().equals("")) {
                    schoolAddressHeadingTextView.setText(UPS_SCHOOL_ADDRESS);
                }else {
                    schoolAddressHeadingTextView.setText(getRedStar(UPS_SCHOOL_ADDRESS), TextView.BufferType.SPANNABLE);
                }
            }
        }
    }
}