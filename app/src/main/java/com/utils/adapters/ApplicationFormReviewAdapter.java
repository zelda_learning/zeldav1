package com.utils.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.utils.FragmentCommunicator;
import com.utils.viewholders.InputAddressViewHolder;
import com.utils.viewholders.InputEditTextViewHolder;
import com.utils.viewholders.InputSpinnerViewHolder;
import com.utils.viewholders.ApplicationFormReviewViewHolder;
import com.zelda.object.ApplicationFormSectionA;
import com.zelda.object.UserApplicationFormA;
import com.zelda.object.UserProfilePersonal;
import com.zeldav1.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import static com.utils.Constants.ELEMENT_ADDRESS;
import static com.utils.Constants.ELEMENT_AREA;
import static com.utils.Constants.ELEMENT_GENDER;
import static com.utils.Constants.ELEMENT_INCOME;

public class ApplicationFormReviewAdapter extends RecyclerView.Adapter{
    private static final String TAG = ApplicationFormReviewAdapter.class.getSimpleName();

    private static final int TYPE_EDIT_TEXT = 1;
    private static final int TYPE_SPINNER = 2;
    private static final int TYPE_ADDRESS = 3;
    private static final int TYPE_REVIEW = 4;

    private List<String> mApplicationFieldKeysList;
    private UserApplicationFormA mUserApplicationFormA;
    private ApplicationFormSectionA mApplicationFormSectionA;

    private String applicationFormAddressNumberedKey;

    private FragmentCommunicator fragmentCommunicator;

    private boolean review = false;

    public ApplicationFormReviewAdapter(){
        mApplicationFieldKeysList = new ArrayList<>();
    }

    public ApplicationFormReviewAdapter(boolean review){
        mApplicationFieldKeysList = new ArrayList<>();
        this.review = review;
    }

    public ApplicationFormReviewAdapter(ApplicationFormSectionA applicationFormSectionA, UserApplicationFormA userApplicationFormA, boolean review){
        this.review = review;
        this.mApplicationFormSectionA = applicationFormSectionA;
        this.mUserApplicationFormA = userApplicationFormA;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView;
        itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_application_form_review_element, parent, false);
        return new ApplicationFormReviewViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (mApplicationFieldKeysList != null && mApplicationFormSectionA  != null && mUserApplicationFormA != null) {
            if (position < mApplicationFieldKeysList.size()) {
                ((ApplicationFormReviewViewHolder) holder).bindData(mApplicationFormSectionA.getFormValue(mApplicationFieldKeysList.get(position)), mUserApplicationFormA.getFormValue(mApplicationFieldKeysList.get(position)));
            }
        }
    }

    @Override
    public int getItemCount() {
        if (null != mApplicationFormSectionA)
            return mApplicationFormSectionA.getNumberElements();
        else
            return 0;
    }

    private String getPositionPrefix(int position) {
        position++;
        String positionPrefix = position + "";

        if (position < 10)
            positionPrefix = "0" + positionPrefix;

        return positionPrefix;
    }

    public void updateApplicationForm(ApplicationFormSectionA applicationFormSectionA) {
        mApplicationFormSectionA = applicationFormSectionA;

        if (mApplicationFieldKeysList.size() != 0)
            mApplicationFieldKeysList = new ArrayList<>();

        mApplicationFieldKeysList.addAll(applicationFormSectionA.getFormValues().keySet());
        Collections.sort(mApplicationFieldKeysList);

        for (int keyPosition = 0; keyPosition < mApplicationFieldKeysList.size(); keyPosition++){
            String positionPrefix = getPositionPrefix(keyPosition);
            final String caseAddress = positionPrefix + "_" + ELEMENT_ADDRESS;
            if (mApplicationFieldKeysList.get(keyPosition).equals(caseAddress)) {
                applicationFormAddressNumberedKey = caseAddress;
            }
        }
        createApplicationAddressObject();
    }

    public void updateUserApplicationFormA(UserApplicationFormA userApplicationFormA) {
        mUserApplicationFormA = userApplicationFormA;
        createApplicationAddressObject();
    }

    private void createApplicationAddressObject() {
        if (mUserApplicationFormA != null) {
            String addressString = mUserApplicationFormA.getNumberedAddressString();
            if (null != addressString)
                mUserApplicationFormA.createAddress(addressString);
        }
    }

    public UserApplicationFormA getUserApplicationForm(){
        return mUserApplicationFormA;
    }

}
