package com.utils.adapters;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;

import com.utils.fragments.CBAFormUniversityFragment;
import com.utils.fragments.UserViewPersonalFragment;
import com.utils.fragments.UserViewSchoolFragment;
import com.zelda.object.UserProfilePersonal;
import com.zelda.object.UserProfileSchool;
import com.zelda.object.UserState;

import java.util.ArrayList;
import java.util.List;

import static com.utils.Constants.USER_PROFILE_HEADING_PERSONAL;
import static com.utils.Constants.USER_PROFILE_HEADING_SCHOOL;
import static com.utils.Constants.USER_PROFILE_HEADING_UNIVERSITY;

public class ViewPagerAdapter extends SmartFragmentStatePagerAdapter {
    private static final String TAG = "UserViewPager";
    private final List<Fragment> mFragmentList = new ArrayList<>();
    private final List<String> mFragmentTitleList = new ArrayList<>();
    private boolean cba;
    private UserState mUserState;
    private UserProfilePersonal mUserProfilePersonal;
    private UserProfileSchool mUserProfileSchool;

    public ViewPagerAdapter(FragmentManager manager, boolean cba, UserState userState, UserProfilePersonal userProfilePersonal, UserProfileSchool userProfileSchool) {
        super(manager);
        this.cba= cba;
        this.mUserState = userState;
        this.mUserProfilePersonal = userProfilePersonal;
        this.mUserProfileSchool = userProfileSchool;
    }

    public ViewPagerAdapter(FragmentManager manager, boolean cba) {
        super(manager);
        this.cba= cba;
    }

    public int getItemPosition(@NonNull Object item) {
        Log.d(TAG, "item post");
        if (item instanceof UserViewPersonalFragment) {
            ((UserViewPersonalFragment) item).updateUserProfilePersonal(mUserProfilePersonal);
        }
        if (item instanceof UserViewSchoolFragment) {
            ((UserViewSchoolFragment) item).updateUserProfileSchool(mUserProfileSchool);
        }
        if (item instanceof CBAFormUniversityFragment) {
            ((CBAFormUniversityFragment) item).updateUserProfileSchool(mUserProfileSchool);
        }
        return super.getItemPosition(item);
    }

    @Override
    public Fragment getItem(int position) {
        if (cba){
            switch (position) {
                case 0:
                    return UserViewPersonalFragment.newInstance(mUserState, mUserProfilePersonal);
                case 1:
                    return UserViewSchoolFragment.newInstance(mUserState, mUserProfileSchool);
                case 2:
                    return CBAFormUniversityFragment.newInstance(mUserState, mUserProfileSchool);
                default:
                    return null;
            }
        } else {
            return mFragmentList.get(position);
        }
    }

    @Override
    public int getCount() {
        if (cba){
            return 3;
        }
        else
            return mFragmentList.size();
    }

    public void addFragment(Fragment fragment, String title) {
        mFragmentList.add(fragment);
        mFragmentTitleList.add(title);
    }

    public CharSequence setPageTitle(String newTitle, int position) {
        if (cba){
            switch (position){
                case 0:
                    return mFragmentTitleList.set(position, USER_PROFILE_HEADING_PERSONAL);

                case 1:
                    return mFragmentTitleList.set(position, USER_PROFILE_HEADING_SCHOOL);

                case 2:
                    return mFragmentTitleList.set(position, USER_PROFILE_HEADING_UNIVERSITY);

                default:
                    return mFragmentTitleList.set(position, USER_PROFILE_HEADING_SCHOOL);
            }
        }
        else
            return mFragmentTitleList.set(position, newTitle);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if (cba){
            switch (position){
                case 0:
                    return USER_PROFILE_HEADING_PERSONAL;

                case 1:
                    return USER_PROFILE_HEADING_SCHOOL;

                case 2:
                    return USER_PROFILE_HEADING_UNIVERSITY;

                default:
                    return USER_PROFILE_HEADING_SCHOOL;
            }
        }
        else
            return mFragmentTitleList.get(position);
    }

    public void updateUserProfilePersonal(UserProfilePersonal userProfilePersonal){
        this.mUserProfilePersonal = userProfilePersonal;
        notifyDataSetChanged();
    }

    public void updateUserProfileSchool(UserProfileSchool userProfileSchool){
        this.mUserProfileSchool = userProfileSchool;
        notifyDataSetChanged();
    }

    public void updateUserState(UserState userState){
        this.mUserState = userState;
    }

}