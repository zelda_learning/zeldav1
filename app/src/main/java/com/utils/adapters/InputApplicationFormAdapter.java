package com.utils.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import com.utils.FragmentCommunicator;
import com.utils.viewholders.InputAddressViewHolder;
import com.utils.viewholders.InputEditTextViewHolder;
import com.utils.viewholders.InputSpinnerViewHolder;
import com.zelda.object.ApplicationFormSectionA;
import com.zelda.object.UserApplicationFormA;
import com.zeldav1.R;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.utils.Constants.ELEMENT_ADDRESS;
import static com.utils.Constants.ELEMENT_AREA;
import static com.utils.Constants.ELEMENT_GENDER;
import static com.utils.Constants.ELEMENT_INCOME;

public class InputApplicationFormAdapter extends RecyclerView.Adapter{
    private static final String TAG = InputApplicationFormAdapter.class.getSimpleName();

    private static final int TYPE_EDIT_TEXT = 1;
    private static final int TYPE_SPINNER = 2;
    private static final int TYPE_ADDRESS = 3;

    private List<String> mApplicationFieldKeysList;
    private UserApplicationFormA mUserApplicationFormA;
    private ApplicationFormSectionA mApplicationFormSectionA;

    private FragmentCommunicator fragmentCommunicator;
    private Context mContext;

    public InputApplicationFormAdapter(Context context, FragmentCommunicator communicator){
        mApplicationFieldKeysList = new ArrayList<>();
        this.fragmentCommunicator = communicator;
        this.mContext = context;
    }

    public InputApplicationFormAdapter(Context context, ApplicationFormSectionA applicationFormSectionA, UserApplicationFormA userApplicationFormA, FragmentCommunicator communicator){
        this.mApplicationFormSectionA = applicationFormSectionA;
        this.mUserApplicationFormA = userApplicationFormA;
        this.fragmentCommunicator = communicator;
        this.mContext = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView;
        switch (viewType){
            case TYPE_EDIT_TEXT:
                itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_input_edittext, parent, false);
                return new InputEditTextViewHolder(itemView);

            case TYPE_SPINNER:
                itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_input_spinner, parent, false);
                return new InputSpinnerViewHolder(itemView);

            case TYPE_ADDRESS:
                itemView= LayoutInflater.from(parent.getContext()).inflate(R.layout.item_input_address, parent, false);
                return new InputAddressViewHolder(itemView);

            default:
                return null;
        }

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        TextWatcher textWatcherLine1 = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String newValue = charSequence.toString();
                if (!newValue.equals("")) {
                    if (!mUserApplicationFormA.containsValue(charSequence.toString())) {
                        mUserApplicationFormA.setAddressLine1(charSequence.toString());
                        fragmentCommunicator.updateProgressBar(mUserApplicationFormA.getNumberElements());
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };

        TextWatcher textWatcherLine2 = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }


            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String newValue = charSequence.toString();
                if (!newValue.equals("")) {
                    mUserApplicationFormA.setAddressLine2(charSequence.toString());
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        };

        TextWatcher textWatcherCity = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String newValue = charSequence.toString();
                if (!newValue.equals("")) {
                    mUserApplicationFormA.setAddressCity(charSequence.toString());
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        };

        TextWatcher textWatcherProvince = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String newValue = charSequence.toString();
                if (!newValue.equals("")) {
                    mUserApplicationFormA.setAddressProvince(charSequence.toString());
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        };

        TextWatcher textWatcherZip = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String newValue = charSequence.toString();
                if (!newValue.equals("")) {
                    mUserApplicationFormA.setAddressZip(charSequence.toString());
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        };

        String positionPrefix = getPositionPrefix(holder.getAdapterPosition());

        if (mUserApplicationFormA != null && mApplicationFormSectionA != null) {
            switch (holder.getItemViewType()) {
                case TYPE_EDIT_TEXT:
                    TextWatcher t = new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                        }

                        @Override
                        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                        }

                        @Override
                        public void afterTextChanged(Editable editable) {
                            String newValue = editable.toString();
                            if (!newValue.equals("")) {
                                String applicationKey = mApplicationFieldKeysList.get(holder.getAdapterPosition());

                                if (!mUserApplicationFormA.containsValue(newValue)) {
                                    mUserApplicationFormA.put(applicationKey, newValue);
                                    fragmentCommunicator.updateProgressBar(mUserApplicationFormA.getNumberElements());
                                }
                            }
                        }
                    };
                    ((InputEditTextViewHolder) holder).bindData(mApplicationFormSectionA.getFormValue(mApplicationFieldKeysList.get(holder.getAdapterPosition())), mUserApplicationFormA.getFormValue(mApplicationFieldKeysList.get(position)), t);
                    break;

                case TYPE_SPINNER:
                    List<String> arrayList = null;
                    String spinnerTopic = "";
                    AdapterView.OnItemSelectedListener spinnerSelectionListener = new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            String currentItemString = parent.getItemAtPosition(position).toString();
                            if (!currentItemString.equals(mContext.getResources().getString(R.string.item_spinner_prompt))) {
                                mUserApplicationFormA.put(parent.getTag().toString(), currentItemString);
                                ((TextView) parent.getChildAt(0)).setTextColor(mContext.getResources().getColor(R.color.Black));
                            }
                            else {
                                ((TextView) parent.getChildAt(0)).setTextColor(mContext.getResources().getColor(R.color.Gray));
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    };

                    final String caseGender = positionPrefix + "_" + ELEMENT_GENDER;
                    final String caseArea = positionPrefix + "_" + ELEMENT_AREA;
                    final String caseIncome = positionPrefix + "_" + ELEMENT_INCOME;
                    if (mApplicationFieldKeysList.get(holder.getAdapterPosition()).equals(caseGender)) {
                        arrayList = mApplicationFormSectionA.getGender();
                        spinnerTopic = caseGender;
                    } else if (mApplicationFieldKeysList.get(holder.getAdapterPosition()).equals(caseArea)) {
                        arrayList = mApplicationFormSectionA.getArea();
                        spinnerTopic = caseArea;
                    } else if (mApplicationFieldKeysList.get(holder.getAdapterPosition()).equals(caseIncome)) {
                        arrayList = mApplicationFormSectionA.getIncome();
                        spinnerTopic = caseIncome;
                    }

                    Log.d(TAG, "Spinner: " + mApplicationFieldKeysList.get(holder.getAdapterPosition()) + "spinnerTopic " + spinnerTopic + "arrayList " + arrayList);

                    ((InputSpinnerViewHolder) holder).bindData(mApplicationFormSectionA.getFormValue(mApplicationFieldKeysList.get(holder.getAdapterPosition())), arrayList, spinnerTopic, spinnerSelectionListener, mUserApplicationFormA);
                    break;

                case TYPE_ADDRESS:

                    ((InputAddressViewHolder) holder).
                            bindData(mApplicationFormSectionA.getFormValue(mApplicationFieldKeysList.get(position)),
                                    textWatcherLine1, textWatcherLine2, textWatcherCity,
                                    textWatcherProvince, textWatcherZip, mUserApplicationFormA.getAddress());
                    break;

            }
        }
    }

    @Override
    public int getItemCount() {
        if (null != mApplicationFormSectionA)
            return mApplicationFormSectionA.getNumberElements();
        else
            return 0;
    }

    @Override
    public int getItemViewType(int position) {
        if (null != mApplicationFieldKeysList) {
            String positionPrefix = getPositionPrefix(position);

            final String caseGender = positionPrefix + "_" + ELEMENT_GENDER;
            final String caseArea = positionPrefix + "_" + ELEMENT_AREA;
            final String caseIncome = positionPrefix + "_" + ELEMENT_INCOME;
            final String caseAddress = positionPrefix + "_" + ELEMENT_ADDRESS;

            if (mApplicationFieldKeysList.get(position).equals(caseGender) ||
                    mApplicationFieldKeysList.get(position).equals(caseArea) ||
                    mApplicationFieldKeysList.get(position).equals(caseIncome)) {
                return TYPE_SPINNER;
            }

            else if (mApplicationFieldKeysList.get(position).equals(caseAddress)) {
                if (mUserApplicationFormA != null)
                    mUserApplicationFormA.setNumberedAddressField(caseAddress);
                return TYPE_ADDRESS;
            }
            else {
                return TYPE_EDIT_TEXT;
            }
        }
        else
            return -1;
    }

    private String getPositionPrefix(int position) {
        position++;
        String positionPrefix = position + "";

        if (position < 10)
            positionPrefix = "0" + positionPrefix;

        return positionPrefix;
    }

    public void updateApplicationForm(ApplicationFormSectionA applicationFormSectionA) {
        mApplicationFormSectionA = applicationFormSectionA;

        if (mApplicationFieldKeysList.size() != 0)
            mApplicationFieldKeysList = new ArrayList<>();

        mApplicationFieldKeysList.addAll(applicationFormSectionA.getFormValues().keySet());
        Collections.sort(mApplicationFieldKeysList);

        createApplicationAddressObject();

    }

    public void updateUserApplicationFormA(UserApplicationFormA userApplicationFormA) {
        mUserApplicationFormA = userApplicationFormA;
        createApplicationAddressObject();
    }

    private void createApplicationAddressObject() {
        if (mUserApplicationFormA != null) {
            String addressString = mUserApplicationFormA.getNumberedAddressString();
            if (null != addressString)
                mUserApplicationFormA.createAddress(addressString);
        }
    }

    public UserApplicationFormA getUserApplicationForm(){
        return mUserApplicationFormA;
    }
}
