package com.utils.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import com.utils.viewholders.InputAddressViewHolder;
import com.utils.viewholders.InputDateViewHolder;
import com.utils.viewholders.InputEditTextViewHolder;
import com.utils.viewholders.InputEmailViewHolder;
import com.utils.viewholders.InputSpinnerViewHolder;
import com.utils.viewholders.InputUploadViewHolder;
import com.utils.viewholders.ToggleViewHolder;
import com.zelda.object.UserProfilePersonal;
import com.zeldav1.R;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import static com.utils.Constants.DOB_HINT;
import static com.utils.Constants.ELEMENT_ADDRESS;
import static com.utils.Constants.ELEMENT_EMAIL;
import static com.utils.Constants.ELEMENT_GENDER;
import static com.utils.Constants.ELEMENT_HOME_ORIGIN;
import static com.utils.Constants.ELEMENT_INCOME;
import static com.utils.Constants.ELEMENT_NATIONALITY;
import static com.utils.Constants.ELEMENT_RACE;
import static com.utils.Constants.ELEMENT_UPLOAD;
import static com.utils.Constants.FIRESTORE_COMBINED_YEARLY_INCOME;
import static com.utils.Constants.FIRESTORE_DATE_BIRTH;
import static com.utils.Constants.FIRESTORE_ID_COPY_UPLOADED;
import static com.utils.Constants.FIRESTORE_MARITAL_STATUS;
import static com.utils.Constants.FIRESTORE_SA_CITIZEN;
import static com.utils.Constants.FIRESTORE_TOGGLE_OI;
import static com.utils.Constants.FIRESTORE_TOGGLE_PI;
import static com.utils.Constants.UPP_COMBINED_YEARLY_INCOME_TAG;
import static com.utils.Constants.UPP_GENDER_TAG;
import static com.utils.Constants.UPP_HOME_ORIGIN_TAG;
import static com.utils.Constants.UPP_MARITAL_STATUS_TAG;
import static com.utils.Constants.UPP_NATIONALITY_TAG;
import static com.utils.Constants.UPP_RACE_TAG;
import static com.utils.Constants.UPP_SA_CITIZEN_TAG;
import static com.utils.Constants.firestore_UPP_List;
import static com.utils.Constants.userProfilePersonalFieldTags;

public class InputPersonalFormAdapter extends RecyclerView.Adapter{
    private static final String TAG = InputPersonalFormAdapter.class.getSimpleName();

    private static final int TYPE_EDIT_TEXT = 1;
    private static final int TYPE_SPINNER = 2;
    private static final int TYPE_ADDRESS = 3;
    private static final int TYPE_EMAIL = 4;
    private static final int TYPE_UPLOAD = 5;
    private static final int TYPE_TOGGLE = 6;
    private static final int TYPE_DATE = 7;

    private static final int CAMERA_REQUEST = 1888;
    private static final int MY_CAMERA_PERMISSION_CODE = 100;

    private Context mContext;
    private List<String> mUserProfilePersonalKeysList = Arrays.asList(firestore_UPP_List);
    private UserProfilePersonal mUserProfilePersonal;
    private HashMap<String, Object> mUserProfilePersonalHashmap;

    private RecyclerView mPersonalRecyclerView;
    private InputUploadViewHolder uploadViewHolder;

    public InputPersonalFormAdapter(Context context, RecyclerView mPersonalRecyclerView) {
        this.mContext = context;
        this.mPersonalRecyclerView = mPersonalRecyclerView;
    }

    public InputPersonalFormAdapter(Context context, UserProfilePersonal userProfilePersonal, RecyclerView mPersonalRecyclerView){
        this.mContext = context;
        mUserProfilePersonal = userProfilePersonal;
        mUserProfilePersonalHashmap = userProfilePersonal.getUserProfilePersonalWrite();
        this.mPersonalRecyclerView = mPersonalRecyclerView;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView;
        switch (viewType){
            case TYPE_EDIT_TEXT:
                itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_input_edittext, parent, false);
                return new InputEditTextViewHolder(itemView);

            case TYPE_SPINNER:
                itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_input_spinner, parent, false);
                return new InputSpinnerViewHolder(itemView);

            case TYPE_ADDRESS:
                itemView= LayoutInflater.from(parent.getContext()).inflate(R.layout.item_input_address, parent, false);
                return new InputAddressViewHolder(itemView);

            case TYPE_EMAIL:
                itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_input_email, parent, false);
                return new InputEmailViewHolder(itemView);

            case TYPE_UPLOAD:
                itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_input_upload, parent, false);
                return new InputUploadViewHolder(itemView);

            case TYPE_TOGGLE:
                itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_toggle_heading, parent, false);
                return new ToggleViewHolder(itemView, mPersonalRecyclerView);

            case TYPE_DATE:
                itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_input_date, parent, false);
                return new InputDateViewHolder(itemView);


            default:
                return null;
        }

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (null != mUserProfilePersonal && null != mUserProfilePersonalHashmap) {
            TextWatcher textWatcherLine1 = null, textWatcherLine2 = null, textWatcherCity = null, textWatcherProvince = null, textWatcherZip = null;
            AdapterView.OnItemSelectedListener spinnerSelectionListener = null;
            String uppValue = "";
            TextWatcher t = null, t1 = null;

            if (holder.getItemViewType() == TYPE_SPINNER) {
                spinnerSelectionListener = new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        String currentItemString = parent.getItemAtPosition(position).toString();
                        if (!currentItemString.equals(mContext.getResources().getString(R.string.item_spinner_prompt))) {
                            mUserProfilePersonalHashmap.put(parent.getTag().toString(), currentItemString);

                            if (null != parent.getChildAt(0))
                                ((TextView) parent.getChildAt(0)).setTextColor(mContext.getResources().getColor(R.color.Black));
                        } else {
                            if (null != parent.getChildAt(0))
                                ((TextView) parent.getChildAt(0)).setTextColor(mContext.getResources().getColor(R.color.Gray));
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                };
            }

            if (holder.getItemViewType() == TYPE_ADDRESS) {

                textWatcherLine1 = new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        String newValue = s.toString();
                        if (!newValue.equals("")) {
                            if (!mUserProfilePersonal.getUserProfilePersonalWrite().containsValue(newValue)) {
                                mUserProfilePersonal.setAddressLine1(newValue);
                            }
                        }
                    }
                };

                textWatcherLine2 = new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }


                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                        String newValue = editable.toString();
                        if (!newValue.equals("")) {
                            mUserProfilePersonal.setAddressLine2(newValue);
                        }
                    }
                };

                textWatcherCity = new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                        String newValue = editable.toString();
                        if (!newValue.equals("")) {
                            mUserProfilePersonal.setAddressCity(newValue);
                        }
                    }
                };

                textWatcherProvince = new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                        String newValue = editable.toString();
                        if (!newValue.equals("")) {
                            mUserProfilePersonal.setAddressProvince(newValue);
                        }

                    }
                };

                textWatcherZip = new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                        String newValue = editable.toString();
                        if (!newValue.equals("")) {
                            mUserProfilePersonal.setAddressZip(newValue);
                        }
                    }
                };
            }

            if (holder.getItemViewType() == TYPE_EDIT_TEXT) {

                if (null != mUserProfilePersonalHashmap.get(mUserProfilePersonalKeysList.get(holder.getAdapterPosition())))
                    uppValue = mUserProfilePersonalHashmap.get(mUserProfilePersonalKeysList.get(holder.getAdapterPosition())).toString();

                t = new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {

                        }

                        @Override
                        public void afterTextChanged(Editable s) {
                            String newValue = s.toString();
                            String userValue = "";

                            String applicationKey = mUserProfilePersonalKeysList.get(holder.getAdapterPosition());

                            if (null != mUserProfilePersonal.getUserProfilePersonalWrite().get(applicationKey))
                                userValue = mUserProfilePersonal.getUserProfilePersonalWrite().get(applicationKey).toString();

                            Log.d(TAG, "applicationKey " + applicationKey + " user Value: " + userValue);
                            if (!userValue.equals(newValue) && !newValue.equals(DOB_HINT)) {
                                mUserProfilePersonal.put(applicationKey, newValue);
                            }
                        }
                    };
            }
            else if (holder.getItemViewType() == TYPE_DATE){
                t1 = new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        String newValue = s.toString();
                        String userValue = "";

                        userValue = mUserProfilePersonal.getDateOfBirth();

                        Log.d(TAG, "userValue " + userValue + " new Value: " + newValue);
                        if (!newValue.equals(userValue) && !newValue.equals(DOB_HINT)) {
                            mUserProfilePersonal.setDateOfBirth(newValue);
                        }
                    }
                };
            }

            String uppKeyName = userProfilePersonalFieldTags.get(mUserProfilePersonalKeysList.get(holder.getAdapterPosition()));
            switch (holder.getItemViewType()) {
                case TYPE_EDIT_TEXT:

                    ((InputEditTextViewHolder) holder).bindData(uppKeyName,
                            uppValue, t);
                    break;

                case TYPE_SPINNER:
                    String[] stringArray = null;
                    String spinnerTopic = "";

                    switch (uppKeyName) {
                        case UPP_GENDER_TAG:
                            stringArray = mContext.getResources().getStringArray(R.array.drop_down_gender);
                            spinnerTopic = ELEMENT_GENDER;
                            break;
                        case UPP_NATIONALITY_TAG:
                            stringArray = mContext.getResources().getStringArray(R.array.drop_down_nationality);
                            spinnerTopic = ELEMENT_NATIONALITY;
                            break;
                        case UPP_HOME_ORIGIN_TAG:
                            stringArray = mContext.getResources().getStringArray(R.array.drop_down_home_origin);
                            spinnerTopic = ELEMENT_HOME_ORIGIN;
                            break;
                        case UPP_RACE_TAG:
                            stringArray = mContext.getResources().getStringArray(R.array.drop_down_race);
                            spinnerTopic = ELEMENT_RACE;
                            break;
                        case UPP_SA_CITIZEN_TAG:
                            stringArray = mContext.getResources().getStringArray(R.array.south_african_citizen);
                            spinnerTopic = FIRESTORE_SA_CITIZEN;
                            break;
                        case UPP_MARITAL_STATUS_TAG:
                            stringArray = mContext.getResources().getStringArray(R.array.marital_status);
                            spinnerTopic = FIRESTORE_MARITAL_STATUS;
                            break;
                        case UPP_COMBINED_YEARLY_INCOME_TAG:
                            stringArray = mContext.getResources().getStringArray(R.array.drop_down_income_array);
                            spinnerTopic = FIRESTORE_COMBINED_YEARLY_INCOME;
                            break;
                    }
                    if (stringArray != null) {
                        List<String> arrayList = Arrays.asList(stringArray);
                        int indexOf = -1;

                        if (null != mUserProfilePersonalHashmap && null != mUserProfilePersonalHashmap.get(spinnerTopic))
                            indexOf = arrayList.indexOf(mUserProfilePersonalHashmap.get(spinnerTopic).toString());
                        ((InputSpinnerViewHolder) holder).bindData(uppKeyName,
                                spinnerTopic, arrayList,
                                spinnerSelectionListener, indexOf);
                    }
                    break;

                case TYPE_ADDRESS:
                    ((InputAddressViewHolder) holder).
                            bindData(uppKeyName,
                                    textWatcherLine1, textWatcherLine2, textWatcherCity,
                                    textWatcherProvince, textWatcherZip, mUserProfilePersonal.getAddress());
                    break;

                case TYPE_EMAIL:
                    ((InputEmailViewHolder) holder).bindData(mUserProfilePersonal);
                    break;

                case TYPE_UPLOAD:
                    uploadViewHolder = ((InputUploadViewHolder) holder);
                    uploadViewHolder.bindData(uppKeyName, mUserProfilePersonal.getIdUploaded(), mUserProfilePersonalKeysList.get(holder.getAdapterPosition()));
                    break;

                case TYPE_TOGGLE:
                    ((ToggleViewHolder) holder).bindData(mUserProfilePersonalKeysList.get(position));
                    break;

                case TYPE_DATE:

                    ((InputDateViewHolder) holder).bindData(uppKeyName, mUserProfilePersonal.getDateOfBirth(), t1);
                    break;
            }
        }
    }

    @Override
    public int getItemCount() {
        if (null != mUserProfilePersonalKeysList)
            return mUserProfilePersonalKeysList.size();
        else
            return 0;
    }

    @Override
    public int getItemViewType(int position) {
        if (null != mUserProfilePersonalKeysList) {
            switch (mUserProfilePersonalKeysList.get(position)) {
                case ELEMENT_NATIONALITY:
                case ELEMENT_INCOME:
                case ELEMENT_GENDER:
                case ELEMENT_RACE:
                case FIRESTORE_COMBINED_YEARLY_INCOME:
                case ELEMENT_HOME_ORIGIN:
                case FIRESTORE_SA_CITIZEN:
                case FIRESTORE_MARITAL_STATUS:
                    return TYPE_SPINNER;

                case ELEMENT_ADDRESS:
                    return TYPE_ADDRESS;

                case ELEMENT_EMAIL:
                    return TYPE_EMAIL;

                case ELEMENT_UPLOAD:
                case FIRESTORE_ID_COPY_UPLOADED:
                    return TYPE_UPLOAD;

                case FIRESTORE_TOGGLE_PI:
                case FIRESTORE_TOGGLE_OI:
                    return TYPE_TOGGLE;

                case FIRESTORE_DATE_BIRTH:
                    return TYPE_DATE;

                default:
                    return TYPE_EDIT_TEXT;

            }
        }
        else
            return -1;
    }

    public void updateUserProfilePersonal(UserProfilePersonal userProfilePersonal){
        mUserProfilePersonal = userProfilePersonal;
        mUserProfilePersonalHashmap = userProfilePersonal.getUserProfilePersonalWrite();

        notifyDataSetChanged();
    }

    public InputUploadViewHolder getUploadViewHolder(){
        return uploadViewHolder;
    }
}
