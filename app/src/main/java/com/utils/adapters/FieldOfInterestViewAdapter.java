package com.utils.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.zeldav1.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import static com.utils.Constants.BURSARY_FIELD_ALL;
import static com.utils.Constants.BURSARY_FIELD_DONT_KNOW;
import static com.utils.Constants.BURSARY_FIELD_STAR_ALL;
import static com.utils.Constants.BURSARY_FIELD_STAR_DONT_KNOW;

public class FieldOfInterestViewAdapter extends RecyclerView.Adapter {
    private static final String TAG = FieldOfInterestViewAdapter.class.getSimpleName();
    private final int TYPE_CHECKBOX = 1;
    private final int TYPE_REVIEW = 2;

    private HashMap<String, Boolean> fieldsHashmap;
    private List<String> fieldsList;
    private boolean review;

    public FieldOfInterestViewAdapter(List<String> fields){
        this.fieldsList = fields;
        this.review = false;
        fieldsHashmap = new HashMap<>();
    }

    //Constructor for full list of all fields, with users previous selections
    public FieldOfInterestViewAdapter(List<String> fields, HashMap<String, Boolean> fieldsHashmap, boolean review){
        this.fieldsList = fields;
        this.review = false;
        this.fieldsHashmap = fieldsHashmap;

    }

    public FieldOfInterestViewAdapter(HashMap<String, Boolean> fieldsHashmap, boolean review){
        this.fieldsHashmap = fieldsHashmap;
        fieldsList = new ArrayList<>();

        for (String fieldValue: fieldsHashmap.keySet()){
            if (fieldsHashmap.get(fieldValue)){
                fieldsList.add(fieldValue);
            }
        }
        this.review = review;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView;
        switch (viewType) {
            case TYPE_CHECKBOX:
                itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_field_of_interest_checkbox, parent, false);
                return new CheckboxViewHolder(itemView);

            case TYPE_REVIEW:
                itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_field_of_interest_review, parent, false);
                return new FOIReviewViewHolder(itemView, parent.getContext());

            default:
                return null;
        }
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, final int position) {
        String fieldKey = fieldsList.get(position);
        switch (holder.getItemViewType()) {
            case TYPE_CHECKBOX:
                ((CheckboxViewHolder) holder).bindData(fieldKey);
                break;

            case TYPE_REVIEW:
                ((FOIReviewViewHolder) holder).bindData(fieldKey);
                break;
        }
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public int getItemCount() {
        if (review){
            int itemCount = 0;
            for (String keyValue: fieldsHashmap.keySet()){
                if (fieldsHashmap.get(keyValue)){
                    itemCount++;
                }
            }
            return itemCount;
        }

        else {
            if (fieldsList == null)
                return 0;
            else
                return fieldsList.size();
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (!review)
            return TYPE_CHECKBOX;
        else
            return TYPE_REVIEW;
    }

    class CheckboxViewHolder extends RecyclerView.ViewHolder {
        CheckBox fieldOfInterest;

        CheckboxViewHolder(View view) {
            super(view);
            fieldOfInterest = view.findViewById(R.id.checkbox);
        }

        public void bindData(final String field) {
            fieldOfInterest.setText(field);

            fieldOfInterest.setChecked(fieldsHashmap.containsKey(field));

            fieldOfInterest.setOnClickListener(v -> {

                int adapterPosition = getAdapterPosition();
                String fieldKeyValue = fieldsList.get(adapterPosition);

                if (fieldsHashmap.containsKey(fieldKeyValue)) {
                    fieldsHashmap.remove(fieldKeyValue);
                }

                else{
                    fieldsHashmap.put(fieldKeyValue, true);
                }
                Log.d(TAG, "true updated: " + fieldsHashmap);
                fieldOfInterest.setChecked(fieldsHashmap.containsKey(fieldKeyValue));

            });
        }
    }

    class FOIReviewViewHolder extends RecyclerView.ViewHolder {
        TextView fieldOfInterestReviewTextView;
        Context context;

        FOIReviewViewHolder(View view, Context context) {
            super(view);
            this.context = context;
            fieldOfInterestReviewTextView = view.findViewById(R.id.item_field_of_interest_review_textview);
        }

        public void bindData(final String field) {
            fieldOfInterestReviewTextView.setText(field);
        }
    }

    public List<String> getFieldsList(){
        return fieldsList;
    }

    public HashMap<String, Boolean> getFieldsHashmap(){
        return fieldsHashmap;
    }

}