package com.utils.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import com.utils.fragments.UserViewSchoolFragment;
import com.utils.viewholders.InputAddressViewHolder;
import com.utils.viewholders.InputEditTextViewHolder;
import com.utils.viewholders.InputMarksViewHolder;
import com.utils.viewholders.InputMatricViewHolder;
import com.utils.viewholders.InputSchoolViewHolder;
import com.utils.viewholders.InputSpinnerViewHolder;
import com.utils.viewholders.InputUploadViewHolder;
import com.utils.viewholders.ToggleViewHolder;
import com.zelda.object.UserProfileSchool;
import com.zeldav1.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import static com.utils.Constants.ELEMENT_CURRENT_GRADE;
import static com.utils.Constants.ELEMENT_MATRIC_CERTIFICATE;
import static com.utils.Constants.ELEMENT_SCHOOL_ENDED;
import static com.utils.Constants.ELEMENT_SCHOOL_STARTED;
import static com.utils.Constants.ELEMENT_UPLOAD;
import static com.utils.Constants.FIRESTORE_CURRENT_GRADE;
import static com.utils.Constants.FIRESTORE_MATRIC_CERTIFICATE;
import static com.utils.Constants.FIRESTORE_MATRIC_CERTIFICATE_SUBJECTS;
import static com.utils.Constants.FIRESTORE_MATRIC_COMPLETED_YEAR;
import static com.utils.Constants.FIRESTORE_MC_UPLOADED;
import static com.utils.Constants.FIRESTORE_SCHOOL_ADDRESS;
import static com.utils.Constants.FIRESTORE_SCHOOL_YEAR_ENDED;
import static com.utils.Constants.FIRESTORE_SCHOOL_YEAR_STARTED;
import static com.utils.Constants.FIRESTORE_SUBJECTS_MARKS_MOST_RECENT;
import static com.utils.Constants.FIRESTORE_TOGGLE_HSC;
import static com.utils.Constants.FIRESTORE_TOGGLE_HSM;
import static com.utils.Constants.FIRESTORE_UNIVERSITY_YEAR;
import static com.utils.Constants.UPS_CURRENT_GRADE;
import static com.utils.Constants.UPS_CURRENT_GRADE_11;
import static com.utils.Constants.UPS_CURRENT_GRADE_12;
import static com.utils.Constants.UPS_CURRENT_GRADE_MATRICULATED;
import static com.utils.Constants.UPS_LAST_SCHOOL_YEAR_ENDED;
import static com.utils.Constants.UPS_LAST_SCHOOL_YEAR_STARTED;
import static com.utils.Constants.UPS_MATRIC_CERTIFICATE;
import static com.utils.Constants.UPS_MATRIC_COMPLETED_YEAR;
import static com.utils.Constants.UPU_UNIVERSITY_YEAR;
import static com.utils.Constants.firestore_UPS_matriculated_List;
import static com.utils.Constants.upsMatriculatedFieldTags;

public class InputCBASchoolMatriculatedAdapter extends RecyclerView.Adapter{
    private static final String TAG = InputCBASchoolMatriculatedAdapter.class.getSimpleName();

    private static final int TYPE_EDIT_TEXT = 1;
    private static final int TYPE_SPINNER = 2;
    private static final int TYPE_ADDRESS = 3;
    private static final int TYPE_UPLOAD = 4;
    private static final int TYPE_SCHOOL = 5;
    private static final int TYPE_MATRIC = 6;
    private static final int TYPE_MARKS = 7;
    private static final int TYPE_TOGGLE = 8;

    private Context mContext;
    private List<String> mUserProfileSchoolKeysList = Arrays.asList(firestore_UPS_matriculated_List);
    private UserProfileSchool mUserProfileSchool;
    private HashMap<String, Object> mUserProfileSchoolHashmap;
    private SubjectsAdapter mSubjectsAdapter;

    private RecyclerView mSchoolRecyclerView;
    private InputEditTextViewHolder postMatricViewHolder;
    private InputSchoolViewHolder schoolViewHolder;
    private InputMarksViewHolder marksViewHolder;
    private InputUploadViewHolder uploadViewHolder, srUploadViewHolder;

    UserViewSchoolFragment.UpdateSchoolAdapter listener;

    public InputCBASchoolMatriculatedAdapter(Context context, UserViewSchoolFragment.UpdateSchoolAdapter listener, RecyclerView mSchoolRecyclerView){
        this.mContext = context;
        this.listener = listener;
        mSubjectsAdapter = new SubjectsAdapter(new ArrayList<>());
        this.mSchoolRecyclerView = mSchoolRecyclerView;
    }

    public InputCBASchoolMatriculatedAdapter(Context context, UserProfileSchool userProfileSchool, UserViewSchoolFragment.UpdateSchoolAdapter listener, RecyclerView mSchoolRecyclerView) {
        this.mContext = context;
        this.listener = listener;
        mSubjectsAdapter = new SubjectsAdapter(new ArrayList<>());
        this.mSchoolRecyclerView = mSchoolRecyclerView;
        updateUserProfileSchool(userProfileSchool);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView;
        switch (viewType){
            case TYPE_EDIT_TEXT:
                itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_input_edittext, parent, false);
                return new InputEditTextViewHolder(itemView);

            case TYPE_SPINNER:
                itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_input_spinner, parent, false);
                return new InputSpinnerViewHolder(itemView);

            case TYPE_ADDRESS:
                itemView= LayoutInflater.from(parent.getContext()).inflate(R.layout.item_input_address, parent, false);
                return new InputAddressViewHolder(itemView);

            case TYPE_MARKS:
                itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_input_marks, parent, false);
                marksViewHolder = new InputMarksViewHolder(itemView, mSubjectsAdapter);
                return marksViewHolder;

            case TYPE_MATRIC:
                itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_input_matric, parent, false);
                return new InputMatricViewHolder(itemView);

            case TYPE_SCHOOL:
                itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_input_school, parent, false);
                schoolViewHolder = new InputSchoolViewHolder(itemView);
                return schoolViewHolder;

            case TYPE_UPLOAD:
                itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_input_upload, parent, false);
                uploadViewHolder = new InputUploadViewHolder(itemView);
                return uploadViewHolder;

            case TYPE_TOGGLE:
                itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_toggle_heading, parent, false);
                return new ToggleViewHolder(itemView, mSchoolRecyclerView);

            default:
                return null;
        }

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        if (null != mUserProfileSchool) {
            TextWatcher textWatcherLine1 = null, textWatcherLine2 = null, textWatcherCity = null, textWatcherProvince = null, textWatcherZip = null;

            String[] stringArray = null;
            String spinnerTopic = "";

            AdapterView.OnItemSelectedListener spinnerSelectionListener = null;

            if (holder.getItemViewType() == TYPE_SPINNER) {
                spinnerSelectionListener = new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        String currentItemString = parent.getItemAtPosition(position).toString();
                        if (!currentItemString.equals(mContext.getResources().getString(R.string.item_spinner_prompt))) {
                            Log.d(TAG, "SpinnerTopic: " + parent.getTag().toString() + " item: " + currentItemString);

                            if (null != parent.getChildAt(0))
                                ((TextView) parent.getChildAt(0)).setTextColor(mContext.getResources().getColor(R.color.Black));

                            switch (parent.getTag().toString()) {
                                case ELEMENT_MATRIC_CERTIFICATE:
                                    mUserProfileSchool.setMatricCertificate(currentItemString);
                                    break;

                                case FIRESTORE_MATRIC_COMPLETED_YEAR:
                                    mUserProfileSchool.setMatricCompletedYear(currentItemString);
                                    mUserProfileSchool.setSchoolYearEnded(currentItemString);

                                    Log.d(TAG, "find schoolYearEnded pos: " + mUserProfileSchoolKeysList.indexOf(FIRESTORE_SCHOOL_YEAR_ENDED));
                                    notifyItemChanged(mUserProfileSchoolKeysList.indexOf(FIRESTORE_SCHOOL_YEAR_ENDED));
                                    break;

                                case ELEMENT_SCHOOL_STARTED:
                                case ELEMENT_SCHOOL_ENDED:
                                    mUserProfileSchool.addSchoolValue(parent.getTag().toString(), currentItemString);
                                    break;

                                case ELEMENT_CURRENT_GRADE:
                                    Log.d(TAG, "getCurrentGrade: " + mUserProfileSchool.getCurrentGrade() + " currentItemString " + currentItemString);
                                    if (!currentItemString.equals(mUserProfileSchool.getCurrentGrade())) {
                                        mUserProfileSchool.setCurrentGrade(currentItemString);
                                        switch (currentItemString) {
                                            case UPS_CURRENT_GRADE_11:
                                            case UPS_CURRENT_GRADE_12:
                                                listener.updateSchoolAdapterToCurrent();
                                                break;

                                            case UPS_CURRENT_GRADE_MATRICULATED:
                                                listener.updateSchoolAdapterToMatriculated();
                                                break;
                                        }
                                    } else {
                                        Log.d(TAG, "Same, no changes: ");
                                    }
                                    break;

                            }
                        } else {
                            if (null != parent.getChildAt(0))
                                ((TextView) parent.getChildAt(0)).setTextColor(mContext.getResources().getColor(R.color.Gray));
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                };
            }

            String upskeyValue = mUserProfileSchoolKeysList.get(position);
            String displayHeadingText = upsMatriculatedFieldTags.get(upskeyValue);

            if (holder.getItemViewType() == TYPE_ADDRESS || holder.getItemViewType() == TYPE_SCHOOL ){

                textWatcherLine1 = new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        String newValue = s.toString();
                        if (!mUserProfileSchool.getUserProfileSchoolWrite().containsValue(newValue)) {
                            mUserProfileSchool.setLine1(newValue);
                        }
                    }
                };

                textWatcherLine2 = new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }


                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                        String newValue = editable.toString();
                        if (!mUserProfileSchool.getUserProfileSchoolWrite().containsValue(newValue)) {
                            mUserProfileSchool.setLine2(newValue);
                        }
                    }
                };

                textWatcherCity = new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                        String newValue = editable.toString();
                        if (!mUserProfileSchool.getUserProfileSchoolWrite().containsValue(newValue)) {
                            mUserProfileSchool.setCity(newValue);
                        }
                    }
                };

                textWatcherProvince = new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                        String newValue = editable.toString();
                        if (!mUserProfileSchool.getUserProfileSchoolWrite().containsValue(newValue)) {
                            mUserProfileSchool.setProvince(newValue);
                        }
                    }
                };

                textWatcherZip = new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                        String newValue = editable.toString();
                        if (!mUserProfileSchool.getUserProfileSchoolWrite().containsValue(newValue)) {
                            mUserProfileSchool.setZip(newValue);
                        }
                    }
                };

            }

            switch (holder.getItemViewType()) {
                case TYPE_EDIT_TEXT:
                    String upsValue;
                    if (null != mUserProfileSchoolHashmap.get(upskeyValue))
                        upsValue = mUserProfileSchoolHashmap.get(upskeyValue).toString();
                    else
                        upsValue = "";

                    Log.d(TAG, "upsKeyName: " + displayHeadingText);

                    TextWatcher t = new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        String newValue = s.toString();
                        String userValue = "";

                        if (holder.getAdapterPosition() < mUserProfileSchoolKeysList.size() && holder.getAdapterPosition() >= 0) {
                            String applicationKey = mUserProfileSchoolKeysList.get(holder.getAdapterPosition());

                            if (null != mUserProfileSchool.getUserProfileSchoolWrite().get(applicationKey))
                                userValue = mUserProfileSchool.getUserProfileSchoolWrite().get(applicationKey).toString();

                            if (!userValue.equals(newValue))
                                mUserProfileSchool.put(applicationKey, newValue);
                        }
                    }
                };

                    ((InputEditTextViewHolder) holder).bindData(displayHeadingText,
                            upsValue, t);
                    break;

                case TYPE_SPINNER:

                    Log.d(TAG, "displayHeadingText: " + displayHeadingText);
                    switch (displayHeadingText) {
                        case UPS_CURRENT_GRADE:
                            stringArray = mContext.getResources().getStringArray(R.array.drop_down_current_grade);
                            spinnerTopic = ELEMENT_CURRENT_GRADE;
                            break;

                        case UPS_MATRIC_CERTIFICATE:
                            stringArray = mContext.getResources().getStringArray(R.array.drop_down_matric_certificate);
                            spinnerTopic = ELEMENT_MATRIC_CERTIFICATE;
                            break;

                        case UPS_MATRIC_COMPLETED_YEAR:
                            stringArray = mContext.getResources().getStringArray(R.array.drop_down_years);
                            spinnerTopic = FIRESTORE_MATRIC_COMPLETED_YEAR;
                            break;

                        case UPS_LAST_SCHOOL_YEAR_STARTED:
                            stringArray = mContext.getResources().getStringArray(R.array.drop_down_years);
                            spinnerTopic = ELEMENT_SCHOOL_STARTED;
                            break;

                        case UPS_LAST_SCHOOL_YEAR_ENDED:
                            stringArray = mContext.getResources().getStringArray(R.array.drop_down_years);
                            spinnerTopic = ELEMENT_SCHOOL_ENDED;
                            break;

                        case UPU_UNIVERSITY_YEAR:
                            stringArray = mContext.getResources().getStringArray(R.array.drop_down_university_year);
                            spinnerTopic = FIRESTORE_UNIVERSITY_YEAR;
                            break;
                    }

                    Log.d(TAG, "Spinner: " + upskeyValue + " spinnerTopic " + spinnerTopic + " arrayList " + Arrays.toString(stringArray));

                    if (stringArray != null) {
                        List<String> arrayList = Arrays.asList(stringArray);
                        int indexOf = -1;

                        if (null != mUserProfileSchoolHashmap && null != mUserProfileSchoolHashmap.get(spinnerTopic))
                            indexOf = arrayList.indexOf(mUserProfileSchoolHashmap.get(spinnerTopic).toString());

                        ((InputSpinnerViewHolder) holder).bindData(displayHeadingText,
                                spinnerTopic, arrayList,
                                spinnerSelectionListener, indexOf);
                    }
                    break;

                case TYPE_ADDRESS:

                    Log.d(TAG, "upskeyValue: " + upskeyValue + " displayHeadingText " + displayHeadingText + " getAddress " + mUserProfileSchool.getSchoolAddress());

                    ((InputAddressViewHolder) holder).
                            bindData(displayHeadingText,
                                    textWatcherLine1, textWatcherLine2, textWatcherCity,
                                    textWatcherProvince, textWatcherZip, mUserProfileSchool.getSchoolAddress());
                    break;

                case TYPE_MARKS:

                    if (null != mUserProfileSchool && null != mUserProfileSchool.getCurrentGrade()){
                        Log.d(TAG, "getCurrentGrade: " + mUserProfileSchool.getCurrentGrade());
                        switch (mUserProfileSchool.getCurrentGrade()) {
                            case UPS_CURRENT_GRADE_MATRICULATED:
                                ((InputMarksViewHolder) holder).bindData(mUserProfileSchool.getMatricCertificateSubjects(), mUserProfileSchool.getMatric().getCurrentGrade());
                                break;
                            case UPS_CURRENT_GRADE_11:
                            case UPS_CURRENT_GRADE_12:
                                ((InputMarksViewHolder) holder).bindData(mUserProfileSchool.getSchoolSubjects(), mUserProfileSchool.getMatric().getCurrentGrade());
                                break;
                            default:
                                ((InputMarksViewHolder) holder).bindData(mUserProfileSchool.getSchoolSubjects(), null);
                                break;
                        }
                    }
                    else {
                        ((InputMarksViewHolder) holder).bindData(mUserProfileSchool.getMatricCertificateSubjects(), null);
                    }
                    break;

                case TYPE_UPLOAD:
                    Log.d(TAG, "Key: " + upskeyValue);
                    uploadViewHolder.bindData(displayHeadingText, mUserProfileSchool.getMatricCertificateUploaded(), upskeyValue);
                    break;

                case TYPE_TOGGLE:
                    ((ToggleViewHolder) holder).bindData(displayHeadingText, mUserProfileSchool.getCurrentGrade());
                    break;
            }
        }
    }

    @Override
    public int getItemCount() {
        if (null != mUserProfileSchoolKeysList)
            return mUserProfileSchoolKeysList.size();
        else
            return 0;
    }

    @Override
    public int getItemViewType(int position) {
        if (null != mUserProfileSchoolKeysList){
            switch (mUserProfileSchoolKeysList.get(position)){
                case FIRESTORE_CURRENT_GRADE:
                case FIRESTORE_MATRIC_COMPLETED_YEAR:
                case FIRESTORE_MATRIC_CERTIFICATE:
                case FIRESTORE_SCHOOL_YEAR_STARTED:
                case FIRESTORE_SCHOOL_YEAR_ENDED:
                    return TYPE_SPINNER;

                case FIRESTORE_MATRIC_CERTIFICATE_SUBJECTS:
                case FIRESTORE_SUBJECTS_MARKS_MOST_RECENT:
                    return TYPE_MARKS;

                case FIRESTORE_SCHOOL_ADDRESS:
                    return TYPE_ADDRESS;

                case FIRESTORE_MC_UPLOADED:
                case ELEMENT_UPLOAD:
                    return TYPE_UPLOAD;

                case FIRESTORE_TOGGLE_HSC:
                case FIRESTORE_TOGGLE_HSM:
                    return TYPE_TOGGLE;

                default:
                    return TYPE_EDIT_TEXT;
            }
        }
        else
            return -1;
    }

    public void updateUserProfileSchool(UserProfileSchool userProfileSchool){
        Log.d(TAG, "UPS updated:" +  mUserProfileSchool);
        mUserProfileSchool = userProfileSchool;
        mUserProfileSchoolHashmap = userProfileSchool.getUserProfileSchoolWrite();
        mSubjectsAdapter.updateSubjectList(mUserProfileSchool.getMatricCertificateSubjects());
    }

    public SubjectsAdapter getSubjectsAdapter(){
        return mSubjectsAdapter;
    }

    public InputUploadViewHolder getUploadViewHolder(){
        return uploadViewHolder;
    }

}
