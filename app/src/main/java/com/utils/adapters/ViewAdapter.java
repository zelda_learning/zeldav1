package com.utils.adapters;

import android.content.Context;
import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.RadioButton;

import com.utils.viewholders.BursaryViewHolder;
import com.utils.viewholders.QuizNBTResultsViewHolder;
import com.utils.viewholders.QuizViewHolder;
import com.zelda.object.Bursary;
import com.zelda.object.NBTQuiz;
import com.utils.viewholders.UniversityImageScrollViewHolder;
import com.zelda.object.UserState;
import com.zeldav1.R;

import java.util.ArrayList;
import java.util.List;

import static com.utils.Constants.NBT_QUIZ_REPORT_PROBLEM_ARRAY;

public class ViewAdapter<T> extends RecyclerView.Adapter {
    private static final String TAG = ViewAdapter.class.getSimpleName();

    private List<T> mObjectList;
    private NBTQuiz mNBTQuiz;
    private int mLayoutItem;
    private RecyclerView recyclerView;
    private String universityId;
    private boolean reduced = false;
    private Context context;
    private UserState mUserState;

    public ViewAdapter(List<T> mObjectList, int layoutItem, RecyclerView recyclerView, UserState userState){
        this.mObjectList = mObjectList;
        this.mLayoutItem = layoutItem;
        this.recyclerView = recyclerView;
        this.mUserState = userState;
    }

    public ViewAdapter(List<T> mObjectList, NBTQuiz NBTQuiz, int layoutItem, RecyclerView recyclerView){
        this.mObjectList = mObjectList;
        this.mNBTQuiz = NBTQuiz;
        this.mLayoutItem = layoutItem;
        this.recyclerView = recyclerView;
        this.context = recyclerView.getContext();
    }

    public ViewAdapter(List<T> mObjectList, int layoutItem, RecyclerView recyclerView, String universityId){
        this.mObjectList = mObjectList;
        this.mLayoutItem = layoutItem;
        this.recyclerView = recyclerView;
        this.universityId = universityId;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(mLayoutItem, parent, false);

        if (reduced){
            int deviceWidth = Resources.getSystem().getDisplayMetrics().widthPixels;
            int itemWidth = deviceWidth - (deviceWidth/100 * 10); // 90% of screen width

            itemView.setLayoutParams(new FrameLayout.LayoutParams(itemWidth, (int) parent.getContext().getResources().getDimension(R.dimen.item_height)));
            itemView.setPadding(4, 4, 4, 4);
        }

        switch(mLayoutItem){
            case R.layout.item_feed_bursary:
                return new BursaryViewHolder(itemView);

            case R.layout.item_university_image:
                return new UniversityImageScrollViewHolder(itemView, recyclerView, universityId);

            case R.layout.item_feed_quiz:
                return new QuizViewHolder(itemView, recyclerView);

            case R.layout.item_nbt_quiz_result:
                List<String> pastChoices = new ArrayList<>();
                for (int c = 0; c < mObjectList.size(); c++){
                    pastChoices.add((String)mObjectList.get(c));
                }
                return new QuizNBTResultsViewHolder(itemView, recyclerView, pastChoices);


            default:
                return null;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (mLayoutItem) {
            case R.layout.item_feed_bursary:
                ((BursaryViewHolder) holder).bindData((Bursary) mObjectList.get(position), mUserState);
                break;

            case R.layout.item_university_image:
                ((UniversityImageScrollViewHolder) holder).bindData((String) mObjectList.get(position));
                break;

            case R.layout.item_nbt_quiz_result:
                ((QuizNBTResultsViewHolder) holder).bindData( (String) mObjectList.get(position), mNBTQuiz, position);

                int id = (position + 1)*100;
                final RadioButton[] rb = new RadioButton[4];

                for (int c = (((QuizNBTResultsViewHolder) holder).reportGroup).getChildCount(); c < 4; c++){
                    rb[c]  = new RadioButton(ViewAdapter.this.context);
                    rb[c].setId(id++);
                    rb[c].setText(NBT_QUIZ_REPORT_PROBLEM_ARRAY[c]);
                    ((QuizNBTResultsViewHolder) holder).reportGroup.addView(rb[c]);
                }
                break;

            default:
                Log.d(TAG, "Default switch. No layout to bind to.");
                break;
        }
    }

    @Override
    public int getItemCount() {
        if (null != mObjectList)
            return mObjectList.size();
        else
            return 0;
    }

    @NonNull
    public T getItem(int pos){
        return mObjectList.get(pos);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
            return position;
    }

    public void setReduced(boolean r){
        this.reduced = r;
    }

    public void updateUserState(UserState userState){
        this.mUserState = userState;
    }

}