package com.utils.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.common.primitives.Doubles;
import com.utils.viewholders.SubjectsViewHolder;
import com.zelda.object.SubjectComparator;
import com.zeldav1.R;
import com.zelda.object.Subject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;

import static com.utils.Constants.SUBJECT_NAME_HINT;

public class SubjectsAdapter extends RecyclerView.Adapter {
    private static final String TAG = SubjectsAdapter.class.getSimpleName();

    private List<Subject> subjectsList;
    private HashMap<String, Double> subjectMarksHashMap;

    public SubjectsAdapter(List<Subject> subjects){
        subjectsList = new ArrayList<>();
        subjectMarksHashMap = new HashMap<>();
        updateSubjectList(subjects);
    }

    @NonNull
    @Override
    public SubjectsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_input_subjects, parent, false);
        return new SubjectsViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, final int position) {
        TextWatcher subjectNameWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String newValue = charSequence.toString();
                String subjectName= subjectsList.get(holder.getAdapterPosition()).getSubjectName();
                Double markValue = subjectsList.get(holder.getAdapterPosition()).getMark();

                if (!newValue.equals(subjectName)) {

                    subjectMarksHashMap.remove(subjectName);

                    if (newValue.equals("") && markValue != 0.0)
                        subjectMarksHashMap.put(SUBJECT_NAME_HINT, markValue);
                    else
                        subjectMarksHashMap.put(newValue, markValue);

                    subjectsList.get(holder.getAdapterPosition()).setSubjectName(newValue);

                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        };

        TextWatcher subjectMarkWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                Double newValue = null;
                if (charSequence != null && charSequence != "")
                    newValue = Doubles.tryParse(charSequence.toString());

                String subjectName= subjectsList.get(holder.getAdapterPosition()).getSubjectName();
                Double markValue = subjectsList.get(holder.getAdapterPosition()).getMark();

                if (null != newValue && !newValue.equals(markValue)) {

                    if (subjectName != null)
                        subjectMarksHashMap.put(subjectName, newValue);

                    subjectsList.get(holder.getAdapterPosition()).setMark(newValue);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        };

        ((SubjectsViewHolder) holder).bindData(subjectsList.get(position), subjectNameWatcher, subjectMarkWatcher);
    }

    @Override
    public int getItemCount() {
        if (null != subjectsList)
            return subjectsList.size();
        else
            return 0;
    }

    public void addSubject(Subject subject){
        if (null == subjectsList) {
            subjectsList = new ArrayList<>();
            subjectsList.add(new Subject(subject));
            subjectMarksHashMap.put(subject.getSubjectName(), subject.getMark());
        }
        else {
            subjectsList.add(new Subject(subject));
            subjectMarksHashMap.put(subject.getSubjectName(), subject.getMark());
        }
        Log.d(TAG, "Subs Adapter subject added list:" + subjectsList.toString() + "\n"
                    + "Hash: " + subjectMarksHashMap);
    }

    public List<Subject> getSubjects(){
        return this.subjectsList;
    }

    public HashMap<String, Double> getSubjectMarksHashMap() {
        return subjectMarksHashMap;
    }

    public void updateSubjectList(List<Subject> subjectList) {
        if (null != subjectList && subjectList.size() > 0) {
            this.subjectsList = subjectList;
            for (ListIterator<Subject> subjectListIterator = subjectList.listIterator(); subjectListIterator.hasNext(); ) {
                Subject subject = subjectListIterator.next();
                subjectMarksHashMap.put(subject.getSubjectName(), subject.getMark());
            }
            Collections.sort(subjectsList, new SubjectComparator());
            notifyDataSetChanged();
        }
        else{
            this.subjectsList.clear();
            this.subjectsList.add(new Subject());
        }
    }

}