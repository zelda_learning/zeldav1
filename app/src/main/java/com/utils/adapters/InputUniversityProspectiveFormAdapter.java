package com.utils.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import com.utils.fragments.CBAFormUniversityFragment;
import com.utils.viewholders.InputAddressViewHolder;
import com.utils.viewholders.InputEditTextViewHolder;
import com.utils.viewholders.InputSpinnerViewHolder;
import com.utils.viewholders.InputUploadViewHolder;
import com.utils.viewholders.ToggleViewHolder;
import com.zelda.object.UserProfileSchool;
import com.zelda.object.UserProfileUniversity;
import com.zeldav1.R;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import static com.utils.Constants.ELEMENT_UPLOAD;
import static com.utils.Constants.FIRESTORE_CURRENT_DEGREE_STARTED_YEAR;
import static com.utils.Constants.FIRESTORE_DEGREE_EXPECTED_DURATION;
import static com.utils.Constants.FIRESTORE_PROSPECTIVE_DEGREE_APPLICATION_YEAR;
import static com.utils.Constants.FIRESTORE_PROSPECTIVE_DEGREE_DURATION;
import static com.utils.Constants.FIRESTORE_TOGGLE_CS;
import static com.utils.Constants.FIRESTORE_TOGGLE_PS;
import static com.utils.Constants.FIRESTORE_UNIVERSITY_SUBJECTS;
import static com.utils.Constants.FIRESTORE_UNIVERSITY_YEAR;
import static com.utils.Constants.firestore_CBA_U_prosp_List;
import static com.utils.Constants.userProfileUniversityProspectiveFieldTags;

public class InputUniversityProspectiveFormAdapter extends RecyclerView.Adapter{
    private static final String TAG = InputUniversityProspectiveFormAdapter.class.getSimpleName();

    private static final int TYPE_EDIT_TEXT = 1;
    private static final int TYPE_SPINNER = 2;
    private static final int TYPE_ADDRESS = 3;
    private static final int TYPE_UPLOAD = 4;
    private static final int TYPE_TOGGLE = 5;
    private static final int TYPE_MARKS = 6;

    private RecyclerView mUniversityRecyclerView;
    private Context mContext;
    private UserProfileSchool mUserProfileSchool;
    private UserProfileUniversity mUserProfileUniversity;
    private List<String> mUniversityFieldsList = Arrays.asList(firestore_CBA_U_prosp_List);

    private HashMap<String, Object> mUserProfileSchoolHashmap;

    private InputUploadViewHolder uTUploadViewHolder;

    CBAFormUniversityFragment.UpdateUniversityAdapter listener;

    public InputUniversityProspectiveFormAdapter(Context context, CBAFormUniversityFragment.UpdateUniversityAdapter listener, RecyclerView mUniversityRecyclerView){
        this.mContext = context;
        this.listener = listener;
        this.mUniversityRecyclerView = mUniversityRecyclerView;
    }

    public InputUniversityProspectiveFormAdapter(Context context, UserProfileSchool userProfileSchool, CBAFormUniversityFragment.UpdateUniversityAdapter listener, RecyclerView mUniversityRecyclerView){
        this.mContext = context;
        mUserProfileSchool = userProfileSchool;
        mUserProfileSchoolHashmap = userProfileSchool.getUserProfileSchoolWrite();
        this.listener = listener;
        this.mUniversityRecyclerView = mUniversityRecyclerView;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView;
        switch (viewType){
            case TYPE_EDIT_TEXT:
                itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_input_edittext, parent, false);
                return new InputEditTextViewHolder(itemView);

            case TYPE_SPINNER:
                itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_input_spinner, parent, false);
                return new InputSpinnerViewHolder(itemView);

            case TYPE_ADDRESS:
                itemView= LayoutInflater.from(parent.getContext()).inflate(R.layout.item_input_address, parent, false);
                return new InputAddressViewHolder(itemView);

            case TYPE_UPLOAD:
                itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_input_upload, parent, false);
                uTUploadViewHolder = new InputUploadViewHolder(itemView);
                return uTUploadViewHolder;

            case TYPE_TOGGLE:
                itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_toggle_heading, parent, false);
                return new ToggleViewHolder(itemView, mUniversityRecyclerView);

            default:
                return null;
        }

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        if (null != mUserProfileSchool) {

            String[] stringArray = null;
            String spinnerTopic = "";

            AdapterView.OnItemSelectedListener spinnerSelectionListener = null;

            if (holder.getItemViewType() == TYPE_SPINNER) {
                spinnerSelectionListener = new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        String currentItemString = parent.getItemAtPosition(position).toString();
                        if (!currentItemString.equals(mContext.getResources().getString(R.string.item_spinner_prompt))) {
                            Log.d(TAG, "SpinnerTopic: " + parent.getTag().toString() + " item: " + currentItemString);

                            if (null != parent.getChildAt(0))
                                ((TextView) parent.getChildAt(0)).setTextColor(mContext.getResources().getColor(R.color.Black));

                            switch (parent.getTag().toString()) {

                                case FIRESTORE_UNIVERSITY_YEAR:
                                    if (null != mUserProfileSchool.getUniversityYear()) {
                                        Log.d(TAG, "getUniversityYear: " + mUserProfileSchool.getUniversityYear() + "currentItemString " + currentItemString);
                                        if (!currentItemString.equals(mUserProfileSchool.getUniversityYear())) {
                                            mUserProfileSchool.setUniversityYear(currentItemString);
                                            if (currentItemString.equals(mContext.getResources().getStringArray(R.array.drop_down_university_year)[0])) {
                                                listener.updateUniversityAdapterToProspective();
                                            } else {
                                                listener.updateUniversityAdapterToCurrent();
                                            }
                                        } else {
                                            Log.d(TAG, "Same, no changes: ");
                                        }
                                    }
                                    else {
                                        Log.d(TAG, "No initial, set changes: ");
                                        mUserProfileSchool.setUniversityYear(currentItemString);
                                        if (currentItemString.equals(mContext.getResources().getStringArray(R.array.drop_down_university_year)[0])) {
                                            listener.updateUniversityAdapterToProspective();
                                        } else {
                                            listener.updateUniversityAdapterToCurrent();
                                        }

                                    }
                                    break;

                                case FIRESTORE_DEGREE_EXPECTED_DURATION:
                                case FIRESTORE_PROSPECTIVE_DEGREE_DURATION:
                                case FIRESTORE_PROSPECTIVE_DEGREE_APPLICATION_YEAR:
                                case FIRESTORE_CURRENT_DEGREE_STARTED_YEAR:
                                    mUserProfileSchool.addUniversityValue(parent.getTag().toString(), currentItemString);
                                    break;
                            }
                        } else {
                            if (null != parent.getChildAt(0))
                                ((TextView) parent.getChildAt(0)).setTextColor(mContext.getResources().getColor(R.color.Gray));
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                };
            }

            String upskeyValue = mUniversityFieldsList.get(holder.getAdapterPosition());
            String upsKeyName = userProfileUniversityProspectiveFieldTags.get(upskeyValue);

            switch (holder.getItemViewType()) {
                case TYPE_EDIT_TEXT:
                    String upsValue;
                    if (null != mUserProfileSchoolHashmap.get(upskeyValue))
                        upsValue = mUserProfileSchoolHashmap.get(upskeyValue).toString();
                    else
                        upsValue = "";

                    Log.d(TAG, "TYPE_EDIT_TEXT: upsKeyName " + upsKeyName);
                    TextWatcher t = new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {

                        }

                        @Override
                        public void afterTextChanged(Editable s) {
                            String newValue = s.toString();
                            String userValue = "";

                            if (holder.getAdapterPosition() < mUniversityFieldsList.size() && holder.getAdapterPosition() >= 0) {
                                String applicationKey = mUniversityFieldsList.get(holder.getAdapterPosition());

                                if (null != mUserProfileSchool.getUserProfileSchoolWrite().get(applicationKey))
                                    userValue = mUserProfileSchool.getUserProfileSchoolWrite().get(applicationKey).toString();

                                if (!userValue.equals(newValue))
                                    mUserProfileSchool.put(applicationKey, newValue);
                            }
                        }
                    };

                    ((InputEditTextViewHolder) holder).bindData(upsKeyName,
                            upsValue, t);
                    break;

                case TYPE_SPINNER:

                    Log.d(TAG, "TYPE_SPINNER:  upskeyValue " + upskeyValue);
                    switch (upskeyValue) {
                        case FIRESTORE_UNIVERSITY_YEAR:
                            stringArray = mContext.getResources().getStringArray(R.array.drop_down_university_year);
                            spinnerTopic = FIRESTORE_UNIVERSITY_YEAR;
                            break;
                        case FIRESTORE_CURRENT_DEGREE_STARTED_YEAR:
                            stringArray = mContext.getResources().getStringArray(R.array.drop_down_years);
                            spinnerTopic = FIRESTORE_CURRENT_DEGREE_STARTED_YEAR;
                            break;
                        case FIRESTORE_PROSPECTIVE_DEGREE_APPLICATION_YEAR:
                            stringArray = mContext.getResources().getStringArray(R.array.drop_down_years_future);
                            spinnerTopic = FIRESTORE_PROSPECTIVE_DEGREE_APPLICATION_YEAR;
                            break;
                        case FIRESTORE_DEGREE_EXPECTED_DURATION:
                        case FIRESTORE_PROSPECTIVE_DEGREE_DURATION:
                            stringArray = mContext.getResources().getStringArray(R.array.drop_down_expected_degree_years);
                            spinnerTopic = FIRESTORE_DEGREE_EXPECTED_DURATION;
                            break;
                    }

                    Log.d(TAG, "Spinner: " + upsKeyName + " spinnerTopic " + spinnerTopic + "arrayList " + Arrays.toString(stringArray));

                    if (stringArray != null) {
                        List<String> arrayList = Arrays.asList(stringArray);
                        int indexOf = -1;

                        if (null != mUserProfileSchoolHashmap && null != mUserProfileSchoolHashmap.get(spinnerTopic))
                            indexOf = arrayList.indexOf(mUserProfileSchoolHashmap.get(spinnerTopic).toString());

                        ((InputSpinnerViewHolder) holder).bindData(upsKeyName,
                                spinnerTopic, arrayList,
                                spinnerSelectionListener, indexOf);
                    }
                    break;

                case TYPE_TOGGLE:
                    ((ToggleViewHolder) holder).bindData(upskeyValue);
                    break;
            }
        }
    }

    @Override
    public int getItemCount() {
        return mUniversityFieldsList.size();
    }

    @Override
    public int getItemViewType(int position) {
        switch (mUniversityFieldsList.get(position)) {
            case FIRESTORE_UNIVERSITY_YEAR:
            case FIRESTORE_CURRENT_DEGREE_STARTED_YEAR:
            case FIRESTORE_DEGREE_EXPECTED_DURATION:
            case FIRESTORE_PROSPECTIVE_DEGREE_APPLICATION_YEAR:
            case FIRESTORE_PROSPECTIVE_DEGREE_DURATION:
                return TYPE_SPINNER;

            case FIRESTORE_UNIVERSITY_SUBJECTS:
                return TYPE_MARKS;

            case ELEMENT_UPLOAD:
                return TYPE_UPLOAD;

            case FIRESTORE_TOGGLE_CS:
            case FIRESTORE_TOGGLE_PS:
                return TYPE_TOGGLE;

            default:
                return TYPE_EDIT_TEXT;
        }
    }

    public void updateUserProfileSchool(UserProfileSchool userProfileSchool){
        Log.d(TAG, "UPS updated:" +  mUserProfileSchool);
        mUserProfileSchool = userProfileSchool;
        mUserProfileSchoolHashmap = userProfileSchool.getUserProfileSchoolWrite();
    }

    public InputUploadViewHolder getUploadViewHolder(){
        return uTUploadViewHolder;
    }
}
