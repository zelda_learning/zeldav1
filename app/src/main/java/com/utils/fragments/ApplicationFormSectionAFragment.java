package com.utils.fragments;

import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.Toast;

import com.utils.FragmentCommunicator;
import com.utils.adapters.InputApplicationFormAdapter;
import com.zelda.object.ApplicationForm;
import com.zelda.object.ApplicationFormSectionA;
import com.zelda.object.UserApplicationFormA;
import com.zelda.object.UserState;

import com.zeldav1.ApplicationFormActivity;
import com.zeldav1.R;

import java.util.Objects;

import leo.me.la.labeledprogressbarlib.LabeledProgressBar;

import static com.utils.Constants.UNREAD_MESSAGE_TAG;
import static com.utils.Constants.USER_STATE_TAG;

public class ApplicationFormSectionAFragment extends Fragment {
    private static final String TAG = ApplicationFormSectionAFragment.class.getSimpleName();
    private Context mContext;
    private UserState mUserState;

    private Bundle mBundle;
    private Boolean mUnreadBoolean;
    private int applicationElements, userElements;

    private RecyclerView mApplicationSectionARecyclerView;
    private InputApplicationFormAdapter mApplicationSectionAViewAdapter;

    private FragmentCommunicator fragmentCommunicator = this::updateProgressValues;

    private Button mSaveUserApplicationFormButton;
    private ConstraintLayout mLoadingContainer, mLoadingCompleteContainer;

    private LabeledProgressBar mApplicationProgressBar;

    private ApplicationFormSectionA mApplicationFormSectionA;
    private UserApplicationFormA mUserApplicationFormA, mSavedUserApplicationFormA;

    private Boolean mSaveClicked = false;

    public ApplicationFormSectionAFragment(){

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Objects.requireNonNull(getActivity()).getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        // Inflate the layout for this fragment
        final View rootView = inflater.inflate(R.layout.fragment_application_form_section_a,
                container, false);
        mApplicationSectionARecyclerView = rootView.findViewById(R.id.application_form_section_a_recycler_view);
        mApplicationProgressBar = rootView.findViewById(R.id.application_form_section_a_progress_bar);
        mLoadingContainer = rootView.findViewById(R.id.application_form_section_a_loading_container);
        mLoadingCompleteContainer = rootView.findViewById(R.id.application_form_section_a_loading_complete_container);
        mSaveUserApplicationFormButton = rootView.findViewById(R.id.application_form_section_a_save_changes_button);

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        mApplicationSectionARecyclerView.setLayoutManager(mLayoutManager);

        if (null != mApplicationFormSectionA)
            mApplicationSectionAViewAdapter = new InputApplicationFormAdapter(mContext, mApplicationFormSectionA, mUserApplicationFormA, fragmentCommunicator);

        else
            mApplicationSectionAViewAdapter = new InputApplicationFormAdapter(mContext, fragmentCommunicator);

        mApplicationSectionARecyclerView.setAdapter(mApplicationSectionAViewAdapter);
        mApplicationSectionARecyclerView.setHasFixedSize(true);
        mApplicationSectionAViewAdapter.notifyDataSetChanged();

        mSaveUserApplicationFormButton.setOnClickListener(v -> {
            mSaveClicked = true;
            Toast.makeText(getContext(), R.string.application_form_application_saved_toast_text, Toast.LENGTH_SHORT).show();
            if (!mUserApplicationFormA.equals(mSavedUserApplicationFormA)) {
                Log.d(TAG, "Not equal, upload.");
                mSavedUserApplicationFormA = new UserApplicationFormA(mUserApplicationFormA);
                ((ApplicationFormActivity) getActivity()).uploadUserApplicationFormSectionA();
            }

            else{
                Log.d(TAG, "equal. do nothing.");
            }
        });

        rootView.getViewTreeObserver().addOnGlobalLayoutListener(() -> {

            Rect r = new Rect();
            rootView.getWindowVisibleDisplayFrame(r);
            int screenHeight = rootView.getRootView().getHeight();

            // r.bottom is the position above soft keypad or device button.
            // if keypad is shown, the r.bottom is smaller than that before.
            int keypadHeight = screenHeight - r.bottom;

            if (keypadHeight > screenHeight * 0.25) {
                // keyboard is opened
                mSaveUserApplicationFormButton.setVisibility(View.GONE);
                mApplicationProgressBar.setVisibility(View.GONE);
                mSaveClicked = false;
            }
            else if (keypadHeight == 0 && !mSaveClicked){
                // keyboard is closed
                mSaveUserApplicationFormButton.startAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.fade_in));
                mApplicationProgressBar.startAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.fade_in));
                mSaveUserApplicationFormButton.setVisibility(View.VISIBLE);
                mApplicationProgressBar.setVisibility(View.VISIBLE);
            }
        });
        return rootView;
    }

    public UserApplicationFormA captureApplicationFormSectionA() {
        return mApplicationSectionAViewAdapter.getUserApplicationForm();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        assert getActivity() != null;
        mBundle = getArguments();
        mContext = getContext();

        if(mBundle != null) {
            mUserState = (UserState)mBundle.getSerializable(USER_STATE_TAG);
            mUnreadBoolean = mBundle.getBoolean(UNREAD_MESSAGE_TAG);
        }

    }

    public void updateApplicationFormSectionA(ApplicationFormSectionA applicationFormSectionA) {
        mApplicationFormSectionA = applicationFormSectionA;
        mApplicationSectionAViewAdapter.updateApplicationForm(mApplicationFormSectionA);
        mApplicationSectionAViewAdapter.notifyDataSetChanged();
        applicationElements = mApplicationFormSectionA.getNumberElements();
        updateProgressValues(userElements);
        screenUpdateLoadingComplete();
    }

    public void updateUserApplicationFormA(UserApplicationFormA userApplicationFormA) {
        mUserApplicationFormA = userApplicationFormA;
        mSavedUserApplicationFormA = new UserApplicationFormA(userApplicationFormA);
        userElements = mUserApplicationFormA.getNumberElements();
        updateProgressValues(userElements);
        mApplicationSectionAViewAdapter.updateUserApplicationFormA(mUserApplicationFormA);
        mApplicationSectionAViewAdapter.notifyDataSetChanged();
    }

    public void updateApplicationForm(ApplicationForm applicationForm) {
        Log.d(TAG, "UpdateApplicationForm: " + applicationForm.toString());
    }

    private void screenUpdateLoadingComplete(){
        mLoadingCompleteContainer.setVisibility(View.VISIBLE);
        mLoadingContainer.setVisibility(View.GONE);
    }

    private void updateProgressValues(int userElements){
        if (checkActive()) {
            this.userElements = userElements;
            if (applicationElements != 0) {
                int progress = (this.userElements * 100) / applicationElements;
                mApplicationProgressBar.setValue(progress);
                mApplicationProgressBar.setLabelText(getResources().getString(R.string.application_form_progress_text, userElements, applicationElements));
            }
        }
    }

    private boolean checkActive() {
        return getActivity() != null;
    }

    public InputApplicationFormAdapter getmApplicationSectionAViewAdapter() {
        return mApplicationSectionAViewAdapter;
    }
}
