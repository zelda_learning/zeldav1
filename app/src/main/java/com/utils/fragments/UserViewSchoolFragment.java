package com.utils.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SimpleItemAnimator;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.utils.UpdateSchoolSubjects;
import com.utils.UpdateUserProfileSchool;
import com.utils.adapters.InputCBASchoolCurrentAdapter;
import com.utils.adapters.InputCBASchoolMatriculatedAdapter;
import com.utils.viewholders.InputMarksViewHolder;
import com.utils.viewholders.InputUploadViewHolder;
import com.zelda.object.UserProfileSchool;
import com.zelda.object.UserState;
import com.zeldav1.R;

import java.util.Arrays;

import static com.utils.Constants.FIRESTORE_MATRIC_CERTIFICATE_SUBJECTS;
import static com.utils.Constants.FIRESTORE_MC_UPLOADED;
import static com.utils.Constants.FIRESTORE_SCHOOL_REPORT_UPLOADED;
import static com.utils.Constants.FIRESTORE_SCHOOL_SUBJECTS;
import static com.utils.Constants.UPS_CURRENT_GRADE_MATRICULATED;
import static com.utils.Constants.USER_PROFILE_SCHOOL_TAG;
import static com.utils.Constants.USER_STATE_TAG;
import static com.utils.Constants.firestore_UPS_current_List;
import static com.utils.Constants.firestore_UPS_matriculated_List;

public class UserViewSchoolFragment extends Fragment implements
        UpdateUserProfileSchool,
        UpdateSchoolSubjects {
    private static final String TAG = UserViewSchoolFragment.class.getSimpleName();
    private Context mContext;
    private Bundle mBundle;

    private UserState mUserState;
    private UserProfileSchool mUserProfileSchool;

    private RecyclerView mUserProfileSchoolRecyclerView;
    private InputCBASchoolCurrentAdapter mCurrentInputSchoolAdapter;
    private InputCBASchoolMatriculatedAdapter mMatriculatedInputSchoolAdapter;

    private Boolean mSaveClicked = false;
    private Boolean mViewInitialized = false, mUserStateUpdated = false, mUserProfileSchoolUpdated = false;

    public UserViewSchoolFragment(){

    }

    public static UserViewSchoolFragment newInstance(UserState userState, UserProfileSchool userProfileSchool) {
        UserViewSchoolFragment userViewSchoolFragment = new UserViewSchoolFragment();
        Bundle b = new Bundle();
        b.putSerializable(USER_STATE_TAG, userState);

        if (null != userProfileSchool) {
            b.putSerializable(USER_PROFILE_SCHOOL_TAG, userProfileSchool);
        }

        userViewSchoolFragment.setArguments(b);
        return userViewSchoolFragment;
    }

    @Override
    public void updateUserProfileSchool(UserProfileSchool userProfileSchool){
        mUserProfileSchool = userProfileSchool;

        if (getActivity() != null && null != mUserProfileSchool) {
            Log.d(TAG, "UPS updated:" +  mUserProfileSchool.toString());
            mUserProfileSchoolUpdated = true;
            mCurrentInputSchoolAdapter.updateUserProfileSchool(mUserProfileSchool);
            mMatriculatedInputSchoolAdapter.updateUserProfileSchool(mUserProfileSchool);

            setAdapterType();

        }
    }

    public void updateUserState(UserState userState){
        mUserState = userState;
        Log.d(TAG, "US updated:" +  mUserState.toString());
        mUserStateUpdated = true;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_recyclerview, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState){

        mUserProfileSchoolRecyclerView = view.findViewById(R.id.fragment_cba_recyclerview);

        mContext = getContext();
        mBundle = getArguments();

        if (null != mBundle) {
            mUserState = (UserState) mBundle.getSerializable(USER_STATE_TAG);
            mUserProfileSchool = (UserProfileSchool) mBundle.getSerializable(USER_PROFILE_SCHOOL_TAG);
        }

        UpdateSchoolAdapter updateAdapterListener = new UpdateSchoolAdapter() {
            @Override
            public void updateSchoolAdapterToMatriculated(){
                //do stuff here
                setMatriculatedSchoolAdapter();
            }

            @Override
            public void updateSchoolAdapterToCurrent(){
                //do stuff here
                setCurrentSchoolAdapter();
            }
        };

        if (null != mUserProfileSchool) {
            mCurrentInputSchoolAdapter = new InputCBASchoolCurrentAdapter(mContext, mUserProfileSchool, updateAdapterListener, mUserProfileSchoolRecyclerView);
            mMatriculatedInputSchoolAdapter = new InputCBASchoolMatriculatedAdapter(mContext, mUserProfileSchool, updateAdapterListener, mUserProfileSchoolRecyclerView);

        }
        else {
            mCurrentInputSchoolAdapter = new InputCBASchoolCurrentAdapter(mContext, updateAdapterListener, mUserProfileSchoolRecyclerView);
            mMatriculatedInputSchoolAdapter = new InputCBASchoolMatriculatedAdapter(mContext, updateAdapterListener, mUserProfileSchoolRecyclerView);
        }
        setAdapterType();

        ((SimpleItemAnimator) mUserProfileSchoolRecyclerView.getItemAnimator()).setSupportsChangeAnimations(false);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);

        mUserProfileSchoolRecyclerView.setLayoutManager(mLayoutManager);
        mViewInitialized = true;
    }

    private void setAdapterType(){
        if (null != mUserProfileSchool && null != mUserProfileSchool.getCurrentGrade() && mUserProfileSchool.getCurrentGrade().equals(UPS_CURRENT_GRADE_MATRICULATED)) {
            setMatriculatedSchoolAdapter();
        } else {
            setCurrentSchoolAdapter();
        }
        mUserProfileSchoolRecyclerView.getAdapter().notifyDataSetChanged();
    }

    private void setCurrentSchoolAdapter() {
        Log.d(TAG, "setCurrentSchoolAdapter");
        mUserProfileSchoolRecyclerView.setAdapter(mCurrentInputSchoolAdapter);
    }

    private void setMatriculatedSchoolAdapter() {
        Log.d(TAG, "setMatriculatedSchoolAdapter");
        mUserProfileSchoolRecyclerView.setAdapter(mMatriculatedInputSchoolAdapter);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (null != savedInstanceState)
            Log.d(TAG, "New onActivityCreated: " + savedInstanceState.toString());
    }

    public void setUploadProgress(double uploadProgress){
        Log.d(TAG, "uploadProgress " + uploadProgress);
        InputUploadViewHolder inputUploadViewHolder = null;
        if (mUserProfileSchoolRecyclerView.getAdapter() instanceof InputCBASchoolCurrentAdapter)
            inputUploadViewHolder = (InputUploadViewHolder)mUserProfileSchoolRecyclerView.findViewHolderForAdapterPosition(Arrays.asList(firestore_UPS_current_List).indexOf(FIRESTORE_SCHOOL_REPORT_UPLOADED));

        else if (mUserProfileSchoolRecyclerView.getAdapter() instanceof InputCBASchoolMatriculatedAdapter)
            inputUploadViewHolder = (InputUploadViewHolder)mUserProfileSchoolRecyclerView.findViewHolderForAdapterPosition(Arrays.asList(firestore_UPS_matriculated_List).indexOf(FIRESTORE_MC_UPLOADED));

        Log.d(TAG, "inputUploadViewHolder " + inputUploadViewHolder);
        if (null != inputUploadViewHolder)
            inputUploadViewHolder.setUploading(uploadProgress);
    }

    public void setUploadFailed(){
        Log.d(TAG, "setUploadFailed ");
        InputUploadViewHolder inputUploadViewHolder = null;
        if (mUserProfileSchoolRecyclerView.getAdapter() instanceof InputCBASchoolCurrentAdapter)
            inputUploadViewHolder = (InputUploadViewHolder)mUserProfileSchoolRecyclerView.findViewHolderForAdapterPosition(Arrays.asList(firestore_UPS_current_List).indexOf(FIRESTORE_SCHOOL_REPORT_UPLOADED));

        else if (mUserProfileSchoolRecyclerView.getAdapter() instanceof InputCBASchoolMatriculatedAdapter)
            inputUploadViewHolder = (InputUploadViewHolder)mUserProfileSchoolRecyclerView.findViewHolderForAdapterPosition(Arrays.asList(firestore_UPS_matriculated_List).indexOf(FIRESTORE_MC_UPLOADED));

        Log.d(TAG, "inputUploadViewHolder " + inputUploadViewHolder);
        if (null != inputUploadViewHolder)
            inputUploadViewHolder.uploadFailed();
    }

    public void setUploadComplete(String filename){
        Log.d(TAG, "setUploadComplete ");
        InputUploadViewHolder inputUploadViewHolder = null;
        if (mUserProfileSchoolRecyclerView.getAdapter() instanceof InputCBASchoolCurrentAdapter)
            inputUploadViewHolder = (InputUploadViewHolder)mUserProfileSchoolRecyclerView.findViewHolderForAdapterPosition(Arrays.asList(firestore_UPS_current_List).indexOf(FIRESTORE_SCHOOL_REPORT_UPLOADED));

        else if (mUserProfileSchoolRecyclerView.getAdapter() instanceof InputCBASchoolMatriculatedAdapter)
            inputUploadViewHolder = (InputUploadViewHolder)mUserProfileSchoolRecyclerView.findViewHolderForAdapterPosition(Arrays.asList(firestore_UPS_matriculated_List).indexOf(FIRESTORE_MC_UPLOADED));

        Log.d(TAG, "inputUploadViewHolder " + inputUploadViewHolder);
        if (null != inputUploadViewHolder)
            inputUploadViewHolder.uploadingComplete(filename);
    }

    @Override
    public void captureSubjectsMarks() {
        Log.d(TAG, "captureSubjectsMarks");

        InputMarksViewHolder inputMarksViewHolder = null;
        if (mUserProfileSchoolRecyclerView.getAdapter() instanceof InputCBASchoolCurrentAdapter) {
            inputMarksViewHolder = (InputMarksViewHolder) mUserProfileSchoolRecyclerView.findViewHolderForAdapterPosition(Arrays.asList(firestore_UPS_current_List).indexOf(FIRESTORE_SCHOOL_SUBJECTS));
            if (null != mUserProfileSchool && null != inputMarksViewHolder)
                mUserProfileSchool.setSchoolSubjects(inputMarksViewHolder.getSubjectsAdapter().getSubjectMarksHashMap());

        }

        else if (mUserProfileSchoolRecyclerView.getAdapter() instanceof InputCBASchoolMatriculatedAdapter) {
            inputMarksViewHolder = (InputMarksViewHolder) mUserProfileSchoolRecyclerView.findViewHolderForAdapterPosition(Arrays.asList(firestore_UPS_matriculated_List).indexOf(FIRESTORE_MATRIC_CERTIFICATE_SUBJECTS));
            if (null != mUserProfileSchool && null != inputMarksViewHolder)
                mUserProfileSchool.setMatricCertificateSubjects(inputMarksViewHolder.getSubjectsAdapter().getSubjectMarksHashMap());

        }

    }

    public interface UpdateSchoolAdapter{
        public void updateSchoolAdapterToMatriculated();
        public void updateSchoolAdapterToCurrent();
    }
}
