package com.utils.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.utils.DialogUtils;
import com.utils.services.FirebaseStorageService;
import com.zelda.object.NBTQuiz;
import com.zelda.object.UserState;
import com.zeldav1.FullScreenImageActivity;
import com.zeldav1.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static com.utils.Constants.IMAGE_ID_TAG;
import static com.utils.Constants.NBT_CHOICE_A;
import static com.utils.Constants.NBT_CHOICE_B;
import static com.utils.Constants.NBT_CHOICE_C;
import static com.utils.Constants.NBT_CHOICE_D;
import static com.utils.Constants.NBT_QUIZ_RETRY_TAG;
import static com.utils.Constants.QUESTION_NUMBER_TAG;
import static com.utils.Constants.QUIZZES;
import static com.utils.Constants.QUIZ_ID_TAG;
import static com.utils.Constants.QUIZ_TYPE_8;
import static com.utils.Constants.UNREAD_MESSAGE_TAG;
import static com.utils.Constants.USER_CHOICE_RETRY_PAST_TAG;
import static com.utils.Constants.USER_CHOICE_TAG;
import static com.utils.Constants.USER_STATE_TAG;

public class NBTQuizQuestionPageFragment extends Fragment {

    private static final String TAG = NBTQuizQuestionPageFragment.class.getSimpleName();

    private List<RadioButton> radioButtonList;
    private List<ImageView> imageViewList;
    private RadioGroup mRadioGroup;
    private NBTQuiz mNBTQuiz;
    private int mQuestionNumber;
    private int mNumberQuestions;
    private ProgressBar mProgressBar, mLoadingProgressBar;
    private Context mContext;
    private Button mNextQuestionButton, mPreviousQuestionButton;
    private ArrayList<String> mUserChoice, mUserChoicePastChoice;
    private View rootView;
    private UserState mUserState;
    private Boolean mUnreadBoolean, mRetryQuestion = false;
    private Bundle mBundle;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_nbt_quiz_question_page, null);
        mBundle = getArguments();
        mContext = getContext();

        if(mBundle != null){
            mNBTQuiz = (NBTQuiz)mBundle.getSerializable(QUIZ_ID_TAG);
            mUserState = (UserState)mBundle.getSerializable(USER_STATE_TAG);
            mUnreadBoolean = mBundle.getBoolean(UNREAD_MESSAGE_TAG);
            mRetryQuestion = mBundle.getBoolean(NBT_QUIZ_RETRY_TAG);
            mQuestionNumber = mBundle.getInt(QUESTION_NUMBER_TAG);

            if (null != mBundle.getSerializable(USER_CHOICE_RETRY_PAST_TAG)) {
                ArrayList<?> userChoiceCast = (ArrayList<?>) mBundle.getSerializable(USER_CHOICE_RETRY_PAST_TAG);
                mUserChoicePastChoice = new ArrayList<>();
                if (null != userChoiceCast) {
                    for (Object s : userChoiceCast) {
                        mUserChoicePastChoice.add((String) s);
                    }
                }
            }

            if(mNBTQuiz != null){
                Log.d(TAG, "quiz found" + mNBTQuiz + "\nuser " + mUserState + "\nquestion # " + mQuestionNumber+ "\nretry # " + mRetryQuestion);
                mNumberQuestions = mNBTQuiz.getNumberQuestions();
                mUserChoice = new ArrayList<>();
                radioButtonList = new ArrayList<>();
                imageViewList = new ArrayList<>();
                rootView = view;
                makeNBTPage();
            }
        }

        return view;
    }

    public void setMsg(String msg){
        Log.d(TAG, "msg " + msg);
    }

    private void makeNBTPage(){

        Log.d(TAG, "makeNBTPage for: " + mQuestionNumber + "/" + mNumberQuestions);
        mRadioGroup = rootView.findViewById(R.id.quiz_8_radio_group);

        android.text.SpannableString nbtQuizTitleString = new SpannableString(mNBTQuiz.getTitle());
        nbtQuizTitleString.setSpan(new UnderlineSpan(), 0, nbtQuizTitleString.length(), 0);
        ((TextView) rootView.findViewById(R.id.quiz_page_title)).setText(nbtQuizTitleString);
        ((TextView)rootView.findViewById(R.id.quiz_8_question_title)).setText( mContext.getString(R.string.item_nbt_quiz_result_title, mQuestionNumber +"") );

        mRadioGroup.clearCheck();
        radioButtonList.add(rootView.findViewById(R.id.quiz_8_answer_1_radio_button));
        radioButtonList.add(rootView.findViewById(R.id.quiz_8_answer_2_radio_button));
        radioButtonList.add(rootView.findViewById(R.id.quiz_8_answer_3_radio_button));
        radioButtonList.add(rootView.findViewById(R.id.quiz_8_answer_4_radio_button));
        imageViewList.add(rootView.findViewById(R.id.quiz_8_answer_1_image_view));
        imageViewList.add(rootView.findViewById(R.id.quiz_8_answer_2_image_view));
        imageViewList.add(rootView.findViewById(R.id.quiz_8_answer_3_image_view));
        imageViewList.add(rootView.findViewById(R.id.quiz_8_answer_4_image_view));

        int width = (int) mContext.getResources().getDimension(R.dimen.question_image_width);
        int height = (int) mContext.getResources().getDimension(R.dimen.question_image_height);

        String questionStoragePrefix = QUIZZES + "/" + mNBTQuiz.getId() + "/" + mQuestionNumber + "/" ;
        String questionImageName = questionStoragePrefix + mQuestionNumber;
        ArrayList<String> optionImageNames = new ArrayList<>();

        optionImageNames.add(NBT_CHOICE_A);
        optionImageNames.add(NBT_CHOICE_B);
        optionImageNames.add(NBT_CHOICE_C);
        optionImageNames.add(NBT_CHOICE_D);

        mLoadingProgressBar = rootView.findViewById(R.id.quiz_8_progress_loading_bar);
        mLoadingProgressBar.setVisibility(View.VISIBLE);
        rootView.findViewById(R.id.quiz_8_question_image_view).setVisibility(View.INVISIBLE);

        FirebaseStorageService.setGlideImageJPG(mContext, questionImageName, rootView.findViewById(R.id.quiz_8_question_image_view),
                new RequestListener() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target target, boolean isFirstResource) {
                        FirebaseStorageService.setGlideImagePNG(rootView.findViewById(R.id.quiz_8_question_image_view), questionImageName);
                        mLoadingProgressBar.setVisibility(View.GONE);
                        DialogUtils.dismissPDialog();
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Object resource, Object model, Target target, DataSource dataSource, boolean isFirstResource) {
                        mLoadingProgressBar.setVisibility(View.GONE);
                        rootView.findViewById(R.id.quiz_8_question_image_view).setVisibility(View.VISIBLE);
                        DialogUtils.dismissPDialog();
                        return false;
                    }

                });

        rootView.findViewById(R.id.quiz_8_question_image_view).setOnClickListener(v -> {
            Intent intent = new Intent(mContext, FullScreenImageActivity.class);
            intent.putExtra(IMAGE_ID_TAG, questionImageName);
            mContext.startActivity(intent);
        });

        rootView.findViewById(R.id.quiz_8_zoom_question_image_view).setOnClickListener(v -> {
            Intent intent = new Intent(mContext, FullScreenImageActivity.class);
            intent.putExtra(IMAGE_ID_TAG, questionImageName);
            mContext.startActivity(intent);
        });

        mNextQuestionButton = rootView.findViewById(R.id.quiz_next_question_button);
        mPreviousQuestionButton = rootView.findViewById(R.id.quiz_previous_button);


        if(mRetryQuestion){
            mNextQuestionButton.setText(getString(R.string.submit));
            mPreviousQuestionButton.setVisibility(View.GONE);

            mNextQuestionButton.setOnClickListener(v -> {
                int selectedId = mRadioGroup.getCheckedRadioButtonId();
                if(selectedId == -1){
                    Toast.makeText(mContext, R.string.quiz_select_answer, Toast.LENGTH_LONG).show();
                }

                else {
                    // find the radiobutton by returned id
                    RadioButton radioButton = (RadioButton) rootView.findViewById(selectedId);
                    Log.d(TAG, "Retry q " + mQuestionNumber);
                    String currentChoice = radioButton.getTag().toString();
                    Log.d(TAG, "past choice: " + mUserChoicePastChoice + " current = " + currentChoice);
                    Log.d(TAG, "replace: " + mUserChoicePastChoice.set(mQuestionNumber-1, currentChoice));

                    mUserChoice.add(radioButton.getTag().toString());
                        Log.d(TAG, "start nbt results");
                        mBundle.putSerializable(USER_CHOICE_TAG, mUserChoicePastChoice);
                        NBTQuizFinishFragment nextFrag= new NBTQuizFinishFragment();
                        nextFrag.setArguments(mBundle);
                        Objects.requireNonNull(getActivity()).getSupportFragmentManager().beginTransaction()
                                .replace(R.id.nbt_quiz_frame_layout_container, nextFrag,"findThisFragment")
                                .addToBackStack(null)
                                .commit();
                    }

            });

        }
        else{
            if (mQuestionNumber == 1){
                mPreviousQuestionButton.setVisibility(View.GONE);
            }
            else {
                mPreviousQuestionButton.setVisibility(View.VISIBLE);
            }
            mPreviousQuestionButton.setOnClickListener(v -> {
                mPreviousQuestionButton.setVisibility(View.VISIBLE);
                mQuestionNumber--;
                mUserChoice.remove(mUserChoice.size()-1);
                makeNBTPage();
            });

            if (null != mNextQuestionButton)
                mNextQuestionButton.setText(getString(R.string.next));
                mNextQuestionButton.setOnClickListener(v -> {
                    int selectedId = mRadioGroup.getCheckedRadioButtonId();
                    if(selectedId == -1){
                        Toast.makeText(mContext, R.string.quiz_select_answer, Toast.LENGTH_LONG).show();
                    }

                    else {
                        // find the radiobutton by returned id
                        RadioButton radioButton = (RadioButton) rootView.findViewById(selectedId);
                        mUserChoice.add(radioButton.getTag().toString());
                        Log.d(TAG, "userChoice: ");
                        for(String uChoice: mUserChoice){
                            Log.d(TAG, "\t" + uChoice + "\n");
                        }

                        mQuestionNumber++;
                        Log.d(TAG, "mQuestionNumber: " + mQuestionNumber + "/" + mNumberQuestions);


                        if (QUIZ_TYPE_8 == mNBTQuiz.getType()&& (mQuestionNumber <= mNumberQuestions))
                            makeNBTPage();

                        else if ( (mNBTQuiz.getType() == QUIZ_TYPE_8) && (mQuestionNumber > mNumberQuestions) ){

                            Log.d(TAG, "start nbt results");
                            mBundle.putSerializable(USER_CHOICE_TAG, mUserChoice);
                            NBTQuizFinishFragment nextFrag= new NBTQuizFinishFragment();
                            nextFrag.setArguments(mBundle);
                            Objects.requireNonNull(getActivity()).getSupportFragmentManager().beginTransaction()
                                    .replace(R.id.nbt_quiz_frame_layout_container, nextFrag,"findThisFragment")
                                    .addToBackStack(null)
                                    .commit();
                        }
                        else {
                            Log.d(TAG, "start other results");

                        }

                    }
                });
        }



        mProgressBar = rootView.findViewById(R.id.quiz_progress_bar);
        mProgressBar.setProgress( (mQuestionNumber*100)/mNumberQuestions );

        View.OnClickListener optionListener = v -> {
            Log.d(TAG, "optionCliocked");
            switch(v.getId()) {
                case R.id.quiz_8_answer_1_image_view:
                    mRadioGroup.check(R.id.quiz_8_answer_1_radio_button);
                    break;

                case R.id.quiz_8_answer_1_radio_button:
                    mRadioGroup.check(R.id.quiz_8_answer_1_radio_button);
                    break;

                case R.id.quiz_8_answer_2_image_view:
                    mRadioGroup.check(R.id.quiz_8_answer_2_radio_button);
                    break;

                case R.id.quiz_8_answer_2_radio_button:
                    mRadioGroup.check(R.id.quiz_8_answer_2_radio_button);
                    break;

                case R.id.quiz_8_answer_3_image_view:
                    mRadioGroup.check(R.id.quiz_8_answer_3_radio_button);
                    break;

                case R.id.quiz_8_answer_3_radio_button:
                    mRadioGroup.check(R.id.quiz_8_answer_3_radio_button);
                    break;

                case R.id.quiz_8_answer_4_image_view:
                    mRadioGroup.check(R.id.quiz_8_answer_4_radio_button);
                    break;

                case R.id.quiz_8_answer_4_radio_button:
                    mRadioGroup.check(R.id.quiz_8_answer_4_radio_button);
                    break;

            }
        };

        for (int c = 0; c < optionImageNames.size(); c++){
                (imageViewList.get(c)).setImageBitmap(null);
                String opt = (questionStoragePrefix + mQuestionNumber + optionImageNames.get(c));

                FirebaseStorageService.setGlideImageWithDimensJPG(imageViewList.get(c), opt, (int)(2*getResources().getDimension(R.dimen.question_image_height)), (int)getResources().getDimension(R.dimen.nbt_option_image_height) );

                radioButtonList.get(c).setTag(optionImageNames.get(c));
                radioButtonList.get(c).setOnClickListener(optionListener);
                imageViewList.get(c).setOnClickListener(optionListener);

        }

    }

}