package com.utils.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SimpleItemAnimator;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Spinner;
import android.widget.TextView;

import com.utils.UpdateSchoolSubjects;
import com.utils.UpdateUserProfileSchool;
import com.utils.adapters.InputUniversityCurrentFormAdapter;
import com.utils.adapters.InputUniversityProspectiveFormAdapter;
import com.utils.viewholders.InputMarksViewHolder;
import com.utils.viewholders.InputUploadViewHolder;
import com.zelda.object.UserProfileSchool;
import com.zelda.object.UserState;
import com.zeldav1.R;

import java.util.Arrays;
import java.util.Objects;

import static com.utils.Constants.FIRESTORE_UNIVERSITY_SUBJECTS;
import static com.utils.Constants.FIRESTORE_UNIVERSITY_TRANSCRIPT_UPLOADED;
import static com.utils.Constants.UPU_PROSPECTIVE;
import static com.utils.Constants.USER_PROFILE_SCHOOL_TAG;
import static com.utils.Constants.USER_STATE_TAG;
import static com.utils.Constants.firestore_CBA_U_current_List;

public class CBAFormUniversityFragment extends Fragment implements
        UpdateUserProfileSchool,
        UpdateSchoolSubjects {
    private static final String TAG = CBAFormUniversityFragment.class.getSimpleName();
    private Context mContext;

    private UserState mUserState;
    private UserProfileSchool mUserProfileSchool;
    private Boolean mUnreadBoolean;

    private Bundle mBundle;
    private RecyclerView mUniversityRecyclerView;
    private InputUniversityProspectiveFormAdapter mProspectiveUniversityInputAdapter;
    private InputUniversityCurrentFormAdapter mCurrentUniversityInputAdapter;
    private View mInputUniversityYearSpinnerViewHolder;
    private TextView mUniversityYearHeadingTextView;
    private Spinner mUniversityYearSpinner;

    private Boolean mViewInitialized = false, mUserStateUpdated = false, mUserProfileSchoolUpdated = false;


    public CBAFormUniversityFragment() {

    }

    public void updateUserState(UserState userState) {
        mUserState = userState;
        mUserStateUpdated = true;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Objects.requireNonNull(getActivity()).getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        return inflater.inflate(R.layout.fragment_recyclerview, container, false);
    }


    public static CBAFormUniversityFragment newInstance(UserState userState, UserProfileSchool userProfileSchool) {
        CBAFormUniversityFragment cbaFormUniversityFragment = new CBAFormUniversityFragment();
        Bundle b = new Bundle();
        b.putSerializable(USER_STATE_TAG, userState);

        if (null != userProfileSchool) {
            b.putSerializable(USER_PROFILE_SCHOOL_TAG, userProfileSchool);
        }

        cbaFormUniversityFragment.setArguments(b);

        return cbaFormUniversityFragment;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState){

        mUniversityRecyclerView = view.findViewById(R.id.fragment_cba_recyclerview);

        mContext = getContext();
        mBundle = getArguments();

        if (null != mBundle) {
            mUserState = (UserState) mBundle.getSerializable(USER_STATE_TAG);
            mUserProfileSchool = (UserProfileSchool) mBundle.getSerializable(USER_PROFILE_SCHOOL_TAG);
        }

        UpdateUniversityAdapter updateAdapterListener = new UpdateUniversityAdapter() {
            @Override
            public void updateUniversityAdapterToProspective(){
                //do stuff here
                Log.d(TAG, "setUniversityProspectiveAdapter");
                setUniversityProspectiveAdapter();
            }

            @Override
            public void updateUniversityAdapterToCurrent(){
                //do stuff here
                Log.d(TAG, "setUniversityCurrentAdapter");
                setUniversityCurrentAdapter();
            }
        };

        if (null != mUserProfileSchool) {
            mProspectiveUniversityInputAdapter = new InputUniversityProspectiveFormAdapter(mContext, mUserProfileSchool, updateAdapterListener, mUniversityRecyclerView);
            mCurrentUniversityInputAdapter = new InputUniversityCurrentFormAdapter(mContext, mUserProfileSchool, updateAdapterListener, mUniversityRecyclerView);

        }
        else {
            mCurrentUniversityInputAdapter = new InputUniversityCurrentFormAdapter(mContext, updateAdapterListener, mUniversityRecyclerView);
            mProspectiveUniversityInputAdapter = new InputUniversityProspectiveFormAdapter(mContext, updateAdapterListener, mUniversityRecyclerView);
        }

        setAdapterType();

        ((SimpleItemAnimator) mUniversityRecyclerView.getItemAnimator()).setSupportsChangeAnimations(false);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);

        mUniversityRecyclerView.setLayoutManager(mLayoutManager);

        mViewInitialized = true;
    }

    public void setUploadProgress(double uploadProgress){
        Log.d(TAG, "uploadProgress " + uploadProgress);
        InputUploadViewHolder inputUploadViewHolder = null;

        if (mUniversityRecyclerView.getAdapter() instanceof InputUniversityCurrentFormAdapter)
            inputUploadViewHolder = (InputUploadViewHolder)mUniversityRecyclerView.findViewHolderForAdapterPosition(Arrays.asList(firestore_CBA_U_current_List).indexOf(FIRESTORE_UNIVERSITY_TRANSCRIPT_UPLOADED));

        Log.d(TAG, "inputUploadViewHolder " + inputUploadViewHolder);
        if (null != inputUploadViewHolder)
            inputUploadViewHolder.setUploading(uploadProgress);
    }

    public void setUploadFailed(){
        Log.d(TAG, "setUploadFailed ");
        InputUploadViewHolder inputUploadViewHolder = null;

        if (mUniversityRecyclerView.getAdapter() instanceof InputUniversityCurrentFormAdapter)
            inputUploadViewHolder = (InputUploadViewHolder)mUniversityRecyclerView.findViewHolderForAdapterPosition(Arrays.asList(firestore_CBA_U_current_List).indexOf(FIRESTORE_UNIVERSITY_TRANSCRIPT_UPLOADED));

        Log.d(TAG, "inputUploadViewHolder " + inputUploadViewHolder);
        if (null != inputUploadViewHolder)
            inputUploadViewHolder.uploadFailed();
    }

    public void setUploadComplete(String filename){
        Log.d(TAG, "setUploadComplete ");
        InputUploadViewHolder inputUploadViewHolder = null;

        if (mUniversityRecyclerView.getAdapter() instanceof InputUniversityCurrentFormAdapter)
            inputUploadViewHolder = (InputUploadViewHolder)mUniversityRecyclerView.findViewHolderForAdapterPosition(Arrays.asList(firestore_CBA_U_current_List).indexOf(FIRESTORE_UNIVERSITY_TRANSCRIPT_UPLOADED));

        Log.d(TAG, "inputUploadViewHolder " + inputUploadViewHolder);
        if (null != inputUploadViewHolder)
            inputUploadViewHolder.uploadingComplete(filename);
    }

    @Override
    public void updateUserProfileSchool(UserProfileSchool userProfileSchool) {
        mUserProfileSchool = userProfileSchool;

        if (getActivity() != null && null != mUserProfileSchool) {
            Log.d(TAG, "UPS updated:" +  mUserProfileSchool.toString());
            mUserProfileSchoolUpdated = true;

            mCurrentUniversityInputAdapter.updateUserProfileSchool(mUserProfileSchool);
            mProspectiveUniversityInputAdapter.updateUserProfileSchool(mUserProfileSchool);

            setAdapterType();

        }

    }

    @Override
    public void captureSubjectsMarks() {
        Log.d(TAG, "captureSubjectsMarks");

        InputMarksViewHolder inputMarksViewHolder = null;

        if (mUniversityRecyclerView.getAdapter() instanceof InputUniversityCurrentFormAdapter) {
            inputMarksViewHolder = (InputMarksViewHolder) mUniversityRecyclerView.findViewHolderForAdapterPosition(Arrays.asList(firestore_CBA_U_current_List).indexOf(FIRESTORE_UNIVERSITY_SUBJECTS));

            if (null != mUserProfileSchool && null != inputMarksViewHolder)
                mUserProfileSchool.setUniversitySubjects(inputMarksViewHolder.getSubjectsAdapter().getSubjectMarksHashMap());
        }

    }

    public interface UpdateUniversityAdapter {
        public void updateUniversityAdapterToProspective();
        public void updateUniversityAdapterToCurrent();
    }

    private void setAdapterType(){
        if (null != mUserProfileSchool && null != mUserProfileSchool.getUniversityYear() && mUserProfileSchool.getUniversityYear().equals(UPU_PROSPECTIVE)) {
            setUniversityProspectiveAdapter();
        } else {
            setUniversityCurrentAdapter();
        }
        mUniversityRecyclerView.getAdapter().notifyDataSetChanged();
    }

    private void setUniversityProspectiveAdapter(){
        mUniversityRecyclerView.setAdapter(mProspectiveUniversityInputAdapter);
    }

    private void setUniversityCurrentAdapter(){
        mUniversityRecyclerView.setAdapter(mCurrentUniversityInputAdapter);
    }
}
