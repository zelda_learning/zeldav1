package com.utils.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.utils.adapters.ViewAdapter;
import com.utils.services.FirebaseFirestoreService;
import com.zelda.object.NBTQuiz;
import com.zelda.object.UserState;
import com.zeldav1.NBTQuizActivity;
import com.zeldav1.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

import static com.utils.Constants.NBT_QUIZ_COMPLETED;
import static com.utils.Constants.QUIZ_ID_TAG;
import static com.utils.Constants.UNREAD_MESSAGE_TAG;
import static com.utils.Constants.USER_CHOICE_TAG;
import static com.utils.Constants.USER_STATE_FIRESTORE;
import static com.utils.Constants.USER_STATE_TAG;

public class NBTQuizFinishFragment extends Fragment {

    private static final String TAG = NBTQuizFinishFragment.class.getSimpleName();

    private NBTQuiz mNBTQuiz;
    private UserState mUserState;
    private Boolean mUnreadBoolean;
    private Button mBackButton, mRestartButton;
    private Bundle mBundle;
    private ArrayList<String> mUserChoice;
    private View rootView;

    private RecyclerView mRecyclerView;
    private FirebaseFirestoreService mFirebaseUtil = new FirebaseFirestoreService();

    public NBTQuizFinishFragment(){}


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_nbt_quiz_finish, null);

        mBundle = getArguments();
        if(mBundle != null){
            mNBTQuiz = (NBTQuiz)mBundle.getSerializable(QUIZ_ID_TAG);
            mUserState = (UserState)mBundle.getSerializable(USER_STATE_TAG);
            mUnreadBoolean = mBundle.getBoolean(UNREAD_MESSAGE_TAG);

            if (null != mBundle.getSerializable(USER_CHOICE_TAG)){
                ArrayList<?> userChoiceCast = (ArrayList<?>) mBundle.getSerializable(USER_CHOICE_TAG);
                mUserChoice = new ArrayList<>();
                if (null != userChoiceCast){
                    for (Object s: userChoiceCast){
                        mUserChoice.add((String)s);
                    }
                }
                Log.d(TAG, "userChoices: " + mUserChoice.toString());
                rootView = view;
                makeQuizFinishNBTView();
            }

        }

        return view;
    }


    private void updateUserState(HashMap<String, Integer> updateUserState){
        mFirebaseUtil.getFirestore().collection(USER_STATE_FIRESTORE).document(mUserState.getId())
                .update(NBT_QUIZ_COMPLETED, updateUserState)
                .addOnSuccessListener(aVoid -> Log.d(TAG, "DocumentSnapshot successfully updated!"))
                .addOnFailureListener(e -> Log.w(TAG, "Error updating document", e));
    }

    private void makeQuizFinishNBTView(){
        mBackButton = rootView.findViewById(R.id.quiz_finish_nbt_back_button);
        mRestartButton = rootView.findViewById(R.id.quiz_finish_nbt_restart_button);
        mRecyclerView = rootView.findViewById(R.id.quiz_finish_nbt_recycler_view);

        mBackButton.setOnClickListener(v -> {
            Objects.requireNonNull(getActivity()).onBackPressed();
        });

        mRestartButton.setOnClickListener(v -> {
            Intent intent = new Intent(getActivity(), NBTQuizActivity.class);
            intent.putExtra(QUIZ_ID_TAG, mNBTQuiz);
            intent.putExtra(USER_STATE_TAG, mUserState);
            intent.putExtra(UNREAD_MESSAGE_TAG, mUnreadBoolean);
            startActivity(intent);
            Objects.requireNonNull(getActivity()).finish();
        });

        ((TextView)rootView.findViewById(R.id.quiz_nbt_finish_heading)).setText(mNBTQuiz.getTitle());
        ((TextView)rootView.findViewById(R.id.quiz_nbt_subtitle_heading)).setText(getResources().getString(R.string.quiz_finish_heading, mNBTQuiz.getNumberQuestions() + ""));

        HashMap<String,Integer> quizList = mUserState.getCompletedNBTQuizzes();
        int correct = 0;
        for(int c = 0; c < mUserChoice.size(); c++){
            Log.d(TAG, "Choice: " + mUserChoice.get(c) + "\tAnswer: " + mNBTQuiz.getAnswers().get(c));
            if ( mUserChoice.get(c).equals( (mNBTQuiz.getAnswers().get(c)) ) ){
                Log.d(TAG, "match");
                correct++;
            }
        }

        quizList.put(mNBTQuiz.getId(), correct);
        ((TextView)rootView.findViewById(R.id.quiz_nbt_results_heading)).setText( Objects.requireNonNull(getContext()).getString(R.string.quiz_nbt_results_heading, correct, mNBTQuiz.getNumberQuestions()));

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(Objects.requireNonNull(getContext()), LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(mLayoutManager);

        ViewAdapter<String> mQuizFeedAdapter = new ViewAdapter<String>(mUserChoice, mNBTQuiz, R.layout.item_nbt_quiz_result, mRecyclerView);

        mRecyclerView.setAdapter(mQuizFeedAdapter);
        mQuizFeedAdapter.notifyDataSetChanged();
        updateUserState(quizList);

    }

    public void setMsg(String msg){
        Log.d(TAG, "msg " + msg);
    }


}
