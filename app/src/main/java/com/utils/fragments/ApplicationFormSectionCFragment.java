package com.utils.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.utils.services.FirebaseFirestoreService;
import com.zelda.object.ApplicationForm;
import com.zelda.object.ApplicationFormSectionC;
import com.zelda.object.UserState;

import com.zeldav1.R;

import java.util.Objects;

import static com.utils.Constants.APPLICATION_FORM_SECTION_C;

public class ApplicationFormSectionCFragment extends Fragment {
    private static final String TAG = ApplicationFormSectionCFragment.class.getSimpleName();
    private Context mContext;
    private UserState mUserState;
    private FirebaseFirestoreService mFirebaseUtil;
    private ApplicationForm mApplicationForm;
    private ApplicationFormSectionC mApplicationFormSectionC;

    private EditText mAwardsEditText, mCareerIntentionsEditText,
            mCulturalActivitiesEditText, mGuardianNameEditText,
            mGuardianNoEditText, mHobbiesEditText, mOtherInfoEditText;

    private Spinner mRelativePetroSASpinner;
    private Button mSaveButton;


    public ApplicationFormSectionCFragment(){

    }

    public void setApplicationFormSectionAObject(){
        mFirebaseUtil.fetchDocument(value -> {
            if (null != value){
                mApplicationFormSectionC = value.toObject(ApplicationFormSectionC.class);
                Log.d(TAG, "Application Form Section A:" + mApplicationFormSectionC.toString());
            }
            else{
                mApplicationFormSectionC = new ApplicationFormSectionC();
                Log.d(TAG, "No Application Form C found. Create new object: " + mApplicationFormSectionC);
            }
            populateViewApplicationFormSectionC();
        }, APPLICATION_FORM_SECTION_C, mApplicationForm.getApplicationId());
    }


    public void populateViewApplicationFormSectionC() {
        displayText(mAwardsEditText, mApplicationFormSectionC.getAwards(), getResources().getString(R.string.application_form_awards_title));
        displayText(mCareerIntentionsEditText, mApplicationFormSectionC.getCareerIntentions(), getResources().getString(R.string.application_form_career_intentions_hint));
        displayText(mCulturalActivitiesEditText, mApplicationFormSectionC.getCulturalActivities(), getResources().getString(R.string.application_form_activities_hint));
        displayText(mGuardianNameEditText, mApplicationFormSectionC.getGuardianName(), getResources().getString(R.string.application_form_guardian_name));
        displayText(mGuardianNoEditText, mApplicationFormSectionC.getGuardianNo(), getResources().getString(R.string.application_form_guardian_contact));
        displayText(mHobbiesEditText, mApplicationFormSectionC.getHobbies(), getResources().getString(R.string.application_form_hobbies_hint));
        displayText(mOtherInfoEditText, mApplicationFormSectionC.getOtherInfo(), getResources().getString(R.string.application_form_other_info_hint));



        ArrayAdapter<CharSequence> adapterRelativePetroSASpinner = ArrayAdapter.createFromResource(mContext, R.array.yes_no, R.layout.spinner_item);
        int spinnerPosition = adapterRelativePetroSASpinner.getPosition(mApplicationFormSectionC.getRelativePetroSA());
        mRelativePetroSASpinner.setSelection(spinnerPosition);
        mRelativePetroSASpinner.setAdapter(adapterRelativePetroSASpinner);

        //ProfileProgress.setProgress(mUserState.getProgress());
    }

    public static void displayText(EditText editText, String field, String fieldText){
        if (null != field)
            editText.setText(field);
        else
            editText.setHint(fieldText);
    }


    public boolean isUpdated(EditText editText, String fieldContent){
        if( editText.getText().toString().trim().length() <= 0){
            return false;
        }else{
            return !(editText.getText().toString().trim().equalsIgnoreCase(fieldContent));
        }
    }

    public void updateValues(){
        if (isUpdated(mAwardsEditText, mApplicationFormSectionC.getAwards())) {
            mApplicationFormSectionC.setAwards(mAwardsEditText.getText().toString().trim());
            //mUserState.incrementProgress();
        }

        if (isUpdated(mCareerIntentionsEditText, mApplicationFormSectionC.getCareerIntentions())) {
            mApplicationFormSectionC.setCareerIntentions(mCareerIntentionsEditText.getText().toString().trim());
            //mUserState.incrementProgress();
        }

        if (isUpdated(mCulturalActivitiesEditText, mApplicationFormSectionC.getCulturalActivities())) {
            mApplicationFormSectionC.setCulturalActivities(mCulturalActivitiesEditText.getText().toString().trim());
            //mUserState.incrementProgress();
        }

        if (isUpdated(mGuardianNameEditText, mApplicationFormSectionC.getGuardianName())) {
            mApplicationFormSectionC.setGuardianName(mGuardianNameEditText.getText().toString().trim());
            //mUserState.incrementProgress();
        }

        if (isUpdated(mGuardianNoEditText, mApplicationFormSectionC.getGuardianNo())) {
            mApplicationFormSectionC.setGuardianNo(mGuardianNoEditText.getText().toString().trim());
            //mUserState.incrementProgress();
        }

        if (isUpdated(mHobbiesEditText, mApplicationFormSectionC.getHobbies())) {
            mApplicationFormSectionC.setHobbies(mHobbiesEditText.getText().toString().trim());
            //mUserState.incrementProgress();
        }

        if (isUpdated(mOtherInfoEditText, mApplicationFormSectionC.getOtherInfo())) {
            mApplicationFormSectionC.setOtherInfo(mOtherInfoEditText.getText().toString().trim());
            //mUserState.incrementProgress();
        }

        mApplicationFormSectionC.setRelativePetroSA(mRelativePetroSASpinner.getSelectedItem().toString());

        mFirebaseUtil.setFirestoreDocument(mContext, APPLICATION_FORM_SECTION_C, mUserState.getId(), mApplicationFormSectionC, null);
    }

    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        mFirebaseUtil = new FirebaseFirestoreService();
        mContext = context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        assert getActivity() != null;
        Log.d(TAG, "ApplicationFormAFragment with: " + mUserState.toString());
        setApplicationFormSectionAObject();
    }

    @Override
    public void onResume() {
        super.onResume();
        setApplicationFormSectionAObject();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Objects.requireNonNull(getActivity()).getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        return inflater.inflate(R.layout.fragment_application_form_section_a, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState){
        mRelativePetroSASpinner = getView().findViewById(R.id.application_form_relatives_petrosa_spinner);

        mSaveButton = view.findViewById(R.id.user_view_overview_save_changes);

        mSaveButton.setOnClickListener(v -> {
            Log.d(TAG, "Saving Changes");
            updateValues();
        });
    }

    private void showToast(String message){
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

}
