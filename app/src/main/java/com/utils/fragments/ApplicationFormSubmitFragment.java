package com.utils.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.utils.adapters.ApplicationFormReviewAdapter;
import com.zelda.object.ApplicationForm;
import com.zelda.object.ApplicationFormSectionA;
import com.zelda.object.UserApplicationFormA;
import com.zelda.object.UserState;
import com.zeldav1.R;

import leo.me.la.labeledprogressbarlib.LabeledProgressBar;

import static com.utils.Constants.APPLICATION_FORM_SECTIONS_TAG;
import static com.utils.Constants.UNREAD_MESSAGE_TAG;
import static com.utils.Constants.USER_STATE_TAG;

public class ApplicationFormSubmitFragment extends Fragment {
    private static final String TAG = ApplicationFormSubmitFragment.class.getSimpleName();

    private Context mContext;
    private UserState mUserState;
    private Boolean mUnreadBoolean;

    private ApplicationForm mApplicationForm;
    private ApplicationFormSectionA mApplicationFormSectionA;
    private UserApplicationFormA mUserApplicationFormA;
    private int applicationElements, userElements, progressValue;

    private Bundle mBundle;
    private View rootView;
    private int mApplicationFormNumberSections;

    private ConstraintLayout mReviewContainer, mSubmitCompleteContainer;

    private TextView mSubmissionCompleteTextView, mSubmitInfoHeading;
    private Button mSubmitButton;
    private ProgressBar mUploadProgressBar;
    private LabeledProgressBar mApplicationProgress;

    private ApplicationFormEventListener applicationFormEventListener;

    private RecyclerView mApplicationSubmitRecyclerView;
    private ApplicationFormReviewAdapter mApplicationFormReviewAdapter;

    public interface ApplicationFormEventListener {
        void uploadUserApplicationFormSectionA();
        void submitApplicationForm(int progressValue);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            applicationFormEventListener = (ApplicationFormEventListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement onSomeEventListener");
        }
    }

    public void updateApplicationFormSectionA(ApplicationFormSectionA applicationFormSectionA) {
        mApplicationFormSectionA = applicationFormSectionA;
        mApplicationFormReviewAdapter.updateApplicationForm(mApplicationFormSectionA);
        mApplicationFormReviewAdapter.notifyDataSetChanged();
        applicationElements = mApplicationFormSectionA.getNumberElements();
        updateProgressValues(userElements);
    }

    public void updateUserApplicationFormA(UserApplicationFormA userApplicationFormA) {
        mUserApplicationFormA = userApplicationFormA;
        mApplicationFormReviewAdapter.updateUserApplicationFormA(mUserApplicationFormA);
        mApplicationFormReviewAdapter.notifyDataSetChanged();
        userElements = mUserApplicationFormA.getNumberElements();
        updateProgressValues(userElements);
    }

    public void updateScreenUploadCancelled(){
        mSubmitButton.setText(getResources().getString(R.string.submit_all_caps));
        mReviewContainer.setVisibility(View.VISIBLE);
        mUploadProgressBar.setVisibility(View.GONE);
    }

    public void updateScreenUploadFinished(){
        mSubmitButton.setVisibility(View.GONE);
        mReviewContainer.setVisibility(View.GONE);
        mUploadProgressBar.setVisibility(View.GONE);
        mSubmitCompleteContainer.setVisibility(View.VISIBLE);
    }

    public void updateScreenUploadStarting(){
        Log.d(TAG, "Screen uploading");
        mSubmitButton.setText(getResources().getString(R.string.updating_changes_text));
        mReviewContainer.setVisibility(View.GONE);
        mUploadProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_application_form_submit, null);

        rootView = view;
        mSubmitButton = view.findViewById(R.id.application_form_submit_button);
        mSubmissionCompleteTextView = view.findViewById(R.id.application_form_submission_complete);
        mSubmitInfoHeading = view.findViewById(R.id.application_form_submit_information);
        mUploadProgressBar = view.findViewById(R.id.application_form_submit_uploading_application_progress_bar);
        mApplicationProgress = view.findViewById(R.id.application_form_submit_progress_bar);

        mReviewContainer = view.findViewById(R.id.application_form_submit_review_container);
        mSubmitCompleteContainer = view.findViewById(R.id.application_form_submit_complete_container);

        mApplicationSubmitRecyclerView = rootView.findViewById(R.id.application_form_submit_recycler_view);

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        mApplicationSubmitRecyclerView.setLayoutManager(mLayoutManager);

        if (null != mApplicationFormSectionA){
            mApplicationFormReviewAdapter = new ApplicationFormReviewAdapter(mApplicationFormSectionA, mUserApplicationFormA, true);
        }
        else {
            mApplicationFormReviewAdapter = new ApplicationFormReviewAdapter(true);
        }

        mApplicationSubmitRecyclerView.setAdapter(mApplicationFormReviewAdapter);
        mApplicationSubmitRecyclerView.setHasFixedSize(true);
        mApplicationFormReviewAdapter.notifyDataSetChanged();

        mSubmitButton.setOnClickListener(v -> {
            applicationFormEventListener.submitApplicationForm(progressValue);
        });
        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        assert getActivity() != null;
        mBundle = getArguments();
        mContext = getContext();

        if(mBundle != null) {
            mUserState = (UserState)mBundle.getSerializable(USER_STATE_TAG);
            mUnreadBoolean = mBundle.getBoolean(UNREAD_MESSAGE_TAG);
            mApplicationFormNumberSections = mBundle.getInt(APPLICATION_FORM_SECTIONS_TAG);
        }

    }

    private void updateProgressValues(int userElements){
        this.userElements = userElements;
        if (applicationElements != 0) {
            progressValue = (this.userElements * 100) / applicationElements;
            mApplicationProgress.setValue(progressValue);
            mApplicationProgress.setLabelText(getResources().getString(R.string.application_form_progress_text, userElements, applicationElements));
        }
    }
}
