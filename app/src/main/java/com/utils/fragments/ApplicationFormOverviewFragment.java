package com.utils.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.utils.CustomUtils;
import com.utils.services.FirebaseStorageService;
import com.zelda.object.ApplicationForm;
import com.zelda.object.Bursary;
import com.zelda.object.UserState;
import com.zeldav1.R;

import java.util.Objects;

import static com.utils.Constants.APPLICATION_FORM_SECTIONS_TAG;
import static com.utils.Constants.APPLICATION_FORM_TAG;
import static com.utils.Constants.BURSARY_TAG;
import static com.utils.Constants.UNREAD_MESSAGE_TAG;
import static com.utils.Constants.USER_STATE_TAG;

public class ApplicationFormOverviewFragment extends Fragment {

    private ApplicationForm mApplicationForm;
    private Bursary mBursary;
    private UserState mUserState;
    private Boolean mUnreadBoolean;
    private int mApplicationFormSections;

    private static final String TAG = ApplicationFormOverviewFragment.class.getSimpleName();

    public ApplicationFormOverviewFragment(){}

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_application_form_overview, null);

        Bundle bundle = getArguments();
        if(bundle != null){
            mApplicationForm = (ApplicationForm) bundle.getSerializable(APPLICATION_FORM_TAG);
            mBursary = (Bursary) bundle.getSerializable(BURSARY_TAG);
            mUserState = (UserState)bundle.getSerializable(USER_STATE_TAG);
            mUnreadBoolean = bundle.getBoolean(UNREAD_MESSAGE_TAG);
            mApplicationFormSections = bundle.getInt(APPLICATION_FORM_SECTIONS_TAG);

            if(null != getActivity() && mBursary != null){
                Log.d(TAG,   "" + mUserState.getId() + "\n" + mBursary.getId());
                ((TextView)view.findViewById(R.id.application_form_bursary_title)).setText(mBursary.getTitle());
                String overviewInfoString = Objects.requireNonNull(getContext().getString(R.string.application_form_overview_information, mApplicationFormSections));
                if(mApplicationFormSections == 1){
                    int fullStopIndex = overviewInfoString.indexOf('.');
                    overviewInfoString = overviewInfoString.substring(0, fullStopIndex - 1) + overviewInfoString.substring(fullStopIndex);
                }
                ((TextView) view.findViewById(R.id.application_form_overview_information)).setText(overviewInfoString);
                ((TextView) view.findViewById(R.id.application_form_overview_requirements)).setText(mBursary.getRequirements());
                FirebaseStorageService.setGlideImage(view.findViewById(R.id.application_form_bursary_image), CustomUtils.setImageNameOnType(Bursary.class, mBursary.getId()));

            }
        }

        return view;
    }

    public void setMsg(String msg){
        Log.d(TAG, "msg " + msg);
    }

}
