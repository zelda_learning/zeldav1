package com.utils.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.utils.DialogUtils;
import com.utils.services.FirebaseStorageService;
import com.zelda.object.NBTQuiz;
import com.zelda.object.UserState;
import com.zeldav1.R;

import java.util.Objects;

import static com.utils.Constants.QUIZ_ID_TAG;
import static com.utils.Constants.UNREAD_MESSAGE_TAG;
import static com.utils.Constants.USER_STATE_TAG;

public class NBTQuizStartFragment extends Fragment {

    private static final String TAG = NBTQuizStartFragment.class.getSimpleName();

    private NBTQuiz mNBTQuiz;
    private UserState mUserState;
    private Boolean mUnreadBoolean;

    public NBTQuizStartFragment(){}

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_nbt_quiz_start, null);

        Bundle bundle = getArguments();
        if(bundle != null){
            mNBTQuiz = (NBTQuiz)bundle.getSerializable(QUIZ_ID_TAG);
            mUserState = (UserState)bundle.getSerializable(USER_STATE_TAG);
            mUnreadBoolean = bundle.getBoolean(UNREAD_MESSAGE_TAG);
            if(mNBTQuiz != null){
                Log.d(TAG, "quiz found" + mNBTQuiz + "user " + mUserState);
                ((TextView)view.findViewById(R.id.quiz_title)).setText(mNBTQuiz.getTitle());
                ((TextView)view.findViewById(R.id.quiz_subtitle)).setText(mNBTQuiz.getSubtitle());
                ((TextView)view.findViewById(R.id.quiz_num_questions)).setText(Objects.requireNonNull(getContext()).getString(R.string.quiz_num_questions, mNBTQuiz.getNumberQuestions() ) );

                // TODO: add images
                FirebaseStorageService.setGlideImage(view.findViewById(R.id.quiz_image), mNBTQuiz.getImageName());
                DialogUtils.dismissPDialog();


            }
        }

        return view;
    }

    public void setMsg(String msg){
        Log.d(TAG, "msg " + msg);
    }


}
