package com.utils.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.utils.UpdateUserProfilePersonal;
import com.utils.adapters.InputPersonalFormAdapter;
import com.utils.viewholders.InputUploadViewHolder;
import com.zelda.object.UserProfilePersonal;
import com.zelda.object.UserState;
import com.zeldav1.R;

import java.util.Arrays;

import static com.utils.Constants.FIRESTORE_ID_COPY_UPLOADED;
import static com.utils.Constants.USER_PROFILE_PERSONAL_TAG;
import static com.utils.Constants.USER_STATE_TAG;
import static com.utils.Constants.firestore_UPP_List;

public class UserViewPersonalFragment extends Fragment implements
        UpdateUserProfilePersonal {
    private static final String TAG = UserViewPersonalFragment.class.getSimpleName();

    private Context mContext;

    private UserState mUserState;
    private UserProfilePersonal mUserProfilePersonal;

    private Bundle mBundle;

    private RecyclerView mUserViewPersonalRecyclerView;
    private InputPersonalFormAdapter mUserViewPersonalInputAdapter;

    private Boolean mViewInitialized = false, mUserStateUpdated = false, mUserProfilePersonalUpdated = false;

    public UserViewPersonalFragment(){

    }

    public static UserViewPersonalFragment newInstance(UserState userState, UserProfilePersonal userProfilePersonal) {
        UserViewPersonalFragment userViewPersonalFragment = new UserViewPersonalFragment();
        Bundle b = new Bundle();
        b.putSerializable(USER_STATE_TAG, userState);

        Log.d(TAG, "New Instance: " + userProfilePersonal);

        if (null != userProfilePersonal) {
            b.putSerializable(USER_PROFILE_PERSONAL_TAG, userProfilePersonal);
        }

        userViewPersonalFragment.setArguments(b);
        return userViewPersonalFragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_userview_personal, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState){
        mUserViewPersonalRecyclerView = view.findViewById(R.id.user_view_personal_recycler_view);

        mContext = getContext();
        mBundle = getArguments();

        if (null != mBundle) {
            mUserState = (UserState) mBundle.getSerializable(USER_STATE_TAG);
            mUserProfilePersonal = (UserProfilePersonal) mBundle.getSerializable(USER_PROFILE_PERSONAL_TAG);
        }

        if (null != mUserProfilePersonal)
            mUserViewPersonalInputAdapter = new InputPersonalFormAdapter(mContext, mUserProfilePersonal, mUserViewPersonalRecyclerView);
        else
            mUserViewPersonalInputAdapter = new InputPersonalFormAdapter(mContext, mUserViewPersonalRecyclerView);

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        mUserViewPersonalRecyclerView.setLayoutManager(mLayoutManager);

        mUserViewPersonalRecyclerView.setAdapter(mUserViewPersonalInputAdapter);

        mViewInitialized = true;

    }

    private void showToast(String message){
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    private void updateLoadingScreen(){
        if (mViewInitialized && mUserStateUpdated) {
            Log.d(TAG, "Screen ready");
        }
        else
            Log.d(TAG, "Not all values ready. Screen shouldn't update.");
    }

    public void setUploadProgress(double uploadProgress){
        Log.d(TAG, "uploadProgress " + uploadProgress);
        InputUploadViewHolder inputUploadViewHolder = (InputUploadViewHolder)mUserViewPersonalRecyclerView.findViewHolderForAdapterPosition(Arrays.asList(firestore_UPP_List).indexOf(FIRESTORE_ID_COPY_UPLOADED));

        Log.d(TAG, "inputUploadViewHolder " + inputUploadViewHolder);
        if (null != inputUploadViewHolder)
            inputUploadViewHolder.setUploading(uploadProgress);
    }

    public void setUploadFailed(){
        Log.d(TAG, "setUploadFailed ");
        InputUploadViewHolder inputUploadViewHolder = (InputUploadViewHolder)mUserViewPersonalRecyclerView.findViewHolderForAdapterPosition(Arrays.asList(firestore_UPP_List).indexOf(FIRESTORE_ID_COPY_UPLOADED));

        Log.d(TAG, "inputUploadViewHolder " + inputUploadViewHolder);
        if (null != inputUploadViewHolder)
            inputUploadViewHolder.uploadFailed();
    }

    public void setUploadComplete(String filename){
        Log.d(TAG, "setUploadComplete ");
        InputUploadViewHolder inputUploadViewHolder = (InputUploadViewHolder)mUserViewPersonalRecyclerView.findViewHolderForAdapterPosition(Arrays.asList(firestore_UPP_List).indexOf(FIRESTORE_ID_COPY_UPLOADED));

        Log.d(TAG, "inputUploadViewHolder " + inputUploadViewHolder);
        if (null != inputUploadViewHolder)
            inputUploadViewHolder.uploadingComplete(filename);
    }

    @Override
    public void updateUserProfilePersonal(UserProfilePersonal userProfilePersonal){
        if (getActivity() != null && null != userProfilePersonal) {
            mUserProfilePersonal = userProfilePersonal;
            Log.d(TAG, "UPP updated:" +  mUserProfilePersonal.toString());
            mUserViewPersonalInputAdapter.updateUserProfilePersonal(mUserProfilePersonal);
            mUserProfilePersonalUpdated = true;
            mUserViewPersonalInputAdapter.notifyDataSetChanged();
        }
    }

}
