package com.utils.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.utils.adapters.SubjectsAdapter;
import com.utils.services.FirebaseFirestoreService;
import com.zelda.object.ApplicationForm;
import com.zelda.object.ApplicationFormSectionB;
import com.zelda.object.Subject;
import com.zelda.object.UserState;
import com.zeldav1.R;

import java.util.Objects;

import static com.utils.Constants.APPLICATION_FORM_SECTION_B;

public class ApplicationFormSectionBFragment extends Fragment {
    private static final String TAG = ApplicationFormSectionBFragment.class.getSimpleName();
    private Context mContext;
    private UserState mUserState;
    private FirebaseFirestoreService mFirebaseUtil;
    private ApplicationForm mApplicationForm;
    private ApplicationFormSectionB mApplicationFormSectionB;

    private EditText mSchoolNameEditText, mSchoolAddressLine1EditText, mSchoolAddressLine2EditText,
            mSchoolAddressCityEditText, mSchoolAddressProvinceEditText, mSchoolAddressZipEditText;
    private Spinner mMatricYearSpinner;
    private SubjectsAdapter mApplicationFormSubjectsAdapter;
    private RecyclerView mSubjectsRecyclerView;
    private Button mSaveButton;
    private ImageView mAddSubject;


    public ApplicationFormSectionBFragment(){

    }

    public void setApplicationFormSectionBObject(){
        mFirebaseUtil.fetchDocument(value -> {
            if (null != value){
                mApplicationFormSectionB = value.toObject(ApplicationFormSectionB.class);
                Log.d(TAG, "Application Form Section B:" + mApplicationFormSectionB.toString());
            }
            else{
                mApplicationFormSectionB = new ApplicationFormSectionB();
                Log.d(TAG, "No Application Form B found. Create new object: " + mApplicationFormSectionB);
            }
            populateViewApplicationFormSectionB();
        }, APPLICATION_FORM_SECTION_B, mApplicationForm.getApplicationId());
    }


    public void populateViewApplicationFormSectionB() {
        if (null != mApplicationFormSectionB) {
            displayText(mSchoolNameEditText, mApplicationFormSectionB.getSchoolName(), getResources().getString(R.string.user_view_profile_school_name));
            displayText(mSchoolAddressLine1EditText, mApplicationFormSectionB.getSchoolAddress().getLine1(), getResources().getString(R.string.application_form_address_line_1));
            displayText(mSchoolAddressLine2EditText, mApplicationFormSectionB.getSchoolAddress().getLine2(), getResources().getString(R.string.application_form_address_line_2));
            displayText(mSchoolAddressCityEditText, mApplicationFormSectionB.getSchoolAddress().getCity(), getResources().getString(R.string.application_form_address_city));
            displayText(mSchoolAddressProvinceEditText, mApplicationFormSectionB.getSchoolAddress().getProvince(), getResources().getString(R.string.application_form_address_province));
            displayText(mSchoolAddressZipEditText, mApplicationFormSectionB.getSchoolAddress().getZip(), getResources().getString(R.string.application_form_address_zip_code));

            ArrayAdapter<CharSequence> adapterMatricYear = ArrayAdapter.createFromResource(mContext, R.array.drop_down_matric_year, R.layout.spinner_item);
            int spinnerPosition = adapterMatricYear.getPosition(mApplicationFormSectionB.getMatricYear());
            mMatricYearSpinner.setSelection(spinnerPosition);
            mMatricYearSpinner.setAdapter(adapterMatricYear);

            mSubjectsRecyclerView.setAdapter(mApplicationFormSubjectsAdapter);

            //ProfileProgress.setProgress(mUserState.getProgress());

            updateSubjects();
        }
    }

    private void updateSubjects() {
        Log.d(TAG, "Update subjects");
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mContext);
        mSubjectsRecyclerView.setLayoutManager(mLayoutManager);

        mApplicationFormSubjectsAdapter.notifyItemInserted(0);

        mSubjectsRecyclerView.scrollToPosition(0);
        mApplicationFormSubjectsAdapter.notifyDataSetChanged();

    }


    private void addNewSubject(){
        Log.d(TAG, "Add new subject");
        if (mSubjectsRecyclerView.getChildCount() > 0){
            Log.d(TAG, "At least 1 sub displayed. Check if current has been filled in");
            View view = mSubjectsRecyclerView.getChildAt(0);

            try {
                String subjectName = ((EditText) view.findViewById(R.id.item_subject_name_edittext)).getText().toString();
                double subjectPercentage = Double.parseDouble(((EditText) view.findViewById(R.id.item_subject_percent_edittext)).getText().toString());

                if (!subjectName.equals("") && subjectPercentage != 0.0){
                    mApplicationFormSubjectsAdapter.addSubject(new Subject());
                    updateSubjects();
                    Log.d(TAG, "Non hint, update Section B: " + mApplicationFormSectionB.toString());
                }

                else {
                    Log.d(TAG, "HINT");
                    Toast.makeText(mContext, "Please enter a subject and a mark.", Toast.LENGTH_SHORT).show();
                }

            } catch (NumberFormatException e) {
                Toast.makeText(mContext, "Please enter a subject and a mark.", Toast.LENGTH_SHORT).show();
            }
        }
        else{
            Log.d(TAG, "No subs. Add");
            if (null != mApplicationFormSubjectsAdapter){
                mApplicationFormSubjectsAdapter.addSubject(new Subject());
                updateSubjects();
            }

        }
    }


    public static void displayText(EditText editText, String field, String fieldText){
        if (null != field)
            editText.setText(field);
        else
            editText.setHint(fieldText);
    }


    public boolean isUpdated(EditText editText, String fieldContent){
        if( editText.getText().toString().trim().length() <= 0){
            return false;
        }else{
            return !(editText.getText().toString().trim().equalsIgnoreCase(fieldContent));
        }
    }

    public void updateValues(){
        if (isUpdated(mSchoolNameEditText, mApplicationFormSectionB.getSchoolName())) {
            mApplicationFormSectionB.setSchoolName(mSchoolNameEditText.getText().toString().trim());
            //mUserState.incrementProgress();
        }

        mApplicationFormSectionB.setMatricYear(mMatricYearSpinner.getSelectedItem().toString());

        if (isUpdated(mSchoolAddressLine1EditText, mApplicationFormSectionB.getSchoolAddress().getLine1())) {
            mApplicationFormSectionB.setSchoolAddressLine1(mSchoolAddressLine1EditText.getText().toString().trim());
            //mUserState.incrementProgress();
        }

        if (isUpdated(mSchoolAddressLine2EditText, mApplicationFormSectionB.getSchoolAddress().getLine2())) {
            mApplicationFormSectionB.setSchoolAddressLine2(mSchoolAddressLine2EditText.getText().toString().trim());
            //mUserState.incrementProgress();
        }

        if (isUpdated(mSchoolAddressCityEditText, mApplicationFormSectionB.getSchoolAddress().getCity())) {
            mApplicationFormSectionB.setSchoolAddressCity(mSchoolAddressCityEditText.getText().toString().trim());
            //mUserState.incrementProgress();
        }

        if (isUpdated(mSchoolAddressProvinceEditText, mApplicationFormSectionB.getSchoolAddress().getProvince())) {
            mApplicationFormSectionB.setSchoolAddressProvince(mSchoolAddressProvinceEditText.getText().toString().trim());
            //mUserState.incrementProgress();
        }

        if (isUpdated(mSchoolAddressZipEditText, mApplicationFormSectionB.getSchoolAddress().getZip())) {
            mApplicationFormSectionB.setSchoolAddressZip(mSchoolAddressZipEditText.getText().toString().trim());
            //mUserState.incrementProgress();
        }

        for (int d = 0; d < mApplicationFormSubjectsAdapter.getItemCount(); d++) {
            View view = mSubjectsRecyclerView.getChildAt(d);
            Log.d(TAG, "view found");
            try {
                if (null != view){
                    String subjectName = ((EditText) view.findViewById(R.id.item_subject_name_edittext)).getText().toString();
                    double subjectPercentage = Double.parseDouble(((EditText) view.findViewById(R.id.item_subject_percent_edittext)).getText().toString());
                } else
                    Log.d(TAG, "view is null");
            } catch (NumberFormatException e) {
                Toast.makeText(mContext, "Please enter a subject and a mark.", Toast.LENGTH_SHORT).show();
                break;
            }
        }

        mFirebaseUtil.setFirestoreDocument(mContext, APPLICATION_FORM_SECTION_B, mUserState.getId(), mApplicationFormSectionB, null);
    }

    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        assert getActivity() != null;
        mFirebaseUtil = new FirebaseFirestoreService();
        Log.d(TAG, "ApplicationFormBFragment with: " + mUserState.toString());
        setApplicationFormSectionBObject();
    }

    @Override
    public void onResume() {
        super.onResume();
        setApplicationFormSectionBObject();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Objects.requireNonNull(getActivity()).getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        return inflater.inflate(R.layout.fragment_application_form_section_b, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState){
        mSchoolNameEditText = view.findViewById(R.id.application_form_school_name_edittext);
        mMatricYearSpinner = view.findViewById(R.id.application_form_matric_year_spinner);
        mSchoolAddressLine1EditText = view.findViewById(R.id.item_input_address_line_1_edittext);
        mSchoolAddressLine2EditText = view.findViewById(R.id.item_input_address_line_2_edittext);
        mSchoolAddressCityEditText = view.findViewById(R.id.item_input_address_city_edittext);
        mSchoolAddressProvinceEditText = view.findViewById(R.id.item_input_address_province_edittext);
        mSchoolAddressZipEditText = view.findViewById(R.id.item_input_address_zip_code_edittext);
        mAddSubject = view.findViewById(R.id.application_form_profile_add_subjects_icon);
        mSubjectsRecyclerView = view.findViewById(R.id.application_form_subjects_recycler_view);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mContext);
        mSubjectsRecyclerView.setLayoutManager(mLayoutManager);

        mSaveButton = view.findViewById(R.id.user_view_overview_save_changes);

        mSaveButton.setOnClickListener(v -> {
            Log.d(TAG, "Saving Changes");
            updateValues();
        });

        mAddSubject.setOnClickListener(v -> {
            addNewSubject();
        });
    }

    private void showToast(String message){
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

}
