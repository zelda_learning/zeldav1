package com.utils;

public interface FragmentCommunicator {
    void updateProgressBar(int userElements);
}