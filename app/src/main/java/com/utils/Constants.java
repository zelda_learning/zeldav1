package com.utils;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class Constants {
    public static final int RC_SIGN_IN = 1;
    public static final int QUESTION_MAX_CHOICES = 5;
    public static final int NBT_MAX_CHOICES = 4;
    public static final String QUESTION_NUMBER_TAG = "questionNumberTag";
    public static final String APPLICATION_PROGRESS_TAG = "ApplicationProgressTag";
    public static final String APPLICATION_CHANGES_MADE_TAG = "ApplicationChangesMadeTag";

    public static final String USER_STATE_STARTED_APPLICATION_IDS = "startedApplicationIds";
    public static final String USER_STATE_COMPLETED_APPLICATION_IDS = "completedApplicationIds";
    public static final String APPLICATION_FORM_TAG = "ApplicationFormTag";
    public static final String APPLICATION_FORM_SECTION_A_TAG = "ApplicationFormATag";
    public static final String BURSARY_TAG = "BursaryTag";
    public static final String BURSARY_ID = "bursaryId";
    public static final String APPLICATION_FORM_SECTIONS_TAG = "ApplicationFormSectionCountTag";
    public static final String APPLICATION_FORM_SUBMITTED_TAG = "SubmittedApplicationFormTag";
    public static final String APPLICATION_FORM_CONTINUE_TAG = "ContinueApplicationFormTag";
    public static final String FIREBASE_USER_TAG = "firebaseUserTag";
    public static final String USER_STATE_TAG = "userStateTag";
    public static final String FIREBASE_UTIL_TAG = "firebaseUtilTag";
    public static final String BURSARY_ID_TAG = "bursaryIdTag";
    public static final String BURSARY_RECOMMENDED_TAG = "bursaryRecommendedTag";
    public static final String BURSARY_PINNED_TAG = "bursaryPinnedTag";
    public static final String TIP_ID_TAG = "tipIdTag";
    public static final String QUIZ_ID_TAG = "quizIdTag";
    public static final String IMAGE_ID_TAG = "imageIdTag";
    public static final String RECOMMENDED_HOME_TAG = "recommendedHomeTag";
    public static final String QUIZ_LIST_TAG = "quizListTag";
    public static final String QUIZ_ID_STRING_TAG = "quizIdStringTag";
    public static final String QUESTIONS_ID_TAG = "questionsIdTag";
    public static final String USER_CHOICE_TAG = "userChoiceTag";
    public static final String USER_CHOICE_RETRY_PAST_TAG = "userChoiceRetryPastTag";
    public static final String UNIVERSITY_ID_TAG = "universityIdTag";
    public static final String UNREAD_MESSAGE_TAG = "unreadMessageTag";
    public static final String USER_APPLICATION_FORM_A_TAG = "UserApplicationFormATag";
    public static final String USER_APPLICATION_FORM_B_TAG = "UserApplicationFormBTag";
    public static final String USER_APPLICATION_FORM_C_TAG = "UserApplicationFormCTag";
    public static final String USER_PROFILE_PERSONAL_TAG= "UserProfilePersonalTag";
    public static final String USER_PROFILE_SCHOOL_TAG= "UserProfileSchoolTag";
    public static final String USER_PROFILE_PERSONALITY_TAG = "UserProfilePersonalityTag";
    public static final String USER_PROFILE_DOCUMENTS_UPLOAD_TAG = "DocumentsUploadTag";
    public static final String TERMS_CONDITIONS_TAG = "tcTag";
    public static final String FROM_CBA_TAG = "fromCBATag";
    public static final String FROM_NEW_USER_TAG = "fromNewUserTag";
    public static final String FROM_HOME_TAG = "fromHomeTag";
    public static final String FROM_BURSARY_TAG = "fromBursaryTag";

    public static final String HOME_START_TRACE = "home_start_trace";
    public static final String QUIZ_START_TRACE = "quiz_start_trace";
    public static final String QUIZ_FEED_TRACE = "quiz_feed_trace";

    public static final String APPLICATION_FORM = "ApplicationForm";
    public static final String APPLICATION_FORM_SECTION_B = "ApplicationFormB";
    public static final String APPLICATION_FORM_SECTION_C = "ApplicationFormC";
    public static final String APPLICATION_FORM_SECTION_A = "ApplicationFormA";
    public static final String APPLICATIONS = "Applications";
    public static final String BURSARIES = "Bursaries";
    public static final String CALLS_TO_ACTION = "CallsToAction";
    public static final String COMPANIES = "Companies";
    public static final String MESSAGES = "Messages";
    public static final String NBT_QUIZZES = "NBTQuizzes";
    public static final String NBT_QUIZZES_PROBLEMS = "NBTQuizzesProblems";
    public static final String QUESTIONS = "questions";
    public static final String QUIZZES = "Quizzes";
    public static final String RESOURCES = "Resources";
    public static final String TIPS = "Tips";
    public static final String UNIVERSITIES = "Universities";
    public static final String USER_APPLICATION_FORM_A = "UserApplicationFormA";
    public static final String USER_APPLICATION_FORM_B = "UserApplicationFormB";
    public static final String USER_APPLICATION_FORM_C = "UserApplicationFormC";
    public static final String USER_PROFILE_SCHOOL =   "UserProfileSchool";
    public static final String USER_PROFILE_EDUCATION =   "UserProfileEducation";
    public static final String USER_PROFILE_PERSONALITY = "UserProfilePersonality";
    public static final String USER_REGISTRATION_TOKENS = "UserRegistrationTokens";
    public static final String USER_STATES = "UserStates";
    public static final String USER_PROFILE_PERSONAL = "UserProfilePersonal";
    public static final String USER_STATE_FIRESTORE = "UserState";
    public static final String USER_STATE_FIELD_PREFERENCE = "fieldPreference";

    public static final String USER_PROFILE_DOCUMENTS_IMG = "uPimG";
    public static final String USER_PROFILE_DOCUMENTS_PDF = "uPpdF";
    public static final String FIRESTORE_DOCUMENTS_ID_COPY = "iD";
    public static final String FIRESTORE_DOCUMENTS_MATRIC_CERTIFICATE = "mC";
    public static final String FIRESTORE_DOCUMENTS_SCHOOL_REPORT = "sR";
    public static final String FIRESTORE_DOCUMENTS_UNIVERSITY_TRANSCRIPT = "uT";


    public static final String FIELD_ML_COMMERCE = "Commerce";
    public static final String FIELD_ML_EBE = "EBE";
    public static final String FIELD_ML_HEALTH_SCIENCE = "Health Science";
    public static final String FIELD_ML_HUMANITIES = "Humanities";
    public static final String FIELD_ML_LAW = "Law";
    public static final String FIELD_ML_SCIENCE = "Science";

    public static final String TAG_OVERVIEW = "Overview";
    public static final String TAG_MANAGER = "Manager";
    public static final String TAG_A = "Section A";
    public static final String TAG_B = "Section B";
    public static final String TAG_C = "Section C";
    public static final String TAG_SUBMIT = "Submit";

    public static final String ANALYTICS_FIRST_SESSION = "first_session";
    public static final String ANALYTICS_EVENT_ABOUT_ZOMILA = "about_zomila";
    public static final String ANALYTICS_EVENT_ABOUT_CBA = "about_cba";
    public static final String ANALYTICS_EVENT_OPT_OUT = "opt_out";
    public static final String ANALYTICS_EVENT_OPT_OUT_DELETE = "opt_out_delete";
    public static final String ANALYTICS_EVENT_OPT_IN = "opt_in";
    public static final String ANALYTICS_EVENT_OPEN_CBA = "open_cba";
    public static final String ANALYTICS_EVENT_OPEN_CBA_REG = "open_cba_reg";
    public static final String ANALYTICS_EVENT_CLOSE_LOGIN = "close_login";
    public static final String ANALYTICS_EVENT_UPDATE_CBA = "update_cba";
    public static final String ANALYTICS_EVENT_SUBMIT_CBA = "submit_cba";
    public static final String ANALYTICS_EVENT_SKIP_CBA = "skip_cba";
    public static final String ANALYTICS_EVENT_RESUBMIT_CBA = "resubmit_cba";
    public static final String ANALYTICS_UUID = "uid";

    public static final String ANALYTICS_ABOUT_ZOMILA = "about_zomila";
    public static final String ANALYTICS_ABOUT_CBA = "about_cba";
    public static final String ANALYTICS_START_CBA_REG = "start_cba_reg";
    public static final String ANALYTICS_START_CBA = "start_cba";
    public static final String ANALYTICS_SKIP_CBA = "skip_cba";
    public static final String ANALYTICS_UPDATE_CBA = "update_cba";
    public static final String ANALYTICS_SUBMIT_CBA = "submit_cba";
    public static final String ANALYTICS_RESUBMIT_CBA = "resubmit_cba";
    public static final String ANALYTICS_CBA_PROGRESS = "cba_progress";
    public static final String ANALYTICS_OPT_IN_REG = "opt_in_reg";
    public static final String ANALYTICS_NOT_OPT_IN_REG = "not_opt_in_reg";
    public static final String ANALYTICS_OPT_IN_CBA = "opt_in_cba";
    public static final String ANALYTICS_OPT_OUT_CBA = "opt_out_cba";
    public static final String ANALYTICS_DELETE_CBA = "delete_cba";

    public static final String ANALYTICS_SUCCESSFUL_LOGIN = "successful_login";
    public static final String ANALYTICS_FAIL_LOGIN = "fail_login";

    public static final String ANALYTICS_YES = "yes";
    public static final String ANALYTICS_NO = "no";

    public static final String APPLICATION_STATUS_ACCEPTED = "Accepted";
    public static final String APPLICATION_STATUS_PENDING = "Pending";
    public static final String APPLICATION_STATUS_IN_REVIEW = "In Review";
    public static final String APPLICATION_STATUS_REJECTED = "Rejected";
    public static final String APPLICATION_STATUS_RETURNED = "Returned";
    public static final String APPLICATION_STATUS_SUBMITTED = "Submitted";

    public static final String MESSAGE_NEW = "newMessage";
    public static final String MESSAGE_NEW_RECEIVED = "newMessageReceived";
    public static final String MESSAGE_NEW_SENT = "newMessageSent";
    public static final String MESSAGE_SEND_TIME = "messageSendTime";

    public static final String SELECT_APPLICATION_METRIC = "selectApplicationMetric";
    public static final String APPLY_ON_CLICK_METRIC = "applyOnClickMetric";
    public static final String SELECT_UNIVERSITY_METRIC = "selectUniversityMetric";
    public static final String SELECT_TIP_METRIC = "selectTipMetric";
    public static final String SELECT_QUIZ_METRIC = "selectQuizMetric";
    public static final String USER_ID_METRIC = "userIdMetric";

    public static final String BURSARY_FIELD_STAR_ALL = "*All";
    public static final String BURSARY_FIELD_STAR_DONT_KNOW = "*NotSure";
    public static final String BURSARY_FIELD_ALL = "All";
    public static final String BURSARY_FIELD_DONT_KNOW = "I'm not sure";
    public static final String BURSARY_FIELD_PROMPT = "Filter by field:";
    public static final String BURSARY_FIELD_PINNED = "Pinned Bursaries";

    public static final String CLOSING_DATE = "closingDate";

    public static final String UNIVERSITY_TYPE_ALL = "All";
    public static final String UNIVERSITY_TYPE_PUBLIC = "Public";
    public static final String UNIVERSITY_TYPE_PRIVATE = "Private";
    public static final String UNIVERSITY_TYPE_T_VET = "T-VET";
    public static final String UNIVERSITY_TYPE_VOCATIONAL = "Vocational Training";
    public static final String UNIVERSITY_TYPE_PROMPT = "Filter by type:";

    public static final int QUIZ_TYPE_8 = 8;
    public static final int PERSONALITY_START_VALUE_DEFAULT = 5;
    public static final int PERSONALITY_CHANGE_VALUE = 1;

    public static final String USER_PROFILE_PERSONALITY_TEST_COMPLETED = "testCompleted";

    public static final String INTRO_QUIZ_ID = "INTRO_QUIZ";
    public static final String INTRO_QUIZ_LABEL = "label";
    public static final String INTRO_QUIZ_TEXT = "text";

    public static final String SUBJECT_NAME_HINT = "Subject name";
    public static final String DOB_HINT = "Click to enter";

    public static final String NBT_CHOICE_A = "a";
    public static final String NBT_CHOICE_B = "b";
    public static final String NBT_CHOICE_C = "c";
    public static final String NBT_CHOICE_D = "d";

    public static final String NBT_QUIZ_COMPLETED_TAG = "completedNBTQuizzesTag";
    public static final String NBT_QUIZ_COMPLETED = "completedNBTQuizzes";
    public static final String NBT_QUIZ_RETRY_TAG = "retryNBTQuizTag";

    public static final String NBT_QUIZ_PROBLEM = "problem";
    public static final String NBT_QUIZ_PROBLEM_QUIZ_ID = "quizId";

    public static final String NBT_QUIZ_QUESTION_UNCLEAR = "QuestionUnclear";
    public static final String NBT_QUIZ_QUESTION_TEXT_ERROR = "QuestionTextIncorrect";
    public static final String NBT_QUIZ_QUESTION_DIAGRAM_ERROR = "QuestionDiagramUnclear";
    public static final String NBT_QUIZ_PROBLEM_ANSWER = "AnswerPossiblyIncorrect";
    public static final String NBT_QUIZ_PROBLEM_WORKING = "WorkingPossiblyIncorrect";

    public static final String [] NBT_QUIZ_REPORT_PROBLEM_ARRAY = {"This question is unclear/confusing",
                                                                    "This questions diagram is unclear",
                                                                    "There is a problem with the text in this question",
                                                                    "This answer appears to be incorrect"};

    public static final String STRING_NDASH = "-";
    public static final String STRING_MDASH = "—";
    public static final String STRING_BOLD = "<b>";
    public static final String STRING_BOLD_CLOSE = "</b>";
    public static final String STRING_NEWLINE = "\n";
    public static final String STRING_ADDRESS_DELIMITER = "$";

    public static final String FIRESTORE_USER_NAME = "username";
    public static final String FIRESTORE_PROGRESS = "progress";
    public static final String FIRESTORE_ENROLL = "enrollment";
    public static final String FIRESTORE_SCHOOL_PROVINCE = "schoolProvince";
    public static final String FIRESTORE_MATRIC_YEAR = "yearOfMatric";
    public static final String FIRESTORE_NDASH = "&ndash;";
    static final String FIRESTORE_MDASH = "&mdash;";
    static final String FIRESTORE_STRONG = "<strong>";
    public static final String FIRESTORE_BOLD = "<b>";
    public static final String FIRESTORE_BR = "<br>";
    static final String FIRESTORE_BR_CLOSE = "<br />";
    public static final String FIRESTORE_BOLD_CLOSE = "</b>";
    static final String FIRESTORE_STRONG_CLOSE = "</strong>";
    public static final String FIRESTORE_NEWLINE = "\\n";
    public static final String USER_FIELD_PREFERENCE_FIRESTORE = "fieldPreference";
    public static final String USER_USERNAME = "username";
    public static final String USER_PROGRESS = "progress";
    public static final String USER_QUOTE = "quote";
    public static final String USER_CALLS_TO_ACTION = "callsToAction";
    public static final String USER_OPT_IN = "optIn";
    public static final String USER_OPT_OUT = "optOut";
    public static final String USER_OPT_OUT_DELETE = "delete";
    public static final String USER_SUBMITTED = "submitted";

    public static final String TRUE = "TRUE";
    public static final String FALSE = "FALSE";
    public static final String BLANK_ADDRESS_DELIMITER = "$$$$$";

    public static final String FILE_TYPE_JPG = "jpg";
    public static final String FILE_TYPE_JPEG = "jpeg";
    public static final String FILE_TYPE_PNG = "png";
    public static final String FILE_TYPE_PDF = "pdf";
    public static final String FILE_TYPE_MSWORD = "msword";

    public static final String UPS_SCHOOL_PROVINCE = "schoolProvince";
    public static final String UPS_YEAR_MATRIC = "yearOfMatric";

    public static final String USER_REGISTRATION_TOKEN = "token";

    public static final String USER_PROFILE_HEADING_OVERVIEW = "Overview";
    public static final String USER_PROFILE_HEADING_PERSONAL = "Personal Info";
    public static final String USER_PROFILE_HEADING_SCHOOL = "School Info";
    public static final String USER_PROFILE_HEADING_UNIVERSITY = "University Info";
    public static final String USER_PROFILE_ESSENTIAL_STAR = "*";

    public static final String CTA_TYPE_BURSARY = "BURSARY";
    public static final String CTA_TYPE_SCHOOL = "SCHOOL_PROMO_CODE";

    public static final String ELEMENT_AREA = "area";
    public static final String ELEMENT_INCOME = "income";
    public static final String ELEMENT_GENDER = "gender";
    public static final String ELEMENT_EMAIL = "email";
    public static final String ELEMENT_ADDRESS = "address";
    public static final String ELEMENT_NATIONALITY = "nationality";
    public static final String ELEMENT_RACE = "race";
    public static final String ELEMENT_HOME_ORIGIN = "homeOrigin";

    public static final String ELEMENT_UPLOAD = "upload";

    public static final String ELEMENT_NO_ADDRESS = "$$$$$";
    public static final String ELEMENT_N_A = "N/A";
    public static final String ELEMENT_NONE = "NONE";

    public static final String ELEMENT_CURRENT_GRADE = "currentGrade";
    public static final String ELEMENT_MATRIC_CERTIFICATE = "matricCertificate";
    public static final String ELEMENT_MATRIC_CERTIFICATE_UPLOADED = "matricCertificateUploaded";
    public static final String ELEMENT_MATRIC_COMPLETED_YEAR = "matricCompleted";

    public static final String ELEMENT_MATRIC = "matric";
    public static final String ELEMENT_SCHOOL = "school";
    public static final String ELEMENT_UNIVERSITY = "university";

    public static final String ELEMENT_SCHOOL_NAME = "schoolName";
    public static final String ELEMENT_SCHOOL_STARTED = "schoolYearStarted";
    public static final String ELEMENT_SCHOOL_ENDED = "schoolYearEnded";
    public static final String ELEMENT_SCHOOL_ADDRESS = "schoolAddress";
    public static final String ELEMENT_SCHOOL_LINE1 = "line1";
    public static final String ELEMENT_SCHOOL_LINE2 = "line2";
    public static final String ELEMENT_SCHOOL_CITY = "city";
    public static final String ELEMENT_SCHOOL_PROVINCE = "province";
    public static final String ELEMENT_SCHOOL_ZIP = "zip";

    public static final String FIRESTORE_TOGGLE_HSC = "toggleHSC";
    public static final String FIRESTORE_TOGGLE_HSM = "toggleHSM";
    public static final String FIRESTORE_TOGGLE_PI = "togglePI";
    public static final String FIRESTORE_TOGGLE_OI = "toggleOI";
    public static final String FIRESTORE_TOGGLE_CS = "toggleCS";
    public static final String FIRESTORE_TOGGLE_PS = "togglePS";

    public static final String FIRESTORE_ALT_CONTACT_NUMBER = "altContactNumber";
    public static final String FIRESTORE_ALT_CONTACT_RELATIONSHIP = "altContactRelationship";
    public static final String FIRESTORE_AWARDS = "awards";
    public static final String FIRESTORE_CAREER = "careerIntentions";
    public static final String FIRESTORE_COMBINED_YEARLY_INCOME = "combinedYearlyIncome";
    public static final String FIRESTORE_CONTACT_NUMBER = "contactNumber";
    public static final String FIRESTORE_CULTURAL = "culturalActivities";
    public static final String FIRESTORE_CRIMES = "crimes";
    public static final String FIRESTORE_DATE_BIRTH = "dateOfBirth";
    public static final String FIRESTORE_DISABILITY = "disability";
    public static final String FIRESTORE_EMAIL_TAG = "email";
    public static final String FIRESTORE_FIRST_NAME = "firstName";
    public static final String FIRESTORE_GENDER = "gender";
    public static final String FIRESTORE_GUARDIAN_OCCUPATION = "guardianOccupation";
    public static final String FIRESTORE_HOBBIES = "hobbies";
    public static final String FIRESTORE_HOME_LANGUAGE = "homeLanguage";
    public static final String FIRESTORE_HOME_ORIGIN = "homeOrigin";
    public static final String FIRESTORE_ID_NUMBER = "idNumber";
    public static final String FIRESTORE_ID_COPY_UPLOADED = "idUploaded";
    public static final String FIRESTORE_LAST_NAME = "lastName";
    public static final String FIRESTORE_MARITAL_STATUS = "maritalStatus";
    public static final String FIRESTORE_NATIONALITY = "nationality";
    public static final String FIRESTORE_PHYSICAL_ADDRESS = "address";
    public static final String FIRESTORE_PLACE_BIRTH = "placeOfBirth";
    public static final String FIRESTORE_OTHER_LANGUAGE = "otherLanguage";
    public static final String FIRESTORE_RACE = "race";
    public static final String FIRESTORE_SA_CITIZEN = "saCitizen";

    public static final String FIRESTORE_CURRENT_GRADE = "currentGrade";
    public static final String FIRESTORE_SUBJECTS_MARKS_MOST_RECENT = "subjectMarksMostRecent";
    public static final String FIRESTORE_MATRIC_CERTIFICATE_SUBJECTS = "matricCertificateSubjects";
    public static final String FIRESTORE_SCHOOL_SUBJECTS = "schoolSubjects";
    public static final String FIRESTORE_UNIVERSITY_SUBJECTS = "universitySubjects";

    public static final String FIRESTORE_MATRIC_CERTIFICATE = "matricCertificate";
    public static final String FIRESTORE_MATRIC_COMPLETED_YEAR = "matricCompletedYear";
    public static final String FIRESTORE_MC_UPLOADED = "matricCertificateUploaded";
    public static final String FIRESTORE_REPEATING_MATRIC_EXAM = "repeatingMatricExam";
    public static final String FIRESTORE_REPEATING_SCHOOL_NAME = "repeatingSchoolName";
    public static final String FIRESTORE_REPEATING_EXAM_DATE = "repeatingExamDate";
    public static final String FIRESTORE_REPEATING_SUBJECT_MAP = "repeatingSubjectMap";
    public static final String FIRESTORE_SCHOOL_NAME = "schoolName";
    public static final String FIRESTORE_SCHOOL_YEAR_STARTED = "schoolYearStarted";
    public static final String FIRESTORE_SCHOOL_YEAR_ENDED = "schoolYearEnded";
    public static final String FIRESTORE_SCHOOL_ADDRESS = "schoolAddress";
    public static final String FIRESTORE_SCHOOL_REPORT_UPLOADED = "schoolReportUploaded";

    public static final String FIRESTORE_CURRENT_UNIVERSITY_NAME = "universityName";
    public static final String FIRESTORE_CURRENT_UNIVERSITY_DEGREE_NAME = "degreeName";
    public static final String FIRESTORE_PROSPECIVE_UNIVERSITY_NAME = "prospectiveUniversityName";
    public static final String FIRESTORE_PROSPECIVE_DEGREE_NAME = "prospectiveDegreeName";

    public static final String FIRESTORE_UNIVERSITY_YEAR = "universityYear";
    public static final String FIRESTORE_CURRENT_DEGREE_STARTED_YEAR = "degreeStartedYear";
    public static final String FIRESTORE_DEGREE_EXPECTED_DURATION = "degreeDuration";
    public static final String FIRESTORE_PROSPECTIVE_DEGREE_DURATION = "prospectiveDegreeDuration";
    public static final String FIRESTORE_PROSPECTIVE_DEGREE_APPLICATION_YEAR = "degreeApplicationYear";

    public static final String FIRESTORE_UNIVERSITY_TRANSCRIPT_UPLOADED = "universityTranscriptUploaded";

    public static final String FIRESTORE_POSTMATRIC_STUDIES = "postMatricStudies";

    public static final String UPS_HS_INFO_TAG = "High School Info";
    public static final String UPP_PI_INFO_TAG = "Personal Identifying Info";
    public static final String UPP_OI_INFO_TAG = "Other Info";
    public static final String UPU_CS_INFO_TAG = "Current University Student";
    public static final String UPU_PS_INFO_TAG = "Prospective University Student";

    public static final String UPP_ALT_CONTACT_NUMBER_TAG = "Alternative contact number";
    public static final String UPP_ALT_CONTACT_RELATIONSHIP_TAG = "Relationship with alternative contact";
    public static final String UPP_AWARDS_TAG = "Awards";
    public static final String UPP_CAREER_INTENTIONS_TAG = "Career Intentions";
    public static final String UPP_COMBINED_YEARLY_INCOME_TAG = "Combined yearly household income for your family (Rands)";
    public static final String UPP_CONTACT_NUMBER_TAG = "Contact number";
    public static final String UPP_CULTURAL_ACTIVITIES_TAG = "Cultural Activities";
    public static final String UPP_CRIMES_TAG = "Have you ever been convicted of a crime? If yes, please explain.";
    public static final String UPP_DATE_BIRTH_TAG = "Date of birth (YYYY-MM-DD)";
    public static final String UPP_DISABILITY_TAG = "Physical disabilities/Chronic illness. Explain";
    public static final String UPP_EMAIL_TAG = "Email";
    public static final String UPP_FIRST_NAME_TAG = "First name";
    public static final String UPP_GENDER_TAG = "Gender";
    public static final String UPP_GUARDIAN_OCCUPATION_TAG = "Parent/Guardian's occupation";
    public static final String UPP_HOBBIES_TAG = "Hobbies";
    public static final String UPP_HOME_LANGUAGE_TAG = "Home language";
    public static final String UPP_HOME_ORIGIN_TAG = "What kind of area do you live in?";
    public static final String UPP_HOME_TOWN_TAG = "Home town";
    public static final String UPP_ID_NUMBER_TAG = "ID Number";
    public static final String UPP_ID_COPY = "Photo/Scan of ID book/card";
    public static final String UPP_LAST_NAME_TAG = "Last name";
    public static final String UPP_MARITAL_STATUS_TAG = "Marital Status";
    public static final String UPP_NATIONALITY_TAG = "Nationality";
    public static final String UPP_PHYSICAL_ADDRESS_TAG = "Physical Address";
    public static final String UPP_PLACE_BIRTH_TAG = "Place of Birth (City)";
    public static final String UPP_OTHER_LANGUAGE_TAG = "Do you speak another language? Which?";
    public static final String UPP_RACE_TAG = "Race";
    public static final String UPP_SA_CITIZEN_TAG = "South African Citizenship";

    public static final String UPU_UNIVERSITY = "Current/Prospective university details";

    public static final String UPS_REPEATING = "Repeating details";
    public static final String UPS_REPEATING_MATRIC_EXAM = "Are you repeating any matric exams?";
    public static final String UPS_REPEATING_SCHOOL_NAME = "Name of school repeating exams at";
    public static final String UPS_REPEATING_EXAM_DATE = "Date of exams to be repeated";
    public static final String UPS_REPEATING_SUBJECT_MAP = "Which subject exams will you be repeating, and what marks did you get the first time";


    public static final String [] firestore_UPP_List = {
            FIRESTORE_TOGGLE_PI,
            FIRESTORE_FIRST_NAME,
            FIRESTORE_LAST_NAME,
            FIRESTORE_EMAIL_TAG,
            FIRESTORE_PHYSICAL_ADDRESS,
            FIRESTORE_DATE_BIRTH,
            FIRESTORE_ID_NUMBER,
            FIRESTORE_ID_COPY_UPLOADED,
            FIRESTORE_CONTACT_NUMBER,
            FIRESTORE_ALT_CONTACT_NUMBER,
            FIRESTORE_ALT_CONTACT_RELATIONSHIP,
            FIRESTORE_TOGGLE_OI,
            FIRESTORE_HOME_LANGUAGE,
            FIRESTORE_OTHER_LANGUAGE,
            FIRESTORE_GENDER,
            FIRESTORE_RACE,
            FIRESTORE_HOME_ORIGIN,
            FIRESTORE_NATIONALITY,
            FIRESTORE_SA_CITIZEN,
            FIRESTORE_PLACE_BIRTH,
            FIRESTORE_MARITAL_STATUS,
            FIRESTORE_GUARDIAN_OCCUPATION,
            FIRESTORE_COMBINED_YEARLY_INCOME,
            FIRESTORE_HOBBIES,
            FIRESTORE_CULTURAL,
            FIRESTORE_CAREER,
            FIRESTORE_AWARDS,
            FIRESTORE_DISABILITY,
            FIRESTORE_CRIMES,
    };

    public static final String [] uPP_List = {
            UPP_PI_INFO_TAG,
            UPP_FIRST_NAME_TAG,
            UPP_LAST_NAME_TAG,
            UPP_EMAIL_TAG,
            UPP_PHYSICAL_ADDRESS_TAG,
            UPP_DATE_BIRTH_TAG,
            UPP_ID_NUMBER_TAG,
            UPP_ID_COPY,
            UPP_CONTACT_NUMBER_TAG,
            UPP_ALT_CONTACT_NUMBER_TAG,
            UPP_ALT_CONTACT_RELATIONSHIP_TAG,
            UPP_OI_INFO_TAG,
            UPP_HOME_LANGUAGE_TAG,
            UPP_OTHER_LANGUAGE_TAG,
            UPP_GENDER_TAG,
            UPP_RACE_TAG,
            UPP_HOME_ORIGIN_TAG,
            UPP_NATIONALITY_TAG,
            UPP_SA_CITIZEN_TAG,
            UPP_PLACE_BIRTH_TAG,
            UPP_MARITAL_STATUS_TAG,
            UPP_GUARDIAN_OCCUPATION_TAG,
            UPP_COMBINED_YEARLY_INCOME_TAG,
            UPP_HOBBIES_TAG,
            UPP_CULTURAL_ACTIVITIES_TAG,
            UPP_CAREER_INTENTIONS_TAG,
            UPP_AWARDS_TAG,
            UPP_DISABILITY_TAG,
            UPP_CRIMES_TAG
    };

    public static final String UPS_CURRENT_GRADE_11 = "11";
    public static final String UPS_CURRENT_GRADE_12 = "12";
    public static final String UPS_CURRENT_GRADE_MATRICULATED = "Matriculated";
    public static final String UPU_PROSPECTIVE = "Not currently at university";

    public static final String UPS_CURRENT_GRADE = "High school grade in 2019";
    public static final String UPS_MATRIC_CERTIFICATE = "Type of certificate";
    public static final String UPS_MATRIC_COMPLETED_YEAR = "Year of matriculation";
    public static final String UPS_UPLOADED = "Photo/Scan of latest school report";
    public static final String UPS_MATRIC_CERTIFICATE_UPLOADED = "Photo/Scan of matric certificate";
    public static final String UPS_SCHOOL_REPORT_UPLOADED = "Photo/Scan of latest school report";

    public static final String UPU_POSTMATRIC_STUDIES = "Any studies you completed after Matric, before attending university";

    public static final String UPS_SUBJECT_MAP = "Most recent subjects and marks";
    public static final String UPS_11_SUBJECT_MAP = "Latest Grade 11 subjects and marks";
    public static final String UPS_12_SUBJECT_MAP = "Latest Grade 12 subjects and marks";
    public static final String UPS_MATRIC_SUBJECT_MAP = "Final matric subjects and marks";
    public static final String UPS_UNIVERSITY_SUBJECT_MAP = "University courses and marks";

    public static final String UPS_SCHOOL_NAME = "High school name";
    public static final String UPS_LAST_SCHOOL_NAME = "Last high school name";
    public static final String UPS_SCHOOL_YEAR_STARTED = "Year started at high school";
    public static final String UPS_LAST_SCHOOL_YEAR_STARTED = "Year started at last high school";
    public static final String UPS_SCHOOL_YEAR_ENDED = "Year will end at high school";
    public static final String UPS_LAST_SCHOOL_YEAR_ENDED = "Year ended at high school";
    public static final String UPS_SCHOOL_ADDRESS = "School address";
    public static final String UPS_LAST_SCHOOL_ADDRESS = "Last school address";

    public static final String UPS_MATRIC = "Matric details";
    public static final String UPS_SCHOOL = "School details";

    public static final String [] firestore_UPS_matriculated_List = {
            FIRESTORE_TOGGLE_HSM,
            FIRESTORE_CURRENT_GRADE,
            FIRESTORE_MATRIC_CERTIFICATE,
            FIRESTORE_MATRIC_COMPLETED_YEAR,
            FIRESTORE_SCHOOL_NAME,
            FIRESTORE_SCHOOL_YEAR_STARTED,
            FIRESTORE_SCHOOL_YEAR_ENDED,
            FIRESTORE_SCHOOL_ADDRESS,
            FIRESTORE_MATRIC_CERTIFICATE_SUBJECTS,
            FIRESTORE_MC_UPLOADED,
            FIRESTORE_POSTMATRIC_STUDIES
    };

    public static final String [] uPS_matriculated_List = {
            UPS_HS_INFO_TAG,
            UPS_CURRENT_GRADE,
            UPS_MATRIC_CERTIFICATE,
            UPS_MATRIC_COMPLETED_YEAR,
            UPS_LAST_SCHOOL_NAME,
            UPS_LAST_SCHOOL_YEAR_STARTED,
            UPS_LAST_SCHOOL_YEAR_ENDED,
            UPS_LAST_SCHOOL_ADDRESS,
            UPS_MATRIC_SUBJECT_MAP,
            UPS_MATRIC_CERTIFICATE_UPLOADED,
            UPU_POSTMATRIC_STUDIES
    };


    public static final String [] firestore_UPS_current_List = {
            FIRESTORE_TOGGLE_HSC,
            FIRESTORE_CURRENT_GRADE,
            FIRESTORE_SCHOOL_NAME,
            FIRESTORE_SCHOOL_YEAR_STARTED,
            FIRESTORE_SCHOOL_YEAR_ENDED,
            FIRESTORE_SCHOOL_ADDRESS,
            FIRESTORE_SCHOOL_SUBJECTS,
            FIRESTORE_SCHOOL_REPORT_UPLOADED
    };

    public static final String [] uPS_current_List = {
            UPS_HS_INFO_TAG,
            UPS_CURRENT_GRADE,
            UPS_SCHOOL_NAME,
            UPS_SCHOOL_YEAR_STARTED,
            UPS_SCHOOL_YEAR_ENDED,
            UPS_SCHOOL_ADDRESS,
            UPS_SUBJECT_MAP,
            UPS_SCHOOL_REPORT_UPLOADED
    };

    public static final String UPU_UNIVERSITY_YEAR = "University year in 2019";

    public static final String UPU_CURRENT_UNIVERSITY_NAME = "Current university name";
    public static final String UPU_CURRENT_DEGREE_NAME = "Name of current degree";
    public static final String UPU_CURRENT_DEGREE_STARTED_YEAR = "Year started studying degree";
    public static final String UPU_CURRENT_COURSES = "Current courses and marks";
    public static final String UPU_CURRENT_TRANSCRIPT_UPLOADED = "Photo/Scan of university transcript ";

    public static final String UPU_PROSPECTIVE_UNIVERSITY_NAME = "First choice university name";
    public static final String UPU_PROSPECTIVE_DEGREE_NAME = "First choice of degree";
    public static final String UPU_PROSPECTIVE_DEGREE_APPLICATION_YEAR = "Year of expected university acceptance";

    public static final String UPU_DEGREE_DURATION = "Expected degree completion time";

    public static final String [] firestore_CBA_U_current_List = {
            FIRESTORE_TOGGLE_CS,
            FIRESTORE_UNIVERSITY_YEAR,
            FIRESTORE_CURRENT_UNIVERSITY_NAME,
            FIRESTORE_CURRENT_UNIVERSITY_DEGREE_NAME,
            FIRESTORE_CURRENT_DEGREE_STARTED_YEAR,
            FIRESTORE_DEGREE_EXPECTED_DURATION,
            FIRESTORE_UNIVERSITY_SUBJECTS,
            FIRESTORE_UNIVERSITY_TRANSCRIPT_UPLOADED
    };

    public static final String [] uPU_CURRENT_List = {
            UPU_CS_INFO_TAG,
            UPU_UNIVERSITY_YEAR,
            UPU_CURRENT_UNIVERSITY_NAME,
            UPU_CURRENT_DEGREE_NAME,
            UPU_CURRENT_DEGREE_STARTED_YEAR,
            UPU_DEGREE_DURATION,
            UPU_CURRENT_COURSES,
            UPU_CURRENT_TRANSCRIPT_UPLOADED

    };

    public static final String [] firestore_CBA_U_prosp_List = {
            FIRESTORE_TOGGLE_PS,
            FIRESTORE_UNIVERSITY_YEAR,
            FIRESTORE_CURRENT_UNIVERSITY_NAME,
            FIRESTORE_CURRENT_UNIVERSITY_DEGREE_NAME,
            FIRESTORE_PROSPECTIVE_DEGREE_APPLICATION_YEAR,
            FIRESTORE_DEGREE_EXPECTED_DURATION,
    };

    public static final String [] uPU_PROSP_List = {
            UPU_PS_INFO_TAG,
            UPU_UNIVERSITY_YEAR,
            UPU_PROSPECTIVE_UNIVERSITY_NAME,
            UPU_PROSPECTIVE_DEGREE_NAME,
            UPU_PROSPECTIVE_DEGREE_APPLICATION_YEAR,
            UPU_DEGREE_DURATION,
    };

    public static Map<String, String> userProfileUniversityProspectiveFieldTags;
    static {
        Map<String, String> aMap = new HashMap<>();
        for (int c = 0; c < firestore_CBA_U_prosp_List.length; c++){
            aMap.put(firestore_CBA_U_prosp_List[c], uPU_PROSP_List[c]);
        }
        userProfileUniversityProspectiveFieldTags = Collections.unmodifiableMap(aMap);
    }


    public static Map<String, String> userProfilePersonalFieldTags;
        static {
            Map<String, String> aMap = new HashMap<>();
            for (int c = 0; c < firestore_UPP_List.length; c++){
                aMap.put(firestore_UPP_List[c], uPP_List[c]);
            }
            userProfilePersonalFieldTags = Collections.unmodifiableMap(aMap);
        }

    public static Map<String, String> upsMatriculatedFieldTags;
    static {
        Map<String, String> aMap = new HashMap<>();
        for (int c = 0; c < firestore_UPS_matriculated_List.length; c++){
            aMap.put(firestore_UPS_matriculated_List[c], uPS_matriculated_List[c]);
        }
        upsMatriculatedFieldTags = Collections.unmodifiableMap(aMap);
    }

    public static Map<String, String> upsCurrentFieldTags;
    static {
        Map<String, String> aMap = new HashMap<>();
        for (int c = 0; c < firestore_UPS_current_List.length; c++){
            aMap.put(firestore_UPS_current_List[c], uPS_current_List[c]);
        }
        upsCurrentFieldTags = Collections.unmodifiableMap(aMap);
    }

    public static Map<String, String> userProfileUniversityCurrentFieldTags;
    static {
        Map<String, String> aMap = new HashMap<>();
        for (int c = 0; c < firestore_CBA_U_current_List.length; c++){
            aMap.put(firestore_CBA_U_current_List[c], uPU_CURRENT_List[c]);
        }
        userProfileUniversityCurrentFieldTags = Collections.unmodifiableMap(aMap);
    }

    public static final int INITIAL_PROGRESS = 0;
    public static final int INITIAL_PROGRESS_OPT_IN = 4;
    public static final int INITIAL_NUMBER_NBT_QUIZZES = 11;
    public static final int FINAL_PROGRESS = 100;

    /**Excluded UPP fields are:
     *      2 toggle headings (Sensitive/Other)
     *      email field       (Copied via CF)
    */
    public static final int UPP_EXCLUDE_SIZE = 3;
    public static final int UPP_EMAIL_PROGRESS = 2;
    public static final int UPP_SIZE = firestore_UPP_List.length - UPP_EXCLUDE_SIZE;
    public static final int UPS_SIZE = 17;

    public static final int UPS_CURRENT_SIZE = firestore_UPS_current_List.length - 1;
    public static final int UPS_MATRICULATED_SIZE = firestore_UPS_matriculated_List.length - 1;

    public static final int UPU_CURRENT_SIZE = firestore_CBA_U_current_List.length - 1;
    public static final int UPU_PROSPECTIVE_SIZE = firestore_CBA_U_prosp_List.length - 1;

    public static final String CBA_ID_APPLICATION_FORM = "CBA";
    public static final String DISPLAY_TEXT_COLLECTION = "DisplayText";
    public static final String DISPLAY_TEXT_FAQ = "FAQ";
    public static final String DISPLAY_TEXT_TCS = "T&C";
    public static final String DISPLAY_TEXT_ABOUT = "ABOUT";
    public static final String DISPLAY_TEXT_TEXT = "text";
}