package com.utils;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import com.google.firebase.analytics.FirebaseAnalytics;

import static com.utils.Constants.ANALYTICS_ABOUT_CBA;
import static com.utils.Constants.ANALYTICS_ABOUT_ZOMILA;
import static com.utils.Constants.ANALYTICS_EVENT_ABOUT_CBA;
import static com.utils.Constants.ANALYTICS_EVENT_ABOUT_ZOMILA;
import static com.utils.Constants.ANALYTICS_EVENT_OPEN_CBA;
import static com.utils.Constants.ANALYTICS_EVENT_OPEN_CBA_REG;
import static com.utils.Constants.ANALYTICS_EVENT_OPT_IN;
import static com.utils.Constants.ANALYTICS_EVENT_SKIP_CBA;
import static com.utils.Constants.ANALYTICS_NO;
import static com.utils.Constants.ANALYTICS_NOT_OPT_IN_REG;
import static com.utils.Constants.ANALYTICS_OPT_IN_CBA;
import static com.utils.Constants.ANALYTICS_OPT_IN_REG;
import static com.utils.Constants.ANALYTICS_SKIP_CBA;
import static com.utils.Constants.ANALYTICS_START_CBA;
import static com.utils.Constants.ANALYTICS_START_CBA_REG;
import static com.utils.Constants.ANALYTICS_YES;

public class FirebaseAnalyticsHelper{
    private static final String TAG = FirebaseAnalyticsHelper.class.getSimpleName();

    private FirebaseAnalytics mFirebaseAnalytics;
    private Bundle analyticsBundle;

    public FirebaseAnalyticsHelper(Context thisContext){
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(thisContext);
        analyticsBundle = new Bundle();
    }

    public void addFirebaseEventMetric(String itemId, String itemName, String contentType){
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, itemId);
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, itemName);
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, contentType);
        bundle.putDouble(FirebaseAnalytics.Param.VALUE, 1);
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
    }

    public void addStringToBundle(String tag, String value){
        analyticsBundle.putString(tag, value);
        Log.d(TAG, "Bundle updated: " + value);
    }

    public void addIntToBundle(String tag, int value){
        analyticsBundle.putInt(tag, value);
        Log.d(TAG, "Bundle updated: " + value);
    }

    public void logFirebaseBundleMetrics(String event){
        Log.d(TAG, "logFirebaseBundleMetrics: " + event);
        mFirebaseAnalytics.logEvent(event, analyticsBundle);
    }

    public void addUserProperty(String tag, String value){
        Log.d(TAG, "addUserProperty: " + tag + " : " + value);
        mFirebaseAnalytics.setUserProperty(tag, value);
    }

    public void logAboutCBAMetric() {
        addIntToBundle(ANALYTICS_ABOUT_CBA, 1);
        logFirebaseBundleMetrics(ANALYTICS_EVENT_ABOUT_CBA);
    }

    public void logAboutZomilaMetric() {
        addIntToBundle(ANALYTICS_ABOUT_ZOMILA, 1);
        logFirebaseBundleMetrics(ANALYTICS_EVENT_ABOUT_ZOMILA);
    }

    public void logOpenCBAMetric() {
        addIntToBundle(ANALYTICS_START_CBA, 1);
        logFirebaseBundleMetrics(ANALYTICS_EVENT_OPEN_CBA);
    }

    public void logOpenCBARegMetric() {
        addIntToBundle(ANALYTICS_START_CBA_REG, 1);
        logFirebaseBundleMetrics(ANALYTICS_EVENT_OPEN_CBA_REG);
    }

    public void logSkipCBAMetric() {
        addIntToBundle(ANALYTICS_SKIP_CBA, 1);
        logFirebaseBundleMetrics(ANALYTICS_EVENT_SKIP_CBA);
    }

    public void logOptInMetric(){
        addIntToBundle(ANALYTICS_OPT_IN_CBA, 1);
        addUserProperty(ANALYTICS_OPT_IN_CBA, ANALYTICS_YES);
        logFirebaseBundleMetrics(ANALYTICS_EVENT_OPT_IN);
    }

    public void logOptInRegistrationMetric(){
        addIntToBundle(ANALYTICS_OPT_IN_CBA, 1);
        addIntToBundle(ANALYTICS_OPT_IN_REG, 1);
        addUserProperty(ANALYTICS_OPT_IN_CBA, ANALYTICS_YES);
        addUserProperty(ANALYTICS_OPT_IN_REG, ANALYTICS_YES);
        logFirebaseBundleMetrics(ANALYTICS_EVENT_OPT_IN);
    }

    public void logNotOptInMetric(){
        addIntToBundle(ANALYTICS_NOT_OPT_IN_REG, 1);
        addUserProperty(ANALYTICS_OPT_IN_CBA, ANALYTICS_NO);
        addUserProperty(ANALYTICS_OPT_IN_REG, ANALYTICS_NO);
        logFirebaseBundleMetrics(ANALYTICS_EVENT_OPT_IN);
    }
}
