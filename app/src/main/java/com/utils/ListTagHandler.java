package com.utils;

import android.text.Editable;
import android.text.Html.TagHandler;

import org.xml.sax.XMLReader;

import java.util.List;

public class ListTagHandler implements TagHandler{
    private static boolean first = true, done = false;
    private String parent = null;
    private int index = 1;

    @Override
    public void handleTag(boolean opening, String tag, Editable output,
                          XMLReader xmlReader) {
        if(tag.equals("<UNORDERED>") && !opening && !done){
            output.append("\n");
            done = true;
        }
        if(tag.equals("<LISTITEM>") && opening && !done) {
            output.append("\n\t•");
            done = true;
        }
    }

    public static String listFormatter(List<String> strings){
        StringBuilder bulletListString = new StringBuilder();
        first = true;

        for(int pos = 0; pos < strings.size(); pos++){
            String item = strings.get(pos);
            String span = formatListItem(item, pos, strings.size());

            if(first){
                bulletListString.append(span);
                first = false;
            }
            else if (pos == strings.size() - 1){
                bulletListString.append("•  ").append(span);
            }
            else {
                bulletListString.append("•  ").append(span).append("\n");
            }
        }
        return bulletListString.toString();
    }

    private static String formatListItem(String string, int pos, int lastPos){
        String strippedText = string.replace("<ol>", "");
        strippedText = strippedText.replace("</ol>", "");
        strippedText = strippedText.replace("<ul>", "");
        strippedText = strippedText.replace("</ul>", "");
        strippedText = strippedText.replace("<div>", "");
        strippedText = strippedText.replace("</div>", "");
        strippedText = strippedText.replace("<p>", "");
        strippedText = strippedText.replace("&amp;", "&");

        if (pos != lastPos) {
            strippedText = strippedText.replace("&nbsp;", "\n");
            strippedText = strippedText.replace("<br>", "\n");
            strippedText = strippedText.replace("</li>", "\n");
            strippedText = strippedText.replace("</p>", "\n");
        }

        if (first){
            first = false;
            return strippedText.replace("<li>", "");
        } else{
            return strippedText.replace("<li>", "•  ");
        }
    }
}