package com.utils.dialogFragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;

import com.utils.Constants;
import com.zeldav1.R;

public class CBASubmitDialogFragment  extends DialogFragment {

    public interface CBASubmitListener{
        public void onSubmitPositiveClick(DialogFragment dialog);
        public void onResubmitPositiveClick(DialogFragment dialog);
        public void onSubmitNegativeClick(DialogFragment dialog);
    }

    // Use this instance of the interface to deliver action events
    CBASubmitDialogFragment.CBASubmitListener mListener;

    public static CBASubmitDialogFragment newInstance(boolean submitted) {
        CBASubmitDialogFragment f = new CBASubmitDialogFragment();

        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putBoolean(Constants.USER_SUBMITTED, submitted);
        f.setArguments(args);

        return f;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            mListener = (CBASubmitDialogFragment.CBASubmitListener) context;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(context.toString()
                    + " must implement CBASubmitListener");
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Build the dialog and set up the button click handlers
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.AlertDialogTheme);
        String message = "", positiveText = "", negativeText = "";
        boolean submitted = false;

        // Get the layout inflater
        LayoutInflater inflater = requireActivity().getLayoutInflater();
        if (getArguments() != null) {
            submitted = getArguments().getBoolean(Constants.USER_SUBMITTED, false);
        }

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        if (!submitted)
            builder.setView(inflater.inflate(R.layout.item_userview_submit_card, null))
                .setPositiveButton(R.string.submit_all_caps, (dialog, id) ->
                        mListener.onSubmitPositiveClick(CBASubmitDialogFragment.this)
                )
                .setNegativeButton(R.string.cancel_all_caps, (dialog, id) ->
                        mListener.onSubmitNegativeClick(CBASubmitDialogFragment.this)
                );
        else
            builder.setView(inflater.inflate(R.layout.item_userview_submit_card, null))
                    .setPositiveButton(R.string.resubmit_all_caps, (dialog, id) ->
                            mListener.onResubmitPositiveClick(CBASubmitDialogFragment.this)
                    )
                    .setNegativeButton(R.string.cancel_all_caps, (dialog, id) ->
                            mListener.onSubmitNegativeClick(CBASubmitDialogFragment.this)
                    );

        return builder.create();
    }
}
