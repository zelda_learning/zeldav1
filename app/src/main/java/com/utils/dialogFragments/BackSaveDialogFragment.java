package com.utils.dialogFragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;

import com.zeldav1.R;

public class BackSaveDialogFragment extends DialogFragment {

    /* The activity that creates an instance of this dialog fragment must
     * implement this interface in order to receive event callbacks.
     * Each method passes the DialogFragment in case the host needs to query it. */
    public interface ApplicationBackSaveDialogFragmentListener {
        public void onBackDialogPositiveClick(DialogFragment dialog);
        public void onBackDialogNegativeClick(DialogFragment dialog);
    }

    // Use this instance of the interface to deliver action events
    BackSaveDialogFragment.ApplicationBackSaveDialogFragmentListener mListener;

    // Override the Fragment.onAttach() method to instantiate the NoticeDialogListener
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            mListener = (BackSaveDialogFragment.ApplicationBackSaveDialogFragmentListener) context;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(context.toString()
                    + " must implement NoticeDialogListener");
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Build the dialog and set up the button click handlers
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.AlertDialogTheme);
        assert getArguments() != null;
        String message = "", positiveText = "", negativeText = "";

        message = getString(R.string.prompt_cba_save_changes_message);
        positiveText = getString(R.string.const_yes);
        negativeText = getString(R.string.const_no);

        builder.setMessage(message)
                .setPositiveButton(positiveText, (dialog, id) ->
                        mListener.onBackDialogPositiveClick(BackSaveDialogFragment.this)
                )
                .setNegativeButton(negativeText, (dialog, id) ->
                        mListener.onBackDialogNegativeClick(BackSaveDialogFragment.this)
                );
        return builder.create();
    }
}
