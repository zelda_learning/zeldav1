package com.utils.dialogFragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;

import com.zeldav1.R;

import static com.utils.Constants.APPLICATION_PROGRESS_TAG;

public class ApplicationUploadDialogFragment extends DialogFragment {

    /* The activity that creates an instance of this dialog fragment must
     * implement this interface in order to receive event callbacks.
     * Each method passes the DialogFragment in case the host needs to query it. */
    public interface ApplicationUploadDialogListener {
        public void onUploadDialogPositiveClick(DialogFragment dialog);
        public void onUploadDialogNegativeClick(DialogFragment dialog);
    }

    public static ApplicationUploadDialogFragment newInstance(int progressValue) {
        ApplicationUploadDialogFragment f = new ApplicationUploadDialogFragment();

        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putInt(APPLICATION_PROGRESS_TAG, progressValue);
        f.setArguments(args);

        return f;
    }

    // Use this instance of the interface to deliver action events
    ApplicationUploadDialogListener mListener;

    // Override the Fragment.onAttach() method to instantiate the NoticeDialogListener
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            mListener = (ApplicationUploadDialogListener) context;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(context.toString()
                    + " must implement ApplicationUploadDialogListener");
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Build the dialog and set up the button click handlers
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        assert getArguments() != null;
        int progressValue = getArguments().getInt(APPLICATION_PROGRESS_TAG);
        String message = "", positiveText = "", negativeText = "";
        //Application incomplete
        if (progressValue != 100){
            message = getString(R.string.application_form_incomplete_submit_dialog_text);
            positiveText = getString(R.string.application_form_submit_incomplete_positive_text);
            negativeText = getString(R.string.application_form_submit_incomplete_negative_text);
        }
        //Application complete
        else{
            message = getString(R.string.application_form_submit_dialog_text);
            positiveText = getString(R.string.const_yes);
            negativeText = getString(R.string.const_no);

        }
        builder.setMessage(message)
                .setPositiveButton(positiveText, (dialog, id) ->
                    mListener.onUploadDialogPositiveClick(ApplicationUploadDialogFragment.this)
                )
                .setNegativeButton(negativeText, (dialog, id) ->
                    mListener.onUploadDialogNegativeClick(ApplicationUploadDialogFragment.this)
                );
        return builder.create();
    }
}