package com.utils;

import com.zelda.object.UserProfilePersonal;

public interface UpdateUserProfilePersonal {
    void updateUserProfilePersonal(UserProfilePersonal userProfilePersonal);
}
