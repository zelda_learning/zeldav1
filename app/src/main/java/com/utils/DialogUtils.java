package com.utils;

import android.app.Activity;
import android.app.ProgressDialog;

public class DialogUtils {
    private static ProgressDialog mPDialog;

    public static ProgressDialog showProgressDialog(Activity activity, String message) {
        mPDialog = new ProgressDialog(activity);
        mPDialog.setMessage(message);
        mPDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mPDialog.setCancelable(true);
        mPDialog.show();
        return mPDialog;

    }

    public static void dismissPDialog(){
        if (mPDialog != null) {
            mPDialog.dismiss();
            mPDialog = null;
        }

    }





}