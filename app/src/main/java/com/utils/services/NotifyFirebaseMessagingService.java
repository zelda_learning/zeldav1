package com.utils.services;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.zeldav1.BursaryViewActivity;
import com.zeldav1.R;

import static com.utils.Constants.BURSARY_ID;
import static com.utils.Constants.BURSARY_ID_TAG;
import static com.utils.Constants.USER_REGISTRATION_TOKEN;
import static com.utils.Constants.USER_REGISTRATION_TOKENS;

public class NotifyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "NFMessagingService";
    private Intent intent = null;

    public static void subscribeToTopic(String topic) {
        FirebaseMessaging.getInstance().subscribeToTopic(topic);
        Log.d(TAG, "Subscribed.");
    }

    public static void unsubscribeFromTopic(String topic){
        FirebaseMessaging.getInstance().unsubscribeFromTopic(topic);
        Log.d(TAG, "Unsubscribed.");
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.d(TAG, "From: " + remoteMessage.getFrom());
        Log.d(TAG, "Message id: " + remoteMessage.getMessageId());

        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());

            if (null == intent) {
                intent = new Intent(this, BursaryViewActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra(BURSARY_ID_TAG, remoteMessage.getData().get(BURSARY_ID));
            }


        }

        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Title: " + remoteMessage.getNotification().getTitle());
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
            sendNotification(remoteMessage.getNotification().getBody(), remoteMessage.getNotification().getTitle());
        }
    }

    @Override
    public void onNewToken(String s){
        super.onNewToken(s);
        Log.d(TAG, "Registering user's InstanceId token: " + s);
        FirebaseFirestoreService firebaseUtil = new FirebaseFirestoreService();

        if (null != FirebaseAuth.getInstance().getCurrentUser())
            firebaseUtil.updateFirestoreDocumentField(USER_REGISTRATION_TOKENS, (FirebaseAuth.getInstance().getCurrentUser()).getUid(), USER_REGISTRATION_TOKEN, s);
    }

    private void sendNotification(String messageBody, String messageTitle) {
        if (null == intent) {
            intent = new Intent(this, BursaryViewActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        }

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addNextIntentWithParentStack(intent);

        PendingIntent pendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        String channelId = getString(R.string.default_notification_channel_id);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, channelId)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle(messageTitle)
                        .setContentText(messageBody)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_DEFAULT);
            if (notificationManager != null) {
                notificationManager.createNotificationChannel(channel);
            }
        }

        if (notificationManager != null) {
            notificationManager.notify(0, notificationBuilder.build());
        }
    }
}