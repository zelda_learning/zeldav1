package com.utils.services;


import android.content.Context;
import android.net.Uri;
import android.util.Log;
import android.widget.Toast;

import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreSettings;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.SetOptions;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.zeldav1.BuildConfig;
import com.zeldav1.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileOutputStream;
import java.io.Serializable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static com.utils.Constants.DISPLAY_TEXT_TEXT;
import static com.utils.Constants.USER_PROFILE_PERSONALITY;
import static com.utils.Constants.USER_REGISTRATION_TOKENS;
import static com.utils.Constants.USER_STATE_FIRESTORE;
import static com.utils.Constants.USER_SUBMITTED;

public class FirebaseFirestoreService implements Serializable {
    private static final String TAG = FirebaseFirestoreService.class.getSimpleName();

    private FirebaseFirestore mFirestore;
    private ListenerRegistration userListener, personalityResultsListener;

    public FirebaseFirestoreService(){
        mFirestore = FirebaseFirestore.getInstance();
        FirebaseFirestoreSettings firestoreSettings = new FirebaseFirestoreSettings.Builder()
                .setTimestampsInSnapshotsEnabled(true)
                .setPersistenceEnabled(true)
                .build();
        mFirestore.setFirestoreSettings(firestoreSettings);
    }

    public FirebaseFirestore getFirestore(){
        return mFirestore;
    }

    public void addFirestoreDocument(String collection, Object object) {

        mFirestore.collection(collection)
                .add(object)
                .addOnSuccessListener(documentReference -> Log.d(TAG, "DocumentSnapshot written with ID: " + documentReference.getId()))
                .addOnFailureListener(e -> Log.w(TAG, "Error writing document " + e));
    }

    public void setFirestoreDocument(String collection, String docKey, Object object) {
        mFirestore.collection(collection)
                .document(docKey)
                .set(object)
                .addOnSuccessListener(documentReference -> {
                    Log.d(TAG, "Uploaded/Updated new doc: " + object );
                })
                .addOnFailureListener(e -> Log.w(TAG, "Error writing document " + e));
    }

    public void setFirestoreDocumentMerge(String collection, String docKey, Object object) {
        mFirestore.collection(collection)
                .document(docKey)
                .set(object, SetOptions.merge())
                .addOnSuccessListener(documentReference -> {
                    Log.d(TAG, "Uploaded/Updated new doc: " + object );
                })
                .addOnFailureListener(e -> Log.w(TAG, "Error writing document " + e));
    }

    public void setFirestoreDocumentMergeUpdate(BooleanCallback booleanCallback, String collection, String docKey, Object object) {
        mFirestore.collection(collection)
                .document(docKey)
                .set(object, SetOptions.merge())
                .addOnSuccessListener(documentReference -> {
                    Log.d(TAG, "Uploaded/Updated new doc: " + object );
                    booleanCallback.onBooleanCallback(true);
                })
                .addOnFailureListener(e -> {
                    Log.w(TAG, "Error writing document " + e);
                    booleanCallback.onBooleanCallback(false);
                });
    }

    public void submitCBAForm(BooleanCallback booleanCallback, String collection, String docKey, String userId) {
        mFirestore.collection(collection)
                .document(docKey)
                .update(USER_SUBMITTED, true)
                .addOnSuccessListener(documentReference ->
                        mFirestore.collection(USER_STATE_FIRESTORE)
                            .document(userId)
                            .update(USER_SUBMITTED, true)
                            .addOnSuccessListener(documentReference1 -> {
                                Log.d(TAG, "Updated : " + userId + " " + true );
                                Log.d(TAG, "Updated  " + docKey + true);
                                booleanCallback.onBooleanCallback(true);
                            })
                            .addOnFailureListener(e -> {
                                Log.w(TAG, "Error writing document " + e);
                                booleanCallback.onBooleanCallback(false);
                            }))
                .addOnFailureListener(e -> {
                    Log.w(TAG, "Error writing document " + e);
                    booleanCallback.onBooleanCallback(false);
                });

    }

    public void resubmitCBAForm(BooleanCallback booleanCallback, String collection, String docKey) {
        mFirestore.collection(collection)
                .document(docKey)
                .update(USER_SUBMITTED, false)
                .addOnSuccessListener(documentReference ->{
                            Log.d(TAG, "Updated  " + docKey + false);
                            mFirestore.collection(collection)
                                    .document(docKey)
                                    .update(USER_SUBMITTED, true)
                                    .addOnSuccessListener(documentReference1 ->{
                                        Log.d(TAG, "Updated  " + docKey + true);
                                        booleanCallback.onBooleanCallback(true);
                                    })
                                    .addOnFailureListener(e -> {
                                        Log.w(TAG, "Error writing document " + e);
                                        booleanCallback.onBooleanCallback(false);
                                    });
                                })
                .addOnFailureListener(e -> {
                    Log.w(TAG, "Error writing document " + e);
                    booleanCallback.onBooleanCallback(false);
                });

    }

    public void setFirestoreDocument(Context mContext, String collection, String docKey, Object object, String newUserCreated) {

        mFirestore.collection(collection).document(docKey)
                .set(object)
                .addOnSuccessListener(aVoid -> {
                    Log.d(TAG, docKey + " successfully written : " + ", Object: " + object);
                    if (null != newUserCreated){
                        Toast.makeText(mContext, newUserCreated, Toast.LENGTH_LONG).show();
                    }
                    else
                        Toast.makeText(mContext, mContext.getResources().getString(R.string.user_view_save_changes_success), Toast.LENGTH_SHORT).show();
                })
                .addOnFailureListener(e -> {
                    Log.w(TAG, "Error writing document " + docKey, e);
                    Toast.makeText(mContext, mContext.getResources().getString(R.string.user_view_save_changes_fail), Toast.LENGTH_SHORT).show();
                });
    }

    public void setFirestoreDocument(BooleanCallback booleanCallback, Context mContext, String collection, String docKey, Object object) {

        mFirestore.collection(collection).document(docKey)
                .set(object)
                .addOnSuccessListener(aVoid -> {
                    if (null != booleanCallback)
                        booleanCallback.onBooleanCallback(true);
                })
                .addOnFailureListener(e -> {
                    Log.w(TAG, "Error writing document " + docKey, e);
                    if (null != booleanCallback)
                        booleanCallback.onBooleanCallback(false);

                });
    }

    public void updateFirestoreDocumentField(String collection, String docKey, String key, Object object) {
        mFirestore.collection(collection).document(docKey)
                .update(key, object)
                .addOnSuccessListener(aVoid -> Log.d(TAG, docKey + " successfully written Key: " + key + ", Object: " + object))
                .addOnFailureListener(e -> Log.w(TAG, "Error writing document " + docKey, e));
    }

    public void fetchDocument(DocumentCallback documentCallback, String collection, String docKey){
        mFirestore.collection(collection).document(docKey)
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        DocumentSnapshot document = task.getResult();
                        if (null != document && document.exists()) {
                            documentCallback.onCallback(document);
                        } else {
                            Log.d(TAG, "No such document, Collection: " + collection + ", Document: " + docKey);
                            documentCallback.onCallback(null);
                        }
                    } else {
                        Log.d(TAG, "Get failed with ", task.getException());
                    }
                });
    }

    public void fetchDocumentField(DocumentFieldCallback documentFieldCallback, String collection, String docKey){
        mFirestore.collection(collection).document(docKey)
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        DocumentSnapshot document = task.getResult();
                        if (null != document && document.exists() && null != document.getString(DISPLAY_TEXT_TEXT)) {

                            documentFieldCallback.onCallback(document.getString(DISPLAY_TEXT_TEXT));
                        } else {
                            Log.d(TAG, "No such document, Collection: " + collection + ", Document: " + docKey);
                            documentFieldCallback.onCallback(null);
                        }
                    } else {
                        Log.d(TAG, "Get failed with ", task.getException());
                        documentFieldCallback.onCallback(null);
                    }
                });
    }

    public void assignUserListener(DocumentCallback documentCallback, String userId){
        userListener = mFirestore.collection(USER_STATE_FIRESTORE).document(userId)
                .addSnapshotListener((documentSnapshot, e) -> {
                    if (e != null){
                        Log.d(TAG, "listen failed " + e.getMessage());
                    }

                    if (documentSnapshot != null && documentSnapshot.exists()){
                        documentCallback.onCallback(documentSnapshot);
                    }else {
                        Log.d(TAG, "No such UserState object: " + userId);
                        documentCallback.onCallback(null);
                    }
                });
    }

    public void assignUserPersonalityResultsListener(DocumentCallback documentCallback, String userId){
        personalityResultsListener = mFirestore.collection(USER_PROFILE_PERSONALITY).document(userId)
                .addSnapshotListener((documentSnapshot, e) -> {
                    if (e != null){
                        Log.d(TAG, "listen failed " + e.getMessage());
                    }

                    if (documentSnapshot != null && documentSnapshot.exists()){
                        Log.d(TAG, "personalityResultsListener object: " + userId);
                        documentCallback.onCallback(documentSnapshot);
                    }else {
                        Log.d(TAG, "No such personalityResultsListener object: " + userId);
                        documentCallback.onCallback(null);
                    }
                });
    }

    public void submitRegistrationToken(String mUserId){
        // This should be deprecated, but it's not because we're using such an old library
        Map<String, Object> regTokenData = new HashMap<>();
        regTokenData.put("token", FirebaseInstanceId.getInstance().getToken());
        mFirestore
                .collection(USER_REGISTRATION_TOKENS)
                .document(mUserId)
                .set(regTokenData);
    }


    public interface DocumentFieldCallback {
        void onCallback(String value);
    }

    public interface DocumentCallback {
        void onCallback(DocumentSnapshot value);
    }

    public interface UploadCallback {
        void onCallback(String uploadedFilename);
    }

    public interface DownloadCallback {
        void onCallback(Uri downloadFilename);
    }

    public interface BooleanCallback {
        void onBooleanCallback(boolean result);
    }

    public void removeUserListener(){
        Log.d(TAG, "removing userLister");
        if (null != userListener)
            userListener.remove();
    }

    public void removeUserPersonalityListener(){
        Log.d(TAG, "removing userPersonalityLister");
        if (null != personalityResultsListener)
            personalityResultsListener.remove();
    }

    public void submitIntroQuizResults(final Context context, final String userId, final int[] results){
        JSONObject jsonObject = new JSONObject();
        JSONArray jsonArray = new JSONArray(Arrays.asList(results));

        try {
            jsonObject.put("inputs", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String filename = userId + ".json";
        String fileContents = jsonObject.toString();
        FileOutputStream outputStream;

        try {
            outputStream = context.openFileOutput(filename, Context.MODE_PRIVATE);
            outputStream.write(fileContents.getBytes());
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        StorageReference storageRef = FirebaseStorage.getInstance(getBuildVariantMLBucket()).getReference(filename);

        UploadTask uploadTask = null;
        uploadTask = storageRef.putFile(Uri.parse(context.getFileStreamPath(filename).toURI().toString()));
        uploadTask.addOnFailureListener(exception -> {
            // Handle unsuccessful uploads
            Log.d(TAG, "FAILED UPLOAD");
        }).addOnSuccessListener(taskSnapshot -> {
            // taskSnapshot.getMetadata() contains file metadata such as size, content-type, etc.
            Log.d(TAG, "UPLOAD Success");
        });
    }

    private static String getBuildVariantBucket(){
        if(BuildConfig.BUILD_TYPE.equalsIgnoreCase("debug")) {
            return "gs://zeldav1-dev.appspot.com/";
        }else{
            return "gs://zeldav1-82682.appspot.com/";
        }
    }

    private static String getBuildVariantMLBucket(){
        if(BuildConfig.BUILD_TYPE.equalsIgnoreCase("debug")) {
            return "gs://zelda-dev-ml-course-bucket";
        }else{
            return "gs://zelda-ml-course-bucket";
        }
    }



}