package com.utils.services;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.webkit.MimeTypeMap;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageException;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.utils.Constants;
import com.utils.GlideApp;
import com.utils.UpdateUploadViewHolderListener;
import com.zeldav1.BuildConfig;
import com.zeldav1.R;

import static com.utils.Constants.FILE_TYPE_JPEG;
import static com.utils.Constants.FILE_TYPE_JPG;
import static com.utils.Constants.FILE_TYPE_MSWORD;
import static com.utils.Constants.FILE_TYPE_PDF;
import static com.utils.Constants.FILE_TYPE_PNG;
import static com.utils.Constants.USER_PROFILE_DOCUMENTS_IMG;
import static com.utils.Constants.USER_PROFILE_DOCUMENTS_PDF;

public class FirebaseStorageService {
    private Context mContext;
    private FirebaseStorage mFirebaseStorage;
    private static final String TAG = FirebaseStorageService.class.getSimpleName();

    public FirebaseStorageService(Context context){
        this.mContext = context;
        mFirebaseStorage = FirebaseStorage.getInstance();
    }

    public static void setGlideImage(final ImageView imageView, final String imageUid){
        Context context = imageView.getContext();
        FirebaseStorage storage = FirebaseStorage.getInstance();
        StorageReference storageRef = storage.getReferenceFromUrl(getBuildVariantBucket()).child(imageUid);

        if (context != null && !(getActivity(context)).isFinishing()) {
            GlideApp.with(context)
                    .load(storageRef)
                    .fitCenter()
                    .into(imageView);
        }
    }

    public static void setGlideImage(final Context context, final ImageView imageView, final String imageUid){
        FirebaseStorage storage = FirebaseStorage.getInstance();
        StorageReference storageRef = storage.getReferenceFromUrl(getBuildVariantBucket()).child(imageUid);

        if (context != null &&  !(getActivity(context)).isFinishing()) {
            GlideApp.with(imageView.getContext())
                    .load(storageRef)
                    .fitCenter()
                    .into(imageView);
        }
    }

    public static void setGlideImagePNG(final ImageView imageView, final String imageUid){

        Context context = imageView.getContext();
        if (context != null && !(getActivity(context)).isFinishing()) {
            FirebaseStorage storage = FirebaseStorage.getInstance();
            StorageReference storageRef = storage.getReferenceFromUrl(getBuildVariantBucket()).child(imageUid + ".png");

            Log.d(TAG, "setGlideImagePNG: " + storageRef.toString());

            GlideApp.with(context)
                    .load(storageRef)
                    .dontAnimate()
                    .fitCenter()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(imageView);
        }
    }

    public static void setGlideImageJPG(final ImageView imageView, final String imageUid){

        Context context = imageView.getContext();
        if (context != null && !(getActivity(context)).isFinishing()) {
            FirebaseStorage storage = FirebaseStorage.getInstance();
            StorageReference storageRef = storage.getReferenceFromUrl(getBuildVariantBucket()).child(imageUid + ".jpg");

            Log.d(TAG, "setGlideImageJPG: " + storageRef.toString());

            GlideApp.with(context)
                    .load(storageRef)
                    .dontAnimate()
                    .fitCenter()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(imageView);
        }
    }

    public static void setGlideImageWithDimensJPG(final ImageView imageView, final String imageUid, final int width, final int height){

        Context context = imageView.getContext();
        if (context != null && !(getActivity(context)).isFinishing()) {
            FirebaseStorage storage = FirebaseStorage.getInstance();
            StorageReference storageRef = storage.getReferenceFromUrl(getBuildVariantBucket()).child(imageUid + ".jpg");

            GlideApp.with(context)
                    .load(storageRef)
                    .fitCenter()
                    .override(width, height)
                    .into(imageView);
        }
    }

    public static void setGlideImageWithDimens(RequestListener<Drawable> createLoggerListener, final ImageView imageView, final String imageFileName, final int width, final int height){

        Context context = imageView.getContext();
        if (context != null && !(getActivity(context)).isFinishing()) {
            FirebaseStorage storage = FirebaseStorage.getInstance();
            StorageReference storageRef = storage.getReferenceFromUrl(getBuildVariantBucket()).child(imageFileName);

            GlideApp.with(context)
                    .load(storageRef)
                    .fitCenter()
                    .listener(createLoggerListener)
                    .override(width, height)
                    .into(imageView);

        }
    }

    public static void setGlideImageJPG(final Context context, final String url,
                                        final ImageView imageView, RequestListener<Drawable> listener) {

        if (context != null && !(getActivity(context)).isFinishing()) {
            FirebaseStorage storage = FirebaseStorage.getInstance();
            StorageReference storageRef = storage.getReferenceFromUrl(getBuildVariantBucket()).child(url + ".jpg");
            if (listener != null) {
                GlideApp.with(context.getApplicationContext())
                        .load(storageRef)
                        .dontAnimate()
                        .fitCenter()
                        .error(ContextCompat.getDrawable(context, R.drawable.profile_image))
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .listener(listener)
                        .into(imageView);
            } else {
                GlideApp.with(context.getApplicationContext())
                        .load(storageRef)
                        .dontAnimate()
                        .error(ContextCompat.getDrawable(context, R.drawable.profile_image))
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(imageView);
            }
        }
    }

    public UploadTask uploadFile(final Uri uri, final String uuid, final String docFolder, final UpdateUploadViewHolderListener listener){
        if (null != uri && null != uri.getPath() && null != uri.getLastPathSegment() && null != listener) {
            StorageReference uploadRef = null;

            String filepath = "";
            ContentResolver cR = mContext.getContentResolver();
            MimeTypeMap mime = MimeTypeMap.getSingleton();
            String postFix = mime.getExtensionFromMimeType(cR.getType(uri));

            if (null != postFix) {
                if (postFix.equals(FILE_TYPE_PDF) || postFix.equals(FILE_TYPE_MSWORD))
                    filepath = USER_PROFILE_DOCUMENTS_PDF;
                else if (postFix.equals(FILE_TYPE_JPG) || postFix.equals(FILE_TYPE_JPEG) || postFix.equals(FILE_TYPE_PNG) )
                    filepath = USER_PROFILE_DOCUMENTS_IMG;

                filepath += "/" + docFolder + "/" + uuid + "." + postFix;
                Log.d(TAG, "Filepath " + filepath);

                uploadRef = mFirebaseStorage.getReference().child(filepath);

                return uploadRef.putFile(uri);

            }
            else {
                Log.d(TAG, "PostFix not found, File error");
            }
        }
        return null;
    }

    public void downloadFile(FirebaseFirestoreService.DownloadCallback downloadCallback, final String filename, final String docFolder){

        StorageReference downloadRef = null;

        String file = filename.substring(0, filename.lastIndexOf("."));

        String filepath = USER_PROFILE_DOCUMENTS_PDF + "/" + docFolder + "/" + file + "." + Constants.FILE_TYPE_PDF;
        downloadRef =  mFirebaseStorage.getReferenceFromUrl(getBuildVariantBucket()).child(filepath);

        Log.d(TAG, "downloading: " + filepath.toString());
        Toast.makeText(mContext, R.string.loading_download_file, Toast.LENGTH_SHORT).show();
        downloadRef.getDownloadUrl()
            .addOnSuccessListener((Uri uri) -> {
                Log.d(TAG, "File Uri found " + uri.toString());
                downloadCallback.onCallback(uri);

            })
            .addOnFailureListener(exception -> {
                if (exception instanceof StorageException){
                    switch (((StorageException) exception).getHttpResultCode()){
                        case 404:
                            Log.d(TAG, "File download failed. Not found");
                            Toast.makeText(mContext, R.string.file_not_found_exception_message, Toast.LENGTH_LONG).show();
                            break;
                        default:
                            Log.d(TAG, "File download failed. " + exception.toString());
                            break;
                    }
                }
            });

    }

    private static String getBuildVariantBucket(){
        if(BuildConfig.BUILD_TYPE.equalsIgnoreCase("debug")) {
            return "gs://zeldav1-dev.appspot.com/";
        }else{
            return "gs://zeldav1-82682.appspot.com/";
        }
    }

    private static String getBuildVariantMLBucket(){
        if(BuildConfig.BUILD_TYPE.equalsIgnoreCase("debug")) {
            return "gs://zelda-dev-ml-course-bucket";
        }else{
            return "gs://zelda-ml-course-bucket";
        }
    }

    private static Activity getActivity(Context context){
        while (context instanceof ContextWrapper) {
            if (context instanceof Activity) {
                return (Activity)context;
            }
            context = ((ContextWrapper)context).getBaseContext();
        }
        return null;
    }

}


